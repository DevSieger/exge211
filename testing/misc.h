#pragma once

#include <ostream>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/dual_quaternion.hpp>

namespace exge211 {

std::ostream & operator << (std::ostream & stream, glm::vec3 const & v);
std::ostream & operator << (std::ostream & stream, glm::vec2 const & v);

std::ostream & operator << (std::ostream & stream, glm::quat const & q);
std::ostream & operator << (std::ostream & stream, glm::dualquat const & j);

}
