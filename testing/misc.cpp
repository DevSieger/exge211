#include "misc.h"

namespace exge211 {

std::ostream &operator <<(std::ostream &stream, const glm::vec3 &v){
    return stream<<"{"<<v.x<<", "<<v.y<<", "<<v.z<<"}";
}


std::ostream &operator <<(std::ostream &stream, const glm::vec2 &v){
    return stream<<"{"<<v.x<<", "<<v.y<<"}";
}


std::ostream &operator <<(std::ostream & stream, const glm::quat & q){
    return stream<<"{"<<q.w<<";"<<q.x<<","<<q.y<<","<<q.z<<"}";
}


std::ostream &operator <<(std::ostream & stream, const glm::dualquat & j){
    return stream<<"dq{ "<<j.real<<" "<<j.dual<<" }";
}

}
