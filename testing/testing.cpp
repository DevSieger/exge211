#include "../common/public_headers.h"
#include PUBLIC_HEADER(logging.h)
#include PUBLIC_HEADER(Resource_loader.h)
#include PUBLIC_HEADER(res/resource_data.h)
#include PUBLIC_HEADER(res/resource_loading_fs.h)
#include PUBLIC_HEADER(res/Animation_data.h)
#include PUBLIC_HEADER(res/default_format.h)
#include "testing.h"
#include <map>
#include <dk/serialize_pod.h>
#include "../render/misc.h"
#include <fstream>
#include "../render/misc.h"


std::ostream & operator << (std::ostream & stream,AABB const & aabb){
    return stream<<"["<<aabb.inf<<";"<<aabb.sup<<"]";
}
std::ostream & operator << (std::ostream & stream,Batch_group const & bg){
    return stream<<dk::fstr("{a:%1%,s:%2%,n:%3%,mt:%4%,ms:%5%}",(int)bg.albedo(),(int)bg.spec(),(int)bg.normal(),(int)bg.material(),(int)bg.mesh());
}

bool operator==(Animated_vertex const& lhv,Animated_vertex const& rhv){
    for(size_t i=0;i<3;++i){
        if(lhv.pos[i]!=rhv.pos[i]){
            return false;
        }
        if(lhv.n[i]!=rhv.n[i]){
            return false;
        }
        if(lhv.b[i]!=rhv.b[i]){
            return false;
        }
        if(lhv.t[i]!=rhv.t[i]){
            return false;
        }
    }
    if(lhv.uv[0]!=rhv.uv[0]){
        return false;
    }
    if(lhv.uv[1]!=rhv.uv[1]){
        return false;
    }
    for(size_t i=0;i<4;++i){
        if(lhv.weight[i]!=rhv.weight[i]){
            return false;
        }
        if(lhv.joint[i]!=rhv.joint[i]){
            return false;
        }
    }
    return true;
}

void print(Model * mdl){
    String str="\n";
    for(Model::Index i=0;i<mdl->lod_count();++i){
        for(Model::LOD::Data const & j:mdl->lod(i)){
            str+=dk::fstr("bg:%1%, mat:%2%, mesh: %3%, type: %4%\n",j.bg,j.material,j.mesh,(u32)j.type);
        }
    }
    log_msg(Detail_level::DEBUG,"boundary %1%, \n %2%",mdl->boundary(),str);
}
/*
 * нужно обязательно заткнуть сохранение в фабрике!
static void resource_factory_test(){
    ResourceLoader loader;
    ResourceFactory_Simple factory;
    loader.load_texture=[](ResourceRequest const & name,TextureData & out){};
    loader.load_skin=[](ResourceRequest const & name,SkinInfo & out){
        if(name=="1"){
            out["A"]="m1";
            out["B"]="m2";
            out["C"]="m1";
        }else if(name=="2"){
            out["A"]="m2";
            out["B"]="m3";
            out["C"]="m4";
            out["D"]="m4";
        }
    };
    loader.load_animation=[](ResourceRequest const & req,AnimationData & anim){
        if(req=="1"){
            anim.channels.resize(1);
        }else if(req=="2"){
            anim.channels.resize(2);
        }
    };

    loader.load_material=[](ResourceRequest const & name,MaterialInfo & out){
        if(name=="m1"){
            out.albedo="a1";
            out.normal="n1";
            out.specular="s1";
            out.spec_pow=1;
        }else if(name=="m2"){
            out.albedo="a2";
            out.normal="n2";
            out.specular="s2";
            out.spec_pow=2;
        }else if(name=="m3"){
            out.albedo="a1";
            out.normal="n2";
            out.specular="a1";
            out.spec_pow=3;
        }else if(name=="m4"){
            out.albedo="a2";
            out.normal="n2";
            out.specular="s2";
            out.spec_pow=4;
        };
    };
    loader.load_model=[](ResourceRequest const & name,AModelData & out){
        if(name=="X"){
            out.init(2);
            out.lod(0).init(2);
            out.boundary(AABB{{0,0,0},{1,1,1}});
            out.lod(0)[0].material="A";
            out.lod(0)[1].material="B";
            out.lod(1).init(2);
            out.lod(1)[0].material="A";
            out.lod(1)[1].material="B";
        }else if(name=="Y"){
            out.init(1);
            out.boundary(AABB{{-1,-1,-1},{1,1,1}});
            out.lod(0).init(3);
            out.lod(0)[0].material="B";
            out.lod(0)[1].material="C";
            out.lod(0)[2].material="D";
        }
    };
    factory.loader(&loader);

    log_msg(Detail_level::DEBUG,"anim 1 size %1%",factory.request_animation("1")->channels.size());
    log_msg(Detail_level::DEBUG,"anim 1 size %1%",factory.request_animation("1")->channels.size());
    log_msg(Detail_level::DEBUG,"anim 2 size %1%",factory.request_animation("2")->channels.size());

    print(factory.request_amodel("X","1"));
    print(factory.request_amodel("X","2"));
    print(factory.request_amodel("Y","1"));
    print(factory.request_amodel("Y","2"));

}
*/

static void test_material_parsing(){
    String str="21e1 \"22.pmg\"\"spec\"\n"
               "\"nor\nmal\"";
    Material_info info;
    if(!default_format::parse_resource(str.c_str(),str.size(),info)){
        log_msg(Detail_level::DEBUG,"failed to parse");
    };
    log_msg(Detail_level::DEBUG,"m: a:%1%, s:%2%, n:%3%, p:%4%",info.albedo,info.specular,info.normal,info.shininess);
}


static void test_skeleton_parsing(){
    String str=" \"root\" \"1 2 3 4 5 6 7 8\" 0 0\n"
               "\"ass\" \"8.1 7.2 6.3 5.4 4.5 3.6 2.7 1.8  \" 1 0 \n";
    Skeleton_info info;
    if(!default_format::parse_resource(str.c_str(),str.size(),info)){
        log_msg(Detail_level::DEBUG,"failed to parse");
    };
    for(auto & j:info.joints){
        log_msg(Detail_level::DEBUG,"\tname:\t%1%,\tbinding_pose:\t%2%,\tindex:\t%3%,\tparent:\t%4%",j.name,j.binding_pose,j.index,j.parent);
    }
    String cnt=default_format::serialize(info);
    log_msg(Detail_level::DEBUG,"serialized skeleton:\n%1%",cnt);
}

static void book_geometry(Animated_mesh & mesh){

    auto & vv = mesh.vertices;
    auto & ix = mesh.indices;
    vv.push_back(Animated_vertex{ {-2,2,0.05},{0,0,1},{1,0,0},{0,1,0},{0,1},{1,0,0,0},{1,0,0,0}});
    vv.push_back(Animated_vertex{ {-2,-2,0.05},{0,0,1},{1,0,0},{0,1,0},{0,0},{1,0,0,0},{1,0,0,0}});

    vv.push_back(Animated_vertex{ {0,2,0.05},{0,0,1},{1,0,0},{0,1,0},{0.5,1},{0.5,0.5,0,0},{1,2,0,0}});
    vv.push_back(Animated_vertex{ {0,-2,0.05},{0,0,1},{1,0,0},{0,1,0},{0.5,0},{0.5,0.5,0,0},{1,2,0,0}});

    vv.push_back(Animated_vertex{ {2,2,0.05},{0,0,1},{1,0,0},{0,1,0},{1,1},{1,0,0,0},{2,0,0,0}});
    vv.push_back(Animated_vertex{ {2,-2,0.05},{0,0,1},{1,0,0},{0,1,0},{1,0},{1,0,0,0},{2,0,0,0}});

    vv.push_back(Animated_vertex{ {-2,2,-0.05},{0,0,-1},{1,0,0},{0,1,0},{0,1},{1,0,0,0},{1,0,0,0}});
    vv.push_back(Animated_vertex{ {-2,-2,-0.05},{0,0,-1},{1,0,0},{0,1,0},{0,0},{1,0,0,0},{1,0,0,0}});

    vv.push_back(Animated_vertex{ {0,2,-0.05},{0,0,-1},{1,0,0},{0,1,0},{0.5,1},{0.5,0.5,0,0},{1,2,0,0}});
    vv.push_back(Animated_vertex{ {0,-2,-0.05},{0,0,-1},{1,0,0},{0,1,0},{0.5,0},{0.5,0.5,0,0},{1,2,0,0}});

    vv.push_back(Animated_vertex{ {2,2,-0.05},{0,0,-1},{1,0,0},{0,1,0},{1,1},{1,0,0,0},{2,0,0,0}});
    vv.push_back(Animated_vertex{ {2,-2,-0.05},{0,0,-1},{1,0,0},{0,1,0},{1,0},{1,0,0,0},{2,0,0,0}});

    ix.clear();
    for(unsigned int i=0;i<4;i+=2){
        ix.push_back({i+0,i+1,i+2});
        ix.push_back({i+1,i+3,i+2});
        ix.push_back({i+7,i+6,i+8});
        ix.push_back({i+9,i+7,i+8});
    }
    ix.push_back({0,6,7});
    ix.push_back({0,7,1});

    ix.push_back({0,2,8});
    ix.push_back({8,6,0});

    ix.push_back({2,4,10});
    ix.push_back({10,8,2});

    ix.push_back({3,1,9});
    ix.push_back({9,1,7});

    ix.push_back({5,3,11});
    ix.push_back({11,3,9});

    ix.push_back({4,5,11});
    ix.push_back({11,10,4});
}

static bool compare_amodel_data(Animated_model_data const & orig, Animated_model_data const & parsed){
    if(parsed.lod_count()!=orig.lod_count()){
        log_msg(Detail_level::DEBUG,"lod count different");
        return false;
    }
    if(parsed.boundary()!=orig.boundary()){
        log_msg(Detail_level::DEBUG,"boundary different %1% and %2%",parsed.boundary(),orig.boundary());
        return false;
    }
    for(size_t lix=0;lix<orig.lod_count();++lix){
        auto & lo=orig.lod(lix);
        auto & lp=parsed.lod(lix);
        if(lo.size()!=lp.size()){
            log_msg(Detail_level::DEBUG,"lod size %1% different: %2% and %3%",lix,lo.size(),lp.size());
            return false;
        }
        for(auto oi=lo.begin(),pi=lp.begin();oi!=lo.end();++oi,++pi){
            assert(oi->material==pi->material);
            assert(oi->mesh.indices==pi->mesh.indices);
            assert(oi->mesh.vertices==pi->mesh.vertices);
            /*
            log_msg(Detail_level::DEBUG,"key %1%",pi->material);
            log_msg(Detail_level::DEBUG,"serialized indices %1%",dk::serialize_cont(pi->mesh.indices));
            log_msg(Detail_level::DEBUG,"serialized vertices %1%",dk::serialize_cont(pi->mesh.vertices));
            */
        }
    }
    return true;
}
static bool compare(Animation_data const& orig, Animation_data const&target){
    if(orig.duration==target.duration){
        if(orig.channels.size()==target.channels.size()){
            bool ret=true;
            for(size_t ch_ix=0; ret && ch_ix<orig.channels.size();++ch_ix){
                Animation_channel const& och=orig.channels[ch_ix];
                Animation_channel const& tch=target.channels[ch_ix];
                ret=och.rotations==tch.rotations && och.translations==tch.translations && och.scales==tch.scales;
            }
            return ret;
        }else{
            log_msg(Detail_level::DEBUG,"different channel size at animation data compare: %1% and %2%",orig.channels.size(),target.channels.size());
        }
    }else{
        log_msg(Detail_level::DEBUG,"different duration at animation data compare: %1% and %2%",orig.duration,target.duration);
    }
    return false;
}
static void test_animation_parsing(){
    Animation_data orig;

    {
        orig.duration=2.0f;
        orig.channels.resize(3);
        orig.channels[0].rotations.emplace_back(0);
        orig.channels[0].rotations.emplace_back(2.0);

        orig.channels[0].scales.emplace_back(0,1,1,1);

        orig.channels[0].translations.emplace_back(0.0);
        orig.channels[0].translations.emplace_back(1.1,glm::vec3{0.0,0.0,0.5});
        orig.channels[0].translations.emplace_back(2.0);

        orig.channels[1].rotations.emplace_back(0.0);
        orig.channels[1].rotations.emplace_back(1.5,glm::angleAxis(glm::pi<float>()*0.5f,glm::vec3{0,1,0}));
        orig.channels[1].rotations.emplace_back(2.0);

        orig.channels[1].scales.emplace_back(0,1,1,1);
        orig.channels[1].translations.emplace_back(0.0);
        orig.channels[1].translations.emplace_back(2.0);

        orig.channels[2].rotations.emplace_back(0.0);
        orig.channels[2].rotations.emplace_back(1.0,glm::angleAxis(-glm::pi<float>()*0.5f,glm::vec3{0,1,0}));
        orig.channels[2].rotations.emplace_back(2.0);

        orig.channels[2].scales.emplace_back(0,1,1,1);
        orig.channels[2].translations.emplace_back(0);
    }

    String data=default_format::serialize(orig);
    std::ofstream file("./book_test.anim",std::ios_base::binary | std::ios_base::trunc);
    if(!file.is_open()){
        log_msg(Detail_level::DEBUG,"can't open file for anim output");
        return;
    }
    file<<data;
    file.close();
    Animation_data after;
    File_loader<Animation_data> loader;
    loader.dir("./");
    loader("book_test.anim",after);
    if(!compare(orig,after)){
        log_msg(Detail_level::DEBUG,"animation parsing test: failed");
    }else{
        log_msg(Detail_level::DEBUG,"animation parsing test:ok");
    }

}

static void test_mesh_generation(){
    Animated_model_data book;
    book.boundary(AABB{{3,3,3},{-3,-3,-3}});
    book.init(1);
    book.lod(1).init(1);
    Animated_submodel_data & smd =book.lod(1)[0];
    smd.material="base";
    book_geometry(smd.mesh);
    String serialized=default_format::serialize(book);
    std::ofstream file("./book.model_data",std::ios_base::binary | std::ios_base::trunc);
    if(!file.is_open()){
        log_msg(Detail_level::DEBUG,"can't open file for model output");
        return;
    }
    file<<serialized;
    file.close();
    Animated_model_data readed;
    File_loader<Animated_model_data> amodel_loader;
    amodel_loader.dir(".");
    amodel_loader("book.model_data",readed);
    compare_amodel_data(book,readed);
}

static void test_mesh_parsing(){
    Animated_model_data orig;
    orig.boundary(AABB{{0,1,2},{0.3,0.5,0.7}});
    orig.init(2);
    orig.lod(0).init(2);
    orig.lod(1).init(1);
    orig.lod(0)[0].material="m0";
    orig.lod(0)[1].material="m1";
    orig.lod(1)[0].material="m3";

    orig.lod(0)[0].mesh.indices={Triangle{'a','b','c'},Triangle{'1','2','3'}};
    orig.lod(0)[0].mesh.vertices={
        Animated_vertex{ {1,1,1},{1,1,1},{1,1,1},{1,1,1},{1,1},{1,1,1,1},{'x','y','z','w'}},
        Animated_vertex{{2,2,1},{3,1,2},{1,2,3},{1,2,3},{2,1},{1,2,1,4},{'a','b','c','d'}}
    };

    orig.lod(1)[0].mesh=orig.lod(0)[1].mesh=orig.lod(0)[0].mesh;
    orig.lod(1)[0].mesh.indices[0][0]='n';
    orig.lod(1)[0].mesh.vertices[0].pos[0]=4;
    String str=default_format::serialize(orig);
    //log_msg(Detail_level::DEBUG,"serialized mesh: %1%",str);
    Animated_model_data parsed;
    default_format::parse_resource(str.c_str(),str.size(),parsed);
    compare_amodel_data(orig,parsed);
}

void run_test()
{
}
