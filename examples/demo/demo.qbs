import qbs
import qbs.TextFile
import qbs.File

CppApplication {
    name: "demo"
    consoleApplication: false

    Group {     // Properties for the produced executable
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: "bin"
    }
    Depends{
        name: "exge211_core"
    }
    Depends{
        name: "exge211_qt"
    }
    Depends{
        name: "exge211_glfw"
    }
    Depends{
        name: "exge211_smplres"
    }
    Depends{
        name: "Qt";
        submodules: ["core","gui","widgets"]
    }
    cpp.defines: [
        "USING_EXGE211_QT"
    ]

    Group{
        name: "qt files"
        files:[
            "ui/info_frame.ui",
        ]
    }

    Group{
        name: "animations"
        prefix: "data/animation/"
        files: "*"
        qbs.install: true
        qbs.installDir: "share/"+project.projectName+"/examples/data/animation/"
    }
    Group{
        name: "materials"
        prefix: "data/material/"
        files: "*"
        qbs.install: true
        qbs.installDir: "share/"+project.projectName+"/examples/data/material/"
    }
    Group{
        name: "models"
        prefix: "data/model/"
        files: "*"
        qbs.install: true
        qbs.installDir: "share/"+project.projectName+"/examples/data/model/"
    }
    Group{
        name: "sceneries"
        prefix: "data/sceneries/"
        files: "*"
        qbs.install: true
        qbs.installDir: "share/"+project.projectName+"/examples/data/sceneries/"
    }
    Group{
        name: "skeletons"
        prefix: "data/skeleton/"
        files: "*"
        qbs.install: true
        qbs.installDir: "share/"+project.projectName+"/examples/data/skeleton/"
    }
    Group{
        name: "skins"
        prefix: "data/skin/"
        files: "*"
        qbs.install: true
        qbs.installDir: "share/"+project.projectName+"/examples/data/skin/"
    }
    Group{
        name: "skyboxes"
        prefix: "data/skybox/"
        files: "*"
        qbs.install: true
        qbs.installDir: "share/"+project.projectName+"/examples/data/skybox/"
    }
    Group{
        name: "textures"
        prefix: "data/texture/"
        files: "*"
        qbs.install: true
        qbs.installDir: "share/"+project.projectName+"/examples/data/texture/"
    }
    Group{
        name: "skybox textures"
        prefix: "data/texture/sb/"
        files: "*"
        qbs.install: true
        qbs.installDir: "share/"+project.projectName+"/examples/data/texture/sb/"
    }
    Group{
        name: "c++ sources"
        prefix: "src/"
        files:[
            "actors_one.cpp",
            "actors_one.h",
            "demo_application.cpp",
            "demo_application.h",
            "demo_scene.h",
            "demo_scene_part.h",
            "directional_light_part.cpp",
            "directional_light_part.h",
            "fp_camera.cpp",
            "fp_camera.h",
            "info_frame.cpp",
            "info_frame.h",
            "main.cpp",
            "point_light_part.cpp",
            "point_light_part.h",
            "scenery_one.cpp",
            "scenery_one.h",
            "small_lights_part.cpp",
            "small_lights_part.h",
        ]
    }

}
