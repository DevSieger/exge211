#pragma once

#include <exge211/common_types.h>
#include <exge211/fps_counter.h>
#include <exge211/application_framework.h>
#include <exge211/scene.h>
#include "info_frame.h"
#include "demo_scene.h"
#include "fp_camera.h"

/// An application class.
class Demo_application : public exge211::GUI_application
{
public:
    /// Initializes an application using the given factory and the specified resource loader.
    Demo_application(exge211::Platform_factory & factory,exge211::Resource_loader& loader);

    bool init()override;

    ~Demo_application();

private:
    /// Sets an information string.
    void set_info(exge211::String const & str);

    /// Simulates 'dt' seconds.
    bool iteration(Time dt)override;

    /// Switches the scene to a new one with the specified index.
    void switch_scene(size_t new_current_sc_ix);

    /// Returns the scene which is currently in use.
    Demo_scene& current_scene(){
        return scenes_[current_scene_ix_];
    }
    /// Returns the scene which is currently in use.
    Demo_scene const& current_scene()const{
        return scenes_[current_scene_ix_];
    }

    /// Sets the pause state.
    void pause(bool paused);

#ifdef USING_EXGE211_QT
    /// A frame for showing information.
    Info_frame info_frame_;
#endif

    /// The pause state.
    bool paused_=false;

    /// The 'simulated' time
    double time_=0;

    /// The camera.
    FP_camera_ptr cam_;

    /// FPS counter.
    exge211::FPS_counter fps_counter_=0.9;

    /// Scenes.
    std::vector<Demo_scene> scenes_;

    /// The index of the current scene.
    size_t current_scene_ix_=0;
};
