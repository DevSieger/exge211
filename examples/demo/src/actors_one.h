#pragma once

#include "demo_scene_part.h"
#include <vector>

/*!
 * \brief Demo_scene_part that manages exge211::Actor objects.
 *
 * This class is used to illustrate an Actor usage.
 */
class Actors_one: public Demo_scene_part{
public:
    void set_up(exge211::Scene_ptr const& scene) override;
    bool iteration(double time)override;
    void pause(bool paused)override;
private:
    /// Sets a rate of actor animation to normal.
    void set_normal_animation_rate();
    /// Stops an actor animation.
    void pause_animation();
    /// An array of actor pointers.
    std::vector<exge211::Actor_ptr> actors_;
    bool paused_    = true;
};
