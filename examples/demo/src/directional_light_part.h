#pragma once

#include "demo_scene_part.h"
#include <vector>

/*!
 * \brief Demo_scene_part that manages exge211::Directional_light objects.
 *
 * This class is used to illustrate a directional light sources usage.
 */
class Directional_light_part: public Demo_scene_part{
public:
    void set_up(exge211::Scene_ptr const& scene) override;
    bool iteration(double time)override;
private:
    std::vector<exge211::Directional_light_ptr> lights_;
};
