#pragma once

#include "demo_scene_part.h"
#include <vector>

/*!
 * \brief Demo_scene_part that manages point light soureces.
 *
 * This class is used to illustrate a point light source usage which did not involved in the calculation of shadows.
 */
class Small_lights_part: public Demo_scene_part{
public:
    void set_up(exge211::Scene_ptr const& scene) override;
    bool iteration(double time)override;
private:
    std::vector<exge211::Point_light_ptr> lights_;
};
