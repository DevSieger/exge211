#include "small_lights_part.h"
#include <exge211/point_light.h>
#include <exge211/scene.h>

void Small_lights_part::set_up(const exge211::Scene_ptr& scene)
{
    lights_.clear();
    for(float x=-38;x<40;x+=4){
        for(float y=-38;y<40;y+=4){
            exge211::Point_light_ptr pl_ptr=scene->add_point_light();
            pl_ptr->radius(0.1f).position(x,y,-0.8f).intensity({0.5,x/80+0.5,y/80+0.5},2,0.4).range(2);
            lights_.emplace_back(pl_ptr);
        }
    }
}

bool Small_lights_part::iteration(double time)
{
    for(exge211::Point_light_ptr & pl: lights_){
        glm::vec3 pos=pl->position();
        pos.z=(sin(time*2)+1)*0.25f-0.85f;
        pl->position(pos);
    }
    return true;
}
