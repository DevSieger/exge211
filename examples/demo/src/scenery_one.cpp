#include "scenery_one.h"
#include <exge211/scene.h>


void Scenery_one::set_up(exge211::Scene_ptr const& scene){

    //this values very inaccurate, glitches is possible
    using exge211::Position;
    exge211::AABB new_boundary(Position(-40,-40,-10),Position(40,40,16));
    new_boundary.add(scene->boundary());
    scene->boundary(new_boundary);

    sceneries_.clear();
    using glm::angleAxis;

    exge211::Static_model_handle static_model=scene->load_scenery_proto("stepper.smodel","stepper.skin");
    exge211::Scenery_ptr sc;

    const exge211::Rotation correction=angleAxis(0.5f*glm::pi<float>(),glm::vec3{1,0,0});
    const exge211::Rotation basic_rotation=angleAxis(glm::pi<float>(),glm::vec3{0,0,1})*correction;

    sc=scene->new_scenery(
                static_model,
                correction,
                {-15,4,0},
                glm::vec3(1)
    );
    sceneries_.push_back(sc);
    sc=scene->new_scenery(
                static_model,
                angleAxis(0.125f*glm::pi<float>(),glm::vec3{0,0,1})*correction,
                {-5,15,0},
                glm::vec3(2)
    );
    sceneries_.push_back(sc);
    exge211::Static_model_handle base_level=scene->load_scenery_proto("base.smodel","base.skin");
    sc=scene->new_scenery(
                base_level,
                basic_rotation,
                {0,0,-1.8},
                glm::vec3(2,2,2)
    );
    sceneries_.push_back(sc);
    sc=scene->new_scenery(
                base_level,
                basic_rotation,
                {80,0,-1.8},
                glm::vec3(2,2,2)
    );
    sceneries_.push_back(sc);
}
