#pragma once

#include "demo_scene_part.h"
#include <list>
#include <dk/concepts.h>

/*!
 * \brief The scene object of this demo.
 *
 * This class is implemented in the strategy-like manner: it contains a set of scene parts (Demo_scene_part).
 * Each scene part is responsible for a some type of scene objects:
 * one for actors, second for sceneries, third for directional light sources, etc..
 * If you want to see an example of how to use some kind of object, try to look the corresponding scene part.
 */
class Demo_scene{
public:
    /*!
     * \brief Initializes Demo_scene using the specified exge211::Scene.
     * \param target_scene A pointer to the exge211::Scene to decorate using scene parts.
     */
    Demo_scene(exge211::Scene_ptr const& target_scene): scene_(target_scene){}

    Demo_scene(Demo_scene&&)=default;
    Demo_scene& operator=(Demo_scene&&)=default;

    Demo_scene(Demo_scene const &)=delete;
    Demo_scene & operator =(Demo_scene const &)=delete;

    /*!
     * \brief Adds the new scene part.
     * \tparam Concrete_scene_part A type of a scene part to add.
     * \tparam Constructor_args Types of arguments to pass to the constructor of the scene part.
     * \param args Arguments to pass to the constructor of the scene part.
     * \return This object.
     */
    template<typename Concrete_scene_part,typename ... Constructor_args>
    Demo_scene& add(Constructor_args&&...args){
        Demo_scene_part* new_part = new Concrete_scene_part(std::forward<Constructor_args>(args)...);
        new_part->set_up(scene());
        parts_.emplace_back(new_part);
        return *this;
    }

    /// Sets the pause state.
    void pause(bool paused){
        for(Demo_scene_part* part_ptr: parts_){
            part_ptr->pause(paused);
        }
    }

    /*!
     * \brief Simulates the logical-model to the given time.
     * \param time A total elapsed time from the application start.
     * \return false if an user's application have to stop.
     */
    bool iteration(double time){
        for(Demo_scene_part* part_ptr: parts_){
            if(!part_ptr->iteration(time))return false;
        }
        return true;
    }

    /// Returns a decorated exge211::Scene object.
    exge211::Scene_ptr const & scene()const{
        return scene_;
    }
    /// \brief Sets a decorated exge211::Scene object.
    /// \todo Remove this
    Demo_scene & scene(exge211::Scene_ptr const& a_scene){
        scene_=a_scene;
        return *this;
    }

    ~Demo_scene(){
        clear();
    }

private:
    void clear(){
        for(Demo_scene_part* part_ptr: parts_){
            delete part_ptr;
        }
    }

    exge211::Scene_ptr scene_;
    std::list<Demo_scene_part*> parts_;
};
