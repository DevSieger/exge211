#include <exge211/actor.h>
#include <exge211/logging.h>
#include <exge211/platform.h>
#include <exge211/renderer.h>
#include <exge211/user_input.h>
#include <exge211/context.h>

#include "demo_application.h"
#include "fp_camera.h"
#include "scenery_one.h"
#include "actors_one.h"
#include "directional_light_part.h"
#include "point_light_part.h"
#include "small_lights_part.h"

#ifdef USING_EXGE211_QT
#include <exge211/qt/gl_window_qt.h>
#endif

using exge211::Platform_factory;
using exge211::Resource_loader;
using exge211::Position;
using exge211::Direction;
using exge211::Keyboard_callbacks;
using exge211::Mouse_callbacks;
using exge211::Detail_level;

Demo_application::Demo_application(Platform_factory &factory, Resource_loader & loader):
    GUI_application(factory,loader)
{
}

bool Demo_application::iteration(GUI_application::Time dt)
{
    cam_->proc(dt);

    if(!window().lock_mouse()){
        cam_->look_at(Position(0,0,1.5),Direction(0,0,1));
    };

    const double time_scale = 1.0;

    if(!paused_){
        time_   += time_scale*dt;
    };

    exge211::FPS_counter::Time_period fps=fps_counter_.get(dt);
    std::string info_str=dk::fstr("fps: %1%;\t\n\nframe time:%2% ms\t\n\n",fps,1000/fps);

    set_info(info_str);

    bool ret = true;
    for(auto& dsc: scenes_){
        exge211::Scene_time scene_dt = (!paused_)? dt : 0;
        scene_dt    *= time_scale;
        dsc.scene()->advance(scene_dt);
        ret = ret && dsc.iteration(time_);
    }

    return ret;
}

void Demo_application::switch_scene(size_t new_current_sc_ix)
{
    current_scene_ix_=new_current_sc_ix;
    renderer().scene(current_scene().scene());
}

void Demo_application::pause(bool paused)
{
    paused_=paused;
    for(auto& dsc: scenes_){
        dsc.pause(paused);
    }
}


bool Demo_application::init()
{
    scenes_.emplace_back(renderer().new_scene());
    // tip: if you don't want to use the auto-advance mode for time of the scene,
    //      then do it before you do any actions that are related to animation.
    scenes_.back().scene()->auto_advance(false);
    scenes_.back().add<Scenery_one>().add<Actors_one>().add<Directional_light_part>();
    //i think independent implementation of Demo_scene_part class for skybox or ambient light is overkill
    scenes_.back().scene()->set_skybox("frozen.png").ambient_light(exge211::RGB(5.5,5.5,7));
    scenes_.back().scene()->shading_variable_f("tone_mapping::key_value",0.20);
    scenes_.back().scene()->shading_variable_f("lum_adaptation::max",5);
    scenes_.back().scene()->sky_light_scale(12);

    scenes_.emplace_back(renderer().new_scene());
    scenes_.back().scene()->auto_advance(false);
    scenes_.back().add<Scenery_one>().add<Actors_one>().add<Point_light_part>();
    scenes_.back().scene()->set_skybox("frozen.png").ambient_light(exge211::RGB(0.07,0.08,0.11));
    scenes_.back().scene()->shading_variable_f("lum_adaptation::min",0.5);
    scenes_.back().scene()->sky_light_scale(3);

    scenes_.emplace_back(renderer().new_scene());
    scenes_.back().scene()->auto_advance(false);
    scenes_.back().add<Scenery_one>().add<Actors_one>().add<Small_lights_part>();
    scenes_.back().scene()->set_skybox("frozen.png").ambient_light(exge211::RGB(0.1,0.1,0.1));
    scenes_.back().scene()->shading_variable_f("tone_mapping::key_value",0.15);
    scenes_.back().scene()->shading_variable_f("lum_adaptation::min",0.9);

    scenes_.emplace_back(renderer().new_scene());
    scenes_.back().scene()->auto_advance(false);
    scenes_.back().add<Scenery_one>().add<Actors_one>().add<Point_light_part>().add<Small_lights_part>();
    scenes_.back().scene()->set_skybox("frozen.png").ambient_light(exge211::RGB(0.2,0.5,2));
    scenes_.back().scene()->sky_light_scale(5.5);

    cam_=std::make_shared<FP_camera>();
    cam_->position(Position{4,-18,4});
    cam_->look_at({0,0,0});
    for(Demo_scene& dsc: scenes_ ){
        dsc.scene()->camera(cam_);
    }
    switch_scene(0);

    // setting up a callbacks for an user input.
    {
        // the velocity of the camera to add when the button is pressed.
        float speed=16;

        Keyboard_callbacks *kctrl=dynamic_cast<Keyboard_callbacks*>(&window());
        Mouse_callbacks *mctrl=dynamic_cast<Mouse_callbacks*>(&window());
        exge211::Window * wndw=&window();

        // the keyboard handling.
        if(kctrl){
            kctrl->on_key_press=[speed,wndw,this](
                    exge211::ctrl::Keyboard_key k,
                    exge211::ctrl::Keyboard_modifiers m){

                // handles numeric keys.
                if((size_t)k>'0'&& (size_t)k<=scenes_.size()+'0'){
                    switch_scene(k-'1');
                    return true;
                }

                Direction vel=cam_->velocity();

                switch(k){
                    case 'A':{
                        vel.x-=speed;
                        break;
                    }
                    case 'D':{
                        vel.x+=speed;
                        break;
                    }
                    case 'S':{
                        vel.y-=speed;
                        break;
                    }
                    case 'W':{
                        vel.y+=speed;
                        break;
                    }
                    case 'F':{
                        vel.z-=speed;
                        break;
                    }
                    case 'R':{
                        vel.z+=speed;
                        break;
                    }
                    case 'Z':{
                        wndw->lock_mouse(!wndw->lock_mouse());
                        return true;
                    }
                    case 'P':{
                        pause(!paused_);
                        return true;
                    }
                    case 'C':{
                        current_scene().scene()->debug((current_scene().scene()->debug()+1)%3);
                        return true;
                    }
                    case '`':{
                        wndw->full_screen(!wndw->full_screen());
                        return true;
                    }
                }

                cam_->velocity(vel);
                return true;
            };
            kctrl->on_key_release=[speed,this](
                    exge211::ctrl::Keyboard_key k,
                    exge211::ctrl::Keyboard_modifiers m){
                Direction vel=cam_->velocity();
                switch(k){
                    case 'A':{
                        vel.x+=speed;
                        break;
                    }
                    case 'D':{
                        vel.x-=speed;
                        break;
                    }
                    case 'S':{
                        vel.y+=speed;
                        break;
                    }
                    case 'W':{
                        vel.y-=speed;
                        break;
                    }
                    case 'F':{
                        vel.z+=speed;
                        break;
                    }
                    case 'R':{
                        vel.z-=speed;
                        break;
                    }
                }
                cam_->velocity(vel);
                return true;
            };
        }else{
            log_msg(Detail_level::NON_CRITICAL,"error: window not kb_ctrl");
        }

        // the mouse handling.
        if(mctrl){
            mctrl->on_mouse_move=[wndw,this](
                    exge211::ctrl::Cursor_position const & pos,
                    exge211::ctrl::Mouse_buttons mbs,
                    exge211::ctrl::Keyboard_modifiers m
                    )
            {
                // 'the 1st person' camera mode.
                if(!mbs && wndw->lock_mouse()){
                    float sens=glm::pi<float>()*0.5;
                    exge211::Context::Size csize=wndw->context().size();
                    if(csize){
                        float dl=sens/glm::max<float>(csize.width,csize.height);
                        cam_->look(pos.x*dl,pos.y*dl);
                    };
                };

                return true;
            };
        }else{
            log_msg(Detail_level::NON_CRITICAL,"error: window not mouse_ctrl");
        }
    }

#ifdef USING_EXGE211_QT
    info_frame_.set_window(static_cast<exge211::GL_window_Qt*>(&window()));
    info_frame_.show();
#endif
    pause(true);
    return true;
}

Demo_application::~Demo_application()
{
}

void Demo_application::set_info(const exge211::String &str)
{
#ifdef USING_EXGE211_QT
    info_frame_.set_info(str);
#endif
}
