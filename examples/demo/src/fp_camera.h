#pragma once

#include <exge211/camera.h>
#include <memory>

/// The extended camera object.
class FP_camera: public exge211::Camera{
public:
    /*!
     * \brief Sets the object to the state at the next time step.
     * \param dt Seconds from the current time step to the next one.
     */
    void proc(float dt);

    /*!
     * \brief Rotates camera view using the specified values.
     * \param x The yaw angle in radians.
     * \param y The pitch angle in radians.
     */
    void look(float x,float y);
    void look_at(exge211::Position const & target_position,exge211::Direction const & up=default_up()){
        Camera::look_at(target_position,up);
        dest_rot_=rotation();
    }
    /*!
     * \brief Sets the velocity of the camera movement to the specified value.
     * \param vel The specified velocity.
     * \return This object.
     */
    FP_camera & velocity(exge211::Direction const& vel){
        vel_=vel;
        return *this;
    }
    /// Returns the velocity of this camera.
    exge211::Direction const & velocity()const{
        return vel_;
    }

private:
    exge211::Rotation dest_rot_{1,0,0,0};
    exge211::Direction vel_{0,0,0};
    int remaining_rot_=0;
};

using FP_camera_ptr=std::shared_ptr<FP_camera>;
