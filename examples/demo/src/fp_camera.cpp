#include "fp_camera.h"


void FP_camera::proc(float dt){
    using exge211::Rotation;
    using exge211::Position;
    using exge211::Direction;
    Rotation rot=rotation();
    Position npos=position();
    Direction dp=dt*vel_;
    const Direction z={0,0,1};
    Direction y=rot*orig_forward();
    y.z=0;
    if(y.x || y.y){
        y=glm::normalize(y);
    }else{
        y=rot*orig_forward();
    }
    const Direction x=rot*Direction{1,0,0};
    npos+=y*dp.y;
    npos+=z*dp.z;
    npos+=x*dp.x;
    position(npos);


    if(remaining_rot_>0){
        --remaining_rot_;
        if(dt>0.04){
            remaining_rot_=0;
        }
        rot=glm::slerp(dest_rot_,rot,remaining_rot_*0.5f);
        rotation(rot);
    }else{
        dest_rot_=rot;
    }

}

void FP_camera::look(float x, float y){
    float ya = glm::pitch(dest_rot_);
    float xa = glm::roll(dest_rot_);
    ya+=y;
    ya=glm::clamp(ya,0.0f,glm::pi<float>());

    xa-=x;
    if(xa>glm::pi<float>()*2){
        xa-=glm::pi<float>()*2;
    }else if(xa<0){
        xa+=glm::pi<float>()*2;
    };
    //it's ok to use glm::vec3 (not Direction) inside glm function
    dest_rot_=glm::angleAxis(xa,glm::vec3{0,0,1})*glm::angleAxis(ya,glm::vec3{1,0,0});
    remaining_rot_=2;
}
