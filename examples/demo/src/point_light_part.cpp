#include "point_light_part.h"
#include <exge211/point_light.h>
#include <exge211/scene.h>

namespace  {

exge211::RGB const& basic_color(size_t index){
    using exge211::RGB;
    static RGB intensity[3] = {
        {0.5,0.7,1},
        {0.7,0.5,1},
        {1,0.7,0.5}
    };
    return intensity[index];
}

float lamp_intensity (size_t index, double time){
    switch( index ){
        case 0:
            return 14 + 4*sin(0.25*time);
        case 1:
            return 8 + 5*sin(4*-time);
        default:
            return 2.5 + 2*sin(4*time)*sin(11*time);
    }
}

exge211::Position lamp_position (size_t index, double time){
    switch( index ){
        case 0:
            return {cos(time*0.125+20)*9, sin(time*0.225+30)*12*cos(time*0.225), 2*sin(time*0.2)*cos(time*0.125)+7};
        case 1:
            return {cos(-time*0.125)*16*cos(-time*0.0125),sin(-time*0.125)*16,0.125*sin(-time*2)+4};
        default:
            return {cos(time*0.7+3)*16,sin(time*0.7+3)*16*cos(time*0.0825),0.425*sin(time*2+4)+3};
    }
}
float target_distance(){
    return 3;
}

}

void Point_light_part::set_up(const exge211::Scene_ptr& scene)
{
    lights_.clear();
    const float radius[3]   = { 0.25, 0.15, 0.08 };
    const float cut_off     = 0.1;

    for(size_t i=0; i<3; ++i){
        exge211::Point_light_ptr pl_ptr;
        pl_ptr=scene->add_point_light(1);
        pl_ptr->radius(radius[i]).cut_off(cut_off);
        pl_ptr->position( lamp_position(i,0));
        pl_ptr->intensity( basic_color(i), target_distance(), lamp_intensity(i,0) );
        lights_.emplace_back(std::move(pl_ptr));
    }
}

bool Point_light_part::iteration(double time)
{
    using exge211::RGB;

    for( size_t i=0; i<3; ++i){
        exge211::Point_light& source = *lights_[i];
        source.position( lamp_position(i, time));
        source.intensity( basic_color(i), target_distance(), lamp_intensity(i, time));
    }

    return 1;
}
