#pragma once

#include <exge211/common_types.h>
#include <QFrame>

namespace exge211{
class GL_window_Qt;
}

namespace Ui {
class Info_frame;
}

/// The widget for displaying a string with the given information.
class Info_frame : public QFrame
{
    Q_OBJECT
public:
    explicit Info_frame(QWidget *parent = 0);
    ~Info_frame();

    /// Sets the string to display.
    void set_info(exge211::String const & str);

    /*!
     * \brief Sets the rendering window.
     * \param wnd The given window.
     * \details If the window has been already set the call is ignored.
     */
    void set_window(exge211::GL_window_Qt *wnd);

private:
    Ui::Info_frame *ui;
    exge211::GL_window_Qt * wnd_=nullptr;
    QWidget *wrapper_=nullptr;
    void closeEvent(QCloseEvent * event) override;
};
