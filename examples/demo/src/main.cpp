#include <exge211/logging.h>
#include <exge211/resource_loader.h>
#include <exge211/smplres/smpl_resource_loader.h>
#include <exge211/smplres/texture_format_stb_image.h>
#include <exge211/smplres/file_loader.h>

#ifdef USING_EXGE211_QT
#include <exge211/qt/platform_factory_qt.h>
#include <exge211/qt/resource_loading.h>
#else
#include <exge211/glfw/platform_factory_glfw.h>
#endif

#include "demo_application.h"
#include <cassert>
#include <iostream>

/// Initializes the logging system of exge211.
void init_logging_system(){
    using namespace exge211;
    logging::init();

    // this copy is used to make a new output functor which in addition writes messages to the console.
    logging::Output_functor file_output=logging::output();

    // the new functor.
    auto output_lambda=[file_output](Detail_level lvl,String const& str){
        file_output(lvl,str);
        if(lvl<=Detail_level::NON_CRITICAL){
            std::cerr<<str<<std::endl;
        }else{
            std::cout<<str<<std::endl;
        }
    };

    logging::output(output_lambda);

    logging::detail_level(Detail_level::DETAILED);
}


// The function is used to relax the File_loader_t interface constraint.
// The generic implementation for exge211::File_loader and exge211::QFile_loader.
template<typename File_loader_t>
inline void set_base_dir(File_loader_t& loader, exge211::String const& path)
{
    loader.prefix(path);
}

/// Sets up the given resource loader.
template<typename File_loader_t>
void init_resource_loader(exge211::Resource_loader& loader){
    using exge211::Smpl_resource_loader;
    Smpl_resource_loader<exge211::Skin_info,File_loader_t> skin_loader;
    Smpl_resource_loader<exge211::Material_info,File_loader_t> mat_loader;
    Smpl_resource_loader<exge211::Skeleton_info,File_loader_t> skel_loader;
    Smpl_resource_loader<exge211::Animated_model_data,File_loader_t> amodel_loader;
    Smpl_resource_loader<exge211::Static_model_data,File_loader_t> smodel_loader;
    Smpl_resource_loader<exge211::Animation_data,File_loader_t> animation_loader;
    Smpl_resource_loader<exge211::Cube_map_data,File_loader_t> skybox_loader;
    Smpl_resource_loader<exge211::Shader_source,File_loader_t> shader_loader;
    Smpl_resource_loader<exge211::Texture_data,File_loader_t> texture_loader;

    // the path to the data directory
    exge211::String base_share_dir("../share/exge211/examples/");

    set_base_dir(skin_loader,base_share_dir+"data/skin/");
    set_base_dir(mat_loader,base_share_dir+"data/material/");
    set_base_dir(skel_loader,base_share_dir+"data/skeleton/");
    set_base_dir(amodel_loader,base_share_dir+"data/model/");
    set_base_dir(smodel_loader,base_share_dir+"data/sceneries/");
    set_base_dir(animation_loader,base_share_dir+"data/animation/");
    set_base_dir(skybox_loader,base_share_dir+"data/skybox/");
    set_base_dir(shader_loader,"../share/exge211/shaders/");
    set_base_dir(texture_loader,base_share_dir+"data/texture/");

    loader.load_skin=skin_loader;
    loader.load_material=mat_loader;
    loader.load_skeleton=skel_loader;
    loader.load_model=amodel_loader;
    loader.load_animation=animation_loader;
    loader.load_static_model=smodel_loader;
    loader.load_skybox=skybox_loader;
    loader.load_shader_source=shader_loader;
    loader.load_texture=texture_loader;
}

int main(int argc, char *argv[])
{
    init_logging_system();
    exge211::Resource_loader loader;
#ifdef USING_EXGE211_QT
    using File_loader=exge211::QFile_loader;
    exge211::Platform_factory_Qt factory(argc,argv);
#else
    using File_loader=exge211::File_loader;
    exge211::Platform_factory_GLFW factory;
#endif
    init_resource_loader<File_loader>(loader);
    Demo_application app(factory,loader);

    app.init();

    return app.run();
}
