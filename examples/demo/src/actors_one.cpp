#include "actors_one.h"
#include <exge211/actor.h>
#include <exge211/scene.h>

namespace{

exge211::Rotation const& correcting_rotation(){
    static const exge211::Rotation value(glm::angleAxis(0.5f*glm::pi<float>(),glm::vec3{1,0,0}));
    return value;
}

exge211::Rotation const& basic_rotation(){
    static const exge211::Rotation value(glm::angleAxis(glm::pi<float>(),glm::vec3{0,0,1})*correcting_rotation());
    return value;
}

}

void Actors_one::set_up( exge211::Scene_ptr const& scene)
{
    using exge211::Position;
    exge211::AABB new_boundary(Position(-40,-40,-10),Position(40,40,50));
    new_boundary.add(scene->boundary());
    scene->boundary(new_boundary);

    actors_.clear();
    exge211::Skeleton_handle dwarf_sk=scene->load_skeleton_type("dwarf.skeleton");
    exge211::Model_handle dwarf_body=scene->load_actor_item_proto("dwarf.model_data","dwarf.skin");
    exge211::Animation_handle dwarf_anim=scene->load_animation("dwarf.anim");
    exge211::Actor_ptr dw=scene->new_actor(dwarf_sk);
    dw->set(0,dwarf_body);
    dw->animation().play(dwarf_anim);
    dw->animation().scale(0.1);
    dw->animation().rotatation(basic_rotation());
    dw->animation().position({0,-5,-1});
    actors_.push_back(dw);

    float scale=0.05;
    const size_t actor_count=20;
    for(size_t i=0;i<actor_count;++i){
        exge211::Actor_ptr walker=scene->new_actor(dwarf_sk);
        walker->set(0,dwarf_body);
        walker->animation().play(dwarf_anim);
        walker->animation().scale(scale);
        scale+=0.0002;
        actors_.emplace_back(std::move(walker));
    }
    set_normal_animation_rate();
}

bool Actors_one::iteration(double time)
{
    float base_anim_rate = paused_ ? 0: 0.5;

    float angle=glm::pi<float>()*1.5+time*0.01 + cos(time*11)*sin(time*5)*0.0025;
    int radius=3;
    float w=0;

    exge211::Actor_ptr const& walker=actors_[0];
    walker->animation().change_rate(cos(time*5)*sin(time*7)*base_anim_rate*2);

    float time_scale    = 0.05;
    for(size_t i=1;i<actors_.size();++i){
        exge211::Actor_ptr const& walker=actors_[i];

        walker->animation().change_rate( sin(time * time_scale ) * base_anim_rate);

        base_anim_rate *= 1.025;
        time_scale     += 0.25;
        angle          *= 1.0002;
        w              += 2;
        radius          = radius % 20;
        radius         += 5.2;

        walker->animation().position(exge211::Position{cos(angle+w)*radius,sin(angle+w)*radius,walker->animation().scale()-1});
        walker->animation().rotatation(glm::angleAxis(angle + glm::pi<float>()*0.5f,glm::vec3{0,0,1})*basic_rotation());
    }
    return true;
}

void Actors_one::pause(bool paused)
{
    if(paused){
        pause_animation();
    }else{
        set_normal_animation_rate();
    }
    paused_ = paused;
}

void Actors_one::set_normal_animation_rate()
{
    float rate=0.25;
    for(exge211::Actor_ptr const& aptr:actors_){
        aptr->animation().change_rate(rate);
        rate+=0.0005;
    }
}

void Actors_one::pause_animation()
{
    for(exge211::Actor_ptr const& aptr:actors_){
        aptr->animation().change_rate(0);
    }
}
