#pragma once

#include "demo_scene_part.h"
#include <vector>
#include <exge211/handles.h>

/*!
 * \brief Demo_scene_part that manages sceneries.
 *
 * This class is used to illustrate a scenery usage.
 */
class Scenery_one: public Demo_scene_part
{
public:
    void set_up(exge211::Scene_ptr const& scene)override;
private:
    std::vector<exge211::Scenery_ptr> sceneries_;
};
