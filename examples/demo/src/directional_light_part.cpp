#include "directional_light_part.h"
#include <exge211/directional_light.h>
#include <exge211/scene.h>

void Directional_light_part::set_up(const exge211::Scene_ptr& scene)
{
    lights_.clear();
    using exge211::RGB;
    const RGB intesity_1(3,3,1.5);
    const RGB direction_1=glm::normalize(glm::vec3(-1,4,-5));
    lights_.emplace_back(scene->add_directional_light(intesity_1,direction_1));
}

bool Directional_light_part::iteration(double time)
{
    if(!lights_.empty()){
        lights_[0]->direction(glm::normalize(exge211::Direction(7*cos(time*0.025),8*sin(time*0.025),-7+7*sin(time*0.0015)*cos(time*0.0025))));
    }
    return true;
}
