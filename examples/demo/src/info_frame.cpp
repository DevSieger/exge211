#include "info_frame.h"
#include "ui_info_frame.h"
#include <QCloseEvent>
#include <QWindow>
#include <exge211/qt/gl_window_qt.h>

Info_frame::Info_frame(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::Info_frame)
{
    ui->setupUi(this);
}

Info_frame::~Info_frame()
{
    delete ui;
}

void Info_frame::set_info(const exge211::String &str)
{
    ui->info1->setText(str.c_str());
}

void Info_frame::set_window(exge211::GL_window_Qt *wnd)
{
    if(!wnd_){
        wnd_=wnd;
        wrapper_=QWidget::createWindowContainer(static_cast<QWindow*>(wnd));
        wrapper_->setFocusPolicy(Qt::StrongFocus);
        ui->splitter->addWidget(wrapper_);
    };
}

void Info_frame::closeEvent(QCloseEvent *event)
{
    if(wnd_){
        if(wnd_->on_close && !wnd_->on_close()){
            event->ignore();
            return;
        }else{
            wrapper_->setParent(nullptr);
            wnd_->setParent(nullptr);
        }
    };
    event->accept();
}
