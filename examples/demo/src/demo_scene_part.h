#pragma once

#include <exge211/handles.h>
#include <dk/concepts.h>

/// An abstract base class for the scene management.
class Demo_scene_part: dk::Non_copyable<Demo_scene_part>{
public:
    /// Sets up the managed objects in the given scene.
    virtual void set_up(exge211::Scene_ptr const&)=0;
    /*!
     * \brief Sets the managed objects to the state at the given time.
     * \param time A total elapsed time from the application start.
     * \return false if an user's application have to stop.
     */
    virtual bool iteration(double time){return true;}

    /*!
     * \brief The pause handler.
     *
     * This method has to change a state of managed objects to match the pause state.
     * Ie if paused==true, then this method has to change the state to "frozen".
     */
    virtual void pause(bool paused){}

    virtual ~Demo_scene_part()=default;
};
