import qbs
import "qbs/config_loader.js" as ConfigLoader

Project{
    property string configPath: {throw("please, set path to your config file in this property");return undefined;}   //name of "path" may be changed
    readonly property string projectName: "exge211"
    readonly property string includeDirectoryName: projectName
    property bool installHeaders: true
    PropertyOptions{
        name: "configPath"
        description: "absolute path to configuration file, take a look at config_example.json"
    }
    Product{
        name: "documentation"
        Group {
            name: "qbs-config example"
            files: ["config_example.json"]
        }
    }

    minimumQbsVersion: "1.6.0"

    property var config: ConfigLoader.loadConfig(configPath);
    references: [
        "exge211_core/exge211_core.qbs",
        "examples/demo/demo.qbs",
        "exge211_smplres/exge211_smplres.qbs",
        "exge211_qt/exge211_qt.qbs",
        "exge211_glfw/exge211_glfw.qbs",
        "converter/converter.qbs"
    ]
}
