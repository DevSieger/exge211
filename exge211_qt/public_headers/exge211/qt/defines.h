#pragma once

#if defined(_WIN32) && !defined(EXGE211_QT_STATIC)

#ifdef EXGE211_QT_EXPORTING
#define EXGE211_QT_EXPORT __declspec(dllexport)
#else
#define EXGE211_QT_EXPORT __declspec(dllimport)
#endif //#ifdef EXGE211_QT_EXPORTING

#else
#define EXGE211_QT_EXPORT
#endif
