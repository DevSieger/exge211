#pragma once

#include "defines.h"
#include <exge211/res/rs_fwd.h>
#include <exge211/common_types.h>

namespace exge211 {

/// \brief A loader to use with exge211::Smpl_resource_loader.
/// \details This loader uses QFile.
class EXGE211_QT_EXPORT QFile_loader{
public:
    /// \brief Sets a prefix for a requested name.
    /// \details you can use ':/' to access qt resources, this is the main purpose of this class
    void prefix(String const& prefix){
        prefix_=prefix;
    }
    /// Sets a suffix for a requested name.
    void suffix(String const& a_suffix){
        suffix_=a_suffix;
    }
    /// Returns the prefix for requested names.
    String const & prefix()const{
        return prefix_;
    }
    /// Returns the suffix for requested names.
    String const & suffix()const{
        return suffix_;
    }
protected:
    /// \brief Returns a content of the file with the given name.
    /// \param name The given name.
    /// \return The file content.
    /// \details The path to the file with the given name is "prefix() + name + suffix()"
    String get(Resource_request const & name);
private:
    String prefix_="./";
    String suffix_;
};

/// \brief A parser to use with exge211::Smpl_resource_loader
/// \details Set the 'Format_base' template parameter of exge211::Smpl_resource_loader to this class to use it.
class EXGE211_QT_EXPORT Texture_format_qt
{
public:
    using Resource=Texture_data;
    bool parse(const char* str_in,size_t str_len, Resource & out);
};

}
