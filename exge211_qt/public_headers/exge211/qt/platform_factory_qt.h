#pragma once

#include <exge211/platform.h>
#include "defines.h"

namespace exge211 {

/*!
 * \brief A factory for creating platform-specific objects which implementation based on Qt.
 *
 * \warning Use ONLY ONE instance of this class (the reason is QApplication).
 * This class is not the real singleton for a good reason.
 */
class EXGE211_QT_EXPORT Platform_factory_Qt : public Platform_factory
{
public:
    /**
     * \brief Constructs the factory using arguments of the "main" function.
     *  \warning The data referred to by argc and argv must stay valid for the entire lifetime of the object. The reason is QApplication::QApplication(int&,char**).
     */
    Platform_factory_Qt(int &argc, char ** argv);

    Window_ptr new_window(int width,int height,String const & title) override;
    GUI_event_loop_ptr get_GUI_event_loop()override;
    ~Platform_factory_Qt();
private:
    GUI_event_loop_ptr event_loop_=nullptr;
};

}
