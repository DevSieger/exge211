#pragma once

#include "defines.h"
#include <exge211/platform.h>
#include <exge211/user_input.h>
#include <QCursor>
#include <QWindow>

namespace exge211 {

/*!
 * \brief An implementation of exge211::Window using Qt.
 * \details Creates the OpenGL context.
 */
class EXGE211_QT_EXPORT GL_window_Qt :public QWindow, public exge211::Window, public Keyboard_callbacks,public Mouse_callbacks
{
    Q_OBJECT
public:
    using Window=exge211::Window;
    /*!
     * \brief Constructs the window using the given parameters.
     * \param width_a The given width of the window.
     * \param height_a The given height of the window.
     * \param title_a The given title of the window.
     */
    GL_window_Qt(int width_a, int height_a, QString const & title_a);

    Context& context()const override;
    bool visible()const override;
    exge211::Window & visible(bool visible_a) override;
    void show()override;
    void resize(int width, int height) override;
    bool full_screen()const override;
    exge211::Window & full_screen(bool fullScreen_) override;
    exge211::Window & lock_mouse(bool) override;
    exge211::Window & lock_keyboard(bool) override;
    bool lock_mouse()const override{
        return mlock_;
    }
    bool lock_keyboard()const override{
        return kblock_;
    }
    ~GL_window_Qt();
private:
    Context * ctx_=nullptr;
    bool fs_=false; ///< A state of the fullscreen mode.
    bool kblock_=false; ///< A state of the keyboard lock.
    bool mlock_=false;  ///< A state of the mouse lock.
    QCursor cursor_;    ///< A cursor shape associated with this window.

    /// Converts position from Qt system.
    ctrl::Cursor_position to_GLSP(QPointF qp);

    //QWindow related methods.
    /* Camel case from qt overridden method names */
    bool event(QEvent * ev)override;
    void keyPressEvent(QKeyEvent * ev)override;
    void keyReleaseEvent(QKeyEvent * ev)override;
    void mouseDoubleClickEvent(QMouseEvent * ev)override;
    void mouseMoveEvent(QMouseEvent * ev)override;
    void mousePressEvent(QMouseEvent * ev)override;
    void mouseReleaseEvent(QMouseEvent * ev)override;
    void wheelEvent(QWheelEvent * ev)override;

    /// Moves cursor to the center of this window.
    void mouse_to_center();
    /// Sets if the cursor is hidden.
    void hide_cursor(bool b);
};

}
