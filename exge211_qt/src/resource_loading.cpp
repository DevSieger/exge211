#include <exge211/logging.h>
#include <exge211/res/resource_data.h>

#include "public_headers.h"
#include PUBLIC_HEADER(resource_loading.h)

#include <QFile>
#include <QImage>

namespace exge211 {

String QFile_loader::get(const Resource_request & name)
{
    String ret;
    QString path=QString::fromStdString(prefix()+name+suffix());
    QFile file(path);
    if(file.open(QFile::ReadOnly)){
        int size=file.size();
        ret.resize(size);
        file.read(&(ret[0]),size);
        file.close();
    }else{
        log_msg(Detail_level::NON_CRITICAL,"can't open qt resource %1%",path.toStdString());
    };
    return ret;
}

bool Texture_format_qt::parse(const char * str_in, size_t str_len, Texture_format_qt::Resource & out)
{
    if(out.width && out.height){
        out.resize();   //we don't want to crash
    }
    QImage image=QImage::fromData((uchar*)str_in,str_len);
    if(image.isNull()){
        return false;
    };
    QImage::Format format;
    switch(out.format){
        case Image_format::RGBA8888 : format= QImage::Format_RGBA8888;  break;
        case Image_format::RGB888   : format= QImage::Format_RGB888;    break;
        default                     : {
            format= QImage::Format_RGBA8888;
            log_msg(Detail_level::NON_CRITICAL,"Unexpected Image_format");
        }
    }

    image=image.convertToFormat(format);
    if(!out.width || !out.height){
        out.height=image.height();
        out.width=image.width();
        out.resize();
    }else{
        if(
                (unsigned int)image.width()!=out.width ||
                (unsigned int)image.height()!=out.height
        ){

            image=image.scaled(out.width,out.height);
        }
    }
    /*
     * QImage uses the other line order.
     * I don't want to use "QImage.mirrored" because of a performance reason.
     */
    const size_t line_size=out.pixel_size()*out.width;
    for(size_t line=0;line<out.height;++line){
        const size_t output_start=line_size*line;
        const size_t input_start=line_size*(out.height-line-1);
        memcpy(&out.data[output_start],image.constBits()+input_start,line_size);
    }
    return true;
}

}
