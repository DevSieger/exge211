#include "public_headers.h"
#include PUBLIC_HEADER(gl_window_qt.h)
#include "context_qt.h"
#include <qsurfaceformat.h>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QMessageBox>

namespace exge211 {

namespace{

ctrl::Keyboard_modifiers convert(Qt::KeyboardModifiers const & km){
    ctrl::Keyboard_modifiers md;
    if(km.testFlag(Qt::ShiftModifier)){
        md|=ctrl::Keyboard_modifier::SHIFT;
    };
    if(km.testFlag(Qt::ControlModifier)){
        md|=ctrl::Keyboard_modifier::CTRL;
    };
    if(km.testFlag(Qt::AltModifier)){
        md|=ctrl::Keyboard_modifier::ALT;
    };
    return md;
}
ctrl::Mouse_button convert(Qt::MouseButton const &  qb){
    switch(qb){
        case Qt::LeftButton: return ctrl::Mouse_button::LEFT;
        case Qt::RightButton: return ctrl::Mouse_button::RIGHT;
        case Qt::MiddleButton: return ctrl::Mouse_button::MIDDLE;
        default: return ctrl::Mouse_button::NONE;
    };
}

ctrl::Mouse_buttons convert(Qt::MouseButtons const & qb){
    ctrl::Mouse_buttons mb;
    if(qb.testFlag(Qt::LeftButton)){
        mb|=ctrl::Mouse_button::LEFT;
    };
    if(qb.testFlag(Qt::RightButton)){
        mb|=ctrl::Mouse_button::RIGHT;
    };
    if(qb.testFlag(Qt::MiddleButton)){
        mb|=ctrl::Mouse_button::MIDDLE;
    };
    return mb;
}

}

bool GL_window_Qt::event(QEvent *ev)
{
    switch( ev->type()){
        case QEvent::Close:
            if(on_close){
                if(!on_close()){
                    return false;
                }
            };
        [[fallthrough]]; default:
            return QWindow::event(ev);
    };
}

ctrl::Cursor_position GL_window_Qt::to_GLSP(QPointF qp)
{
    return ctrl::Cursor_position(float(qp.x())-width()*0.5f,height()*0.5f-float(qp.y()));
}

void GL_window_Qt::keyPressEvent(QKeyEvent *ev)
{
    if(on_key_press){
        if(on_key_press(ev->key(),convert(ev->modifiers()))){
            ev->accept();
        }
    }
}

void GL_window_Qt::keyReleaseEvent(QKeyEvent *ev){
    if(on_key_release){
        if(on_key_release(ev->key(),convert(ev->modifiers()))){
            ev->accept();
        }
    }
}
/* ctrl::MButton buttom,ctrl::MPosition const &,ctrl::MButtons, ctrl::KbModifiers */
void GL_window_Qt::mouseDoubleClickEvent(QMouseEvent *ev){
    if(on_double_click){
        if(on_double_click(convert(ev->button()),to_GLSP(ev->localPos()),convert(ev->buttons()), convert(ev->modifiers()))){
            ev->accept();
        }
    }
}

void GL_window_Qt::mouseMoveEvent(QMouseEvent *ev){
    if(on_mouse_move){
        if(on_mouse_move(to_GLSP(ev->localPos()),convert(ev->buttons()), convert(ev->modifiers()))){
            ev->accept();
        }
    }
    if(lock_mouse()){
        mouse_to_center();
    };
}

void GL_window_Qt::mousePressEvent(QMouseEvent *ev){
    if(on_mouse_press){
        if(on_mouse_press(convert(ev->button()),to_GLSP(ev->localPos()),convert(ev->buttons()), convert(ev->modifiers()))){
            ev->accept();
        }
    }
}

void GL_window_Qt::mouseReleaseEvent(QMouseEvent *ev){
    if(on_mouse_release){
        if(on_mouse_release(convert(ev->button()),to_GLSP(ev->localPos()),convert(ev->buttons()), convert(ev->modifiers()))){
            ev->accept();
        }
    }
}
//bool(float,ctrl::MPosition const &,ctrl::MButtons, ctrl::KbModifiers)>;
void GL_window_Qt::wheelEvent(QWheelEvent *ev)
{
    if(on_wheel){
        if(on_wheel(ev->angleDelta().y()*0.125f,to_GLSP(ev->posF()),convert(ev->buttons()), convert(ev->modifiers()))){
            ev->accept();
        }
    }
}

void GL_window_Qt::mouse_to_center()
{

    QPoint p(width()/2,height()/2);
    p=mapToGlobal(p);
    QPoint r=QCursor::pos()-p;
    if(r.x()||r.y()){
        QCursor::setPos(p);
    };
}

void GL_window_Qt::hide_cursor(bool b)
{
    if(b){
        setCursor(QCursor(Qt::BlankCursor));
    }else{
        setCursor(cursor_);
    }
}

 GL_window_Qt::GL_window_Qt(int width_a, int height_a, QString const & title_a)
{
    QWindow::setSurfaceType(QWindow::OpenGLSurface);
    //#if !defined(__ANDROID__)
        QSurfaceFormat gl_format=QSurfaceFormat::defaultFormat();
        gl_format.setRenderableType(QSurfaceFormat::OpenGL);
        gl_format.setProfile(QSurfaceFormat::CoreProfile);
        gl_format.setVersion(4,4);
        gl_format.setSamples(1);
        //gl_format.setOption(QSurfaceFormat::DebugContext,true);
        gl_format.setSwapBehavior(QSurfaceFormat::DoubleBuffer);
        gl_format.setRedBufferSize(8);
        gl_format.setGreenBufferSize(8);
        gl_format.setBlueBufferSize(8);
        gl_format.setAlphaBufferSize(0);
        gl_format.setDepthBufferSize(24);
        gl_format.setStencilBufferSize(8);
        gl_format.setSwapInterval(0);

        setFormat(gl_format);
    //#endif
        Context_Qt * ctx = new Context_Qt(this);
        ctx->setFormat(requestedFormat());
        ctx->create();
        ctx_=ctx;
        QWindow::create();
        QWindow::setTitle(title_a);
        QWindow::resize(width_a,height_a);
        QSurfaceFormat real_format=ctx->format();
        int major=real_format.majorVersion();
        int minor=real_format.minorVersion();
        if(major<4 || (major==4 && minor<3)){
            QMessageBox mb(
                        QMessageBox::Critical,
                        "Error",
                        QString("Your have opengl version %1.%2.\n"
                                "Minimal supported version is 4.3.\n"
                                "To solve this problem try to update video card driver."
                        ).arg(major).arg(minor));
            mb.exec();
        }
}


Context& GL_window_Qt::context() const
{
    assert(ctx_);
    return *ctx_;
}

bool GL_window_Qt::visible() const
{
    return QWindow::isVisible();
}

 GL_window_Qt::~GL_window_Qt(){
    if(ctx_){
        delete ctx_;
    }
}
Window & GL_window_Qt::visible(bool visible_a)
{
    QWindow::setVisible(visible_a);
    return *this;
}

void GL_window_Qt::show()
{
    if(fs_){
        QWindow::showFullScreen();
    }else{
        QWindow::showNormal();
    }
}

void GL_window_Qt::resize(int width, int height)
{
    QWindow::resize(width,height);
}

bool GL_window_Qt::full_screen() const
{
    return fs_;
}

Window & GL_window_Qt::full_screen(bool fullScreen_)
{
    if(fs_!=fullScreen_){
        fs_=fullScreen_;
        show();
    }
    return *this;
}

Window & GL_window_Qt::lock_mouse(bool b)
{
    if(b!=mlock_){
        QWindow::setMouseGrabEnabled(b);
        mlock_=b;
        if(mlock_){
            mouse_to_center();
        }
        hide_cursor(b);
    };
    return *this;
}

Window & GL_window_Qt::lock_keyboard(bool b)
{
    if(b!=mlock_){
        QWindow::setKeyboardGrabEnabled(b);
        mlock_=b;
    };
    return *this;
}

}
