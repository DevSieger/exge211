#include "gui_event_loop_qt.h"
#include <QAbstractEventDispatcher>

namespace exge211 {

void GUI_event_loop_Qt::process_events()
{
    app_.eventDispatcher()->processEvents(QEventLoop::AllEvents);
}

bool GUI_event_loop_Qt::running() const
{
    return running_;
}

GUI_event_loop &GUI_event_loop_Qt::running(bool running_a)
{
    running_=running_a;
    return *this;
}

GUI_event_loop_Qt::GUI_event_loop_Qt(int &argc, char ** argv):
    app_(argc,argv)
{
}

GUI_event_loop_Qt::~GUI_event_loop_Qt()
{

}

}
