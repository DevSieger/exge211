#include "context_qt.h"

namespace exge211 {

Context_Qt::Context_Qt(QWindow *parent): QOpenGLContext(parent),wnd_(parent)
{
}

void Context_Qt::make_current()
{
    QOpenGLContext::makeCurrent(wnd_);
}

Context_Qt::~Context_Qt()
{

}

void Context_Qt::swap_buffers()
{
    QOpenGLContext::swapBuffers(wnd_);
}


Context::Size Context_Qt::size() const
{
    Context::Size size;
    size.width  = wnd_->geometry().width();
    size.height = wnd_->geometry().height();
    return size;
}

}
