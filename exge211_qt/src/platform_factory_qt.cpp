#include "public_headers.h"
#include PUBLIC_HEADER(platform_factory_qt.h)
#include PUBLIC_HEADER(gl_window_qt.h)
#include "gui_event_loop_qt.h"
#include <exge211/logging.h>

namespace exge211 {

Platform_factory_Qt::Platform_factory_Qt(int& argc, char **argv){

    static bool first_time=true;

    if(first_time){
        first_time=false;
    }else{
        log_msg(Detail_level::CRITICAL,"You have to use only one instance of Platform_factory_Qt because of QApplication.");
    }

    /*
     * That good reason from a warning in the class description:
     * Yes, it is possible to make event_loop singleton.
     * But then qt shows the warnings about threads and QThreadStorage because of the lifespan of QApplication (IMHO).
     */
    event_loop_=std::make_shared<GUI_event_loop_Qt>(argc,argv);
}

Window_ptr Platform_factory_Qt::new_window(int width, int height,  String const &title)
{
    /*
     * I think it is safe to use std::shared_ptr without any custom deleters in this context.
     * Cross-dll problems aren't expected.
     */
    return std::make_shared<GL_window_Qt>(width,height,QString(title.c_str()));
}

GUI_event_loop_ptr Platform_factory_Qt::get_GUI_event_loop()
{
    return event_loop_;
}

Platform_factory_Qt::~Platform_factory_Qt()
{

}


}
