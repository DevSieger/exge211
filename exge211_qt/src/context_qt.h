#pragma once

#include <exge211/context.h>
#include <qopenglcontext.h>
#include <qwindow.h>

namespace exge211 {

class Context_Qt : public OGL_context,public QOpenGLContext
{
public:
    Size size() const override;
    void make_current() override;
    ~Context_Qt();
    void swap_buffers() override;
private:
    friend class GL_window_Qt;
    Context_Qt(QWindow * parent);
    QWindow * wnd_;
};

}
