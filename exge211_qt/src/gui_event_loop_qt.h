#pragma once

#include <exge211/platform.h>
#include <QApplication>

namespace exge211 {

class GUI_event_loop_Qt : public GUI_event_loop
{
public:
    void process_events()override;
    bool running()const override;
    GUI_event_loop & running(bool running_a) override;
    GUI_event_loop_Qt(int& argc,char ** argv);
    ~GUI_event_loop_Qt();
private:
    QApplication app_;
    bool running_=true;
};

}
