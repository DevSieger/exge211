import qbs

Library{

    name: "exge211_qt"
    Depends {
        name: "cpp"
    }
    Depends{
            name: "Qt"
            submodules: ["core","gui","opengl"]
    }
    Depends{
        name: "exge211_core"
    }

    type: "dynamiclibrary"

    cpp.cxxLanguageVersion: "c++14"
    cpp.defines: [
            "EXGE211_QT_EXPORTING",
            "PUBLIC_HEADERS_PATH_EXGE211_QT="+path+"/"+publicHeaders.prefix
    ]

    Group {
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: qbs.targetOS.contains("windows")?"bin":"lib"
    }
	
    Export {
        Depends {
            name: "cpp"
        }
        cpp.includePaths: [path+"/"+publicHeaders.baseDir]
        cpp.cxxLanguageVersion:  "c++14";
    }
    Group{
        id: publicHeaders
        name: "c++ public headers"
        property string baseDir: "public_headers"
        property string includePath: project.includeDirectoryName+"/qt/"
        prefix: baseDir+"/"+ publicHeaders.includePath
        files:[
            "defines.h",
            "gl_window_qt.h",
            "platform_factory_qt.h",
            "resource_loading.h",
        ]
        qbs.install:  project.installHeaders
        qbs.installDir: "include/"+ publicHeaders.includePath
    }
    Group{
        name: "c++ sources"
        prefix: "src/"
        files:[
            "context_qt.cpp",
            "context_qt.h",
            "gl_window_qt.cpp",
            "gui_event_loop_qt.cpp",
            "gui_event_loop_qt.h",
            "platform_factory_qt.cpp",
            "public_headers.h",
            "resource_loading.cpp",
        ]
    }
}
