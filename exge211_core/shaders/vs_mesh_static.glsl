#version 430 core
layout(location=0) in vec3 pos;
layout(location=1) in vec3 n;
layout(location=2) in vec3 t;
layout(location=3) in vec3 b;
layout(location=4) in vec2 uv;
layout(location=10) in uint material_in;
layout(location=11) in vec3 center_in;
layout(location=12) in vec4 rotation_in;
layout(location=13) in vec3 scale_in;

void init(){
}

vec3 transform(in vec3 v,in vec4 quat){
    return v+ 2 * cross( quat.xyz, (cross(quat.xyz,v)+quat.w*v));
}

vec3 get_position(){
    return transform(pos*scale_in,rotation_in)+center_in;
}

void get_tangent_space(out vec3 norm,out vec3 tangent,out vec3 binorm){
    norm=transform(n,rotation_in);
    tangent=transform(t,rotation_in);
    binorm=transform(b,rotation_in);
}

vec2 get_uv(){
    return uv;
}

uint get_mat(){
    return material_in;
}
