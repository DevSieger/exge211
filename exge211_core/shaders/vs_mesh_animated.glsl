#version 430 core
layout(location=0) in vec3 pos;
layout(location=1) in vec3 n;
layout(location=2) in vec3 t;
layout(location=3) in vec3 b;
layout(location=4) in vec2 uv;
layout(location=5) in vec4 weight;
layout(location=6) in ivec4 joint;
layout(location=10) in uint joint_offset;
layout(location=11) in uint material_in;
layout(location=12) in uint transform_offset;

vec4 c0;
vec4 ce;

struct Joint{
    vec4 r;
    vec4 d;
};
layout (std140) buffer Offsets
{
    Joint off_tr[];
};

layout (std140) buffer Joints
{
    Joint joints[];
};

vec4 quat_mult(vec4 q1,vec4 q2){
    return vec4(q1.w*q2.xyz+q2.w*q1.xyz+cross(q1.xyz,q2.xyz),q1.w*q2.w-dot(q1.xyz,q2.xyz));
}

void init(){
    c0 = vec4(0,0,0,0);
    ce = vec4(0,0,0,0);
    vec4 rot = joints[joint_offset-1].r;
    vec4 jr[4];
    vec4 je[4];
    for(int i=0;i<4;++i){
	Joint j=joints[joint[i]+joint_offset];
	Joint o=off_tr[joint[i]+transform_offset];
	jr[i]=quat_mult(j.r,o.r);
	je[i]=quat_mult(j.d,o.r)+quat_mult(j.r,o.d);
    }
    for(int i=1;i<4;++i){
	if(dot(jr[0],jr[i])<0){
        jr[i]   = -jr[i];
        je[i]   = -je[i];
	}
    }
    for(int i=0;i<4;++i){
	c0+=jr[i]*weight[i];
	ce+=je[i]*weight[i];
    }
    float inv_len=inversesqrt(dot(c0,c0));
    c0*=inv_len;
    ce*=inv_len;

    /*
     * домножаем еще и на поворот объекта сразу, т.к. нужно и для касательного пространтсва и для позиции. сдвиг позже применяется
     */
    c0=quat_mult(rot,c0);
    ce=quat_mult(rot,ce);
}

vec3 transform(in vec3 v,in vec4 quat){
    return v+ 2 * cross( quat.xyz, (cross(quat.xyz,v)+quat.w*v));
}

vec3 transform(in vec3 v,in vec4 quat,in vec4 dual){
    return 2*(cross(quat.xyz, cross(quat.xyz,v) + v * quat.w + dual.xyz) + dual.xyz * quat.w - quat.xyz * dual.w) + v;
}

vec3 get_position(){
    vec3 tpos=transform(pos,c0,ce);
    const vec4 pos_sc=joints[joint_offset-1].d;
    tpos*=pos_sc.w; //масштабирование
    tpos+=pos_sc.xyz;	//сдвиг
    return tpos;
}

void get_tangent_space(out vec3 norm,out vec3 tangent,out vec3 binorm){
    norm=transform(n,c0);
    tangent=transform(t,c0);
    binorm=transform(b,c0);
}

vec2 get_uv(){
    return uv;
}

uint get_mat(){
    return material_in;
}
