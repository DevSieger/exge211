#version 430 core
layout(location=0) in vec3 pos;
layout(location=1) in vec3 cpos;
layout(location=2) in vec3 intensity_in;
layout(location=3) in vec3 attenuation_in;
layout(location=4) in float range;
layout(location=5) in float radius_in;
layout(location=6) in float cut_off_in;
flat out vec3 center;
flat out vec3 intensity;
flat out vec3 attenuation_mult;
flat out float radius;
flat out float cut_off;
flat out float range_vs;

out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};

uniform float near_plane;
uniform float radius_modifier;

layout (std140) uniform Transform
{
    mat4x4 cam;
    mat4x4 proj;
};

void main(void)
{
    gl_Position=cam*vec4(radius_modifier*range*pos+cpos,1);
    gl_Position.z=min(gl_Position.z,-near_plane);
    gl_Position=proj*gl_Position;
    center=(cam*vec4(cpos,1)).xyz;
    intensity=intensity_in;
    radius=radius_in;
    attenuation_mult=attenuation_in;
    cut_off=cut_off_in;
    range_vs=range;
}
