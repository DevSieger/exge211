#version 430

#ifndef WORK_GROUP_SIZE
#define WORK_GROUP_SIZE  1024
#endif

layout (local_size_x = WORK_GROUP_SIZE, local_size_y = 1) in;

#ifndef GAUSS_SIGMA
#define GAUSS_SIGMA 3
#endif

#ifndef FILTER_RADIUS
// hint: the three-sigma rule.
#define FILTER_RADIUS 10
#endif

#ifndef HORIZONTAL_PASS
#define HORIZONTAL_PASS 1
#endif


// 
// Aligned to 32 (warp / half wavefront).
#define ALIGNED_RADIUS    ((FILTER_RADIUS + 31)/ 32 * 32 )


#ifndef Value
#define Value vec3
#endif

shared Value cache[WORK_GROUP_SIZE + ALIGNED_RADIUS * 2];

float weight(int offset){
    float x2    = float(offset);
    x2         *= x2;
    return exp( - x2 / ((GAUSS_SIGMA*GAUSS_SIGMA)*2)) / (GAUSS_SIGMA * 2.5066282746310005);
}


uniform sampler2D tx_input_a;
// an output of the vertical pass is a transposed image to improve the performance.
layout(rgba16f) restrict writeonly uniform image2D img_output_a;

#if HORIZONTAL_PASS


Value read(int x){
    return texture(tx_input_a, (vec2(x,gl_WorkGroupID.y)+vec2(0.5))/imageSize(img_output_a).xy).rgb;
}

void write(Value value, uint x){
    imageStore( img_output_a, ivec2(x, gl_WorkGroupID.y), vec4(value,0));
}

uint outputSize(){
    return imageSize(img_output_a).x;
}

#else

Value read(int x){
    return texture(tx_input_a, (vec2(gl_WorkGroupID.y,x)+vec2(0.5))/imageSize(img_output_a).yx).rgb;
}

void write(Value value, uint x){
    imageStore( img_output_a, ivec2(x, gl_WorkGroupID.y), vec4(value,0));
}

uint outputSize(){
    return imageSize(img_output_a).x;
}

#endif

void main(){
    uint image_size = outputSize();
    const uint index    = gl_LocalInvocationIndex;

    {
        cache[index]   = read(int(index) - ALIGNED_RADIUS);
        
        if( index < 2*ALIGNED_RADIUS ){
            cache[WORK_GROUP_SIZE+index]   = read(int(index) + WORK_GROUP_SIZE - ALIGNED_RADIUS);
        }
        
        memoryBarrierShared();
        barrier();
        
        int cache_ix        = ALIGNED_RADIUS + int(index);
        Value value         = cache[cache_ix]*weight(0);
        for(int offset = 1; offset <= FILTER_RADIUS; ++offset){
            value      += (cache[ cache_ix - offset] + cache[ cache_ix + offset]) * weight(offset);
        }
        write(value, index);
    }

    for( uint x_offset     = WORK_GROUP_SIZE ; x_offset < image_size; x_offset+= WORK_GROUP_SIZE){

        memoryBarrierShared();
        barrier();

        // the first 'ALIGNED_RADIUS' values in cache remained from previous step.
        // the next 'ALIGNED_RADIUS' values have been loaded already.
        if( index < ALIGNED_RADIUS*2 )
            cache[index] = cache[ WORK_GROUP_SIZE + index ];

        memoryBarrierShared();
        barrier();

        cache[ ALIGNED_RADIUS*2 + index ] = read( int(x_offset + index + ALIGNED_RADIUS) );

        memoryBarrierShared();
        barrier();

        int cache_ix        = ALIGNED_RADIUS + int(index);
        Value value               = weight(0) * cache[cache_ix];
        for(int offset = 1; offset <= FILTER_RADIUS; ++offset){
            value      += (cache[ cache_ix - offset] + cache[ cache_ix + offset]) * weight(offset);
        }

        write(value , x_offset + index );
    }
    
}
