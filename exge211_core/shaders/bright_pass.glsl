#version 430
uniform sampler2D tx_total_log_lum;

#ifndef LUMINANCE_EPSILON
#define LUMINANCE_EPSILON 0.000000000001
#endif

layout (std140) uniform Scene_cfg
{
    vec3 ambient_intencity;
    float sky_light_scale;

    vec4 rt_metric;

    vec3 bloom_weights;
    float bp_threshold_r;

    float adaptation_rate;
    float adaptation_min;
    float adaptation_max;
    float dt;

    float burn_out_ratio;
    float tm_key;
};

vec3 bloom_bright_pass(vec3 illumination){
    const vec3 luma_w   = vec3(0.2126,0.7152,0.0722);
    float average       = texelFetch(tx_total_log_lum,ivec2(0,0),0).x;
    float lum           = dot(luma_w,illumination) + LUMINANCE_EPSILON;
    return illumination * (max(0, lum - average * bp_threshold_r ) / lum);
}
