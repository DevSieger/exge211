#version 430 core
layout(location=0) in vec3 pos;
layout(location=1) in vec3 n;
layout(location=2) in vec3 t;
layout(location=3) in vec2 uv;
layout(location=4) in vec4 incet;
layout(location=5) in int mat_id;


void init(){

}

vec3 get_position(){
    return pos*incet.w+incet.xyz;
}

void get_tangent_space(out vec3 norm,out vec3 tangent,out vec3 binorm){
    norm=n;
    tangent=t;
    binorm=cross(n,t);
}

vec2 get_uv(){
    return uv;
}

uint get_mat(){
    return mat_id;
}
