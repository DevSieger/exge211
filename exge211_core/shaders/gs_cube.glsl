#version 430 core
layout(triangles) in;
layout(triangle_strip,max_vertices=18) out;

in vec3 position[];
layout (std140) uniform PointLightData
{
    mat4x4 transform[6];
    vec2 proj_z;
    vec2 proj_w;
};

out gl_PerVertex
{
    vec4 gl_Position;
    float gl_PointSize;
    float gl_ClipDistance[];
};

void main(void)
{
    for(int lr=0;lr<6;++lr){
	for(int i=0;i<3;++i){
	    gl_Position=transform[lr]*vec4(position[i],1);
	    gl_Layer=lr;
	    EmitVertex();
	};
	EndPrimitive();
    };
}
