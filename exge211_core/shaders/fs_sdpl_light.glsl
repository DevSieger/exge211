#version 430
layout(early_fragment_tests) in;
flat in vec3 center;
flat in vec3 intensity;
flat in vec3 attenuation_mult;
flat in float radius;
flat in float range_vs;
flat in float cut_off;

vec3 get_position();
float get_depth();
vec3 get_normal();
vec2 screen_size();


vec3 dir;
float dist;//from surface

void init_light(){
    dir     = get_position() - center;
    dist    = length(dir);
    if( range_vs < dist ){
        discard;
    }

    if( dist > 0)
        dir    /= dist;

    dist   -= radius;

}

vec3 get_light_dir(){
    return dir;
}

vec3 get_intensity()
{
    const vec3 luma_w   = vec3(0.2126,0.7152,0.0722);
    float luminance     = dot(intensity, luma_w);
    float attenuation   = 1 / ( attenuation_mult[0]+attenuation_mult[1]*dist + attenuation_mult[2]*dist*dist );

    attenuation         = clamp(attenuation, 0, 1);

    if(luminance > cut_off){
        attenuation     = (luminance * attenuation - cut_off) / (luminance - cut_off);
    }else{
        attenuation     = 0;
    }

    attenuation         = clamp(attenuation, 0, 1);

    return attenuation * intensity;
}
