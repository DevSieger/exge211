#version 430
layout(early_fragment_tests) in;

uniform sampler2DArrayShadow shadow_map;
uniform sampler3D rotation_tx;

#ifndef EPSILON
#define EPSILON 0.000000000001
#endif

struct CascadeData{
    mat4x4 proj;
    float near;
    float overlap;
    vec2 xz_scale;
};
layout (std140) uniform DlFullData{
    vec3 direction;
    uint cascade_count;
    vec4 intensity;
    CascadeData cascade[12];
};

layout (std140) uniform Transform
{
    mat4x4 cam;
    mat4x4 proj;
};


vec3 get_position();
float get_depth();
vec3 get_normal();
vec2 screen_size();

float get_shadow(vec4 light_sp_c,vec3 pos,float depth,float radius,uint index){
    const int tap_count=8;
    vec2 poissonDisk[tap_count] = {
	vec2(-0.2989156f, -0.9237617f),
	vec2(0.1207097f, 0.05943017f),
	vec2(-0.6414264f, 0.2613896f),
	vec2(0.4419505f, -0.6159539f),
	vec2(0.4633472f, 0.6824971f),
	vec2(-0.8443285f, -0.4166567f),
	vec2(-0.2492237f, 0.9514638f),
	vec2(0.9595674f, 0.1863758f)
    };
    float weight[tap_count]={
	0.121,0.129,
	0.1245,0.128,
	0.127,0.125,
	0.122,0.1235
    };

    //минимальное количество пикселей на экране, которое захватывает выборка
    const float pixel_count=2;
    vec2 tsr=screen_size()*vec2(proj[0][0],proj[1][1]);

    float min_radius=pixel_count*2.0*depth/min(tsr.x,tsr.y);

    //r - радиус выборки в координатах текстуры
    float r=max(radius,min_radius)*cascade[index].xz_scale.x;

	//очень мелкий шум, зависит от координат в view space, меняется при движении камеры, и это на мой взгляд лучше, чем неподвижный
    vec2 rotation=r*normalize(texture(rotation_tx,pos*257).xy + vec2(EPSILON));
    mat2 rot=mat2(rotation,rotation.yx);
    rot[1].x = -rot[1].x;
    float sv=0;
    for(int i=0;i<tap_count;++i){
	sv+=weight[i]*texture(shadow_map,light_sp_c.xywz+vec4(rot*poissonDisk[i],0,0.00009));
    };
    return sv;
}

void init_light(){
}

vec3 get_light_dir(){
    return direction;
}

vec3 get_intensity()
{
    const float shadow_radius=0.0;
    const float depth=get_depth();
    const vec3 dir=direction;
    const vec3 normal=get_normal();
    const vec3 position=get_position();
    uint index=0;
    for(;index<cascade_count && depth>cascade[index].near;index++){
    }
    index--;
    index=clamp(index,0,cascade_count-1);
    vec4 ls=cascade[index].proj*vec4(position,1);
    ls=(ls+vec4(1))*(0.5/ls.w);
    ls.w=float(index);
    float shadow=get_shadow(ls,position,depth,shadow_radius,index);

    if(index+1<cascade_count){
	float t=(cascade[index+1].near-depth);
	if(t<=cascade[index].overlap){
	    t/=cascade[index].overlap;
	    ls=cascade[index+1].proj*vec4(position,1);
	    ls=(ls+vec4(1))*(0.5/ls.w);
	    ls.w=index+1;
	    shadow=mix(get_shadow(ls,position,depth,shadow_radius,index+1),shadow,t);
	}
    }

    if(shadow<=0.00001){
	discard;
    }
    return shadow*intensity.xyz;
}
