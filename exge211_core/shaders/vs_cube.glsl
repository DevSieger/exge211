#version 430 core
out vec3 position;
vec3 get_position();
void init();
void main(void)
{
    init();
    position=get_position();
}
