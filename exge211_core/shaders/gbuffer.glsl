#version 430 core

layout(binding=20) uniform sampler2D rt_albedo;
layout(binding=21) uniform sampler2D rt_specular;
layout(binding=22) uniform sampler2D rt_normal;
layout(binding=23) uniform sampler2D rt_light;
layout(binding=5) uniform sampler2D tx_depth;

layout (std140) uniform Transform
{
    mat4x4 cam;
    mat4x4 proj;
};


vec3 load_albedo(ivec2 coord);
vec4 load_specular_shininess(ivec2 coord);
vec3 load_intensity(ivec2 coord);
vec3 load_position(ivec2 coord);
vec3 load_normal(ivec2 coord);
vec2 screen_size();
vec4 pack_specular_shininess(vec3 specular,float shininess);
float load_view_depth(ivec2 coord);

vec3 load_albedo(ivec2 coord){
    return texelFetch(rt_albedo,coord,0).rgb;
}

vec4 load_specular_shininess(ivec2 coord){
    vec4 ss=texelFetch(rt_specular,coord,0).rgba;
    ss.w*=32;
    return ss;
}

vec3 load_intensity(ivec2 coord){
    return texelFetch(rt_light,coord,0).rgb;
}
vec2 screen_size(){
    return vec2(textureSize(rt_albedo,0));
}
vec3 load_position(ivec2 coord){
    float depth=texelFetch(tx_depth,coord,0).x*2-1;
    vec3 pd;
    const float e=-1;
    vec2 nc=(vec2(coord)/screen_size()-vec2(0.5))*2;
    pd.z=proj[3][2]/(e*depth-proj[2][2]);
    pd.x=e*nc.x*pd.z/proj[0][0];
    pd.y=e*nc.y*pd.z/proj[1][1];
    return pd;
}
float load_view_depth(ivec2 coord){
    float depth=texelFetch(tx_depth,coord,0).x*2-1;
    const float e=-1;
    return -proj[3][2]/(e*depth-proj[2][2]);
}


vec3 load_normal(ivec2 coord){
    vec3 n;
    n.xy=texelFetch(rt_normal,coord,0).xy;
    n.xy=n.xy*4-vec2(2);
    n.z=dot(n.xy,n.xy);
    float g=sqrt(1-n.z*0.25);
    n.xy*=g;
    n.z=1-n.z*0.5;
    return n;
}

vec2 decode_normal(vec3 n){
    float p=sqrt(n.z*8+8);
    return n.xy/p+0.5;
}


vec4 pack_specular_shininess(vec3 specular,float shininess){
    return vec4(specular,shininess/32.0);   // i hope compiler can tranform '/' to '*' for constant
}
