#version 430 core

layout (std140) uniform Transform
{
    mat4x4 cam;
    mat4x4 proj;
};

out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};

out mat3x3 tangent_space;

smooth out vec2 uv_coord;
flat out uint mat_vs;

void init();
vec3 get_position();
void get_tangent_space(out vec3 norm_a,out vec3 tangent_a,out vec3 binorm_a);
vec2 get_uv();
uint get_mat();

void main(void)
{
    init();

    gl_Position=proj*cam*vec4(get_position(),1);

    get_tangent_space(tangent_space[2],tangent_space[0],tangent_space[1]);
    tangent_space=mat3x3(cam)*tangent_space;
    uv_coord=get_uv();
    mat_vs=get_mat();
}
