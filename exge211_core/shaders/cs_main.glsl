#version 430
layout (local_size_x = 32, local_size_y = 32) in;

#ifndef NORMALS_FACTOR
#define NORMALS_FACTOR 0.35
#endif

#ifndef DEPTH_FACTOR
#define DEPTH_FACTOR 0.65
#endif

#define PI 3.1415926535897932384626433832795

#define  INDEX  gl_LocalInvocationIndex

#ifndef BORDER_SIZE
#define BORDER_SIZE 1
#endif

#ifndef EPSILON
#define EPSILON 0.0000001
#endif

uniform sampler2D small_blur_tx;
uniform sampler2D medium_blur_tx;
uniform sampler2D large_blur_tx;
uniform sampler2D tx_total_log_lum;

layout (std140) uniform Scene_cfg
{
    vec3 ambient_intencity;
    float sky_light_scale;

    vec4 rt_metric;

    vec3 bloom_weights;
    float bp_threshold_r;

    float adaptation_rate;
    float adaptation_min;
    float adaptation_max;
    float dt;

    float burn_out_ratio;
    float tm_key;
};

layout(rgb10_a2) restrict uniform image2D rt_color;

shared vec3 shared_v[gl_WorkGroupSize.x * gl_WorkGroupSize.y];

/* gbuffer */
vec3 load_intensity(ivec2 coord);
float load_view_depth(ivec2 coord);
vec3 load_normal(ivec2 coord);
vec2 screen_size();


uint offset_to_index(ivec2 offset){
    ivec2 new_loc   = ivec2(gl_LocalInvocationID.xy) + offset;
    new_loc         = clamp( new_loc, ivec2(0), ivec2(gl_WorkGroupSize.xy) - ivec2(1));
    return new_loc.y * gl_WorkGroupSize.x + new_loc.x;
}

float get_outline(float depth, vec3 normal){
    const float weights[3]  = { 1, 2, 1 };

    const ivec2 mask[4]     = {
        ivec2(-1,0),
        ivec2(-1,-1),
        ivec2(0,-1),
        ivec2(1,-1)
    };

    memoryBarrierShared();
    barrier();

    shared_v[ INDEX ].xy    = normal.xy;

    memoryBarrierShared();
    barrier();

    float outline_normal    = 0;
    float outline_depth     = 0;

    {
        vec2 g_x    = vec2(0,0);
        vec2 g_y    = vec2(0,0);

        vec2 t;

        // a manual unroll for the sobel operator
        //---- y-offset == -1
        t       = shared_v[offset_to_index(ivec2(-1,-1))].xy;
        g_x    -= t * weights[0];
        g_y    -= t * weights[0];

        t       = shared_v[offset_to_index(ivec2(0,-1))].xy;
        g_y    -= t * weights[1];

        t       = shared_v[offset_to_index(ivec2(1,-1))].xy;
        g_x    += t * weights[0];
        g_y    -= t * weights[2];
        //---- y-offset == 0
        t       = shared_v[offset_to_index(ivec2(-1,0))].xy;
        g_x    -= t * weights[1];

        t       = shared_v[offset_to_index(ivec2(1,0))].xy;
        g_x    += t * weights[1];
        //---- y-offset == 1
        t       = shared_v[offset_to_index(ivec2(-1,1))].xy;
        g_x    -= t * weights[2];
        g_y    += t * weights[0];

        t       = shared_v[offset_to_index(ivec2(0,1))].xy;
        g_y    += t * weights[1];

        t       = shared_v[offset_to_index(ivec2(1,1))].xy;
        g_x    += t * weights[2];
        g_y    += t * weights[2];
        //----

        vec2 grad           = vec2( length(g_x),length(g_y) );
        outline_normal      = length(grad);

        float angle         = ( outline_normal!=0  ) ? atan(grad.y,grad.x) : 0;

        int mask_ix         = int(floor(
            mod(
                (angle+(9.0/8.0)*PI)*4/PI,  4.0
            )));

        shared_v[ INDEX ].z = outline_normal;

        memoryBarrierShared();
        barrier();

        float adj;

        adj = shared_v[offset_to_index(mask[mask_ix])].z;
        if( adj > outline_normal ) outline_normal = 0;

        adj = shared_v[offset_to_index(-mask[mask_ix])].z;
        if( adj > outline_normal ) outline_normal = 0;

        outline_normal  = smoothstep( 4, 5, outline_normal);
    }

    shared_v[ INDEX ].x=depth;
    memoryBarrierShared();
    barrier();

    {
        vec2 m  = vec2(0,0);
        float t;
        // a manual unroll for the sobel operator
        //---- y-offset == -1
        t       = shared_v[offset_to_index(ivec2(-1,-1))].x;
        m.x    -= t * weights[0];
        m.y    -= t * weights[0];

        t       = shared_v[offset_to_index(ivec2(0,-1))].x;
        m.y    -= t * weights[1];

        t       = shared_v[offset_to_index(ivec2(1,-1))].x;
        m.x    += t * weights[0];
        m.y    -= t * weights[2];
        //---- y-offset == 0
        t       = shared_v[offset_to_index(ivec2(-1,0))].x;
        m.x    -= t * weights[1];

        t       = shared_v[offset_to_index(ivec2(1,0))].x;
        m.x    += t * weights[1];
        //---- y-offset == 1
        t       = shared_v[offset_to_index(ivec2(-1,1))].x;
        m.x    -= t * weights[2];
        m.y    += t * weights[0];

        t       = shared_v[offset_to_index(ivec2(0,1))].x;
        m.y    += t * weights[1];

        t       = shared_v[offset_to_index(ivec2(1,1))].x;
        m.x    += t * weights[2];
        m.y    += t * weights[2];
        //----

        outline_depth   = length(m) / shared_v[offset_to_index(ivec2(0,0))].x;

        float angle     = ( outline_depth!=0 ) ? atan(m.y,m.x) : 0;
        int mask_ix     = int(floor(
            mod(
                (angle+(9.0/8.0)*PI)*4/PI,  4.0
            )
            ));

        shared_v[ INDEX ].z = outline_depth;

        memoryBarrierShared();
        barrier();

        float adj;

        adj = shared_v[offset_to_index(mask[mask_ix])].z;
        if(adj>outline_depth) outline_depth = 0;

        adj = shared_v[offset_to_index(-mask[mask_ix])].z;
        if(adj>outline_depth) outline_depth = 0;

        outline_depth   = smoothstep( 0.0f, 0.5f, outline_depth);
    }

    return 1 - clamp( outline_normal * NORMALS_FACTOR + outline_depth * DEPTH_FACTOR, 0, 1);
}

vec3 RGB_to_xyY(vec3 rgb){
    const mat3x3 to_XYZ = mat3x3 (
        0.4124,    0.2126, 0.0193,
        0.3576,    0.7152, 0.1192,
        0.1805,    0.0722, 0.9502
    );
    vec3 XYZ    = to_XYZ * rgb;
    return  vec3( XYZ.xy/ ( dot(XYZ,vec3(1)) + EPSILON ),XYZ.y);
}

vec3 xyY_to_RGB(vec3 xyY){
    vec3 XYZ;
    XYZ.y       = xyY.z;
    XYZ.xz      = (xyY.z / (xyY.y + EPSILON) ) * vec2 (xyY.x, 1-xyY.x-xyY.y);

    const mat3x3 to_RGB = mat3x3 (
        3.2406,     -0.9689,    0.0557,
        -1.5372,    1.8758,     -0.2040,
        -0.4986,    0.0415,     1.0570
    );

    return  to_RGB * XYZ;
}

void main() {
    // the texel position.
    const ivec2 sc_pos  = ivec2( (gl_WorkGroupSize.xy - uvec2(BORDER_SIZE*2) ) * gl_WorkGroupID.xy ) - ivec2(BORDER_SIZE) + ivec2(gl_LocalInvocationID.xy);
    float depth         = load_view_depth(sc_pos);
    float outline       = get_outline(depth,load_normal(sc_pos));

    vec3 intensity      = load_intensity(sc_pos);

    vec3 small_blur     = texture(small_blur_tx,vec2(sc_pos)/screen_size()).rgb;
    vec3 medium_blur    = texture(medium_blur_tx,vec2(sc_pos.yx)/screen_size().yx).rgb;
    vec3 large_blur     = texture(large_blur_tx,vec2(sc_pos.yx)/screen_size().yx).rgb;

    vec3 bloom          = vec3(0);

    vec3 blur_w         = bloom_weights;
    bloom              += blur_w.x  * small_blur;
    bloom              += blur_w.y * medium_blur;
    bloom              += blur_w.z  * large_blur;


    vec3 illu_xyY   =   RGB_to_xyY( intensity*outline + bloom);

    // the "Reinhard at al." global tone mapping operator.
    float average   =   texelFetch(tx_total_log_lum,ivec2(0,0),0).x;

    float slumi     =   tm_key * illu_xyY.z / average;
    float white     =   burn_out_ratio * average;
    illu_xyY.z      =   slumi / (1 + slumi) * ( 1 + slumi / (white * white) );
    illu_xyY.z      =   clamp(illu_xyY.z,0,1);

    vec3 color      = xyY_to_RGB(illu_xyY);

    if(
        gl_LocalInvocationID.x    >=   BORDER_SIZE &&
        gl_LocalInvocationID.y    >=   BORDER_SIZE &&
        gl_LocalInvocationID.x    <   gl_WorkGroupSize.x-BORDER_SIZE &&
        gl_LocalInvocationID.y    <   gl_WorkGroupSize.y-BORDER_SIZE
    ){
        imageStore(rt_color,sc_pos,vec4(color,1));
    }

}
