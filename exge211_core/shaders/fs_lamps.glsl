#version 430 core
out vec4 color;
flat in vec3 intensity;

void main(void)
{

    color=vec4(intensity,1);
}
