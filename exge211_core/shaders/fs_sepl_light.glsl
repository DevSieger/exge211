#version 430

#ifndef EPSILON
#define EPSILON 0.000000000001
#endif

layout(early_fragment_tests) in;
flat in vec3 center;
flat in vec3 intensity;
flat in vec3 attenuation_mult;
flat in float radius;
flat in float range_vs;
flat in float cut_off;

vec3 get_position();
float get_depth();
vec3 get_normal();
vec2 screen_size();

uniform samplerCubeShadow cube_shadow;
uniform sampler3D rotation_tx;

layout (std140) uniform Transform
{
    mat4x4 cam;
    mat4x4 proj;
};
layout (std140) uniform PointLightData
{
    mat4x4 transform[6];
    vec2 proj_z;
    vec2 proj_w;
};
const ivec3 v_i[6]={
    ivec3(2,1,0),
    ivec3(2,1,0),

    ivec3(0,2,1),
    ivec3(0,2,1),

    ivec3(0,1,2),
    ivec3(0,1,2),
};

float shadow(vec3 direction, vec3 pos){
    const int tap_count=8;
    vec2 poisson_disk[tap_count] = {
	vec2(-0.2989156f, -0.9237617f),
	vec2(0.1207097f, 0.05943017f),
	vec2(-0.6414264f, 0.2613896f),
	vec2(0.4419505f, -0.6159539f),
	vec2(0.4633472f, 0.6824971f),
	vec2(-0.8443285f, -0.4166567f),
	vec2(-0.2492237f, 0.9514638f),
	vec2(0.9595674f, 0.1863758f)
    };
    float weight[tap_count]={
	0.121,0.129,
	0.1245,0.128,
	0.127,0.125,
	0.122,0.1235
    };
    const vec3 ad=abs(direction);
    int index=0;
    if(ad.x<ad.y){
	index=1;
    }
    if(ad.y<ad.z && ad.x<ad.z){
	index=2;
    }

    float unprojected_z=-ad[index];
    if(direction[index]<0){
	index=index*2+1;
    }else{
	index=index*2;
    }

    vec2 light_space_zw=vec2(
	unprojected_z*proj_z.x+proj_w.x,
	unprojected_z*proj_z.y+proj_w.y
    );
    const float ls_depth=0.5*(light_space_zw.x/light_space_zw.y+1);

    //минимальное количество пикселей на экране, которое захватывает выборка
    const float pixel_count=2;
    vec2 tsr=textureSize(cube_shadow,0);

    float min_radius=pixel_count*2*abs(direction[v_i[index].z])/min(tsr.x,tsr.y);
    float r=min_radius;

    vec2 rotation=r*normalize(texture(rotation_tx,pos*257).xy + vec2(EPSILON));
    mat2 rot=mat2(rotation,rotation.yx);
    rot[1].x = -rot[1].x;

    float sv=0;
    for(int i=0;i<tap_count;++i){
	vec2 poisson_sample=rot*poisson_disk[i];
	vec3 offset=vec3(0,0,0);
	offset[v_i[index].x]=poisson_sample.x;
	offset[v_i[index].y]=poisson_sample.y;
    sv+=weight[i]*texture(cube_shadow,vec4(direction+offset,ls_depth + 0.0009));
    };
    return sv;
}
float fast_shadow(vec3 direction){
    const vec3 ad=abs(direction);
    int index=0;
    if(ad.x<ad.y){
	index=1;
    };
    if(ad.y<ad.z && ad.x<ad.z){
	index=2;
    }

    float unprojected_z=-ad[index];
    vec2 light_space_zw=vec2(
	unprojected_z*proj_z.x+proj_w.x,
	unprojected_z*proj_z.y+proj_w.y
    );
    const float ls_depth=0.5*(light_space_zw.x/light_space_zw.y+1);
    return texture(cube_shadow,vec4(direction,ls_depth+0.00001));
}



vec3 dir;
float dist;

void init_light(){
    dir     = get_position()-center;
    dist    = length(dir);

    if(range_vs<dist){
        discard;
    }

    if( dist > 0)
        dir    /= dist;

    dist   -= radius;
}

vec3 get_light_dir(){
    return dir;
}

vec3 get_intensity()
{
    float shadow        = shadow( dir*dist ,get_position());

    const vec3 luma_w   = vec3(0.2126,0.7152,0.0722);
    float luminance     = dot(intensity, luma_w);
    float attenuation   = 1 / ( attenuation_mult[0]+attenuation_mult[1]*dist + attenuation_mult[2]*dist*dist );

    attenuation         = clamp(attenuation, 0, 1);

    if(luminance > cut_off){
        attenuation     = (luminance * attenuation - cut_off) / (luminance - cut_off);
    }else{
        attenuation     = 0;
    }

    attenuation         = clamp(attenuation, 0, 1);

    return shadow * attenuation * intensity;
}
