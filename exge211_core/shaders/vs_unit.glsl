#version 430
in vec2 position;
out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};
out vec2 coord;
void main(void)
{
    gl_Position=vec4(position,-1,1);
    coord = gl_Position.xy*0.5+0.5;
}
