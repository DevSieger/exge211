#version 430
layout (local_size_x = 64, local_size_y = 16) in;

#ifndef SAT_READ_ILLUMINATION
#define SAT_READ_ILLUMINATION   1
#endif

#ifndef SAT_HORIZONTAL
#define SAT_HORIZONTAL          1
#endif

#ifndef SAT_TOP_LEVEL
#define SAT_TOP_LEVEL           0
#endif

#if SAT_HORIZONTAL
ivec2 tx_coord(int offset){
    return ivec2(gl_GlobalInvocationID.y*4+offset,gl_GlobalInvocationID.x);
}
#else
ivec2 tx_coord(int offset){
    return ivec2(gl_GlobalInvocationID.x, gl_GlobalInvocationID.y*4+offset);
}
#endif

#if SAT_READ_ILLUMINATION

uniform sampler2D tx_input;

layout(r32f,binding=6)      writeonly restrict  uniform image2D img_values;
layout(binding=16)   uniform sampler2D tx_total_log_lum;

float load_data(int offset){
    const vec3 luma_w   = vec3 (0.2126,0.7152,0.0722);
    vec3 value          = texelFetch(tx_input,tx_coord(offset),0).rgb;
    return dot( luma_w, value);
}

#else
#if !SAT_TOP_LEVEL
layout(r32f,binding=6) restrict  uniform image2D img_values;
#else
layout(r32f,binding=7) restrict  uniform image2D img_values;
#endif

float load_data(int offset){
    return imageLoad(img_values, tx_coord(offset)).x;
}

#endif

#if !SAT_TOP_LEVEL
layout(r32f,binding=7)      writeonly restrict  uniform image2D img_reduced;

void store_reduced(float value){
    // vertical orientation is more optimal
    imageStore(img_reduced, ivec2(gl_GlobalInvocationID.x,gl_WorkGroupID.y+1),vec4(value,0,0,0));
}
#endif

void store_data(float value,int offset){
    imageStore(img_values, tx_coord(offset) ,vec4(value,0,0,0));
}

shared float storage[1024];

void main(){

    /*
    * Our "the front" / "the warp" is a vertical line, not horizontal one, like in the usual scan !!
    * This is done to make last steps of algorithm effective + better use of mem banks.
    */

    const uint max_ix   = gl_WorkGroupSize.x * gl_WorkGroupSize.y;
    const uint ix       = gl_LocalInvocationIndex;

    // TODO: what is better: the coalesced access to an image or avoidance of mem. bank conflicts?
    // TODO: multi-read.
    vec4 val            = vec4( load_data(0),load_data(1),load_data(2),load_data(3));

#if SAT_READ_ILLUMINATION
    float lum_g_mean    = texelFetch(tx_total_log_lum,ivec2(0,0),0).x;
    val                /= lum_g_mean;
    val                -= vec4(1);
#endif

    val.x               = dot(val,vec4(1,1,1,1));
    storage[ix]         = val.x;

    for( uint offset = 1; offset<16; offset*=2 ){

        uint t_ix           = ix + gl_WorkGroupSize.x * offset;

        memoryBarrierShared();
        barrier();

        if( t_ix < max_ix)
            storage[t_ix]  += val.x;

        memoryBarrierShared();
        barrier();

        val.x               = storage[ix];
    }

    val.x   -= val.y + val.z + val.w;
    store_data(val.x,0);
    val.x   += val.y;
    store_data(val.x,1);
    val.x   += val.z;
    store_data(val.x,2);
    val.x   += val.w;
    store_data(val.x,3);

#if !SAT_TOP_LEVEL
    if(gl_LocalInvocationID.y==gl_WorkGroupSize.y-1){
        store_reduced(val.x);
    }
#endif

}
