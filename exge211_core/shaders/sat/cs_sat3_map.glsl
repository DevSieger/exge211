#version 430
layout (local_size_x = 64, local_size_y = 16) in;

layout(r32f,binding=6)            restrict  uniform image2D img_values;
layout(r32f,binding=7)  readonly  restrict  uniform image2D img_reduced;

#ifndef SAT_HORIZONTAL
#define SAT_HORIZONTAL          1
#endif

#if SAT_HORIZONTAL

void main(){
    //TODO: cache img_reduced to shader memory
    ivec3 coord         = ivec3( gl_GlobalInvocationID.y*4, gl_WorkGroupID.x, gl_GlobalInvocationID.x );
    float ext_value     = imageLoad( img_reduced, coord.xy ).x;
    float img_value     = imageLoad( img_values,  coord.zx ).x;
    imageStore( img_values, coord.zx, vec4(img_value + ext_value,0,0,0) );

    coord.x++;
    ext_value           = imageLoad( img_reduced, coord.xy ).x;
    img_value           = imageLoad( img_values,  coord.zx ).x;
    imageStore( img_values, coord.zx, vec4(img_value + ext_value,0,0,0) );

    coord.x++;
    ext_value           = imageLoad( img_reduced, coord.xy ).x;
    img_value           = imageLoad( img_values,  coord.zx ).x;
    imageStore( img_values, coord.zx, vec4(img_value + ext_value,0,0,0) );

    coord.x++;
    ext_value           = imageLoad( img_reduced, coord.xy ).x;
    img_value           = imageLoad( img_values,  coord.zx ).x;
    imageStore( img_values, coord.zx, vec4(img_value + ext_value,0,0,0) );
}

#else

void main(){
    //TODO: cache img_reduced to shader memory
    ivec2 coord         = ivec2( gl_GlobalInvocationID.x, gl_GlobalInvocationID.y*4 );
    float ext_value     = imageLoad( img_reduced, ivec2(gl_GlobalInvocationID.x, gl_WorkGroupID.y)).x;
    float img_value     = imageLoad( img_values,  coord ).x;
    imageStore( img_values, coord, vec4(img_value + ext_value,0,0,0) );

    coord.y++;
    img_value           = imageLoad( img_values,  coord ).x;
    imageStore( img_values, coord, vec4(img_value + ext_value,0,0,0) );

    coord.y++;
    img_value           = imageLoad( img_values,  coord ).x;
    imageStore( img_values, coord, vec4(img_value + ext_value,0,0,0) );

    coord.y++;
    img_value           = imageLoad( img_values,  coord ).x;
    imageStore( img_values, coord, vec4(img_value + ext_value,0,0,0) );
}

#endif
