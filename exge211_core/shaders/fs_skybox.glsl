#version 430 core
out vec4 color;
in vec3 position_vs;
layout(binding=4) uniform samplerCube skybox;

layout (std140) uniform Scene_cfg
{
    vec3 ambient_intencity;
    float sky_light_scale;

    vec4 rt_metric;

    vec3 bloom_weights;
    float bp_threshold_r;

    float adaptation_rate;
    float adaptation_min;
    float adaptation_max;
    float dt;

    float burn_out_ratio;
    float tm_key;
};

void main(void)
{
    vec3 pos    = position_vs.xzy;
    pos.y       = -pos.y;
    color=texture(skybox,pos)*sky_light_scale;
}
