#version 430 core

uniform mat4x4 transform_matrix;    //from world/scene space to shadow map space

out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};

vec3 get_position();
void init();
void main(void)
{
    init();
    gl_Position=transform_matrix*vec4(get_position(),1);
}
