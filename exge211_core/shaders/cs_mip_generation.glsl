#version 430
layout (local_size_x = 64, local_size_y = 16) in;

#ifndef Value
#define Value vec3
#endif

#ifndef to_vec4
#define to_vec4(x) vec4(x,0)
#endif

#ifndef BRIGHT_PASS
#define BRIGHT_PASS 0
#endif

#ifndef LEVELS_TO_GENERATE
#define LEVELS_TO_GENERATE 3
#endif

#ifndef IMG_FORMAT
#define IMG_FORMAT rgba16f
#endif

uniform sampler2D img_level_0;

layout( IMG_FORMAT) uniform image2D img_level_1;

#if LEVELS_TO_GENERATE > 1
layout( IMG_FORMAT) uniform image2D img_level_2;
#endif

#if LEVELS_TO_GENERATE > 2
layout( IMG_FORMAT) uniform image2D img_level_3;
#endif

shared Value storage[64*16*2];

#if BRIGHT_PASS
vec3 bloom_bright_pass(vec3 illumination);
Value load(ivec2 coord){
    return bloom_bright_pass(texelFetch(img_level_0,coord,0).xyz);
}
#else
Value load(ivec2 coord){
    return texelFetch(img_level_0,coord,0).xyz;
}
#endif


void main(){
    const uint work_group_size  = gl_WorkGroupSize.x * gl_WorkGroupSize.y;
    const uint tile_size        = gl_WorkGroupSize.x;
    ivec2 tx_coord              = ivec2(gl_GlobalInvocationID.x, gl_WorkGroupID.y*tile_size + gl_LocalInvocationID.y*2);
    Value tmp                   = load(tx_coord);
    tmp                        += load(tx_coord + ivec2(0,1));
    storage[gl_LocalInvocationIndex] = tmp;
    
    tx_coord.y                 += 32;
    tmp                         = load(tx_coord);
    tmp                        += load(tx_coord + ivec2(0,1));
    storage[gl_LocalInvocationIndex + work_group_size] = tmp;

    memoryBarrierShared();
    barrier();
    
    {
        uint ix                 = gl_LocalInvocationIndex*2;
        tmp                     = storage[ix];
        tmp                    += storage[ix+1];
        uvec2 new_local         = uvec2( gl_LocalInvocationID.x % (tile_size/2), gl_LocalInvocationID.y*2 + gl_LocalInvocationID.x / (tile_size/2));
        ivec2 out_coord         = ivec2( (tile_size/2)*gl_WorkGroupID.xy + new_local );
        // texture sizes are rounded down when building mipmaps (63 x 63 -> 31 x 31), so we always have 4 values in one texel.
        tmp                    *= 0.25;
        imageStore(img_level_1,out_coord, to_vec4(tmp));
    }

#if LEVELS_TO_GENERATE > 1
    memoryBarrierShared();
    barrier();

    storage[gl_LocalInvocationIndex] = tmp;

    memoryBarrierShared();
    barrier();

    //TODO: think about mem. bank conflicts
    if(gl_LocalInvocationIndex < work_group_size / 4) {
        uvec2 new_local         = uvec2( gl_LocalInvocationID.x % (tile_size/4), gl_LocalInvocationID.y*4 + gl_LocalInvocationID.x / (tile_size/4));
        ivec2 out_coord         = ivec2( (tile_size/4)*gl_WorkGroupID.xy + new_local );
        uint ix                 = (gl_LocalInvocationIndex + (gl_LocalInvocationIndex / (tile_size/4))*(tile_size/4) ) * 2;
        tmp                     = storage[ix];
        tmp                    += storage[ix+1];
        tmp                    += storage[ix + tile_size/2];
        tmp                    += storage[ix + tile_size/2+1];

        tmp                    *= 0.25;
        imageStore(img_level_2,out_coord, to_vec4(tmp));
    }
#endif

#if LEVELS_TO_GENERATE > 2
    memoryBarrierShared();
    barrier();

    storage[gl_LocalInvocationIndex] = tmp;

    memoryBarrierShared();
    barrier();

    if(gl_LocalInvocationIndex < work_group_size / 16) {
        uvec2 new_local         = uvec2( gl_LocalInvocationID.x % (tile_size/8), gl_LocalInvocationID.y*8 + gl_LocalInvocationID.x / (tile_size/8));
        ivec2 out_coord         = ivec2( (tile_size/8)*gl_WorkGroupID.xy + new_local );
        uint ix                 = (gl_LocalInvocationIndex + (gl_LocalInvocationIndex / (tile_size/8))*(tile_size/8) ) * 2;
        tmp                     = storage[ix];
        tmp                    += storage[ix+1];
        tmp                    += storage[ix + tile_size/4];
        tmp                    += storage[ix + tile_size/4+1];

        tmp                    *= 0.25;
        imageStore(img_level_3,out_coord, to_vec4(tmp));
    }
#endif

}

