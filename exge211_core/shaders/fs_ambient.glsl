#version 430 core
layout(location = 0) out vec4 intensity_out;

layout (std140) uniform Scene_cfg
{
    vec3 ambient_intencity;
    float sky_light_scale;

    vec4 rt_metric;

    vec3 bloom_weights;
    float bp_threshold_r;

    float adaptation_rate;
    float adaptation_min;
    float adaptation_max;
    float dt;

    float burn_out_ratio;
    float tm_key;
};

/* gbuffer */
vec3 load_albedo(ivec2 coord);

void main(void)
{
    ivec2 coord     = ivec2(gl_FragCoord.xy);
    vec3 albedo     = load_albedo(coord);

    intensity_out   = vec4(albedo*ambient_intencity,1);
}
