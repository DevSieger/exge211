#version 430
layout(location=0) in vec3 position;
out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};
layout (std140) uniform Transform
{
    mat4x4 cam;
    mat4x4 proj;
    vec4 eye;
};
out vec3 position_vs;
void main(void)
{
    vec4 tmp=cam*vec4(position,0);
    tmp.w=1;
    tmp=proj*tmp;
    tmp.z=0.999999*tmp.w;
    gl_Position=tmp;

    position_vs = position;
}
