#version 430

layout (local_size_x = 32, local_size_y = 32) in;

#ifndef Value
#define Value vec3
#endif

#ifndef VEC4_TO_VALUE
#define VEC4_TO_VALUE(v) (vec3(v.rgb))
#endif

#ifndef VALUE_TO_VEC4
#define VALUE_TO_VEC4(v) (vec4(v,0))
#endif


#ifndef IMG_FORMAT
#define IMG_FORMAT rgba16f
#endif

#ifndef INPUT_MIP_LEVEL
#define INPUT_MIP_LEVEL 0
#endif

uniform sampler2D input_img;

layout(IMG_FORMAT) restrict uniform image2D output_img;
shared Value storage[1024];

#ifndef BORDER_WIDTH
#define BORDER_WIDTH 2
#endif

Value get_value(ivec2 offset){

    ivec2 new_coord     = ivec2(gl_LocalInvocationID.xy)+offset;
    bvec4 is_available  = bvec4( lessThan(ivec2(-1),new_coord), lessThan(new_coord,ivec2(gl_WorkGroupSize.xy)));

    Value ret = Value(0);

    // the return value does not influence the output when this condition is false.
    if( all(is_available) )
        ret = storage[new_coord.y*int(gl_WorkGroupSize.x) + new_coord.x];

    return ret;
}

ivec2 img_coord(){
    return ivec2( (gl_WorkGroupSize.xy - 2*uvec2(BORDER_WIDTH) )*gl_WorkGroupID.xy + gl_LocalInvocationID.xy) - ivec2(BORDER_WIDTH);
}

Value load(){
    return VEC4_TO_VALUE( texelFetch(input_img,img_coord(),INPUT_MIP_LEVEL));
}

void store(Value val){
    bvec4 is_writable = bvec4( lessThan(uvec2(BORDER_WIDTH-1), gl_LocalInvocationID.xy), lessThan(gl_LocalInvocationID.xy,gl_WorkGroupSize.xy-uvec2(BORDER_WIDTH)));
    if(all(is_writable))
        imageStore(output_img, img_coord(), VALUE_TO_VEC4(val));
}

vec3 bloom_bright_pass(vec3 illumination);

void main(){

    const float weight[BORDER_WIDTH+1] = { 6/16.0 , 4/16.0 , 1/16.0};

    Value val                           = load();

    // custom section
    val                                 = bloom_bright_pass(val);
    //

    storage[gl_LocalInvocationIndex]    = val;

    memoryBarrierShared();
    barrier();


    val    *= weight[0];
    val    += (get_value(ivec2(-1,0)) + get_value(ivec2(1,0))) * weight[1];
    val    += (get_value(ivec2(-2,0)) + get_value(ivec2(2,0))) * weight[2];

    memoryBarrierShared();
    barrier();

    storage[gl_LocalInvocationIndex]    = val;

    memoryBarrierShared();
    barrier();

    val    *= weight[0];
    val    += (get_value(ivec2(0,-1)) + get_value(ivec2(0,1))) * weight[1];
    val    += (get_value(ivec2(0,-2)) + get_value(ivec2(0,2))) * weight[2];

    store(val);

}
