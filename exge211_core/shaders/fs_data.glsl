#version 430
in mat3x3 tangent_space;
smooth in vec2 uv_coord;

layout(binding=1) uniform sampler2DArray albedo_texture;
layout(binding=2) uniform sampler2DArray normal_map;
layout(binding=3) uniform sampler2DArray specular_texture;

struct Material{
    uint albedo_tx;
    uint spec_tx;
    uint norm_tx;
    float spec_pow;
};

layout (std140) buffer MaterialUBO
{
    Material material[];
};

flat in uint mat_vs;
layout (std140) uniform Transform
{
    mat4x4 cam;
    mat4x4 proj;
};
layout(location = 0) out vec2 normal_out;
layout(location = 1) out vec4 albedo;
layout(location = 2) out vec4 spec;

vec2 decode_normal(vec3 normal);

void main(void)
{
    vec3 n_coord=2*texture( normal_map, vec3(uv_coord, material[mat_vs].norm_tx ) ).xyz+vec3(-1);
    n_coord=normalize(tangent_space*n_coord);

    normal_out=decode_normal(n_coord);
    albedo=vec4(
		clamp(texture( albedo_texture, vec3(uv_coord, material[mat_vs].albedo_tx ) ).rgb,0.05,0.95),
		1);

    spec=vec4(
		texture( specular_texture, vec3(uv_coord, material[mat_vs].spec_tx ) ).rgb,
		material[mat_vs].spec_pow/32.0);
}
