#version 430 core
layout(location=0) in vec3 pos;
layout(location=1) in vec3 cpos;
layout(location=2) in vec3 intensity_in;
layout(location=5) in float radius_in;

flat out vec3 intensity;

out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};

uniform float near_plane;
uniform float radius_modifier;

layout (std140) uniform Transform
{
    mat4x4 cam;
    mat4x4 proj;
};

void main(void)
{
    gl_Position=cam*vec4(radius_in*pos+cpos,1);
    gl_Position=proj*gl_Position;
    intensity=intensity_in;
}
