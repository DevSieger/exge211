#version 430 core

out vec3 color;
in vec2 coord;

layout(binding=7) uniform sampler2DArray shadow_map;
layout(binding=6) uniform sampler3D rotation_tx;

uniform uint debug_mode;

struct CascadeData{
    mat4x4 proj;
    float near;
    float overlap;
    vec2 xz_scale;
};
layout (std140) uniform DlFullData{
    vec3 direction;
    uint cascade_count;
    vec4 intensity;
    CascadeData cascade[12];
};

ivec2 sc;

vec3 load_albedo(ivec2 coord);
vec4 load_specular_shininess(ivec2 coord);
vec3 load_intensity(ivec2 coord);
vec3 load_position(ivec2 coord);
vec3 load_normal(ivec2 coord);

layout(rgba8,binding=5) readonly restrict uniform image2D rt_color;
vec4 get_color(){
    return imageLoad(rt_color,sc).xyzw;
}

vec4 cascades(){
    vec3 pd=load_position(sc);
    vec3 pos=pd.xyz;
    float depth=-pd.z;
    uint index=0;
    for(;index<cascade_count && depth>cascade[index].near;index++){
    }
    index--;
    index=clamp(index,0,cascade_count-1);
    vec4 clr[14]={
	vec4(1,0,0,1),
	vec4(0,1,0,1),
	vec4(1,1,0,1),
	vec4(0,0,1,1),
	vec4(1,0,1,1),
	vec4(0,1,1,1),
	vec4(1,1,0.5,1),
	vec4(0.5,0.25,0.25,1),
	vec4(0.25,0.5,0.25,1),
	vec4(0.5,0.5,0.25,1),
	vec4(0.25,0.25,0.5,1),
	vec4(0.5,0.25,0.5,1),
	vec4(0.25,0.5,0.5,1),
	vec4(0.5,0.5,0.5,1)
    };
    vec4 ret=clr[index];
    if(index+1<cascade_count){
	float t=(cascade[index+1].near-depth);
	if(t<=cascade[index].overlap){
	    ret=vec4(1,1,1,1);
	}
    }

    return ret;
}

void main(void)
{
    sc=ivec2(gl_FragCoord.xy);
    if(debug_mode==1){
        color               = cascades().rgb;
    }else if(debug_mode==2){
        color               = load_normal(sc)*0.5 + vec3(0.5);
    }
}
