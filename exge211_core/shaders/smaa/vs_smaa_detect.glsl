#version 430

in vec2 position;

out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};

out vec2 tx_coord;
out vec4 offsets[3];

void SMAAEdgeDetectionVS(vec2 texcoord, out vec4 offset[3]);

void main(void)
{
    gl_Position=vec4(position,-1,1);
    tx_coord = gl_Position.xy*0.5+0.5;
    SMAAEdgeDetectionVS(tx_coord,offsets);
}
