#version 430 core

layout(binding=14) uniform sampler2D rt_color;

out vec2 edges;
in vec2 tx_coord;
in vec4 offsets[3];

vec2 SMAALumaEdgeDetectionPS(vec2 texcoord, vec4 offset[3], sampler2D colorTex);

void main(void)
{
    edges=SMAALumaEdgeDetectionPS(tx_coord,offsets,rt_color);
}
