#version 430

in vec2 position;

out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};

out vec2 tx_coord;
out vec4 offset;

void SMAANeighborhoodBlendingVS(vec2 texcoord, out vec4 offset);

void main(void)
{
    gl_Position=vec4(position,-1,1);
    tx_coord = gl_Position.xy*0.5+0.5;
    SMAANeighborhoodBlendingVS(tx_coord,offset);
}
