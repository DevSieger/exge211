#version 430 core

layout(binding=9) uniform sampler2D tx_area;
layout(binding=10) uniform sampler2D tx_search;
layout(binding=11) uniform sampler2D rt_edges;

out vec4 weight;
in vec2 tx_coord;
in vec2 pixcoord;
in vec4 offsets[3];

vec4 SMAABlendingWeightCalculationPS(vec2 texcoord, vec2 pixcoord, vec4 offset[3], sampler2D edgesTex, sampler2D areaTex, sampler2D searchTex, vec4 subsampleIndices);

void main(void)
{
    weight=SMAABlendingWeightCalculationPS(tx_coord, pixcoord, offsets, rt_edges, tx_area, tx_search, vec4(0,0,0,0));
}
