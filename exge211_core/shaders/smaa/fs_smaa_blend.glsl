#version 430 core

layout(binding=12) uniform sampler2D rt_weight;
layout(binding=14) uniform sampler2D rt_color;

out vec4 color;
in vec2 tx_coord;
in vec4 offset;

vec4 SMAANeighborhoodBlendingPS(vec2 texcoord, vec4 offset, sampler2D colorTex, sampler2D blendTex);

void main(void)
{
    color=SMAANeighborhoodBlendingPS(tx_coord, offset, rt_color, rt_weight);
}
