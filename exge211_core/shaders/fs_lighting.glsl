#version 430 core
layout(early_fragment_tests) in;

layout(location = 0) out vec4 intensity_out;

layout (std140) uniform Transform
{
    mat4x4 cam;
    mat4x4 proj;
};

void init_light();
vec3 get_intensity();
//expensive function
vec3 get_light_dir();

/* gbuffer */
vec3 load_albedo(ivec2 coord);
vec4 load_specular_shininess(ivec2 coord);
vec3 load_position(ivec2 coord);
vec3 load_normal(ivec2 coord);

vec3 n;
vec3 pos;
vec4 spec;
ivec2 coord;

void init_data(){
    coord=ivec2(gl_FragCoord.xy);
    pos=load_position(coord);
    n=load_normal(coord);
    spec=load_specular_shininess(coord);
}


vec3 get_albedo(){
    return load_albedo(coord);
}

vec3 get_specular(){
    return spec.xyz;
}


vec3 get_position(){
    return pos.xyz;
}

float get_depth(){
    return -pos.z;
}

vec3 get_normal(){
    return n;
}

float get_shininess(){
    return spec.w;
}

void main(void)
{
    init_data();
    init_light();

    vec3 dir=get_light_dir();
    const float cos_th=-dot(dir,n);

    if(cos_th<0.00001){
	discard;
    }

    vec3 intensity=get_intensity();
    const vec3 view_dir=normalize(get_position());

    vec3 diffuse=min(1,cos_th)*intensity;
    vec3 specular=pow( clamp(-dot(view_dir,reflect(dir,n)),0,1),get_shininess())*intensity;
    intensity_out=vec4(get_albedo()*diffuse+get_specular()*specular,1);
}
