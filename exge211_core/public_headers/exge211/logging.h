#pragma once

#include "defines.h"
#include "common_types.h"
#include <dk/format_string.hpp>

/*!
 * \file
 * \brief This file contains functions and types of the logging system.
 */
namespace exge211 {

/// The threshold level of a log detalization.
enum class Detail_level{
    CRITICAL,   ///< Only critical errors will be logged.
    NON_CRITICAL,   ///< All errors will be logged.
    DEBUG, ///< All errors and debug messages will be logged.
    DETAILED,   ///< A verbose logging mode, all messages will be logged.
    DISABLED    ///< This value disables a logging.
};

/*!
 * \brief Outputs the given message of the specified detailization level to the log.
 * \param lvl The specified detailization level.
 * \param message The given message.
 *
 * All messages passed to this function then forwarded to the format functor of the logging system.
 * The format functor can modify the log message, add a timestamp, etc..
 * Then a result of formatting is sent to the output functor, which is responsible for an actual output of the message, eg writes it in a file.
 */
void EXGE211_CORE_EXPORT log_msg(Detail_level lvl,String const & message);

/*!
 * \brief Outputs the given message of the specified detailization level to the log.
 * \param lvl The specified detailization level.
 * \param message The given message. You can use %N% to place Nth argument, where N is a natural number.
 * \tparam Args A type of an argument to substitution.
 * \param args An argument to substitution.
 *
 * To convert the specified arguments to a string the overloaded operator << of std::ostream is used.
 */
template<typename ...Args>
void log_msg(Detail_level lvl, String const & message , Args...args){
    log_msg(lvl,dk::fstr(message,std::forward<Args>(args)...));
}

namespace logging {

/*!
 * \brief The type of the output functor of the logging system.
 *
 * See the description of log_msg for the information about functor roles.
 * \param lvl is a detalization level of an incoming message.
 * \param msg is a log message.
 */
using Output_functor=Function<void(Detail_level lvl,String const & msg)>;
/*!
 * \brief The type of the format functor of the logging system.
 *
 * See the description of log_msg for the information about functor roles.
 * \param lvl is a detalization level of an incoming message.
 * \param msg is a log message.
 * \return Returns a formatted message.
 */
using Formatting_functor=Function<String(Detail_level lvl,String const & msg)>;

/// Sets the new format functor
void EXGE211_CORE_EXPORT format(Formatting_functor new_format);

/// Sets the new output functor
void EXGE211_CORE_EXPORT output(Output_functor new_output);

/// Returns the format functor that is currently in use.
Formatting_functor const& EXGE211_CORE_EXPORT format();

/// Returns the output functor that is currently in use.
Output_functor  const& EXGE211_CORE_EXPORT output();

/// Sets the new detalization level of the logging system.
void EXGE211_CORE_EXPORT detail_level(Detail_level level);
/// Returns the detalization level that is currently in use.
Detail_level EXGE211_CORE_EXPORT detail_level();

/// Initializes the logging system using the specified output and format functors.
void EXGE211_CORE_EXPORT init(Output_functor output, Formatting_functor format);

/*!
 * \brief Initializes the logging system using the default output and format functors.
 * \param file_path A path to the given file.
 * \param date_format_prefix A format of the date-time prefix. For the information about format specifiers see documentation of std::strftime.
 * \return true if an initialization succeed.
 * \details
 * The default output functor writes an incoming messages to the given file.
 * The default format functor adds a prefix with date and time of an incoming message in the specified format.
 */
bool EXGE211_CORE_EXPORT init(String const& file_path="error.log", String const& date_format_prefix="[%y.%m.%d %H:%M:%S]:");

/*!
 * \brief Initializes the logging system using the given output functor and default format functors.
 * \param output The given output functor.
 * \param date_format_prefix A format of the date-time prefix. For the information about format specifiers see documentation of std::strftime.
 * \details
 * The default format functor adds a prefix with date and time of an incoming message in the specified format.
 */
void EXGE211_CORE_EXPORT init(Output_functor output, String const& date_format_prefix="[%y.%m.%d %H:%M:%S]:");

}

}
