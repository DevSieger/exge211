#pragma once

#include <string>
#include <functional>
#include <cstdint>

namespace exge211 {

/// An alias for a string type.
using String = std::string;

/// An alias for a functor template type.
template<typename Signature>
using Function=std::function<Signature>;

/// An alias for a 32-bit unsigned integer type.
using u32=std::uint32_t;

/// An alias for a 64-bit unsigned integer type.
using u64=std::uint64_t;

/// An alias for resource names/keys.
using Resource_request=String;

using Scene_time    = double;

/// A class representing a window size.
struct Window_frame_size{
    using Value=u32;
    Value width;
    Value height;

    /// returns true if the size is specified.
    explicit operator bool() {
        return width && height;
    }
};

/// Checks whether two sizes match
/// \return true if two sizes match, false otherwise.
inline bool operator == (Window_frame_size const& lhr, Window_frame_size const& rhs){
    return lhr.width==rhs.width && lhr.height==rhs.height;
}

/// Checks whether two sizes are different
/// \return false if two sizes match, true otherwise.
inline bool operator != (Window_frame_size const& lhr, Window_frame_size const& rhs){
    return !(lhr==rhs);
}

}

