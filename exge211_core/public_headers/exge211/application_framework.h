#pragma once

#include "defines.h"
#include <dk/concepts.h>
#include "handles.h"

namespace exge211 {

class Renderer;
class Platform_factory;
class Resource_loader;

//TODO: переписать комментарии по человечи
/*!
 * \brief A base for an user provided class with application logic.
 *
 * This class manages initialization of Renderer and main window.
 * Usage of this class is complitly optional, it is possible to use exge211 without it. This is like an example.
 */
class EXGE211_CORE_EXPORT GUI_application: dk::Non_copyable<GUI_application>
{
public:

    /// Ititializes exge211 using given factory and resource loader
    GUI_application(Platform_factory & factory,Resource_loader& loader);

    using Time = double;

    /*!
     * \brief Implements an initialization of the user's application
     * \return true if initialization succeed
     * \details This class implementation just returns true. An user have to provide his own initialization code in an overriding method.
     */
    virtual bool init();

    /*!
     * \brief Starts the main loop.
     * \return Non zero value if error happens.
     */
    int run();

    virtual ~GUI_application();
protected:

    /// Returns reference to managed Renderer object.
    Renderer& renderer()const{
        return *renderer_;
    }

    /// Returns reference to managed Window object.
    Window& window()const{
        return *wnd_;
    }

private:

    /*!
     * \brief Implements an iteration of an user's application.
     * \param t Time elapsed from a previous call of this object's GUI_application::iteration.
     * \return false if an user's application have to stop.
     * \details This class implementation just returns true. An user have to provide his own iteration code in an overriding method.
     */
    virtual bool iteration(Time t);

    //following members are self explainig
    GUI_event_loop_ptr event_loop_=nullptr;
    Window_ptr wnd_=nullptr;
    Renderer * renderer_=nullptr;
};

}
