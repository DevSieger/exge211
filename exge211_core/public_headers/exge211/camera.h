#pragma once

#include "defines.h"
#include "vector_types.h"


namespace exge211 {

/*!
 * \brief The camera class
 */
class EXGE211_CORE_EXPORT Camera
{

public:
    /// Returns the value, that is used as a default up direction parameter.
    static Direction const& default_up();

    /*!
     * \brief Default constructor.
     *
     * The initial view direction of a camera is (0,0,-1) (i.e. the value of screen_space_fow()).
     * The initial position of a camera is (0,0,0).
     */
    Camera();

    /// Returns the distance from the position of this camera to the nearest plane of this camera's view frustum.
    float nearest()const{
        return near_;
    }

    /// Sets the distance from the position of this camera to the nearest plane of this camera's view frustum.
    Camera & nearest(float nearest_a){
        near_=nearest_a;
        return *this;
    }

    /// Returns the distance from the position of this camera to the farthest plane of this camera's view frustum.
    float farthest()const{
        return far_;
    }

    /// Sets the distance from the position of this camera to the farthest plane of this camera's view frustum.
    Camera & farthest(float farthest_a){
        far_=farthest_a;
        return *this;
    }

    /*!
     * \brief Returns the rotation of this camera.
     * \return The quaternion defining a transform from the initial camera view direction to the current camera view direction.
     */
    Rotation const & rotation() const{
        return rot_;
    }

    /*!
     * \brief Sets the rotation of this camera.
     * \param q The quaternion defining a transform from the initial camera view direction to the new camera view direction.
     * \return This camera.
     */
    Camera & rotation(Rotation const & q) {
        rot_=q;

        // only unit quaternions are used to represent rotations, so we need to normalize input.
        const float threshold=0.0002f;
        float n2=glm::dot(rot_,rot_);
        if(n2>1+threshold || n2<1-threshold){
            rot_=glm::normalize(rot_);
        };
        return *this;
    }

    /// Returns the position of this camera.
    Position position()const{
        return pos_;
    }

    /*!
     * \brief Sets the position of this camera.
     * \return This camera.
     */
    Camera & position(Position const & new_position){
        pos_=new_position;
        return *this;
    }

    /// Returns the field of view of this camera in radians.
    float fov()const{
        return fov_;
    }

    /*!
     * \brief Sets the field of view of this camera.
     * \param field_of_view The field of view in radians.
     * \return This camera.
     */
    Camera & fov(float field_of_view){
        fov_=field_of_view;
        return *this;
    }

    /// Returns a view direction of this camera.
    Direction direction()const{
        return rotation()*orig_forward();
    }

    /// Returns the up direction of this camera.
    Direction up()const{
        return rotation()*orig_up();
    }

    /*!
     * \brief Sets a view direction of this camera.
     * \param new_view_direction The specified new view direction.
     * \param v_up A new up direction that determines a new orientation of this camera.
     * \return This camera.
     */
    Camera & direction(Direction const & new_view_direction, Direction const & v_up=default_up());

    /*!
     * \brief Rotates this camera using the specified transform.
     * \param camera_space_rotation A quaternion, that represents a rotation in the camera local coordinate system.
     * \return This camera.
     */
    Camera & rotate(Rotation const & camera_space_rotation);

    /*!
     * \brief Rotate this camera to look at the specified location.
     * \param target_position a position of the specified location.
     * \param v_up A camera up direction that is used as a hint for a new orientation of this camera.
     */
    void look_at(Position const & target_position,Direction const & v_up =default_up());

    ~Camera()=default;

protected:
    /// Returns the orignal view direction of a camera.
    static Direction const& orig_forward();
    /// Returns the orignal up direction of a camera.
    static Direction const& orig_up();
private:
    Rotation rot_={1,0,0,0};
    Position pos_={0,0,0};
    float fov_=glm::radians(45.0f);
    float near_=1.0f;
    float far_=200.0f;
};

}
