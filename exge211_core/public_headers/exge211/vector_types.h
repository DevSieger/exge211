#pragma once

#include <glm/gtc/quaternion.hpp>
#include <glm/vec3.hpp>

namespace exge211 {

//templates are used directly to simplify the forward declaration.
/// An alias for a quaternion that describes a rotation.
using Rotation=glm::tquat<float,glm::highp>;    //glm::quat

/// An alias for a vector type that describes a position.
using Position=glm::tvec3<float, glm::highp>;   //glm::vec3

/// An alias for a vector type that describes a direction.
using Direction=glm::tvec3<float, glm::highp>;  //glm::vec3

/*!
 * \brief An alias for a vector type that describes a color, or an intensity, or some similar value.
 *
 * Coordinates of the instance of that type isn't normalized.
 */
using RGB=glm::tvec3<float, glm::highp>;

/*!
 * \brief An alias for a vector type that describes a color, or some similar value.
 *
 * Coordinates of the instance of that type have to be normalized.
 */
using RGB_norm=glm::tvec3<float, glm::highp>;

}
