#pragma once

#include "defines.h"
#include "camera.h"
#include "point_light.h"
#include "aabb.h"
#include "common_types.h"
#include "vector_types.h"
#include "handles.h"
#include <dk/concepts.h>

namespace exge211 {

class Scene_implementation;

/*!
 * \brief An object respresenting a scene.
 * \details An instance of this class is obtainable using Renderer::new_scene.
 */
class EXGE211_CORE_EXPORT Scene: dk::Non_copyable<Scene>
{
public:
    /// \brief Sets the given camera to use.
    /// \return This object.
    Scene & camera(Camera_ptr cam);

    /// Returns a smart pointer to the camera that is used by this scene.
    Camera_ptr camera()const;

    /// Returns the scaling factor that is applied to the illumination values of the skybox .
    float sky_light_scale()const;

    /*!
     * \brief Sets the scaling factor that is applied to the illumination values of the skybox .
     * \return This object.
     */
    Scene & sky_light_scale(float new_scale);

    /// Returns the intensity of ambient lighting.
    RGB const & ambient_light()const;

    /*!
     * \brief Sets the intensity of ambient lighting to the given value.
     * \param The given value.
     * \return This object.
     */
    Scene & ambient_light(RGB const& a_ambient_light);

    /// Returns the boundary of this scene.
    AABB const& boundary()const;

    /*!
     * \brief Sets the boundary of this scene to the specified value.
     * \param new_boundary The specified value.
     * \return This object.
     *
     * A bounding volume of each object has to be inside the scene bounding box.
     */
    Scene& boundary(AABB const& new_boundary);

    /*!
     * \brief Sets the skybox of scene to the requested skybox.
     * \param skybox A name of the requested skybox.
     * \return This object.
     */
    Scene &  set_skybox(Resource_request const& skybox);

    /*!
     * \brief Loads the requested skeleton type and returns a handle to it.
     * \param skeleton_type The name of the requested skeleton type.
     * \return The handle pointing to the loaded skeleton.
     */
    Skeleton_handle load_skeleton_type(Resource_request const & skeleton_type);

    /*!
     * \brief Notifies the engine that the given skeleton type is out of use.
     * \param skeleton A handle pointing to the given skeleton type.
     * \return True if skeleton type is unloaded, or false if the unloading is deferred.
     * \warning "skeleton" has to be obtained from THIS scene, using Scene::load_skeleton_type.
     */
    bool relax(Skeleton_handle skeleton);

    /*!
     * \brief Loads a prototype of the actor item (i.e. a concrete model).
     * \param model A name of the requested model.
     * \param skin A name of the skin to use.
     * \return A handle to the loaded prototype.
     *
     * The skin is a mapping of the texture keys specified in the requested model to the real texture names.
     */
    Model_handle load_actor_item_proto(Resource_request const & model,Resource_request const & skin);

    /*!
     * \brief Notifies the engine that the given prototype is out of use.
     * \param model A handle pointing to the given prototype.
     * \return True if prototype is unloaded, or false if the unloading is deferred.
     * \warning "model" has to be obtained from THIS scene, using Scene::load_actor_item_proto.
     */
    bool relax(Model_handle model);

    /*!
     * \brief Creates the new actor with the given skeleton.
     * \param skeleton_type A handle to the given skeleton.
     * \return A smart pointer to the created actor.
     * \warning "skeleton_type" has to be obtained from THIS scene, using Scene::load_skeleton_type.
     */
    Actor_ptr new_actor(Skeleton_handle skeleton_type);

    /*!
     * \brief Loads a prototype of the scenery (i.e. a concrete static model).
     * \param model A name of the requested model.
     * \param skin A name of the skin to use.
     * \return A handle to the loaded prototype.
     *
     * The skin is a mapping of the texture keys specified in the requested model to the real texture names.
     */
    Static_model_handle load_scenery_proto(Resource_request const & model,Resource_request const & skin);

    /*!
     * \brief Notifies the engine that the given prototype is out of use.
     * \param model A handle pointing to the given prototype.
     * \return True if prototype is unloaded, or false if the unloading is deferred.
     * \warning "model" has to be obtained from THIS scene, using Scene::load_scenery_proto.
     */
    bool relax(Static_model_handle model);

    /*!
     * \brief Creates the new scenery using the given prototype and the specified parameters.
     * \param proto A handle to the given prototype. That handle have to be obtained from this scene, using Scene::load_scenery_proto.
     * \param rotation The specified rotation of the scenery to create.
     * \param position The specified position of the scenery to create.
     * \param scale The specified scale of the scenery to create. Each coordinate of that vector denotes the scale of the corresponding coordinate of scenery.
     * \return A smart pointer to the created scenery.
     */
    Scenery_ptr new_scenery(Static_model_handle proto, Rotation const& rotation, Position const& position, Direction const& scale);

    /*!
     * \brief Loads the requested animation.
     * \param animation A name of the requested animation.
     * \return A handle to the loaded animation.
     */
    Animation_handle load_animation(Resource_request const& animation);

    /*!
     * \brief Notifies the engine that the given animation is out of use.
     * \param model A handle pointing to the given animation.
     * \return True if animation is unloaded, or false if the unloading is deferred.
     * \warning "animation" has to be obtained from THIS scene, using Scene::load_animation.
     */
    bool relax(Animation_handle animation);

    /*!
     * \brief Creates a new point light source.
     * \param shadow If this parameter is true, then the light created will be used in the calculation of shadows.
     * \return A smart pointer to the created light source.
     */
    Point_light_ptr add_point_light(bool shadow=false);

    /*!
     * \brief Creates a new directional light source.
     * \param intensity The intensity of the created light source.
     * \param direction The direction of a light from the created source.
     * \return The smart pointer to the created light source.
     */
    Directional_light_ptr add_directional_light(RGB const& intensity,Direction const& direction);

    /*!
     * \brief Sets a value of the backend specific variable that is used in the shading.
     * \param name      A name of the variable.
     * \param value     The value of the variable.
     * \return          This object.
     *
     * There is two different sets: variables with integral values and variables with floating point values.
     * It is important to use a correct type.
     */
    Scene& shading_variable_f(String const& name, float value);
    /// \copydoc shading_variable_f
    Scene& shading_variable_i(String const& name, int value);

    /*!
     * \brief Advances time of the scene by the given value.
     * \param dt        the given value, in seconds.
     * \warning If dt is negative, then behaviour is undefined.
     */
    void advance(Scene_time dt);

    /*!
     * \brief   Changes a state of the auto-advance mode for time of the scene to the given one.
     * \param enabled   The new state of the auto-advance mode.
     * \return          This object.
     *
     * The auto-advancing mode means that the internal timer is used instead of values that are accumulated using #advance method.
     * On entering the auto-advance mode the internal timer is set to the accumulated time value.
     * On leaving the auto-advance mode the accumulated time value is set to the value stored in the internal timer.
     *
     * \note    The auto-advancing is turned on by default.
     */
    Scene& auto_advance(bool enabled);

    /// Tests if the auto-advance mode of scene time is enabled.
    bool auto_advance() const;

    using Visual_debug_type=unsigned int;
    /*!
     * \brief Switches rendering to debug mode. This method only used for development purposes.
     * \param b Acceptable values:
     *  - 0 to disable "debugging";
     *  - 1 to show CSM shadow map areas;
     *  - 2 to show normals;
     */
    void debug(Visual_debug_type b);

    /// Returns the debug mode state.
    Visual_debug_type debug()const;



private:
    friend class Scene_implementation;
    Scene()=default;
    ~Scene()=default;
};

}
