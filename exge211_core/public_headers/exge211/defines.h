#pragma once

// EXGE211_CORE_EXPORT is a macro to handle exporting/importing symbols from shared libraries

#if defined(_WIN32) && !defined(EXGE211_CORE_STATIC)

#ifdef EXGE211_CORE_EXPORTING
#define EXGE211_CORE_EXPORT __declspec(dllexport)
#else
#define EXGE211_CORE_EXPORT __declspec(dllimport)
#endif //#ifdef EXGE211_CORE_EXPORTING

#else
#define EXGE211_CORE_EXPORT
#endif //#if defined(_WIN32) && !defined(EXGE211_CORE_STATIC)
