#pragma once

/*!
  \file
  This file contains forward declarations of the resource classes.
*/

#include <glm/fwd.hpp>

/* glm/fwd.hpp doesn't have a forward declaration of 'dualquat' */

namespace glm{
template <typename T, glm::qualifier>
struct tdualquat;
}

namespace exge211 {

using Joint=glm::tdualquat<float,glm::highp>;

struct Texture_data;
struct Cube_map_data;
struct Skybox_info;
class Shader_source;
struct Material_info;
struct Skeleton_info;
class Skin_info;
class Animated_model_data;
class Static_model_data;

struct Animation_data;

}
