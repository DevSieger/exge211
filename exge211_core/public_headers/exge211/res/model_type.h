#pragma once

#include "../common_types.h"
#include <vector>
#include <cassert>

namespace exge211 {

/*!
 * \brief A class for accessing an array of values that have the same level of detail.
 * \tparam Data_type The type of an element in the array (eg the mesh type).
 */
template<typename Data_type>
class LOD{
public:
    /// The type of an element in the array.
    using Value=Data_type;

    using iterator=Value*;
    using const_iterator=const Value*;
    using Size=size_t;

    /// Returns an iterator to the first element of the array.
    iterator begin(){
        return begin_;
    }
    /// Returns an iterator to the element folloing the last element of the array.
    iterator end(){
        return end_;
    }

    /// Returns an iterator to the first element of the array.
    const_iterator begin()const{
        return begin_;
    }

    /// Returns an iterator to the element folloing the last element of the array.
    const_iterator end()const{
        return end_;
    }

    /// Returns the numer of elements in the array.
    Size size()const{
        return Size(end_-begin_);
    }

    /// \brief Checks whether the array is empty.
    /// \return true if the array is empty.
    bool empty()const{
        return begin_==end_;
    }

    /// Returns a reference to the element at position ix.
    Value & operator[](Size ix){
        return begin_[ix];
    }
    /// Returns a reference to the element at position ix.
    Value const & operator[](Size ix)const{
        return begin_[ix];
    }

private:
    LOD()=default;
    LOD(LOD && other)=delete;
    ~LOD()=default;

    Value * begin_=nullptr;
    Value * end_=nullptr;
};

/*!
 * \brief A template class to create the model structure.
 * \details The model structure is a set of the arrays, each of those represents the level of detail.
 * \note I hope the implementation of this class does not violate the strict aliasing rule =).
 */
template< typename Submodel_type>
class Model_type_base{
public:
    /// A type of a LoD index.
    using Index=u32;
    /// An interface for accessing the data of the specified LoD.
    using LOD=exge211::LOD<Submodel_type>;

    static_assert(alignof(Submodel_type)<=alignof(Submodel_type*),
                  "An alignment of Submodel_type has to be less or equal to the pointer alignment. "
                  "It is ok in most cases, especially on the 64 bit platform.");

    /// \brief Access the specified LoD.
    /// \param An index of the specified LoD.
    /// \return Returns the interface to access data of the specified LoD.
    LOD const & lod(Index ix)const{
        return const_cast<Model_type_base*>(this)->lod(ix);
    }

    /// \brief Access the specified LoD.
    /// \param An index of the specified LoD.
    /// \return Returns the interface to access data of the specified LoD.
    LOD & lod(Index ix){
        const size_t number_of_lods=lod_count();
        if(ix>=number_of_lods){
            ix=number_of_lods;
        }
        return *reinterpret_cast<LOD*>(static_cast<Submodel_type**>(data_)+ix);
    }

    /// \brief (Re)initilezes this object using the given parameters.
    /// \param number_of_lods The number of levels of detail.
    /// \param lod_sizes The array containing size of each LoD. A size of the array has to be 'number_of_lods'.
    void init(Index number_of_lods,size_t* lod_sizes){
        assert(lod_sizes);
        clear();

        size_t submodels_number=0;

        //i don't like to include <numeric> in this header to accumulate/scan LOD sizes
        for(Index i=0;i<number_of_lods;++i){
            submodels_number+=lod_sizes[i];
        }

        //the size of an array of pointers.
        //Each pointer (exluding the last) in this array points to the start of a new level of detail.
        //The last pointer in the array points to an element following the last element in the last level of details.
        const size_t prefix_size=(number_of_lods+1)*sizeof(void*);

        //a padding is needed to ensure an alignment of elements.
        const size_t padding=alignof(Submodel_type)-1 - (prefix_size+alignof(Submodel_type)-1) % alignof(Submodel_type);

        const size_t data_size=(submodels_number)*sizeof(Submodel_type);

        data_=operator new(prefix_size+padding+data_size);
        Submodel_type* const sm_begin=reinterpret_cast<Submodel_type*>(static_cast<char*>(data_)+prefix_size+padding);

        Submodel_type** ptr=static_cast<Submodel_type**>(data_);
        Submodel_type* sm_ptr=sm_begin;

        // the pointer array initialization.
        for(size_t i=0,i_end=number_of_lods; i<i_end ; ++i){
            ptr[i]=sm_ptr;
            sm_ptr+=lod_sizes[i];
        }
        Submodel_type* const sm_end=sm_ptr;
        ptr[number_of_lods]=sm_end;

        // an initialization of elements.
        for(sm_ptr=sm_begin; sm_ptr!=sm_end ; ++sm_ptr){
            new(sm_ptr) Submodel_type();
        }
    }

    /*!
     * \brief (Re)initializes this object using LOD sizes of the given prototype.
     * \tparam Other_submodel_type an element type of the prototype.
     * \param prototype The given prototype.
     */
    template<typename Other_submodel_type>
    void init(Model_type_base<Other_submodel_type> const& prototype){
        clear();
        if(!prototype.lod_count())return;
        std::vector<size_t> lod_sizes(prototype.lod_count());
        for(size_t i=0,i_end=lod_sizes.size();i<i_end;++i){
            lod_sizes[i]=prototype.lod(i).size();
        }
        init(lod_sizes.size(),lod_sizes.data());
    }

    ///  Returns the number of levels of detail.
    u32 lod_count()const{
        return *(static_cast<void***>(data_))-static_cast<void**>(data_)-1;
    }

    /// \brief A default constructor
    /// \details
    /// \warning The constructed object has to be initialized using Model_type_base::init before use of any other method.
    Model_type_base()=default;

    Model_type_base(Model_type_base && other) noexcept {
        move_from(other);
    }
    Model_type_base & operator = (Model_type_base && other)noexcept{
        move_from(other);
    }

    //implicitly declared copy constructor/assignment is defined as deleted because of move constructor & assignment

    ~Model_type_base(){
        clear();
    }

private:

    void move_from(Model_type_base & other){
        data_=other.data_;
        other.data_=nullptr;
    }

    /// Deinitializes this object.
    void clear(){
        if(data_){
            Submodel_type* sm_it=(static_cast<Submodel_type**>(data_))[0];
            Submodel_type* sm_end=(static_cast<Submodel_type**>(data_))[lod_count()];
            for(;sm_it!=sm_end;++sm_it){
                sm_it->~Submodel_type();
            }
            operator delete(data_);
            data_=nullptr;
        }
    }

    void* data_=nullptr;
};

}
