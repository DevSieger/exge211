#pragma once

#include "../common_types.h"

#include <vector>
#include <map>

#include "model_type.h"

#include <glm/gtx/dual_quaternion.hpp>
#include "rs_fwd.h"
#include "../aabb.h"

//todo: data packing

namespace exge211 {


enum class Image_format{
    RGB888,
    RGBA8888,
    RGB_32F
};

/// Texture properties.
struct Texture_config{
    using Size_value=u32;
    Size_value width        = 0;
    Size_value height       = 0;
    Image_format format     = Image_format::RGBA8888;

    ///Returns the number of chanels.
    Size_value chanels()const{
        switch(format){
            case Image_format::RGB888: return 3;
            case Image_format::RGBA8888: return 4;
            case Image_format::RGB_32F: return 3;
        }
        return 0;
    }
};

/// The texture data.
struct Texture_data: Texture_config{
    using Data=std::vector<char>;

    /// Binary data of the texture.
    Data data;
    /// A name of the texture. This is used in logging.
    String name;

    /// Returns the size of the pixel in bytes.
    size_t pixel_size()const{
        switch(format){
            case Image_format::RGB888: return 3;
            case Image_format::RGBA8888: return 4;
            case Image_format::RGB_32F: return 3*sizeof(float);
        }
        return 0;
    }

    /// Returns the total size of the texure binary data.
    size_t size()const{
        return width*height*pixel_size();
    }
    /// Resizes the binary data storage to match the current texture configuration.
    void resize(){
        data.resize(size());
    }
};

/// \brief Cube map template.
/// \tparam Type A data type of the face.
template<typename Type>
struct Cube_map_base{
    /// The face of cube map.
    enum Face{
        RIGHT,
        LEFT,
        BOTTOM,
        TOP,
        FRONT,
        BACK
    };

    /// A data type of the face.
    using Data=Type;

    /// The face data.
    Data data[6];

    /// Access the specified face
    Data & operator[](Face f){
        return data[f];
    }
    /// Access the specified face
    Data const & operator[](Face f)const{
        return data[f];
    }
};

/// The cube map texture used for skyboxes
struct Cube_map_data: Cube_map_base<Texture_data>{};

/// \brief The shader source code.
/// \note I suppose this is not the string because ... i wanted the guaranteed continuous storage without c++11. The legacy.
class Shader_source: public std::vector<char>{
    using std::vector<char>::vector;
};

/// The material description.
struct Material_info{
    /// A texture name for the albedo.
    Resource_request albedo;
    /// A texture name for the specular.
    Resource_request specular;
    /// A texture name for the normal map.
    Resource_request normal;
    /// The shininess from the Phong reflection model.
    float shininess;
};

/// The skeleton description.
struct Skeleton_info{
    /// The joint description.
    struct JointInfo{
        String name;    ///< name of the joint.
        Joint binding_pose; ///< The state of the joint in the binding pose of skeleton.
        u32 index; ///< The index of the joint, that is used in validation.
        u32 parent; ///< The parent joint of the joint.
    };
    /// \brief Joints of skeleton.
    /// \details Joints have to be ordered.
    std::vector<JointInfo> joints;
};


/// An abstract material key used in models to refer a material.
using Material_key=String;

/// The mapping from an abstract material keys to real material names.
class Skin_info: public std::map<Material_key,Resource_request>{};

/// The vertex data of an animated model.
struct Animated_vertex{
    ///The position of the vertex.
    float pos[3];
    /// The normal,the tangent and the binormal of the vertex.
    float n[3],t[3],b[3];
    /// The texture coordinate of the vertex.
    float uv[2];

    // todo: use char to reduce the size.
    /// An array of joint weights for Animated_vertex::joint.
    float weight[4];
    // todo: use char to reduce the size.
    /// Indices of joints that affects the vertex.
    std::int32_t joint[4];
};

/// The vertex data of a static model.
struct Static_vertex{
    ///The position of the vertex.
    glm::vec3 pos;
    /// The normal,the tangent and the binormal of the vertex.
    glm::vec3 n,t,b;
    /// The texture coordinate of the vertex.
    glm::vec2 uv;
};

/// Structure describing the face in the mesh.
class Triangle{
public:
    /// An index type of the vertex.
    using Index=std::uint32_t;

    /// The default constructor.
    Triangle()=default;
    /// Initializes the triangle containing vertices with the given indices.
    Triangle(Index a,Index b, Index c):i_{a,b,c} {}

    /*!
     * \brief Returns the index of the specified vertex of this triangle.
     * \param The index of the specified vertex in this triangle (ie: 0,1,2).
     * \return The index of the specified vertex in the mesh.
     */
    Index & operator[](int i){
        return i_[i];
    }

    bool operator==(Triangle const& other)const{
        return i_[0]==other.i_[0] && i_[1]==other.i_[1] && i_[2]==other.i_[2];
    }
    bool operator!=(Triangle const& other)const{
        return i_[0]!=other.i_[0] || i_[1]!=other.i_[1] || i_[2]!=other.i_[2];
    }

private:
    /// Vertex indices (the mesh indexation).
    Index i_[3];
};

/// Structure describing the mesh of an animated model.
struct Animated_mesh{
    using Vertices = std::vector<Animated_vertex> ;
    using Indices = std::vector<Triangle>;
    /// Vertices of the mesh.
    Vertices vertices;
    /// Faces of the mesh.
    Indices indices;
    /// An index of the binding pose of the skeleton in the Animated_model_data::transforms() "array".
    u32 transform_ix;
};

/// Structure describing the mesh of a static model.
struct Static_mesh{
    using Vertices = std::vector<Static_vertex> ;
    using Indices = std::vector<Triangle>;
    /// Vertices of the mesh.
    Vertices vertices;
    /// Faces of the mesh.
    Indices indices;
};

/// \brief A type of the material.
/// \note Actually, for the future use.
enum class Material_type:u32{
    NORMAL=0
};

/// The submodel data of an animated model.
struct Animated_submodel_data{
    Animated_mesh mesh;
    Material_key material;
    Material_type type;
};

/// The submodel data of a static model.
struct Static_submodel_data{
    Static_mesh mesh;
    Material_key material;
    Material_type type;
};

/// The helper base for an object with a boundary.
class Bounded{
public:
    /// Access the bounding box.
    AABB & boundary(){
        return boundary_box_;
    }
    /// Access the bounding box.
    AABB const & boundary()const{
        return boundary_box_;
    }
    /// Sets the bounding box.
    void boundary(AABB const & box ){
        boundary_box_=box;
    }

private:
    AABB boundary_box_;
};

/// An animated model.
class Animated_model_data: public Model_type_base<Animated_submodel_data>, public Bounded{
public:
    /// A type for storing the binding pose of the skeleton.
    using Transforms = std::vector<Joint>;
    /// A type for an 'array' of the binding poses.
    using Transforms_cont = std::vector<Transforms>;

    /// Access the binding pose array.
    Transforms_cont const & transforms()const{
        return transforms_;
    }
    /// Access the binding pose array.
    Transforms_cont& transforms(){
        return transforms_;
    }

private:
    Transforms_cont transforms_;
};

/// An static model.
class Static_model_data: public Model_type_base<Static_submodel_data>, public Bounded{};

}
