#pragma once

#include <vector>
#include <utility>
#include "../vector_types.h"

namespace exge211 {

/// A structure describing transforms of one joint/bone over time.
struct Animation_channel{
    using Time=float;

    ///A helper class that adds time parameter to the given transform type.
    template<typename Transform_type>
    struct Frame: Transform_type{

        Frame()=default;

        template<typename ...Args>
        Frame(Time a_time,Args && ... args):
            Transform_type(std::forward<Args>(args)...),
            time(a_time)
        {}

        bool operator == (Frame const & other)const{
            return time==other.time && (Transform_type&)*this == (Transform_type&)other;
        }

        Time time=0;
    };

    using Rotation=Frame<exge211::Rotation>;
    using Translation=Frame<exge211::Direction>;
    using Scale=Frame<exge211::Direction>;

    using Rotations=std::vector<Rotation>;
    using Translations=std::vector<Translation>;
    using Scales=std::vector<Scale>;

    /// Rotation changes of a joint.
    Rotations rotations;
    /// Translation changes of a joint.
    Translations translations;
    /// Scale changes of a joint.
    Scales scales;
};

/// A structure describing an animation of some skeleton.
struct Animation_data{
    using Channels=std::vector<Animation_channel>;
    /*!
     * \brief An array of animation channels.
     *
     * N-th channel corresponds to N-th joint in the array Skeleton_info::joints of the target skeleton.
     */
    Channels channels;

    /// The total duration of this animation.
    Animation_channel::Time duration;
};

}
