#pragma once

#include <memory>

namespace exge211 {

/// A rate of an animation
using Animation_rate=float; //TODO: place this in a more appropriate place

// The following types are opaque for end user:
class Animation_base;
class Model_base;
class Skeleton_type_base;
class Static_model_base;

// This is forward declarations:
class Actor;
class Camera;
class Directional_light;
class GUI_event_loop;
class Point_light;
class Scene;
class Scenery;
class Window;

// Finally, here are handles:

/// A handle for a loaded animation.
using Animation_handle=Animation_base*;

/// A handle for a loaded animated 3d model.
using Model_handle=Model_base const*;

/// A handle for a loaded skeleton.
using Skeleton_handle=Skeleton_type_base const*;

/// A handle for a loaded static 3d model (that is used in sceneries).
using Static_model_handle=Static_model_base const*;

// Here are 'smart' pointers:

/// The smart pointer to manage an actor.
using Actor_ptr=std::shared_ptr<Actor>;

/// The smart pointer to manage a camera.
using Camera_ptr=std::shared_ptr<Camera>;

/// The smart pointer to manage a directional light source.
using Directional_light_ptr=std::shared_ptr<Directional_light>;

/// The smart pointer to manage an event loop.
using GUI_event_loop_ptr=std::shared_ptr<GUI_event_loop>;

/// The smart pointer to manage a point light source.
using Point_light_ptr=std::shared_ptr<Point_light>;

/// The smart pointer to manage a scene.
using Scene_ptr=std::shared_ptr<Scene>;

/// The smart pointer to manage a scenery.
using Scenery_ptr=std::shared_ptr<Scenery>;

/// The smart pointer to manage a window.
using Window_ptr=std::shared_ptr<Window>;

}
