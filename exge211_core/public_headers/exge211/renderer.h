#pragma once

#include "defines.h"
#include <dk/concepts.h>
#include "handles.h"

namespace exge211 {

class Context;
class Resource_loader;

/// An object which is responsible to rendering.
class EXGE211_CORE_EXPORT Renderer: dk::Non_copyable<Renderer>
{
public:
    /*!
     * \brief Constructs a new renderer using the given context and the specified resource loader.
     * \param ctx The given context.
     * \param res_loader The specified resource loader.
     *
     * An user have to set a valid scene before use of this object.
     * A rendering will be done to the window, which has been used to obtain the given context.
     */
    Renderer(Context & ctx,Resource_loader const& res_loader);
    ~Renderer();

    /*!
     * \brief Sets the specified scene to render.
     * \param scene_arg A smart pointer to the specified scene.
     * \return This object.
     *
     * \warning The specified scene has to be obtained from this concrete renderer using Renderer::new_scene.
     */
    Renderer & scene(Scene_ptr const& scene_arg);

    /// Returns the rendered scene.
    Scene_ptr const& scene()const;

    /*!
     * \brief Creates a new scene.
     * \return A smart pointer to the created scene.
     *
     * The created scene may be used only with this renderer.
     */
    Scene_ptr new_scene();

    /// Draws the scene.
    void render();
private:
    struct Data;
    /// d-pointer
    Data * data_=nullptr;
};

}
