#pragma once

#include "defines.h"
#include "common_types.h"
#include <dk/concepts.h>

namespace exge211 {

/// An abstract class that encapsulates a platform-dependent window context.
class Context: dk::Non_copyable<Context>
{
public:
    using Size=Window_frame_size;

    /// Type of a context
    enum class Type{
        OGL_DESKTOP ///< a value for Context object for opengl 4.4+
    };

    /// Returns which type of context this object is.
    virtual Type type()const=0;

    /// Returns the size of a window having this context.
    virtual Size size() const =0;

    virtual ~Context()=default;
protected:
    /// Renderer is main user of protected functions of this class.
    friend class Renderer;

    /// Makes this context current
    virtual void make_current()=0;

    /// Initializes this context
    virtual void initialize()=0;

    /// Swaps front and back buffers.
    virtual void swap_buffers()=0;

    Context()=default;
};


/// a window context for Opengl 4.4+
class  EXGE211_CORE_EXPORT OGL_context: public Context
{
public:
    void  initialize() override;

    Type type()const final{
        return Type::OGL_DESKTOP;
    }
private:
    /// Reserved for a better implementation of handling of function pointers.
    void * reserved_;
};

}
