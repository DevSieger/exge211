#pragma once

#include "defines.h"
#include "common_types.h"
#include "handles.h"
#include "animation.h"
#include <dk/concepts.h>

namespace exge211 {

/*!
 * \brief An animated object.
 *
 * This class represents a set of animated models that share one skeleton.
 * To obtain instance of this class use Scene::new_actor
 */
class EXGE211_CORE_EXPORT Actor: dk::Non_copyable<Actor>{
public:
    using Slot_index=u32;

    /*!
     * \brief Sets a slot to the given model
     * \param slot An index of the slot
     * \param item A handle pointing to the given model
     * \return A reference to this object
     *
     * Obviously, a slot can reference to only one model.
     * The appearance of this actor consists of models that are referenced by its slots.
     */
    Actor & set(Slot_index slot, Model_handle item);

    /*!
     * \brief Empties a slot.
     * \param slot An index of the slot
     * \return A reference to this object
     */
    Actor & empty(Slot_index slot);

    /// Returns an animation object of this actor
    Animation& animation();
    /// Returns an animation object of this actor
    Animation const& animation()const;

private:
    friend class Actor_impl;
    Actor()=default;
    ~Actor()=default;
};

}
