#pragma once

#include "defines.h"
#include "common_types.h"
#include "res/rs_fwd.h"
#include <dk/concepts.h>

namespace exge211 {

/*!
 * \brief An object, which provides an interface to flexible resource data loading.
 */
class EXGE211_CORE_EXPORT Resource_loader: dk::Non_copyable<Resource_loader>
{
public:
    /*!
     * \brief A functor for loading the shader source code.
     * \param name A name of the requested resource.
     * \param out The object to write the source code.
     */
    Function<void(Resource_request const & name,Shader_source & out)>
        load_shader_source=nullptr;

    /*!
     * \brief A functor for loading the texture data.
     * \param name A name of the requested resource.
     * \param out The object to write the data.
     *
     * If the "out" argument has some parameters specified, then the data written have to match them.
     */
    Function<void(Resource_request const & name,Texture_data & out)>
        load_texture=nullptr;

    /*!
     * \brief A functor for loading data of the requested skin.
     * \param name The name of the requested skin.
     * \param out The object to write the data.
     */
    Function<void(Resource_request const & name,Skin_info & out)>
        load_skin=nullptr;

    /*!
     * \brief A functor for loading data of the requested material.
     * \param name The name of the requested material.
     * \param out The object to write the data.
     */
    Function<void(Resource_request const & name,Material_info & out)>
        load_material=nullptr;

    /*!
     * \brief A functor for loading data of the requested model.
     * \param name The name of the requested model.
     * \param out The object to write the data.
     */
    Function<void(Resource_request const & name,Animated_model_data & out)>
        load_model=nullptr;

    /*!
     * \brief A functor for loading data of the requested static model.
     * \param name The name of the requested model.
     * \param out The object to write the data.
     */
    Function<void(Resource_request const & name,Static_model_data & out)>
        load_static_model=nullptr;

    /*!
     * \brief A functor for loading data of the requested skeleton.
     * \param name The name of the requested skeleton.
     * \param out The object to write the data.
     */
    Function<void(Resource_request const & name,Skeleton_info & out)>
        load_skeleton=nullptr;

    /*!
     * \brief A functor for loading data of the requested animation.
     * \param name The name of the requested animation.
     * \param out The object to write the data.
     */
    Function<void(Resource_request const & name,Animation_data & out)>
        load_animation=nullptr;

    /*!
     * \brief A functor for loading data of the requested skybox.
     * \param name The name of the requested skybox.
     * \param out The object to write the data.
     */
    Function<void(Resource_request const & name, Cube_map_data & out)>
        load_skybox=nullptr;
};

}
