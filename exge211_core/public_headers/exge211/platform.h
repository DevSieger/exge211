#pragma once

#include "defines.h"
#include "common_types.h"
#include <dk/concepts.h>
#include "handles.h"

namespace exge211 {

/// The event loop.
class EXGE211_CORE_EXPORT GUI_event_loop: dk::Non_copyable<GUI_event_loop> {
public:
    /// Processes pending events.
    virtual void process_events()=0;
    /// Returns true if the event loop is running.
    virtual bool running()const=0;
    /*!
     * \brief Turns the event loop on/off.
     * \param new_state If this true - turns the event loop on, otherwise turns it off.
     * \return This object.
     */
    virtual GUI_event_loop & running(bool new_state)=0;

    virtual ~GUI_event_loop(){}
};

/*
 * TODO: callback integration
 */

class Context;

/// An abstract window to wrap a platform dependent window implementation.
class EXGE211_CORE_EXPORT Window: dk::Non_copyable<Window>
{
public:
    /*!
     * \brief A functor type for callback, that handles window closing.
     * \return
     * true to accept a closing.
     * false to indicate that the window has to ignore attempt to close.
     */
    using Close_func=Function<bool()>;

    /// Returns the context of this window.
    virtual Context & context()const=0;

    /// Returns true if this window is visible.
    virtual bool visible()const=0;

    /*!
     * \brief Hides/shows this window.
     * \param new_state
     * if new_state is true, then window becomes visible,
     * otherwise window becomes hidden.
     * \return This window.
     */
    virtual Window & visible(bool new_state)=0;

    /// Shows this window.
    virtual void show()=0;

    /// Resizes this window to the specified width and height.
    virtual void resize(int width,int height)=0;

    /// Returns true if this window is a fullscreen window.
    virtual bool full_screen()const=0;

    /*!
     * \brief Sets this window fullscreen mode.
     *  \returns this object
     */
    virtual Window & full_screen(bool fullScreen_)=0;

    /*!
     * \brief Locks/unlocks the mouse.
     * \param state if this value is true then this window locks the mouse, unlocks otherwise.
     *
     * "Locks the mouse" means that this window grabs the mouse (i.e. only this window receives all mouse events),
     * and the cursor is hidden.
     */
    virtual Window & lock_mouse(bool state)=0;

    /*!
     * \brief Locks/unlocks the keyboard.
     * \param state if this value is true then this window locks the keyboard, unlocks otherwise.
     *
     * "Locks the keyboard" means that this window grabs the keyboard (i.e. only this window receives all keyboard events)
     */
    virtual Window & lock_keyboard(bool)=0;

    /// Returns true if the mouse is locked.
    virtual bool lock_mouse()const=0;

    /// Returns true if the keyboard is locked.
    virtual bool lock_keyboard()const=0;

    virtual ~Window() {}

    /// A functor, that is called, when the user tries to close this window.
    Close_func on_close=nullptr;
};

/// An abstract factory for obtaining pointers to an actual implementations of platform-dependent objects.
class EXGE211_CORE_EXPORT Platform_factory: dk::Non_copyable<Platform_factory>
{
public:
    virtual ~Platform_factory() {}

    /*!
     * \brief Creates a new window.
     * \param width The width of the new window.
     * \param height The height of the new window.
     * \param title The title of the new window.
     * \return A smart pointer that manages the created window.
     *
     * The created window is hidden initially, a user has to show the window manually.
     * \note GUI_application::run shows the managed window.
     */
    virtual Window_ptr new_window(int width,int height,String const & title)=0;

    /// Creates a new event loop object.
    virtual GUI_event_loop_ptr get_GUI_event_loop()=0;
};

}
