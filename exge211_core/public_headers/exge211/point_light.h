#pragma once

#include "defines.h"
#include "vector_types.h"
#include <memory>
#include <dk/concepts.h>


namespace exge211 {

/// The point light source.
class EXGE211_CORE_EXPORT Point_light: dk::Non_copyable<Point_light>{
public:
    /// A vector type containing attenuation values, that are used in lighting equations.
    using Attenuation_mult=glm::vec3;

    /// Returns true, if this light source is used in the shadow calculation.
    bool shadow()const;

    /*!
     * \brief Sets, if this light source is used in the shadow calculation or not.
     * \param s If s value is true, than this light source is used in the shadow calculation.
     * \return This object.
     */
    Point_light & shadow(bool s);

    /*!
     * \brief Sets the intensity of this light to the specified value.
     * \return This object.
     *
     * Also sets the range of this light source to an appropriate value.
     */
    Point_light & intensity(RGB const & intens);
    /*!
     * \brief Sets the intensity of this light
     * \param color The color of the light.
     * \param dist The given distance.
     * \param intensity_on_dist The intensity of illumination on the target distance.
     * \return This object.
     *
     * Sets the intensity for the light source that the illumination of the object at a given distance will be equal to intensity_on_dist.
     * "color" parameter is a multiplier for the intensity.
     * Also sets the range of this light source to an appropriate value.
     */
    Point_light & intensity(RGB_norm const & color,float dist, float intensity_on_dist);

    /*!
     * \brief Sets a radius of light source to the given value.
     * \param r The given radius.
     * \param reset If this value is true, then resets the attenuation to the value corresponding to the new radius.
     * \return This object.
     *
     * Also sets the range of this light source to an appropriate value.
     */
    Point_light & radius(float r,bool reset=true);

    /*!
     * \brief Sets the attenuation of this source's light.
     * \param new_attenuation New attenuation values.
     * \return This object.
     *
     * Changes the range of this light source to an appropriate value.
     */
    Point_light & attenuation(Attenuation_mult const & new_attenuation);

    /// Returns the intensity of this light source.
    RGB const & intensity()const;

    /// Returns the radius of this light source.
    float radius()const;

    /// Returns the attenuation of this light source
    Attenuation_mult const & attenuation()const;

    /*!
     * \brief Sets the range of this light source to the given value.
     * \brief r The given value of a range.
     * \return This object.
     *
     * To set a new value of range this method alters the cut_off parameter.
     * Changes cut_off of this light source to an appropriate value.
     */
    Point_light & range(float r);

    /// \brief Returns the range of this light source.
    /// \details The range of a light source is the maximal distance at which the effect of this light source is taken into account.
    float range()const;

    /*!
     * \brief Sets a minimal visible value of illumination for this light source to the specified value.
     * \param value the specified value.
     * \return This object.
     *
     * Changes the range of this light source to an appropriate value.
     */
    Point_light & cut_off(float value);

    /// Returns a minimal visible value of illumination for this light source.
    float cut_off()const;

    /// Sets the position of this light source.
    Point_light & position(Position const & pos);

    /// Sets a coordinates of position of this light source.
    Point_light & position(float x,float y,float z);

    /// Returns the position of this light source.
    Position const & position()const;

private:
    friend class Point_light_impl;
    Point_light()=default;
    ~Point_light()=default;

    /// Recalculates the value of attenuation.
    Point_light & reset_attenuation();

    /// Returns the attenuation value for the given distance.
    float attenuation(float dist)const;

    /// Returns maximum value of intensity.
    float max_intensity()const;
};

}
