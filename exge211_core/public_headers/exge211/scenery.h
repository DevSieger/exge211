#pragma once

#include "defines.h"
#include <dk/concepts.h>

namespace exge211 {

/*!
 * \brief The scenery.
 *
 * The scenery is a static and immovable object.
 * The instance of this class is obtainable using Scene::new_scenery.
 */
class EXGE211_CORE_EXPORT Scenery: dk::Non_copyable<Scenery>
{
private:
    friend class Scenery_impl;
    Scenery()=default;
    ~Scenery()=default;
};

}
