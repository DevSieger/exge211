#pragma once

#include "defines.h"
#include "vector_types.h"
#include <limits>

namespace exge211 {

/// An axis-aligned bounding box
struct EXGE211_CORE_EXPORT AABB{
    Position min;   ///< The corner of this box which has minimal coordinates
    Position max;   ///< The corner of this box which has maximal coordinates

    /*!
     * \brief Constructs an empty box.
     *
     * A bounding box is considered empty if coordinates of its 'min' member are all set to positive infinity
     *  and coordinates of its 'max' member are all set to negative infinity.
     */
    AABB():
        min(std::numeric_limits<float>::infinity(),
            std::numeric_limits<float>::infinity(),
            std::numeric_limits<float>::infinity()),
        max(-std::numeric_limits<float>::infinity(),
            -std::numeric_limits<float>::infinity(),
            -std::numeric_limits<float>::infinity())
    {}

    /*!
     * \brief Constructs a box using two corners
     * \param min_a The corner with minimal coordinates
     * \param max_a The corner with maximal coordinates
     */
    AABB(Position const& min_a, Position const& max_a):
        min(min_a), max(max_a){

    }

    /// Returns true if the given point is in this bounding box
    bool covered(Position const & v)const{
        return v.x>=min.x && v.y>=min.y && v.z>=min.z &&
                v.x<=max.x && v.y<=max.y && v.z<=max.z;
    }

    /// Returns true if the given point is in the interior of this bounding box
    bool interior(Position const & v)const{
        return v.x>min.x && v.y>min.y && v.z>min.z &&
                v.x<max.x && v.y<max.y && v.z<max.z;
    }

    /// Returns the size of this bounding box
    Direction size()const{
        return max-min;
    }

    /// Extends this bounding box to include other AABB
    void add(AABB const & a){
        for(Position::length_type i=0;i<3;++i){
            if(min[i]>a.min[i]){
                min[i]=a.min[i];
            }
            if(max[i]<a.max[i]){
                max[i]=a.max[i];
            }
        }
    }

    /// Extends this bounding bounding box to include the given point
    void add(Position const & point ){
        for(register size_t i=0;i<3;++i){
            if(point[i]<min[i]){
                min[i]=point[i];
            }
            if(point[i]>max[i]){
                max[i]=point[i];
            }
        }
    }

    bool operator ==(AABB const & other)const{
        return other.min==min && other.max==max;
    }
    bool operator !=(AABB const & other)const{
        return other.min!=min || other.max!=max;
    }

    /*!
     * \brief Returns a corner of this box with the given index
     *
     * \param index
     * The index of a corner.
     * The best way to explain a corner indexation is to give an example.
     * Example: if we have an AABB initialized with AABB(Position(0,0,0),Position(1,1,1)) then indices of its corners are:
     * - 0 for a corner that has coordinates (0,0,0)
     * - 1 for (0,0,1)
     * - 2 (e.g. 0b010) for (0,1,0)
     * - 3 (e.g. 0b011) for (0,1,1), and so on.
     *
     * \return A corner of this box with the given index
     */
    Position corner(size_t index)const{
        return Position(
                    (index & 1<<0)?max.x:min.x,
                    (index & 1<<1)?max.y:min.y,
                    (index & 1<<2)?max.z:min.z
                         );
    }

    /// Sets this box to an empty box
    void reset(){
        min.x=min.y=min.z=std::numeric_limits<float>::infinity();
        max.x=max.y=max.z=-std::numeric_limits<float>::infinity();
    }

};

/// Returns true if the given boxes are intersected
inline bool intersected(AABB const & a,AABB const & b){
    return a.min.x <= b.max.x && a.min.y <= b.max.y && a.min.z <= b.max.z &&
           a.max.x >= b.min.x && a.max.y >= b.min.y && a.max.z >= b.min.z;
}

}
