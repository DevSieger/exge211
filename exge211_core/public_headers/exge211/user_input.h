#pragma once

#include "defines.h"
#include <glm/vec2.hpp>
#include <dk/flag.hpp>
#include "common_types.h"

namespace exge211 {

/// A namespace to encapsulate the user input related stuff (enumerators, aliases).
namespace ctrl {

/// An alias for the cursor position type.
using Cursor_position = glm::vec2;

/// An enumerator denoting a mouse button.
enum class Mouse_button{
    NONE=0,
    LEFT=1,
    RIGHT=1<<1,
    MIDDLE=1<<2
};

/// The mask defining the set of mouse buttons.
using Mouse_buttons=dk::Flag<Mouse_button>;

/// An enumerator denoting a modifier key.
enum class Keyboard_modifier{
    NONE=0,
    SHIFT=1,
    CTRL=1<<1,
    ALT=1<<2
};

/// The mask defining the set of modifier keys.
using Keyboard_modifiers=dk::Flag<Keyboard_modifier>;

/// Alias for key value.
using Keyboard_key = int;

}

/// A set of callbacks to handle an input from the keyboard.
class EXGE211_CORE_EXPORT Keyboard_callbacks{
public:
    /*!
     * \brief A type of callback for the keyboard input.
     * \param key The key generating the "event".
     * \param modifiers The active modifiers.
     * \return True if the "event" is handled.
     */
    using Key_callback=Function<bool(ctrl::Keyboard_key key, ctrl::Keyboard_modifiers modifiers)>;

    /// A callback for "the key press" event.
    Key_callback on_key_press;
    /// A callback for "the key release" event.
    Key_callback on_key_release;
};

/// A set of callbacks to handle an input from the mouse.
class EXGE211_CORE_EXPORT Mouse_callbacks{
public:
    /*!
     * \brief A type of callback for the "press" / "release" / "double click" events.
     * \param mb The mouse button generating the event.
     * \param pos The position of the cursor.
     * \param other_mb The mouse buttons pressed down.
     * \param modifiers The active modifiers.
     * \return True if the event is handled.
     *
     * For the "release" event "other_mb" exludes "mb", "mb" is included in "other_mb" in other events.
     */
    using Click_callback=Function<bool(ctrl::Mouse_button mb,ctrl::Cursor_position const & pos,ctrl::Mouse_buttons other_mb, ctrl::Keyboard_modifiers modifiers)>;

    /*!
     * \brief A type of callback for the mouse move events.
     * \param pos The position of the cursor.
     * \param mbs The mouse buttons pressed down.
     * \param modifiers The active modifiers.
     * \return True if the event is handled.
     */
    using Move_callback=Function<bool(ctrl::Cursor_position const & pos,ctrl::Mouse_buttons mbs, ctrl::Keyboard_modifiers modifiers)>;

    /*!
     * \brief A type of callback for the mouse wheel events.
     * \param angle The angle of the wheel rotation. A positive angle means that the mouse wheel was rotated forwards away from the user.
     * \param pos The position of the cursor.
     * \param mbs The mouse buttons pressed down.
     * \param modifiers The active modifiers.
     * \return True if the event is handled.
     */
    using Wheel_callback=Function<bool(float angle,ctrl::Cursor_position const & pos,ctrl::Mouse_buttons mbs, ctrl::Keyboard_modifiers modifiers)>;

    /// A callback for the mouse button press event.
    Click_callback on_mouse_press;

    /// A callback for the mouse button release event.
    Click_callback on_mouse_release;

    /// A callback for the mouse button double click event.
    Click_callback on_double_click;

    /// A callback for the mouse move event.
    Move_callback on_mouse_move;

    /// A callback for the mouse wheel event.
    Wheel_callback on_wheel;
};

}
