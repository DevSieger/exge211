#pragma once

#include "res/rs_fwd.h"

/*!
  \file
  This file contains forward declarations of most of the other public classes in exge211, exluding logging.h, common_types.h, handels and resource data.
  However, the resource data forward declarations are included from the other header.
*/

//TODO: update this file
namespace exge211 {

struct AABB;
class Actor;
class Animated_actor;
class Animation;
class Camera;
class Context;
class FPS_counter;
class GUI_application;
class GUI_event_loop;
class Keyboard_callbacks;
class Mouse_callbacks;
class Platform_factory;
class Point_light;
class Renderer;
class Resource_loader;
class Scene;
class Scenery;
class Window;

inline bool intersected(AABB const & a,AABB const & b);

}

namespace glm{

template <typename T, glm::precision>
struct tquat<float,glm::highp>;
template <typename T, glm::precision>
struct tvec3<float, glm::highp>;

}

namespace exge211{

using Rotation=glm::tquat<float,glm::highp>;
using Position=glm::tvec3<float, glm::highp>;
using Direction=glm::tvec3<float, glm::highp>;

}
