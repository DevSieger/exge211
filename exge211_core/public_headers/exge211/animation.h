#pragma once

#include "defines.h"
#include "handles.h"
#include "vector_types.h"
#include <dk/concepts.h>

namespace exge211 {

/*!
 * \brief Class that manages animation properties of an Actor
 */
class EXGE211_CORE_EXPORT Animation: dk::Non_copyable<Animation>
{
public:
    /*!
     * \brief Sets the given animation to start play.
     * \param animation A handle to the given animation, that has been obtained using Scene::load_animation call.
     */
    void play(Animation_handle animation);

    /*!
     * \brief Sets a rate of the currently plaing animation.
     * \param rate An multiplier of an original rate.
     */
    void change_rate(Animation_rate rate);

    /// Returns a position of the corresponding actor.
    Position const & position()const;

    /// Sets a position of the corresponding actor.
    /// \return A reference to this object.
    Animation& position(const Position & a_position);

    /// Returns a rotation of the corresponding actor.
    Rotation const & rotatation()const;

    /// Sets a rotation of the corresponding actor.
    /// \return A reference to this object.
    Animation& rotatation(const Rotation & a_rotatation);

    /// Returns a scale of the corresponding actor.
    float scale()const;

    /// Sets a scale of the corresponding actor.
    /// \return A reference to this object.
    Animation& scale(float sc);

private:
    friend class Actor_impl;
    Animation()=default;
    ~Animation()=default;
};

}
