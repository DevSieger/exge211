#pragma once

#include "defines.h"
#include "vector_types.h"
#include <dk/concepts.h>

namespace exge211 {

/*!
 * \brief A source of directional light.
 * \details Shadows from this type of light sources are always casted.
 */
class EXGE211_CORE_EXPORT Directional_light: dk::Non_copyable<Directional_light>{
public:
    /// Returns a direction of light.
    Direction const& direction()const;

    /// Sets the direction of light.
    Directional_light& direction(Direction const& new_direction);

    /// Returns an intensity of light.
    RGB const& intensity()const;

    /// Sets an intensity of light.
    Directional_light& intensity(RGB const& new_intensity);

private:
    friend class Directional_light_impl;
    Directional_light()=default;
    ~Directional_light()=default;
};

}
