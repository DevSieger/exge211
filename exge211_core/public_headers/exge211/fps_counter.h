#pragma once

#include "defines.h"
namespace exge211 {

/*!
 * \brief A simple FPS rate counter.
 * \note This class is candidate for exclusion from exge211.
 */
class EXGE211_CORE_EXPORT FPS_counter
{
public:
    using Time_period=double;

    /*!
     * \brief Constructs FPS_counter
     *
     * \param decay_a
     * This value defines smoothness of a counting.
     * The higher the value, the smoother a counting.
     * Correct values are: [0,1).
     */
    FPS_counter(Time_period decay_a);

    /*!
     * \brief Gets the FPS rate.
     * \param dt Time, that has elapsed from the previous frame.
     * \return the FPS rate.
     *
     * Actually this method counts a rate, with which it is called.
     */
    Time_period get(double dt);

    /// Resets the accumulated rate.
    void reset();

private:
    Time_period aft_=0; ///< the accumulated rate
    Time_period decay_; ///< a decay value, see the constructor description.
};

}
