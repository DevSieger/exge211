#pragma once

#include <memory>

namespace exge211 {

class Scene_implementation;
class Skybox;

using Skybox_handle = const Skybox*;
using Scene_impl_ptr=std::shared_ptr<Scene_implementation>;
using Scene_impl_wptr=std::weak_ptr<Scene_implementation>;
using Skybox_ptr = std::shared_ptr<const Skybox>;

}
