#pragma once

#include <glm/fwd.hpp>

/*
 * TODO:
 * найти кривые конструкции class A: public B{} использовашиеся чтобы форвардить то, что должно быть using'ом.
 * с ADL могут возникнуть проблемы, не надо так)
 */

namespace glm{
template <typename T, glm::qualifier>
struct tdualquat;
}

namespace exge211 {

using Joint=glm::tdualquat<float,glm::highp>;

}
