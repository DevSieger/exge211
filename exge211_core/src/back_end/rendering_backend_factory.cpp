#include "rendering_backend_factory.h"
#include "ogl4/rendering_backend_factory_ogl4.h"
#include "../public_headers.h"
#include PUBLIC_HEADER(context.h)

namespace exge211 {

Rendering_backend_factory_ptr Rendering_backend_factory::get_factory(Context const& ctx)
{
    switch(ctx.type()){
        case Context::Type::OGL_DESKTOP:{
            return std::make_shared<Rendering_backend_factory_OGL4>();
        }
    }
    return nullptr;
}

}
