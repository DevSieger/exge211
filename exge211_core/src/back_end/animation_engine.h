#pragma once

#include "../public_headers.h"
#include PUBLIC_HEADER(handles.h)
#include PUBLIC_HEADER(common_types.h)

#include "animation_system.h"
#include <dk/timer.hpp>

namespace exge211 {

/// An interface for managing the classical skeletal animation.
class Animation_engine: public Animation_system{
public:
    using Timer=dk::Timer;

    virtual Animation_handle load(Resource_request const & req)=0;
    virtual bool relax(Animation_handle animation)=0;

    // time_point is the current total time of the scene.
    virtual void play(Animation_state & obj,Animation_handle animation, Scene_time time_point)=0;
    virtual void change_rate(Animation_state & obj,Animation_rate rate, Scene_time time_point)=0;
};

}
