#include "csm.h"
#include "../../public_headers.h"
#include PUBLIC_HEADER(aabb.h)
#include PUBLIC_HEADER(camera.h)

#include "../../front_end/lighting/lighting_data.h"

#include <glm/gtc/matrix_transform.hpp>

namespace exge211{

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4x4;

namespace{

struct Cuboid{
    Position v[8];
    Position & operator()(bool x,bool y,bool z){
        return v[x|(y<<1)|(z<<2)];
    }
    Position const & operator()(bool x,bool y,bool z)const{
        return v[x|(y<<1)|(z<<2)];
    }
    Position & operator[](int i){
        return v[i];
    }
    Position const & operator[](int i)const{
        return  v[i];
    }
};

/*
 * convention for Cuboid::operator() : **0 - near, **1 -far, 0** - left, 1** -right, *0* - down, *1* - up.
 * frustum given at light space, z points to light.
 * f - must be frustum.
 */
void make_frustum_boundary(AABB & aabb_out,float & t_out,vec2 const & required_size, Cuboid const & f){
    int inf[2]={0,0},sup[2]={0,0};
    for(int i=1;i<4;++i){
        for(int j=0;j<2;++j){
            if(f.v[i][j]<f.v[inf[j]][j]){
                inf[j]=i;
            };
            if(f.v[i][j]>f.v[sup[j]][j]){
                sup[j]=i;
            };
        };
    }
    vec2 min_size(f.v[sup[0]].x-f.v[inf[0]].x,f.v[sup[1]].y-f.v[inf[1]].y);
    if( min_size.x>required_size.x ||  min_size.y>required_size.y ){
        return;
    }
    float t[2]={0,0};
    for(int i=0;i<2;++i){
        /*
         * важны только ребра, выходящие из наименьших/наибольших по координатам углов.
         * обусловленно линейностью frustum, т.к. прямые ребер пересекаются в одной точке, и тд..
         */
        vec3 sv=f.v[sup[i]+4]-f.v[sup[i]]; //far-near
        vec3 iv=f.v[inf[i]+4]-f.v[inf[i]];
        if(iv[i]<0 && sv[i]>0){
            float c=-iv[i]/sv[i];
            t[i]=(required_size[i]-min_size[i])/( (1+c)*sv[i] );
            aabb_out.min[i]=t[i]*iv[i]+f.v[inf[i]][i];
            aabb_out.max[i]=t[i]*sv[i]+f.v[sup[i]][i];
        }else if(iv[i]<0){
            t[i]=-(required_size[i]-min_size[i])/iv[i];
            aabb_out.min[i]=t[i]*iv[i]+f.v[inf[i]][i];
            aabb_out.max[i]=f.v[sup[i]][i];
        }else if(sv[i]>0){
            t[i]=(required_size[i]-min_size[i])/sv[i];
            aabb_out.min[i]=f.v[inf[i]][i];
            aabb_out.max[i]=t[i]*sv[i]+f.v[sup[i]][i];
        }else{
            t[i]=0;
        }
    };
    t_out=glm::min(t[0],t[1]);
    aabb_out.max.z=aabb_out.min.z=f.v[0].z;
    for(int i=1;i<4;++i){
        if(aabb_out.min.z>f.v[i].z){
            aabb_out.min.z=f.v[i].z;
        }
        if(aabb_out.max.z<f.v[i].z){
            aabb_out.max.z=f.v[i].z;
        }
    }
    for(int i=0;i<4;++i){
        vec3 v=t_out*(f.v[i+4]-f.v[i])+f.v[i];
        if(aabb_out.min.z>v.z){
            aabb_out.min.z=v.z;
        }
        if(aabb_out.max.z<v.z){
            aabb_out.max.z=v.z;
        }
    }
}
void reduce_frustum(Cuboid & frustum_inout,float t_near){
    for(int i=0;i<4;++i){
        frustum_inout[i]=(frustum_inout[i+4]-frustum_inout[i])*t_near+frustum_inout[i];
        if(t_near>1){
            vec3 tmp=frustum_inout[i];
            frustum_inout[i]=frustum_inout[i+4];
            frustum_inout[i+4]=tmp;
        };
    }
}

}

// TODO: rewrite this.
size_t set_up_csm_data( DL_CSM_data& output, Directional_light_data const& light, AABB const& scene_boundary, Camera const& camera, glm::mat4x4 const& inv_camera_pvm, size_t shadow_map_size)
{
    static_assert(DL_CSM_data::MAX_CASCADE_SIZE,"DL_CSM_data::MAX_CASCADE_SIZE is zero");

    output.direction = glm::conjugate(camera.rotation())*light.direction;
    output.intensity = light.intensity;

    size_t shadow_map_count=0;

    using glm::dot;
    using glm::cross;
    using glm::normalize;

    /* light's space */
    mat4x4 to_ls;
    vec3 x,y,z;
    z=-light.direction;

    vec3 dir=vec3(0,1,0);
    y=dir-(dot(z,dir)*z);

    if(dot(y,y)<0.00001){
        dir=vec3(0,0,1);
        y=dir-dot(y,dir)*y;
    }

    y=normalize(y);

    x=cross(y,z);

    to_ls[0]=vec4(x,0);
    to_ls[1]=vec4(y,0);
    to_ls[2]=vec4(z,0);
    to_ls[3]=vec4(0,0,0,1);

    to_ls=glm::transpose(to_ls); // now actualy to light's space =)

    //to_ls матрица перехода world space-> light space

    float scene_top=-std::numeric_limits<float>::infinity(); // минимальное значение z для верхней границы карт теней.
    for(int i=0;i<8;++i){
        vec4 c_lvl;
        for(int j=0;j<3;++j){
            c_lvl[j]=( i & (1<<j) ) ? scene_boundary.max[j]:scene_boundary.min[j];
        };
        c_lvl=to_ls*c_lvl;
        if(scene_top<c_lvl.z){
            scene_top=c_lvl.z;
        }
    }

    // матрица перехода ss->ls
    mat4x4 p=to_ls*inv_camera_pvm;
    float camera_near=camera.nearest(), camera_far=camera.farthest();

    Cuboid frust; //усеч. перамида обзора в light space
    AABB cam_border; //границы камеры в light space

    for(int i=0;i<8;++i){
        vec4 corner;
        corner.x=(i&1)*2-1;
        corner.y=(i&2)-1;
        corner.z=(i&4)*0.5-1;
        corner.w=1;
        // corner[i] из {-1,1}
        corner=p*corner;
        corner/=corner.w;
        //теперь получили углы экранного куба в light space

        for(int j=0;j<3;++j){
            frust.v[i][j]=corner[j];
            if(corner[j]<cam_border.min[j] || !i){
                cam_border.min[j]=corner[j];
            }
            if(corner[j]>cam_border.max[j] || !i){
                cam_border.max[j]=corner[j];
            }
        }
    }

    vec2 front_min{frust[0][0],frust[0][1]}; // AABB переднего плана камеры
    vec2 front_max{front_min};

    for(int i=1;i<4;++i){
        for(int j=0;j<2;++j){
            if(frust[i][j]<front_min[j]){
                front_min[j]=frust[i][j];
            }
            if(frust[i][j]>front_max[j]){
                front_max[j]=frust[i][j];
            }
        };
    }

    const size_t overlap_pixels     = 4;
    const float progression_rate    = 2;

    vec3 cam_size=cam_border.size()/(shadow_map_size-1.0f);
    float min_pixel_size;
    {
        const float to_be_safe  = 2;
        vec2 min_pixel_size_v   = (front_max - front_min) / (shadow_map_size - overlap_pixels * progression_rate * to_be_safe - 1.0f);
        min_pixel_size          = glm::max( min_pixel_size_v.x,min_pixel_size_v.y);
    }
    float pixel_size[DL_CSM_data::MAX_CASCADE_SIZE];
    /*
     * todo : adjust constants
    */
    pixel_size[0]=0.004;
    for(size_t i=1;i<DL_CSM_data::MAX_CASCADE_SIZE;++i){
        pixel_size[i]=pixel_size[i-1]*progression_rate;
    }
    pixel_size[0]=glm::max(min_pixel_size,pixel_size[0]);
    pixel_size[DL_CSM_data::MAX_CASCADE_SIZE-1]=glm::max( cam_size.x,cam_size.y);
    pixel_size[DL_CSM_data::MAX_CASCADE_SIZE-1]=glm::ceil(pixel_size[DL_CSM_data::MAX_CASCADE_SIZE-1]*5)*0.2;

    size_t start=0;
    size_t end=DL_CSM_data::MAX_CASCADE_SIZE-1;

    for(;start<DL_CSM_data::MAX_CASCADE_SIZE-1;++start){
        if(pixel_size[0] < pixel_size[start+1]){
            break;
        }
    }
    for(;end;--end){
        if(pixel_size[end-1] < pixel_size[DL_CSM_data::MAX_CASCADE_SIZE-1]){
            break;
        }
    }
    pixel_size[end]= pixel_size[DL_CSM_data::MAX_CASCADE_SIZE-1];
    shadow_map_count=end+1-start;
    if(start>0){
        for(int i=0;start<=end;start++,i++){
            pixel_size[i]=pixel_size[start];
        }

    }
    for(size_t c =0 ; c<shadow_map_count;++c){
        AABB cs_brd;
        float t=0;
        vec2 req_size=vec2(pixel_size[c]*(shadow_map_size-1));
        make_frustum_boundary(cs_brd,t,req_size,frust);
        assert(glm::dot(cs_brd.size(),cs_brd.size())); //assertion better than driver crash...
        output.cascade[c].near_plane=camera_near;
        float overlap=0;
        if(c+1<shadow_map_count){
            // inaccurate
            overlap = pixel_size[c+1] * overlap_pixels;
        };
        t-=overlap/(camera_far-camera_near);
        assert(t>0);
        camera_near=t*(camera_far-camera_near)+camera_near;
        if(camera_far<camera_near){
            shadow_map_count=c+1;
        }

        output.cascade[c].overlap=overlap;
        reduce_frustum(frust,t);

        //а теперь квантование, прилепим к сетке.
        vec2 tv(cs_brd.min.x,cs_brd.min.y);
        tv/=pixel_size[c];
        tv=tv-glm::floor(tv);
        tv*=pixel_size[c];
        cs_brd.min-=vec3(tv,0);
        cs_brd.max+=vec3(vec2(pixel_size[c])-tv,0);
        //мы вставили дополнительный пиксель

        if(cs_brd.max.z<scene_top){
            cs_brd.max.z=scene_top;
        }

        mat4x4 proj=glm::ortho(cs_brd.min.x,cs_brd.max.x,cs_brd.min.y,cs_brd.max.y,-cs_brd.max.z,-cs_brd.min.z);

        //xy_scale - значение проектирующее xy в light clip space
        output.cascade[c].xy_scale=vec2(proj[0][0],proj[1][1])*0.5f;
        output.cascade[c].proj=proj*to_ls;
    }
    output.map_count=shadow_map_count;
    return shadow_map_count;
}

}
