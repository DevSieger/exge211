#pragma once
#include "../../../public_headers.h"
#include PUBLIC_HEADER(res/resource_data.h)
#include "../../ogl_common/ogl_functions.h"

namespace exge211{

/// We need a dynamic specification, so this is just a simple class.
class Image_format_traits{
public:
    static Image_format_traits const& get(Image_format format);

    /// Returns the pixel format(e.g. GL_RGB, GL_RED...)
    GLenum const& pixel_format()const   { return p_format_;}
    /// Returns the value type (e.g. GL_UNSIGNED_BYTE, GL_FLOAT...)
    GLenum const& pixel_type()const     { return p_type_;}

    //It is ok to copy objects of this class.
private:
    Image_format_traits(GLenum p_format,GLenum p_type);
    GLenum p_format_;
    GLenum p_type_;
};

}
