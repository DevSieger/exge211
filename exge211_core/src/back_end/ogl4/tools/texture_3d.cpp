#include "texture_3d.h"
#include "../../ogl_common/error_checking.h"


namespace exge211 {

Texture_3D::Texture_3D()
{
    target_=GL_TEXTURE_3D;
}
bool Texture_3D::storage(int width, int height, int depth, int levels, int format)
{
    recreate();
    bind();
    check_gl_error();
    levels_=levels;
    format_=format;
    size_[0]=width;
    size_[1]=height;
    size_[2]=depth;
    glTexStorage3D( target_, levels, format, width, height, depth);
    rebind_to_units();
    if(check_gl_error()){
        return false;
    }else{
        return true;
    }

}

}
