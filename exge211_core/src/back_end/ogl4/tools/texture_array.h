#pragma once

#include <string>
#include "../../ogl_common/ogl_functions.h"
#include "texture_3d.h"

namespace exge211 {

struct Texture_data;
class Texture_array : public Texture_3D
{
public:
    Texture_array();

    /*!
     * \brief Sets the texture at the specified index to the given one.
     * \param tx                        The texture data.
     * \param index                     The specified index.
     *
     * The given texture data has to match the size of the texture array.
     * It binds the texture array.
     */
    void load(Texture_data const & tx,unsigned int index);
private:

};

}
