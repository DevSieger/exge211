#include "../../../public_headers.h"
#include PUBLIC_HEADER(logging.h)

#include "frame_buffer.h"
#include "../../ogl_common/error_checking.h"

namespace exge211 {

Frame_buffer::Frame_buffer()
{
    init();
}

Frame_buffer::~Frame_buffer()
{
    if(id_){
        glDeleteFramebuffers(1,&id_);
    }
}
Frame_buffer::Frame_buffer(GLuint id_):
    id_(id_)
{

}
void Frame_buffer::init()
{
    glGenFramebuffers(1,&id_);
    check_gl_error();
}


void Frame_buffer::bind(GLenum mode)const
{
    glBindFramebuffer(mode,id_);
    check_gl_error();
}

bool Frame_buffer::check() const
{
    bind();
    GLenum status = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
    if(status==GL_FRAMEBUFFER_COMPLETE){
        return true;
    }else{
        switch(status){
            case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
                log_msg(Detail_level::CRITICAL,"the FBO %1% status: INCOMPLETE_ATTACHMENT",id());
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
                log_msg(Detail_level::CRITICAL,"the FBO %1% status: INCOMPLETE_MISSING_ATTACHMENT",id());
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
                log_msg(Detail_level::CRITICAL,"the FBO %1% status: INCOMPLETE_DRAW_BUFFER",id());
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
                log_msg(Detail_level::CRITICAL,"the FBO %1% status: INCOMPLETE_READ_BUFFER",id());
                break;
            case GL_FRAMEBUFFER_UNSUPPORTED:
                log_msg(Detail_level::CRITICAL,"the FBO %1% status: UNSUPPORTED",id());
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
                log_msg(Detail_level::CRITICAL,"the FBO %1% status: INCOMPLETE_MULTISAMPLE",id());
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
                log_msg(Detail_level::CRITICAL,"the FBO %1% status: INCOMPLETE_LAYER_TARGETS",id());
                break;
            case GL_FRAMEBUFFER_UNDEFINED:
                log_msg(Detail_level::CRITICAL,"the FBO %1% status: UNDEFINED",id());
                break;
        };
        return false;
    };
}

bool Frame_buffer::attach_to_color(const Texture &tx, int attachment_index, GLint level)
{
    bind();
    glFramebufferTexture(GL_DRAW_FRAMEBUFFER,GL_COLOR_ATTACHMENT0+attachment_index,tx.id(),level);
    return !check_gl_error();
}

bool Frame_buffer::attach_to_depth(const Texture &tx, GLint level)
{
    bind();
    glFramebufferTexture(GL_DRAW_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,tx.id(),level);
    return !check_gl_error();
}

bool Frame_buffer::attach_to_stencil(const Texture& tx, GLint level)
{
    bind();
    glFramebufferTexture(GL_DRAW_FRAMEBUFFER,GL_STENCIL_ATTACHMENT,tx.id(),level);
    return !check_gl_error();
}

bool Frame_buffer::attach_to_depth_stencil(const Texture & tx, GLint level)
{
    bind();
    glFramebufferTexture(GL_DRAW_FRAMEBUFFER,GL_DEPTH_STENCIL_ATTACHMENT,tx.id(),level);
    return !check_gl_error();
}

bool Frame_buffer::attach_layer_to_color(const Texture &tx, int attachment_index, GLint layer, GLint level)
{
    bind();
    glFramebufferTextureLayer(GL_DRAW_FRAMEBUFFER,GL_COLOR_ATTACHMENT0+attachment_index,tx.id(),level,layer);
    return !check_gl_error();
}

bool Frame_buffer::attach_layer_to_depth(const Texture &tx, GLint layer, GLint level)
{
    bind();
    glFramebufferTextureLayer(GL_DRAW_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,tx.id(),level,layer);
    return !check_gl_error();
}


Frame_buffer &Frame_buffer::default_fbo()
{
    static Frame_buffer s_default_fbo((GLuint)0);
    return s_default_fbo;
}

}
