#include "../../../public_headers.h"
#include PUBLIC_HEADER(logging.h)

#include "texture.h"
#include "../../ogl_common/error_checking.h"

#include <utility>
#include <algorithm>

namespace exge211 {

void Texture::update_mipmaps()
{
    if(levels_>1){
        bind();
        glGenerateMipmap(target_);
        check_gl_error();
    }
}

void Texture::bind_to_texture_unit(GLuint texture_unit)
{
    if(texture_unit<1){
        log_msg(Detail_level::NON_CRITICAL,"do not bind to the texture unit 0! this unit is reserved.");
        return;
    }

    auto it=std::find(texture_bindings_.begin(),texture_bindings_.end(),texture_unit);
    if(it!=texture_bindings_.end()){
        return;
    }
    texture_bindings_.push_back(texture_unit);
    bind_to_texture_unit_once(texture_unit);
}

void Texture::bind_to_image_unit(GLuint unit, GLenum access, GLint level, GLenum format)
{
    // I don't like to call break_image_unit_binding, because break_image_unit_binding calls unnecessary glBindImageTexture.
    auto it=std::find_if(image_bindings_.begin(),image_bindings_.end(),[unit](Image_binding const& subject){ return subject.unit==unit;});
    if(it!=image_bindings_.end()){
        if(it->access==access && it->level==level && it->format==format){
            return;
        }
        image_bindings_.erase(it);
    }
    image_bindings_.emplace_back(Image_binding{unit,access,level,format});
    bind_to_image_unit_once(unit,access,level,format);
}

void Texture::bind_to_image_unit(GLuint unit, GLenum access, GLint level)
{
    bind_to_image_unit(unit,access,level,format_);
}

void Texture::break_image_unit_binding(GLuint unit)
{
    //It is guaranteed that there is only one binding to this image unit.
    auto it=std::find_if(image_bindings_.begin(),image_bindings_.end(),[unit](Image_binding const& subject){ return subject.unit==unit;});
    if(it!=image_bindings_.end()){
        it=image_bindings_.erase(it);
        glBindImageTexture(unit,0,0,GL_FALSE,0,GL_READ_ONLY,GL_R8);
    };
}

void Texture::break_texture_unit_binding(GLuint unit)
{
    //It is guaranteed that there is only one binding to this texture unit.
    auto it=std::find(texture_bindings_.begin(),texture_bindings_.end(),unit);
    if(it!=texture_bindings_.end()){
        texture_bindings_.erase(it);
        glActiveTexture(GL_TEXTURE0+unit);
        glBindTexture(target_,0);
        glActiveTexture(GL_TEXTURE0);
    };
}

void Texture::filter(GLint min_filter, GLint mag_filter)
{
    bind();
    glTexParameteri(target_,GL_TEXTURE_MIN_FILTER,min_filter);
    check_gl_error();
    glTexParameteri(target_,GL_TEXTURE_MAG_FILTER,mag_filter);
    check_gl_error();
}

Texture::~Texture()
{
    if(id_){
        glDeleteTextures(1,&id_);
        id_=0;
    };
}

Texture::Texture(Texture&& source):
    target_(source.target_),
    levels_(source.levels_),
    format_(source.format_),
    id_(source.id_),
    image_bindings_(std::move(source.image_bindings_)),
    texture_bindings_(std::move(source.texture_bindings_))
{
    source.levels_=0;
    source.id_=0;
    source.image_bindings_.clear();
    source.texture_bindings_.clear();
}

void Texture::rebind_to_units()
{
    for(Image_binding const& binding: image_bindings_){
        glBindImageTexture(binding.unit,id_,binding.level,GL_TRUE,0,binding.access,binding.format);
        check_gl_error();
    }

    for(Unit texture_unit: texture_bindings_){
        glActiveTexture(GL_TEXTURE0+texture_unit);
        check_gl_error();
        bind();
    }
    glActiveTexture(GL_TEXTURE0);
}

void Texture::recreate()
{
    if(id_){
        bind();
        GLuint min_filter;
        GLuint mag_filter;
        float anisotropy_degree;
        glGetTexParameterIuiv(target_,GL_TEXTURE_MIN_FILTER,&min_filter);
        glGetTexParameterIuiv(target_,GL_TEXTURE_MAG_FILTER,&mag_filter);
        glGetTexParameterfv(target_,GL_TEXTURE_MAX_ANISOTROPY_EXT,&anisotropy_degree);

        glDeleteTextures(1,&id_);


        glGenTextures(1,&id_);
        bind();
        glTexParameteri(target_,GL_TEXTURE_MIN_FILTER,min_filter);
        check_gl_error();
        glTexParameteri(target_,GL_TEXTURE_MAG_FILTER,mag_filter);
        check_gl_error();
        glTexParameterf(target_,GL_TEXTURE_MAX_ANISOTROPY_EXT,anisotropy_degree);
        check_gl_error();
        // I'm not sure if it is correct to rebind the new texture without the defined storage to the image/texture units.
    }else{
        glGenTextures(1,&id_);
        bind();
    }
}

void Texture::anisotropy(float value)
{
    if(ogl_ext_EXT_texture_filter_anisotropic){
        /* todo: this is so terrible!!! */
        float max_anisotropy;
        glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT,&max_anisotropy);
        if(max_anisotropy<value){
            value=max_anisotropy-0.01f;
        }
        bind();
        glTexParameterf(target_,GL_TEXTURE_MAX_ANISOTROPY_EXT,value);
        check_gl_error();
    };

}

void Texture::bind_to_image_unit_once(GLuint unit, GLenum access, GLint level, GLenum format) const
{
    glBindImageTexture(unit,id(),level,GL_TRUE,0,access,format);
    check_gl_error();
}

void Texture::bind_to_image_unit_once(GLuint unit, GLenum access, GLint level) const
{
    bind_to_image_unit_once(unit,access,level,format_);
}

void Texture::bind_to_texture_unit_once(GLuint texture_unit) const
{
    if(texture_unit<1){
        log_msg(Detail_level::NON_CRITICAL,"do not bind to the texture unit 0! this unit is reserved.");
        return;
    }

    glActiveTexture(GL_TEXTURE0+texture_unit);
    check_gl_error();
    bind();
    glActiveTexture(GL_TEXTURE0);
}

}
