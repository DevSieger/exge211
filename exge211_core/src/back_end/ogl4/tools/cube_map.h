#pragma once

#include "../../ogl_common/ogl_functions.h"
#include "texture_2d.h"


namespace exge211 {

struct Cube_map_data;

class Cube_map:public Texture_2D
{
public:
    Cube_map();
    void load(const Cube_map_data & tx);
};
}
