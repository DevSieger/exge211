#pragma once

#include "texture.h"
#include <glm/vec3.hpp>

namespace exge211 {

class Texture_3D: public Texture
{
public:
    Texture_3D();
    using Size=glm::uvec3;
    Size const & size()const{
        return size_;
    }

    /*!
     * \brief (Re)initializes the texture, and specifies its storage using the given parameters.
     * \param width                     The width of the texture, in texels.
     * \param height                    The height ot the texture, in texels.
     * \param depth                     The texture depth, in texels, or the number of textures in the texture array.
     * \param levels                    The number of the mipmap levels.
     * \param format                    The inner format of the texture (the sized one, see glTexStorage3D, https://www.opengl.org/wiki/GLAPI/glTexStorage3D).
     * \return                          true on success.
     *
     * The immutable storage is used, so the texture should be recreated, i.e. the texture id is changed.
     * The bindings to the texture/image units are restored for the recreated texture.
     * It binds the texture.
     */
    bool storage(int width, int height, int depth, int levels, int format=GL_RGBA8);
private:
    Size size_;
};

}
