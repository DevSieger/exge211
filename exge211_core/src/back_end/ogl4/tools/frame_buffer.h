#pragma once

#include "../../ogl_common/ogl_functions.h"
#include "texture.h"
#include <dk/concepts.h>

namespace exge211 {

/// The frame buffer object wrapper.
class Frame_buffer: dk::Non_copyable<Frame_buffer>
{
public:
    Frame_buffer();
    ~Frame_buffer();

    GLuint id()const{
        return id_;
    }

    void bind(GLenum mode = GL_DRAW_FRAMEBUFFER)const;

    /// \brief Tests if the framebuffer object comlete.
    ///
    /// It breaks the GL_DRAW_FRAMEBUFFER binding.
    /// If the framebuffer object incomplete, the error is written to the log.
    bool check()const;

    /*!
     * \brief Attaches the given texture as the render target for the color output.
     * \param tx                        The given texture.
     * \param attachment_index          The index of the color attachment point.
     * \param level                     The mipmap level of the given texture to attach.
     * \return                          true on success.
     *
     * It breaks the GL_DRAW_FRAMEBUFFER binding.
     */
    bool attach_to_color(Texture const & tx, int attachment_index,GLint level=0);

    /*!
     * \brief Attaches the given texture as the render target for the depth output.
     * \param tx                        The given texture.
     * \param level                     The mipmap level of the given texture to attach.
     * \return                          true on success.
     *
     * It breaks the GL_DRAW_FRAMEBUFFER binding.
     */
    bool attach_to_depth(Texture const & tx, GLint level=0);

    /*!
     * \brief Attaches the given texture as the render target for the stencil output.
     * \param tx                        The given texture.
     * \param level                     The mipmap level of the given texture to attach.
     * \return                          true on success.
     *
     * It breaks the GL_DRAW_FRAMEBUFFER binding.
     */
    bool attach_to_stencil(Texture const & tx, GLint level=0);

    /*!
     * \brief Attaches the given texture as the render target for the depth and stencil output
     * \param tx                        The given texture.
     * \param level                     The mipmap level of the given texture to attach.
     * \return                          true on success.
     *
     * It breaks the GL_DRAW_FRAMEBUFFER binding.
     */
    bool attach_to_depth_stencil(Texture const & tx, GLint level=0);

    /*!
     * \brief Attaches the specified layer of the given texture as the render target for the color output.
     * \param tx                        The given texture.
     * \param attachment_index          The index of the color attachment point.
     * \param layer                     The index of the specified layer.
     * \param level                     The mipmap level of the given texture to attach.
     * \return                          true on success.
     *
     * It breaks the GL_DRAW_FRAMEBUFFER binding.
     */
    bool attach_layer_to_color(Texture const & tx, int attachment_index,GLint layer,GLint level=0);

    /*!
     * \brief Attaches the specified layer of the given texture as the render target for the depth output.
     * \param tx                        The given texture.
     * \param layer                     The index of the specified layer.
     * \param level                     The mipmap level of the given texture to attach.
     * \return                          true on success.
     *
     * It breaks the GL_DRAW_FRAMEBUFFER binding.
     */
    bool attach_layer_to_depth(Texture const & tx,GLint layer, GLint level=0);

    /// Returns the default framebuffer.
    static Frame_buffer & default_fbo();
private:
    Frame_buffer(GLuint id_);
    void init();
    GLuint id_;
};

}
