#pragma once

#include <vector>
#include <glm/vec2.hpp>
#include <string>
#include "../../ogl_common/ogl_functions.h"

#include "texture.h"

namespace exge211 {

class Texture_2D:public Texture
{
public:
    using Size=glm::uvec2;
    Texture_2D();

    Size const & size()const{
        return size_;
    }

    /*!
     * \brief (Re)initializes the texture, and specifies its storage using the given parameters.
     * \param width             The width of the texture, in texels.
     * \param height            The height ot the texture, in texels.
     * \param levels            The number of the mipmap levels.
     * \param format            The inner format of the texture (the sized one, see glTexStorage2D, https://www.opengl.org/wiki/GLAPI/glTexStorage2D).
     * \return                  true on success.
     *
     * The immutable storage is used, so the texture should be recreated, i.e. the texture id is changed.
     * The bindings to the texture/image units are restored for the recreated texture.
     * It binds the texture.
     */
    bool storage(int width, int height, int levels, int format=GL_RGBA8);

private:
    Size size_;
};

}
