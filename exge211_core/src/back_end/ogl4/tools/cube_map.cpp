#include "../../../public_headers.h"
#include PUBLIC_HEADER(logging.h)
#include PUBLIC_HEADER(res/resource_data.h)

#include "cube_map.h"
#include "../../ogl_common/error_checking.h"
#include "image_format_traits.h"

namespace exge211 {

Cube_map::Cube_map()
{
    target_=GL_TEXTURE_CUBE_MAP;
}

void Cube_map::load(const Cube_map_data & tx)
{
    for(Texture_data const& txi: tx.data){
        if(txi.width!=size()[0] || txi.height!=size()[1]){
            log_msg(Detail_level::CRITICAL,"texture \"%1%\" has wrong size(%2%x%3%) for load to array, must be %4%x%5%",txi.name,txi.width,txi.height,size()[0],size()[1]);
            return;
        }
    }
    bind();
    GLenum face[6];
    face[Cube_map_data::RIGHT]=GL_TEXTURE_CUBE_MAP_POSITIVE_X;
    face[Cube_map_data::BOTTOM]=GL_TEXTURE_CUBE_MAP_POSITIVE_Y;
    face[Cube_map_data::FRONT]=GL_TEXTURE_CUBE_MAP_POSITIVE_Z;
    face[Cube_map_data::LEFT]=GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
    face[Cube_map_data::TOP]=GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
    face[Cube_map_data::BACK]=GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;

    for(size_t i=0;i<6;++i){
        Texture_data const& txi=tx[(Cube_map_data::Face)i];
        auto& f_traits=Image_format_traits::get(txi.format);
        glTexSubImage2D(face[i],0, 0, 0, txi.width, txi.height, f_traits.pixel_format(), f_traits.pixel_type(), &txi.data[0]);
    }
    check_gl_error();
}

}
