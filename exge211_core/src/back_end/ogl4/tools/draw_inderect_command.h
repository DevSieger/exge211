#pragma once

#include "../../ogl_common/ogl_functions.h"

namespace exge211 {


/// \brief A structure to use with glMultiDrawElementsIndirect
/// \details See the description of glMultiDrawElementsIndirect (e.g. https://www.opengl.org/wiki/GLAPI/glMultiDrawElementsIndirect )
struct Draw_inderect_command{
    GLuint  count;
    GLuint  instance_count;
    GLuint  first_index;
    GLuint  base_vertex;
    GLuint  base_instance;
};

}
