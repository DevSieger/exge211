#pragma once

#include "../../ogl_common/ogl_functions.h"
#include <list>

namespace exge211 {

/// The base class for all kinds of textures.
class Texture
{
public:
    GLuint id()const{
        return id_;
    }

    void bind() const{
        glBindTexture(target_,id_);
    }

    void update_mipmaps();                                                          // also binds the texture.

    /*!
     * \brief Binds the texture to the given texture unit.
     * \param texture_unit          The given texture unit. 0 is invalid value.
     * \details The zero texture unit is reserved to use with generic bindings.
     *
     * If the binding to the texture unit exists, then no bindings performed.
     */
    void bind_to_texture_unit(GLuint texture_unit);

    /*!
     * \brief Binds the given level of the texture to the specified image unit.
     * \param unit                  The index of the image unit.
     * \param access                The type of the access to be performed by shaders   (i.e. GL_READ_ONLY, GL_WRITE_ONLY, GL_READ_WRITE).
     * \param level                 The level of the texture.
     * \param format                The format of storing data into the image from shaders   (e.g. GL_RGBA8, GL_R32F).
     *
     * This method don't set the format of the texture.
     * If the binding to the specified image unit with the same parameters exists, then no bindings performed.
     * See the documentation of glBindImageTexture (e.g. https://www.opengl.org/wiki/GLAPI/glBindImageTexture ).
     */
    void bind_to_image_unit(GLuint unit,GLenum access,GLint level,GLenum format);

    /*!
     * \brief Binds the given level of the texture to the specified image unit.
     * \param unit                  The index of the image unit.
     * \param access                The type of the access to be performed by shaders   (i.e. GL_READ_ONLY, GL_WRITE_ONLY, GL_READ_WRITE).
     * \param level                 The level of the texture.
     *
     * The format of storing data into the image from shaders is the internal format that was used to define the storage of the texture (e.g. see Texture_2D::storage).
     * If the binding to the specified image unit with the same parameters exists, then no bindings performed (including the internal format currently used).
     * See the documentation of glBindImageTexture (e.g. https://www.opengl.org/wiki/GLAPI/glBindImageTexture ).
     */
    void bind_to_image_unit(GLuint unit,GLenum access,GLint level=0);


    /*!
     * \brief Breaks the binding of the texture with the given image unit.
     * \param unit                  The image unit.
     *
     * If the texture isn't bound to the given image unit, then no actions performed.
     */
    void break_image_unit_binding(GLuint unit);

    /*!
     * \brief Breaks the binding of the texture with the given texture unit.
     * \param unit                  The texture unit.
     *
     * If the texture isn't bound to the given texture unit, then no actions performed.
     */
    void break_texture_unit_binding(GLuint unit);

    /*!
    * \brief Sets the texture filtering functions to the given one.
    * \param min_filter             The given texture minifying function (e.g. GL_LINEAR_MIPMAP_LINEAR) (see GL_TEXTURE_MIN_FILTER).
    * \param mag_filter             The given texture magnification function ( GL_LINEAR or GL_NEAREST) (see GL_TEXTURE_MAG_FILTER).
    * \details Binds the texture.
    */
    void filter(GLint min_filter, GLint mag_filter);

    /// \brief Sets the maximum degree of anisotropy (i.e. GL_TEXTURE_MAX_ANISOTROPY_EXT)
    /// \details Binds the texture if the EXT_texture_filter_anisotropic extension is available, otherwise the call is ignored.
    void anisotropy(float value);

    /// Returns an internal format of the texture (e.g. GL_RGBA8) if it is defined.
    GLenum format() const{
        return format_;
    }

    /*!
     * \brief Temporarily binds the given level of the texture to the specified image unit.
     * \param unit                  The index of the image unit.
     * \param access                The type of the access to be performed by shaders   (i.e. GL_READ_ONLY, GL_WRITE_ONLY, GL_READ_WRITE).
     * \param level                 The level of the texture.
     * \param format                The format of storing data into the image from shaders   (e.g. GL_RGBA8, GL_R32F).
     *
     * This method perform only opengl binding of the texture. The binding will not been restored on the texture recreation.
     * This method don't set the format of the texture.
     * See the documentation of glBindImageTexture (e.g. https://www.opengl.org/wiki/GLAPI/glBindImageTexture ).
     */
    void bind_to_image_unit_once(GLuint unit,GLenum access,GLint level,GLenum format) const;

    /*!
     * \brief Temporarily binds the given level of the texture to the specified image unit.
     * \param unit                  The index of the image unit.
     * \param access                The type of the access to be performed by shaders   (i.e. GL_READ_ONLY, GL_WRITE_ONLY, GL_READ_WRITE).
     * \param level                 The level of the texture.
     *
     * This method perform only opengl binding of the texture. The binding will not been restored on the texture recreation.
     * The format of storing data into the image from shaders is the internal format that was used to define the storage of the texture (e.g. see Texture_2D::storage).
     * See the documentation of glBindImageTexture (e.g. https://www.opengl.org/wiki/GLAPI/glBindImageTexture ).
     */
    void bind_to_image_unit_once(GLuint unit,GLenum access,GLint level=0) const;

    /*!
     * \brief Temporarily binds the texture to the given texture unit.
     * \param texture_unit          The given texture unit. 0 is invalid value.
     *
     * This method perform only opengl binding of the texture. The binding will not been restored on the texture recreation.
     * The zero texture unit is reserved to use with generic bindings.
     * If the binding to the texture unit exists, then no bindings performed.
     */
    void bind_to_texture_unit_once(GLuint texture_unit) const;

    // it is only one pointer for a safety.
    virtual ~Texture();
    Texture(Texture&& source);

    //methods:
protected:
    Texture()=default;
    /// Rebinds the texture to the previously used image & texture units.
    void rebind_to_units();

    /// \brief Deletes the underlying texture object and creates a new one.
    /// \details It binds the texture.
    void recreate();

    //data fields
protected:
    /// The target to which texture is bound (e.g. GL_TEXTURE_2D).
    GLenum target_;                             ///< \details the value is defined by the child class.

    unsigned levels_=0;                         ///< the number of mipmap levels.
    GLenum format_;                             ///< The internal format of the texture, e.g. GL_RGBA8.
private:

    using Unit=GLuint;
    GLuint id_=0;

    /*
     * rebind_to_units is mostly used to reestablish the bindings of the recreated texture.
     * However, when the old texture object is deleted, the associated image binding states are cleared,
     * so we can't restore them using only the OpenGL API.
     * This structure is used to save them.
     */
    struct Image_binding{
        GLuint unit;
        GLenum access;
        GLint level;
        GLenum format;
    };

    std::list<Image_binding> image_bindings_;
    std::list<Unit> texture_bindings_;
};

}
