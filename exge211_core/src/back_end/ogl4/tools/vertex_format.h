#pragma once

#include "../../ogl_common/ogl_functions.h"
#include <vector>

namespace exge211 {

struct Attribute_format{
    enum class Type{
        FLOAT,
        INTEGER,
        NORMALIZED,                         // The floating point type with normalized values.
        DOUBLE
    };

    /// The vertex attribute type (in shader).
    Type result_type;

    /// The vertex attribute index.
    GLuint index;

    /// The number of components.
    GLuint size;

    /// \brief The data type for storing the component in the VBO.
    /// \details e.g. GL_BYTE, GL_UNSIGNED_INT ( the type parameter of glVertexAttrib[I/L]Pointer ).
    GLenum type;

    /// The offset of the vertex attribute in the data store of the VBO.
    size_t offset;
};

/*!
 * \brief An utility class for handling the vertex format.
 *
 * It is the wrapper around glVertexAttribPointer.
 * The vertex attribute data is assumed to be interleaved in the VBO.
 */
struct Vertex_format{

    /// Adds the vertex attribute with the specified parameters.
    Vertex_format & add(Attribute_format::Type result_type,GLuint index,GLuint size,GLenum type,size_t offset);

    // 'add' using result_type == FLOAT
    Vertex_format & add_f(GLuint index,GLuint size,GLenum type,size_t offset);
    // 'add' using result_type == INTEGER
    Vertex_format & add_i(GLuint index,GLuint size,GLenum type,size_t offset);
    // 'add' using result_type == NORMALIZED
    Vertex_format & add_n(GLuint index,GLuint size,GLenum type,size_t offset);
    // 'add' using result_type == DOUBLE
    Vertex_format & add_d(GLuint index,GLuint size,GLenum type,size_t offset);

    // To use with the interleaved attribute data.
    /// Sets the size of the data of single vertex to the size of the given type.
    template<typename T>
    Vertex_format & set_stride(){
        stride=sizeof(T);
        return *this;
    }

    /*!
     * \brief Applies the vertex format to the given VBO.
     * \param vbo                       An id of the given VBO.
     * \param instancing_divisor        See glVertexAttribDivisor (https://www.opengl.org/wiki/GLAPI/glVertexAttribDivisor).
     * \param offset                    The offset of the vertex data in the given VBO.
     *
     * instancing_divisor is applied to all vertex attributes.
     * Binds the given VBO to GL_ARRAY_BUFFER.
     */
    void apply(GLuint vbo,GLuint instancing_divisor,size_t offset=0);

    /// Disables the vertex attributes used in the format.
    void disable();

    using Attributes=std::vector<Attribute_format>;

    Attributes attributes;
    GLuint stride;
};

}
