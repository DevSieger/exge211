#include "image_format_traits.h"
#include PUBLIC_HEADER(logging.h)

namespace exge211{

const exge211::Image_format_traits&exge211::Image_format_traits::get(Image_format format)
{
    static const Image_format_traits rgb888         (GL_RGB,    GL_UNSIGNED_BYTE);
    static const Image_format_traits rgba8888       (GL_RGBA,   GL_UNSIGNED_BYTE);
    static const Image_format_traits rgb_f32        (GL_RGB,    GL_FLOAT);
    switch (format) {
        case Image_format::RGB888:      return rgb888;
        case Image_format::RGBA8888:    return rgba8888;
        case Image_format::RGB_32F:     return rgb_f32;
        default:                        {
            log_msg(Detail_level::CRITICAL,"an unexpected format value: %1%", (size_t)format);
            return rgba8888;
        }
    }
}

Image_format_traits::Image_format_traits(GLenum p_format, GLenum p_type):
    p_format_(p_format), p_type_(p_type)
{}

}
