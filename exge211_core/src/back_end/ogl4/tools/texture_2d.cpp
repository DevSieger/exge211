#include "../../../public_headers.h"
#include PUBLIC_HEADER(res/resource_data.h)
#include PUBLIC_HEADER(resource_loader.h)

#include "texture_2d.h"
#include "../../ogl_common/error_checking.h"


namespace exge211 {

Texture_2D::Texture_2D()
{
    target_=GL_TEXTURE_2D;
}

bool Texture_2D::storage(int width, int height,int levels,int format)
{
    recreate();
    bind();
    check_gl_error();
    levels_=levels;
    format_=format;
    size_[0]=width;
    size_[1]=height;
    glTexStorage2D( target_, levels, format, width, height );
    check_gl_error();
    rebind_to_units();
    if(check_gl_error()){
        return false;
    }else{
        return true;
    }
}

}
