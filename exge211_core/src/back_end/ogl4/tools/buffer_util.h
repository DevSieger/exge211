#pragma once

#include "../../ogl_common/ogl_functions.h"

namespace exge211 {

/*!
 * \brief Resizes the back buffer to the given size, copies to it a content of the front buffer and swaps the 'front' and the 'back' values.
 * \param front                 The front buffer.
 * \param back                  The back buffer.
 * \param allocated_size        The size of the front buffer, in bytes.
 * \param requested_size        The new size of the back buffer, in bytes.
 * \param alignment             The buffer alignment size, in bytes.
 * \return false if the call is ignored otherwise true.
 *
 * The value of allocated_size is set to the new size of the new 'front' buffer.
 * The front buffer is cleared (resized to zero size) after the content copying.
 * The GL_STATIC_DRAW usage pattern is set to both buffers.
 * If requested_size < allocated_size then the call is ignored, no actions performed, and the function returns false.
 */
bool swapping_buffer_resize(GLuint& front,GLuint& back, size_t & allocated_size, size_t requested_size ,size_t alignment);

}
