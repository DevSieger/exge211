#include "../../../public_headers.h"
#include PUBLIC_HEADER(res/resource_data.h)
#include PUBLIC_HEADER(logging.h)

#include "texture_array.h"
#include "../../ogl_common/error_checking.h"
#include "image_format_traits.h"

namespace exge211 {

Texture_array::Texture_array()
{
    target_=GL_TEXTURE_2D_ARRAY;
}

void Texture_array::load(const Texture_data & tx, unsigned int index)
{
    if(index>=size()[2]){
         log_msg(Detail_level::CRITICAL,"requested to load index greater than size");
        return;
    }
    if(tx.width!=size()[0] || tx.height!=size()[1]){
        log_msg(Detail_level::CRITICAL,"texture \"%1%\" has wrong size(%2%x%3%) for load to array, must be %4%x%5%",tx.name,tx.width,tx.height,size()[0],size()[1]);
    }
    bind();

    auto& f_traits=Image_format_traits::get(tx.format);

    glTexSubImage3D(target_, 0, 0, 0,index,tx.width, tx.height,1, f_traits.pixel_format(), f_traits.pixel_type(), &tx.data[0]);
    check_gl_error();
}

}
