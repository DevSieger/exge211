#include "vertex_format.h"
#include "../../ogl_common/error_checking.h"


namespace exge211 {

using Attr_type=Attribute_format::Type;

Vertex_format &Vertex_format::add(Attribute_format::Type result_type, GLuint index, GLuint size, GLenum type, size_t offset)
{
    attributes.push_back({result_type,index,size,type,offset});
    return *this;
}

Vertex_format &Vertex_format::add_f(GLuint index, GLuint size, GLenum type, size_t offset)
{
    return add(Attr_type::FLOAT,index,size,type,offset);
}

Vertex_format &Vertex_format::add_i(GLuint index, GLuint size, GLenum type, size_t offset)
{
    return add(Attr_type::INTEGER,index,size,type,offset);
}

Vertex_format &Vertex_format::add_n(GLuint index, GLuint size, GLenum type, size_t offset)
{
    return add(Attr_type::NORMALIZED,index,size,type,offset);
}

Vertex_format &Vertex_format::add_d(GLuint index, GLuint size, GLenum type, size_t offset)
{
    return add(Attr_type::DOUBLE,index,size,type,offset);
}

void Vertex_format::apply(GLuint vbo, GLuint instancing_divisor, size_t offset){
    using AType=Attribute_format::Type;
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    for(Attribute_format & a: attributes ){
        glEnableVertexAttribArray(a.index);
        check_gl_error();
        switch(a.result_type){
            case AType::FLOAT:
                glVertexAttribPointer(a.index,a.size,a.type,GL_FALSE,stride,(GLvoid*)(a.offset+offset));
                break;
            case AType::NORMALIZED:
                glVertexAttribPointer(a.index,a.size,a.type,GL_TRUE,stride,(GLvoid*)(a.offset+offset));
                break;
            case AType::INTEGER:
                glVertexAttribIPointer(a.index,a.size,a.type,stride,(GLvoid*)(a.offset+offset));
                break;
            case AType::DOUBLE:
                glVertexAttribLPointer(a.index,a.size,a.type,stride,(GLvoid*)(a.offset+offset));
                break;
        }
        check_gl_error();
        glVertexAttribDivisor(a.index,instancing_divisor);
        check_gl_error();
    }
}

void Vertex_format::disable()
{
    for(Attribute_format & a: attributes ){
        glDisableVertexAttribArray(a.index);
        check_gl_error();
    }
}

}
