#include "buffer_util.h"
#include "../../ogl_common/error_checking.h"
#include <utility>

namespace exge211 {

bool swapping_buffer_resize(GLuint & front, GLuint & back, size_t & allocated_size, size_t requested_size, size_t alignment)
{
    if(allocated_size < requested_size){
        glBindBuffer(GL_COPY_WRITE_BUFFER,back);
        glBindBuffer(GL_COPY_READ_BUFFER,front);

        size_t new_size=requested_size+alignment-1;
        new_size -= new_size%alignment;

        glBufferData(GL_COPY_WRITE_BUFFER, new_size, NULL, GL_STATIC_DRAW);
        check_gl_error();
        glCopyBufferSubData(GL_COPY_READ_BUFFER,GL_COPY_WRITE_BUFFER,0,0,allocated_size);
        check_gl_error();
        glBufferData(GL_COPY_READ_BUFFER, 0, NULL, GL_STATIC_DRAW);
        check_gl_error();

        std::swap(front,back);
        allocated_size=new_size;
        return true;
    }else{
        return false;
    }
}

}
