#pragma once
#include "tools/texture_2d.h"

namespace exge211 {

/// A helper class to manage utility textures for the SMAA.
class Smaa_textures{
public:
    void init();
private:
    Texture_2D area_tx_;
    Texture_2D search_tx_;
};

}
