#pragma once

#include "../../public_headers.h"
#include PUBLIC_HEADER(res/animation_data.h)

#include <unordered_map>
#include <map>
#include "../resource_factory.h"
#include "storage/material_storage.h"
#include "storage/animated_mesh_storage.h"
#include "storage/static_mesh_storage.h"
#include "storage/texture_storage.h"
#include "storage/skybox_storage.h"
#include "model_data.h"

namespace exge211 {

class Resource_loader;

struct Animated_submodel_proto{
    Batch_group bg;
    Animated_mesh_storage::Index mesh;
    Material_key material_key;
    Material_type type;
};

struct Static_submodel_proto{
    Batch_group bg;
    u32 mesh;
    Material_key material_key;
    Material_type type;
};

class Animated_model_proto: public Model_type_base<Animated_submodel_proto>,public Bounded{};
class Static_model_proto: public Model_type_base<Static_submodel_proto>, public Bounded{};
class Static_instance_storage;

namespace cpu_animator {
class Skeleton_type;
}

class Simple_resource_factory: public Resource_factory
{
public:
    Simple_resource_factory();
    ~Simple_resource_factory();

    bool init(
            Material_storage* mat, Texture_storage* tx,
            Animated_mesh_storage* mesh, Resource_loader const* a_loader,
            Static_mesh_storage* smesh,Static_instance_storage* sinst,
            Skybox_storage* sk
            );

    Simple_resource_factory & loader(Resource_loader const* res_loader){
        loader_=res_loader;
        return *this;
    }

    Model_handle request_amodel(Resource_request const & model,Resource_request const & skin) override;
    bool relax(Model_handle prototype) override;

    Model_instance_base * construct_model(Model_handle proto) override;
    void release(Model_instance_base * model) override;

    Static_model_handle request_smodel(Resource_request const & model,Resource_request const & skin) override;
    bool relax(Static_model_handle prototype) override;

    Static_model_instance_base * construct_model(Static_model_handle proto, Rotation const& rotation, Position const& position, const glm::vec3 & scale) override;
    void release(Static_model_instance_base * model) override;

    Animation_data * request_animation(Resource_request const & animation) override;
    bool relax(Animation_data * prototype) override;

    Skeleton_handle request_skeleton_type(Resource_request const & skeleton_type) override;
    bool relax(Skeleton_handle skeleton_type) override;

    Skeleton_instance_base * construct_skeleton(Skeleton_handle skeleton_type) override;
    void release(Skeleton_instance_base * skeleton) override;

    Skybox_ptr get_skybox( Resource_request const& name) override;

private:
    struct Model_entry{
        Resource_request model;
        Resource_request skin;
        bool operator == (Model_entry const & me)const{
            return me.model==model && me.skin==skin;
        }

        struct Hash{
            size_t operator()(Model_entry const & me)const{
                std::hash<Resource_request> hash;
                return (hash(me.model)*11)^(hash(me.skin)*23);
            }
        };
    };
    /*
     * memory usage?
     */


    //unordered_map не должна портить указатели
    using Animated_model_cache=std::unordered_map<Model_entry,ogl4::Model,Model_entry::Hash>;
    using Static_model_cache=std::unordered_map<Model_entry,ogl4::Static_model,Model_entry::Hash>;
    template<typename Value>
    using Request_cache=std::unordered_map<Resource_request,Value>;

    template<typename Value_type>
    struct Grouped{
        using Value=Value_type;
        Value value;
        Batch_group bg;
    };

    using Material_entry=Grouped<Material_storage::Index>;
    using Texture_entry=Grouped<Texture_storage::Index>;
    using Loaded_skin=std::map<Material_key,Material_entry>;

    Resource_loader const* loader_=nullptr;
    Animated_model_cache amodel_cache_;
    Static_model_cache smodel_cache_;
    Request_cache<const cpu_animator::Skeleton_type* > skel_cache_;
    Request_cache<Animation_data> anim_cache_;
    Request_cache<Material_entry> mat_cache_;
    Request_cache<Loaded_skin> skin_cache_;
    Request_cache<Animated_model_proto> amesh_cache_;
    Request_cache<Static_model_proto> smesh_cache_;
    Request_cache<Texture_entry> tx_cache_[(size_t)Texture_type::TOTAL];
    Request_cache<std::weak_ptr<const Skybox>> sky_cache_;

    Material_storage* mat_storage_ = nullptr;
    Texture_storage* tx_storage_ = nullptr;
    Animated_mesh_storage* amesh_storage_ = nullptr;
    Static_mesh_storage* smesh_storage_ = nullptr;
    Static_instance_storage* st_inst_storage = nullptr;
    Skybox_storage* skybox_storage = nullptr;
    Resource_loader const& loader(){
        return *loader_;
    }

    Model_handle get_amodel( Resource_request const& model, Resource_request const & skin);
    Static_model_handle get_smodel( Resource_request const& model, Resource_request const & skin);
    Loaded_skin const & get_skin(Resource_request const & req);
    Material_entry get_material(Resource_request const & req);
    Texture_entry get_texture(Texture_type type, Resource_request const & req);
    Animated_model_proto const & get_model_proto(Resource_request const & req);
    Static_model_proto const & get_static_model_proto( Resource_request const& req);
    Skeleton_handle get_skeleton( Resource_request const& req);
    Animation_data * get_animation( Resource_request const& req);

    bool is_valid(Animated_model_data const& model);

    bool store(Texture_data_uptr&& tx_ptr, Texture_type type, Texture_storage::Index & ix_out, Batch_group & bg_out);
    bool store(const Material_simple & mat, Material_storage::Index & ix_out, Batch_group & bg_out);
    u32 store(const Animated_model_data::Transforms & transforms);
    bool store(const Animated_mesh & mesh_in, u32 ibp_offset, Animated_mesh_storage::Index & index_out, Batch_group & batch_out);
    bool store(const Static_mesh & mesh_in, Static_mesh_storage::Index & index_out, Batch_group & batch_out);
    bool configure(Texture_data & tx, Texture_type type);
    void install(ogl4::Static_model_instance* model, const Rotation & rotation, const Position & position, const glm::vec3 & scale);
    void uninstall(ogl4::Static_model_instance* model);

    void release_skybox(Resource_request const& name, Skybox_handle h);
};

}
