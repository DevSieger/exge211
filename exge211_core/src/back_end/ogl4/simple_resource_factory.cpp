#include "../../public_headers.h"
#include PUBLIC_HEADER(resource_loader.h)
#include PUBLIC_HEADER(logging.h)

#include "simple_resource_factory.h"
#include "storage/static_instance_storage.h"
#include "../cpu_animator/skeleton_instance.h"
#include "../cpu_animator/skeleton_type.h"
#include <memory>
#include <utility>

//#define DEBUG_TESTING_MSG
#ifdef DEBUG_TESTING_MSG
#define dbg_log(...) log_msg(Detail_level::DEBUG, __VA_ARGS__ );
#else
#define dbg_log(...) ;
#endif


namespace exge211 {

using cpu_animator::Skeleton_type;
using cpu_animator::Skeleton_instance;

Simple_resource_factory::Simple_resource_factory()
{

}

Simple_resource_factory::~Simple_resource_factory()
{
    for(auto & i:skel_cache_){
        delete i.second;
    }
}

bool Simple_resource_factory::init(Material_storage * mat, Texture_storage * tx, Animated_mesh_storage * mesh, Resource_loader const* a_loader, Static_mesh_storage* smesh, Static_instance_storage* sinst, Skybox_storage * sk)
{
    mat_storage_=mat;
    tx_storage_=tx;
    amesh_storage_=mesh;
    smesh_storage_=smesh;
    st_inst_storage=sinst;
    skybox_storage=sk;
    loader(a_loader);
    return mat && tx && mesh;
}

Model_handle Simple_resource_factory::request_amodel(const Resource_request & model, const Resource_request & skin)
{
    return get_amodel(model,skin);
}

bool Simple_resource_factory::relax(Model_handle prototype)
{
    return false;
}

Animation_data *Simple_resource_factory::request_animation(const Resource_request & animation)
{
    return get_animation(animation);
}

bool Simple_resource_factory::relax(Animation_data * prototype)
{
    return false;
}

Skeleton_handle Simple_resource_factory::request_skeleton_type(const Resource_request & skeleton_type)
{
    return get_skeleton(skeleton_type);
}

bool Simple_resource_factory::relax(Skeleton_handle skeleton_type)
{
    return false;
}

Skeleton_instance_base *Simple_resource_factory::construct_skeleton(Skeleton_handle skeleton_type)
{
    assert(skeleton_type);
    Skeleton_type const& sk_type=*static_cast<Skeleton_type const*>(skeleton_type);
    Skeleton_instance_base* ret=new Skeleton_instance(sk_type);
    return ret;
}

void Simple_resource_factory::release(Skeleton_instance_base * skeleton)
{
    Skeleton_instance* sk=static_cast<Skeleton_instance*>(skeleton);
    delete sk;
}

Skybox_ptr Simple_resource_factory::get_skybox(const Resource_request& name)
{
    auto it = sky_cache_.find(name);
    Skybox_ptr ptr;
    if(it == sky_cache_.end()){
        Cube_map_data cubemap;
        for(size_t i=0;i<6;++i){
            auto& cm_data = cubemap.data[i];
            skybox_storage->configure(cm_data);
        }
        loader().load_skybox(name,cubemap);
        Skybox_handle handle  = skybox_storage->add(cubemap);
        ptr = Skybox_ptr(handle, [name,this](Skybox_handle h)->void{ release_skybox(name,h); });
        sky_cache_.emplace(name,ptr);
    }else{
        ptr = it->second.lock();
    }
    return ptr;
}

void Simple_resource_factory::release_skybox( Resource_request const& name, Skybox_handle h)
{
    if( sky_cache_.erase(name)){
        skybox_storage->remove(h);
    }else{
        log_msg(Detail_level::NON_CRITICAL,"attempt to release an unregistered skybox \"%1%\"", name);
    }
}

Model_instance_base* Simple_resource_factory::construct_model(Model_handle proto)
{
    auto  ret = new ogl4::Model_instance(proto);
    return ret;
}

void Simple_resource_factory::release(Model_instance_base * model)
{
    auto ptr = &ogl4::Model_instance::downcast(*model);
    delete ptr;
}

Static_model_handle Simple_resource_factory::request_smodel(const Resource_request & model, const Resource_request & skin)
{
    return get_smodel(model,skin);
}

bool Simple_resource_factory::relax(Static_model_handle prototype)
{
    return false;
}

Static_model_instance_base *Simple_resource_factory::construct_model(Static_model_handle proto, const Rotation & rotation, const Position & position, glm::vec3 const& scale)
{
    ogl4::Static_model_instance* ret=new ogl4::Static_model_instance(proto);
    install(ret,rotation,position,scale);
    return ret;
}

void Simple_resource_factory::release(Static_model_instance_base * model)
{
    ogl4::Static_model_instance* ptr = &ogl4::Static_model_instance::downcast(*model);
    uninstall(ptr);
    delete ptr;
}

Model_handle Simple_resource_factory::get_amodel( Resource_request const& model, const Resource_request & skin)
{
    dbg_log( "requested model %1% with skin %2%",model,skin );
    Model_entry entry{model,skin};
    auto& cache=amodel_cache_;
    auto it=cache.find(entry);
    if(it==cache.end()){

        Loaded_skin const & skin=get_skin(entry.skin);
        Animated_model_proto const & proto=get_model_proto(entry.model);
        ogl4::Model model;
        model.init(proto);
        model.boundary(proto.boundary());

        for( ogl4::Model::Index lod_ix=0 ; lod_ix<proto.lod_count() ; ++lod_ix ){
            auto & lod_proto=proto.lod(lod_ix);
            auto & lod=model.lod(lod_ix);
            for( ogl4::Model::LOD::Size ix=0 ; ix<lod_proto.size() ; ++ix ){
                ogl4::Submodel & sm=lod[ix];
                Animated_submodel_proto const & sm_proto=lod_proto[ix];
                sm.mesh=sm_proto.mesh;
                sm.type=sm_proto.type;

                /* skin binding */
                auto s_it=skin.find(sm_proto.material_key);
                if(s_it==skin.end()){
                    log_msg(Detail_level::CRITICAL,"not found material key %1% for model %2% in skin %3%, 0 used",sm_proto.material_key,entry.model,entry.skin);
                    sm.material=0;
                    //The first material have to be in the first group, so the default-constructed Batch_group object is used. The error has happened anyway.
                }else{
                    sm.material=s_it->second.value;
                    sm.bg=s_it->second.bg;
                }
                sm.bg.mesh(sm_proto.bg);
            }
        }

        auto res=cache.emplace(std::move(entry),std::move(model));
        if(!res.second){
            log_msg(Detail_level::CRITICAL,"model{%1%,%2%} not inserted in cache",entry.model,entry.skin);
        };
        return &res.first->second;
    }else{
        dbg_log("model %1% with skin %2% cached",model,skin);
        return &it->second;
    }
}
Static_model_handle Simple_resource_factory::get_smodel(const Resource_request & model, const Resource_request & skin)
{
    dbg_log( "requested static model %1% with skin %2%",model,skin );
    Model_entry entry{model,skin};
    auto& cache=smodel_cache_;
    auto it=cache.find(entry);
    if(it==cache.end()){
        Loaded_skin const & skin=get_skin(entry.skin);
        Static_model_proto const & proto=get_static_model_proto(entry.model);
        ogl4::Static_model model;
        model.init(proto);
        model.boundary(proto.boundary());

        for( ogl4::Static_model::Index lod_ix=0 ; lod_ix<proto.lod_count() ; ++lod_ix ){
            auto & lod_proto=proto.lod(lod_ix);
            auto & lod=model.lod(lod_ix);
            for( ogl4::Static_model::LOD::Size ix=0 ; ix<lod_proto.size() ; ++ix ){
                ogl4::Submodel & sm=lod[ix];
                Static_submodel_proto const & sm_proto=lod_proto[ix];
                sm.mesh=sm_proto.mesh;
                sm.type=sm_proto.type;

                /* skin binding */
                auto s_it=skin.find(sm_proto.material_key);
                if(s_it==skin.end()){
                    log_msg(Detail_level::CRITICAL,"not found material key %1% for static model %2% in skin %3%, 0 used",sm_proto.material_key,entry.model,entry.skin);
                    sm.material=0;
                }else{
                    sm.material=s_it->second.value;
                    sm.bg=s_it->second.bg;
                    sm.bg.mesh(sm_proto.bg);
                }
            }
        }

        auto res=cache.emplace(std::move(entry),std::move(model));
        if(!res.second){
            log_msg(Detail_level::CRITICAL,"static model{%1%,%2%} not inserted in cache",entry.model,entry.skin);
        };
        return &res.first->second;
    }else{
        dbg_log("static model %1% with skin %2% cached",model,skin);
        return &it->second;
    }
}
Simple_resource_factory::Loaded_skin const& Simple_resource_factory::get_skin(const Resource_request & skin)
{
    dbg_log( "requested skin %1%",skin );
    auto it=skin_cache_.find(skin);
    if(it==skin_cache_.end()){
        Skin_info skin_info;
        Loaded_skin loaded_skin;
        loader().load_skin(skin,skin_info);
        for(auto & it:skin_info){
            Material_entry ix=get_material(it.second);
            loaded_skin.emplace(it.first,ix);
        }
        /*
         * warning: проверить адекватность
         */
        auto res=skin_cache_.emplace(Resource_request(skin),std::move(loaded_skin));
        if(!res.second){
            log_msg(Detail_level::CRITICAL,"skin{%1%} not inserted in cache",skin);
        };
        return res.first->second;
    }else{
        dbg_log("skin %1% cached",skin);
        return it->second;
    }
}

Simple_resource_factory::Material_entry Simple_resource_factory::get_material(const Resource_request & req)
{
    dbg_log( "requested material %1%",req );
    auto it=mat_cache_.find(req);
    if(it==mat_cache_.end()){
        Material_info info;
        Material_simple mat;
        Material_entry entry;
        loader().load_material(req,info);
        Texture_entry tx_a=get_texture(Texture_type::ALBEDO,info.albedo);
        Texture_entry tx_s=get_texture(Texture_type::SPECULAR,info.specular);
        Texture_entry tx_m=get_texture(Texture_type::NORMAL,info.normal);
        mat.albedo=tx_a.value;
        mat.normal=tx_m.value;
        mat.specular=tx_s.value;
        mat.spec_pow=info.shininess;

        if(!store(mat,entry.value,entry.bg) ){
            log_msg(Detail_level::CRITICAL,"can't store material{%1%}",req);
        }
        entry.bg.albedo(tx_a.bg).spec(tx_s.bg).normal(tx_m.bg);

        auto res=mat_cache_.emplace(req,entry);

        if(!res.second){
            log_msg(Detail_level::CRITICAL,"material{%1%} not inserted in cache",req);
        };
        return res.first->second;
    }else{
        dbg_log("material %1% cached",req);
        return it->second;
    }
}

Simple_resource_factory::Texture_entry Simple_resource_factory::get_texture(Texture_type type, const Resource_request & req){
    dbg_log( "requested texture(%2%) %1%",req,(size_t)type );
    auto & cache=tx_cache_[(size_t)type];
    auto it=cache.find(req);
    if(it==cache.end()){
        Texture_data_uptr tx_ptr(new Texture_data);
        configure(*tx_ptr,type);
        loader().load_texture(req,*tx_ptr);
        Texture_entry entry;
        if(!store(std::move(tx_ptr),type,entry.value,entry.bg)){
            log_msg(Detail_level::CRITICAL,"can't store texture{ %1% }",req);
        }

        auto res=cache.emplace(req,entry);
        if(!res.second){
            log_msg(Detail_level::CRITICAL,"texture{%1%} not inserted in cache",req);
        }
        return res.first->second;
    }else{
        dbg_log("texture(%2%) %1% cached",req,(size_t)type );
        return it->second;
    }
}

const Animated_model_proto &Simple_resource_factory::get_model_proto(const Resource_request & req)
{
    dbg_log( "requested model proto %1%",req );
    auto& cache=amesh_cache_;
    auto it=cache.find(req);
    if(it==cache.end()){
        Animated_model_data data;
        Animated_model_proto proto;

        loader().load_model(req,data);

        if(!is_valid(data)){
            //i don't see any good way to handle situation, so let's write message about this error to the log and try to ignore error.
            //return value in this case is default constructed Animated_model_proto

            log_msg(Detail_level::CRITICAL,"trying to load invalid model \"%1%\"",req);
        }else{

            proto.init(data);
            proto.boundary(data.boundary());

            std::vector<u32> ibp_offsets;
            for(auto& ibp: data.transforms()){
                ibp_offsets.push_back( store(ibp));
            }

            for( Animated_model_data::Index lod_ix=0 ; lod_ix<data.lod_count() ; ++lod_ix ){
                auto & lod_proto=proto.lod(lod_ix);
                auto & lod_data=data.lod(lod_ix);
                for( Animated_model_data::LOD::Size ix=0 ; ix<lod_data.size() ; ++ix ){
                    Animated_submodel_proto & mesh_proto=lod_proto[ix];
                    Animated_submodel_data const & mesh_data=lod_data[ix];
                    mesh_proto.type=mesh_data.type;
                    mesh_proto.material_key=mesh_data.material;
                    const u32 ibp_ix=ibp_offsets[mesh_data.mesh.transform_ix];
                    store(mesh_data.mesh,ibp_ix,mesh_proto.mesh,mesh_proto.bg);
                }
            }
        }
        auto res=cache.emplace(Resource_request(req),std::move(proto));
        if(!res.second){
            log_msg(Detail_level::CRITICAL,"mesh{%1%} not inserted in cache",req);
        }
        return res.first->second;
    }else{
        dbg_log("model proto %1% cached",req);
        return it->second;
    }
}

const Static_model_proto &Simple_resource_factory::get_static_model_proto(const Resource_request & req)
{
    dbg_log( "requested model proto %1%",req );
    auto& cache=smesh_cache_;
    auto it=cache.find(req);
    if(it==cache.end()){
        Static_model_data data;
        Static_model_proto proto;

        loader().load_static_model(req,data);

        proto.init(data);
        proto.boundary(data.boundary());

        for( Static_model_data::Index lod_ix=0 ; lod_ix<data.lod_count() ; ++lod_ix ){
            auto & lod_proto=proto.lod(lod_ix);
            auto & lod_data=data.lod(lod_ix);
            for( Static_model_data::LOD::Size ix=0 ; ix<lod_data.size() ; ++ix ){
                Static_submodel_proto & mesh_proto=lod_proto[ix];
                Static_submodel_data const & mesh_data=lod_data[ix];
                mesh_proto.type=mesh_data.type;
                mesh_proto.material_key=mesh_data.material;
                store(mesh_data.mesh,mesh_proto.mesh,mesh_proto.bg);
            }
        }

        auto res=cache.emplace(Resource_request(req),std::move(proto));
        if(!res.second){
            log_msg(Detail_level::CRITICAL,"mesh{%1%} not inserted in cache",req);
        }
        return res.first->second;
    }else{
        dbg_log("model proto %1% cached",req);
        return it->second;
    }
}

Skeleton_handle Simple_resource_factory::get_skeleton(const Resource_request & req)
{
    auto it=skel_cache_.find(req);
    if(it==skel_cache_.end()){
        Skeleton_info info;
        loader().load_skeleton(req,info);
        Skeleton_type * sk=new Skeleton_type(info);
        auto res=skel_cache_.emplace(req,sk);
        if(!res.second){
            log_msg(Detail_level::CRITICAL,"skeleton{%1%} not inserted in cache",req);
        }
        return res.first->second;
    }else{
        dbg_log("skeleton %1% cached",req);
        return it->second;
    }
}

Animation_data *Simple_resource_factory::get_animation(const Resource_request & req)
{
    auto it=anim_cache_.find(req);
    if(it==anim_cache_.end()){
        Animation_data anim;
        loader().load_animation(req,anim);
        auto res=anim_cache_.emplace(req,std::move(anim));
        if(!res.second){
            log_msg(Detail_level::CRITICAL,"animation{%1%} not inserted in cache",req);
        }
        return &res.first->second;
    }else{
        dbg_log("animation %1% cached",req);
        return &it->second;
    }
}

bool Simple_resource_factory::is_valid(const Animated_model_data& model)
{
    //only minimal checking
    return !model.transforms().empty() && model.lod_count();
}


bool Simple_resource_factory::store(Texture_data_uptr && tx_ptr, Texture_type type, Texture_storage::Index & ix_out, Batch_group & bg_out)
{
#ifndef DEBUG_TESTING_MSG
    return tx_storage_->store(std::move(tx_ptr),type,ix_out,bg_out);
#else
    return true;
#endif

}

bool Simple_resource_factory::store(const Material_simple & mat, Material_storage::Index & ix_out, Batch_group & bg_out)
{
#ifndef DEBUG_TESTING_MSG
    return mat_storage_->store(mat,ix_out,bg_out);
#else
    return true;
#endif
}

u32 Simple_resource_factory::store(const Animated_model_data::Transforms& transforms)
{
    return amesh_storage_->add(transforms);
}

bool Simple_resource_factory::store(const Animated_mesh & mesh_in, u32 ibp_offset, Animated_mesh_storage::Index & index_out, Batch_group & batch_out)
{
#ifndef DEBUG_TESTING_MSG
    amesh_storage_->add(mesh_in,ibp_offset, index_out,batch_out);
#endif
    return true;
}

bool Simple_resource_factory::store(const Static_mesh & mesh_in, Static_mesh_storage::Index & index_out, Batch_group & batch_out)
{
#ifndef DEBUG_TESTING_MSG
    smesh_storage_->add(mesh_in,index_out,batch_out);
#endif
    return true;
}

bool Simple_resource_factory::configure(Texture_data & tx, Texture_type type)
{
    return tx_storage_->configure(tx,type);
}

void Simple_resource_factory::install(ogl4::Static_model_instance * model, const Rotation & rotation, const Position & position,glm::vec3 const& scale)
{
#ifndef DEBUG_TESTING_MSG
    st_inst_storage->install(model,rotation,position,scale);
#endif
}

void Simple_resource_factory::uninstall(ogl4::Static_model_instance * model){
#ifndef DEBUG_TESTING_MSG
    st_inst_storage->uninstall(model);
#endif
}



}
