#pragma once

#include "../../../public_headers.h"
#include PUBLIC_HEADER(res/resource_data.h)

#include "../../ogl_common/ogl_functions.h"
#include "../../ogl_common/buffer/ssbo.h"
#include "../binding_points.h"
#include "../tools/vertex_format.h"
#include "batch_group.h"
#include <vector>
#include <dk/concepts.h>

namespace exge211 {

/*!
 * \brief A class to manage a storage of the animated meshes.
 *
 * Since the mesh depends on the binding pose, the binding pose data are handled too.
 */
class Animated_mesh_storage: dk::Non_copyable<Animated_mesh_storage>{
public:
    using Index=u32;
    using Buffer_index=unsigned int;

    struct Mesh_info{
        //to use with the `Draw_inderect_command` structure
        u32 first_index;
        u32 index_count;
        u32 base_vertex;

        u32 transform_offset; ///< the starting index of the mesh's binding pose in the SSBO.
    };

    Animated_mesh_storage();

    void init();

    /*!
     * \brief Adds a new mesh with the given data to the storage.
     * \param mesh_in       The given mesh data. transform_ix is ignored.
     * \param ibp_offset    The offset of the binding pose to use with the mesh. It have to be obtained using Animated_mesh_storage::add(Animated_model_data::Transforms const&)
     * \param index_out     A variable to output the index of the new mesh.
     * \param batch_inout   The resulting batch group, a value of Batch_group::mesh is updated.
     */
    void add(Animated_mesh const & mesh_in, u32 ibp_offset, Index & index_out,Batch_group & batch_inout);   //TODO: make index_out the return value.

    /*!
     * \brief Adds the given binding pose to the storage.
     * \param ibp           The given binding pose.
     * \return              The offset of the binding pose in the SSBO.
     */
    u32 add(Animated_model_data::Transforms const& ibp);

    /// Binds the SSBO containing binding poses to the given storage block of the specified shader.
    void bind(ogl::Shader_program & shader, const char * block_name);

    /// Commits all deferred actions, e.g. upload data to the GPU memory.
    void commit();

    /// Sets up the bindings of managed objects to match the specified batch.
    void set_up(Batch_group batch_group);

    /// Returns an information about the specified mesh.
    Mesh_info const & operator[](Index index) const{
        return registry_[index];
    }

    //TODO: change this to "binding_is_broken" or remove.
    Function<void()> buffer_replaced;

    ~Animated_mesh_storage();
private:
    using Vertex_store=std::vector<Animated_vertex>;
    using Index_store=std::vector<Triangle>;
    using Registry=std::vector<Mesh_info>;

    Registry registry_;


    Vertex_format v_format_;    ///< the vertex format
    size_t vbo_size_=0;         //in bytes
    size_t ibo_size_=0;         //in bytes
    size_t vertex_end_=0;       //an index
    size_t triangles_end_=0;    //an index

    //Back buffers are used to resize the VBO via a content copying.
    GLuint vbo_=0;
    GLuint vbo_back_=0;
    GLuint ibo_=0;              //the index buffer
    GLuint ibo_back_=0;

    bool commited_=true;

    Vertex_store vertices_;     ///< The vertex data to upload to the GPU.
    Index_store indices_;       ///< The index data to upload to the GPU.
    ogl::SSBO<void,Joint> transforms_{ssbo_binding_points::MESH_INV_BP_TRANSFROMS};
};

}
