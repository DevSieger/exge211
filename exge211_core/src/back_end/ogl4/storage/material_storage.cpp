#include "material_storage.h"

namespace exge211 {

Material_storage::Material_storage()
{
    ssbo_.size_align(sizeof(Material_simple)*32);
    ssbo_.array_data().reserve(32);
}

void Material_storage::init()
{
}

bool Material_storage::store(const Material_simple & mat, Index & ix_out, Batch_group & bg_out)
{
    ix_out=ssbo_.array_data().size();
    ssbo_.array_data().emplace_back(mat);
    invalidated_=true;
    return true;
}

void Material_storage::bind(ogl::Shader_program & sp, const char * block_name)
{
    ssbo_.set_binding(sp,block_name);
}

void Material_storage::commit()
{
    if(invalidated_){
        ssbo_.commit();
        invalidated_=false;
    }
}

}
