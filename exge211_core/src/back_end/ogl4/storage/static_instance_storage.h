#pragma once

#include "../../../public_headers.h"
#include PUBLIC_HEADER(vector_types.h)

#include "../model_data.h"
#include "../../ogl_common/ogl_functions.h"
#include "../tools/vertex_format.h"
#include <vector>
#include <dk/concepts.h>

namespace exge211 {

/// A class to manage a storage of the instance data of the sceneries.
class Static_instance_storage: dk::Non_copyable<Static_instance_storage>{
public:
    Static_instance_storage();

    /*!
     * \brief Adds a new instance data using the specified parameters, and binds the given model object to it.
     * \param model             The given model object.
     * \param rotation          The rotation of the object.
     * \param position          The location of the object.
     * \param scale             The coordinatewise scale of the object.
     */
    void install(ogl4::Static_model_instance * model, const Rotation & rotation, const Position & position, glm::vec3 const& scale);

    /*!
     * \brief Removes the instance data of the given model object.
     * \param model             The given model object.
     */
    void uninstall(ogl4::Static_model_instance * model);

    void init();

    /// Commits all deferred actions, e.g. upload data to the GPU memory.
    void commit();

    /// Binds the managed VBO.
    void set_up();

    Function<void()> buffer_replaced;

    ~Static_instance_storage();

private:
    using Index=u32;

    struct Instance_data{
        u32 mat;                        // the material
        Position pos;                   // the position
        Rotation rot;                   // the rotation
        glm::vec3 scale;                // the coordinatewise scale
    };

    struct Cache_entry{
        Cache_entry(Index ix, Instance_data const& d):
            data(d),index(ix){
        }
        Instance_data data;
        Index index;    // The instance index in the VBO.
    };

    using Cache=std::vector<Cache_entry>;

    /// Resizes the VBO to contain the given size.
    void grow(Index requested_size);

    /// \brief Adds the new instance with the given data.
    /// \return     The new instance index.
    Index add(Instance_data const& data);

    /// Removes the instance data with the given index.
    void remove(Index index);

    /// Unused indices.
    std::vector<Index> unused_;

    Vertex_format format_;

    GLuint bo_=0;               ///< The name of the active VBO.
    //The auxiliary VBO are used to resize the VBO via a content copying.
    GLuint bo_back_=0;          ///< The name of the auxiliary VBO.

    size_t allocated_=0;        ///< How much instances the allocated space can contain.
    size_t requested_=0;        ///< How much instances the allocated space should contain.

    Cache cache_;               ///< The data to upload to the GPU.
    bool commited_=true;
};

}
