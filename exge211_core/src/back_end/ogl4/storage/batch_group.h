#pragma once

#include <cstdint>

namespace exge211 {

// Be careful! Many (all?) arithmetic operations require operands to be big enough, at least 'int',
// and the integral promotion happens: e.g. unsigned char -> signed(!) int.
using Batch_subgroup=unsigned char;

/// A class for defining an object groups whose elements may be drawn at once.
class Batch_group{
public:
    using Value=std::uint32_t;

    /// Returns the batch index for the albedo texture.
    Batch_subgroup albedo()const{
        return key(ALBEDO_INDEX);
    }
    /// Sets the batch index for the albedo to the given value.
    Batch_group & albedo(Batch_subgroup value){
        return key(ALBEDO_INDEX,value);
    }
    /// Sets the albedo batch index to the albedo batch index of the given batch group.
    Batch_group & albedo(Batch_group const & other){
        return key(ALBEDO_INDEX,other);
    }

    /// Returns the batch index for the specular texture.
    Batch_subgroup spec()const{
        return key(SPECULAR_INDEX);
    }
    /// Sets the batch index for the specular texture to the given value.
    Batch_group & spec(Batch_subgroup value){
        return key(SPECULAR_INDEX,value);
    }
    /// Sets the specular batch index to the specular batch index of the given batch group.
    Batch_group & spec(Batch_group const & other){
        return key(SPECULAR_INDEX,other);
    }

    /// Returns the batch index for the normal map.
    Batch_subgroup normal()const{
        return key(NORMAL_MAP_INDEX);
    }
    /// Sets the batch index for the normal map to the given value.
    Batch_group & normal(Batch_subgroup value){
        return key(NORMAL_MAP_INDEX,value);
    }
    /// Sets the noramal map batch index to the noramal map batch index of the given batch group.
    Batch_group & normal(Batch_group const & other){
        return key(NORMAL_MAP_INDEX,other);
    }

    /// Returns the mesh batch index.
    Batch_subgroup mesh()const{
        return key(MESH_INDEX);
    }
    /// Sets the mesh batch index to the given value.
    Batch_group & mesh(Batch_subgroup value){
        return key(MESH_INDEX,value);
    }
    /// Sets the mesh batch index to the mesh batch index of the given batch group.
    Batch_group & mesh(Batch_group const & other){
        return key(MESH_INDEX,other);
    }

    bool operator==(Batch_group const& rhv)const{
        return data_==rhv.data_;
    }
    bool operator!=(Batch_group const& rhv)const{
        return data_!=rhv.data_;
    }

    Batch_group()=default;

    /// Returns the version of this object to use with the shadow map rendering.
    Batch_group shadow_pass(){
        return Batch_group(data_ & shadow_pass_mask);
    }

    /// Returns the unsigned integer used for sorting.
    Value value() const{
        return data_;
    }

private:
    using Ix=unsigned int;
    using Bit_shift=unsigned int;
    using Bit_mask=Value;

    enum: Ix{
        ALBEDO_INDEX=0,
        SPECULAR_INDEX=1,
        NORMAL_MAP_INDEX=2,
        MESH_INDEX=3
    };

    static constexpr Bit_mask shadow_pass_mask=~(0xffffff); // Resets albedo, specular and normal map batch indices.

    constexpr explicit Batch_group(Value val): data_(val){}

    Batch_group & key(Ix ix,Batch_subgroup val){
        at(ix)=val;
        return *this;
    }
    Batch_group & key(Ix ix,Batch_group const & other){
        at(ix)=other.at(ix);
        return *this;
    }
    Batch_subgroup key(Ix ix)const{
        return at(ix);
    }

    // May be it is worth to rewrite this using bitwise operators.
    Batch_subgroup & at(Ix i){
        return reinterpret_cast<Batch_subgroup * >(&data_)[i];
    }
    Batch_subgroup const & at(Ix i) const{
        return reinterpret_cast<const Batch_subgroup * >(&data_)[i];
    }

    Value data_=0;
};

}
