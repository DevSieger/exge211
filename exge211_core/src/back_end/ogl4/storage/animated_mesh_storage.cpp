#include "../../../public_headers.h"
#include PUBLIC_HEADER(logging.h)

#include "animated_mesh_storage.h"
#include <glm/trigonometric.hpp>
#include <glm/geometric.hpp>
#include "../tools/buffer_util.h"

namespace exge211 {

namespace{

constexpr GLsizeiptr BUFFER_ALIGNMENT= 1<<22;

}

Animated_mesh_storage::Animated_mesh_storage()
{
    using AType=Attribute_format::Type;
    v_format_.set_stride<Animated_vertex>();
    v_format_.attributes={
        {AType::FLOAT,0,3,GL_FLOAT,offsetof(Animated_vertex,Animated_vertex::pos)},   //for IDE searches
        {AType::FLOAT,1,3,GL_FLOAT,offsetof(Animated_vertex,Animated_vertex::n)},
        {AType::FLOAT,2,3,GL_FLOAT,offsetof(Animated_vertex,Animated_vertex::t)},
        {AType::FLOAT,3,3,GL_FLOAT,offsetof(Animated_vertex,Animated_vertex::b)},
        {AType::FLOAT,4,2,GL_FLOAT,offsetof(Animated_vertex,Animated_vertex::uv)},
        {AType::FLOAT,5,4,GL_FLOAT,offsetof(Animated_vertex,Animated_vertex::weight)},
        {AType::INTEGER,6,4,GL_INT,offsetof(Animated_vertex,Animated_vertex::joint)}
    };
}

void Animated_mesh_storage::init()
{
    if(vbo_){
        log_msg(Detail_level::CRITICAL,"attempted to initialize already initialized Mesh Storage");
        return;
    }
    GLuint buffers[4];
    glGenBuffers(4,buffers);

    vbo_=buffers[0];
    vbo_back_=buffers[1];
    ibo_=buffers[2];
    ibo_back_=buffers[3];
    vbo_size_=BUFFER_ALIGNMENT;
    ibo_size_=BUFFER_ALIGNMENT;

    glBindBuffer(GL_COPY_WRITE_BUFFER,vbo_);
    glBufferData(GL_COPY_WRITE_BUFFER, vbo_size_, NULL, GL_STATIC_DRAW);
    check_gl_error();

    glBindBuffer(GL_COPY_WRITE_BUFFER,ibo_);
    glBufferData(GL_COPY_WRITE_BUFFER, ibo_size_, NULL, GL_STATIC_DRAW);
    check_gl_error();
}

u32 Animated_mesh_storage::add(Animated_model_data::Transforms const& ibp)
{
    auto& tr_storage=transforms_.array_data();
    u32 ret=tr_storage.size();
    tr_storage.insert(tr_storage.end(),ibp.begin(),ibp.end());
    return ret;
}

void Animated_mesh_storage::add(Animated_mesh const& mesh_in, u32 ibp_offset, Animated_mesh_storage::Index & index_out, Batch_group & batch_out)
{
    index_out=registry_.size();

    Mesh_info info;
    info.first_index=triangles_end_*3;
    info.index_count=mesh_in.indices.size()*3;
    info.base_vertex=vertex_end_;
    info.transform_offset=ibp_offset;

    registry_.push_back(info);

    vertices_.insert(vertices_.end(),mesh_in.vertices.begin(),mesh_in.vertices.end());
    vertex_end_+=mesh_in.vertices.size();

    indices_.insert(indices_.end(),mesh_in.indices.begin(),mesh_in.indices.end());
    triangles_end_+=mesh_in.indices.size();

    commited_=false;
}

void Animated_mesh_storage::bind(ogl::Shader_program & shader, const char * block_name)
{
    transforms_.set_binding(shader,block_name);
}

void Animated_mesh_storage::commit()
{
    if(commited_) return;

    if(
            swapping_buffer_resize(vbo_,vbo_back_,vbo_size_,vertex_end_*sizeof(Animated_vertex),BUFFER_ALIGNMENT)||
            swapping_buffer_resize(ibo_,ibo_back_,ibo_size_,triangles_end_*sizeof(Triangle),BUFFER_ALIGNMENT)
            ){
        buffer_replaced();
    }

    assert(!vertices_.empty());
    assert(!indices_.empty());

    size_t vertex_copy_begin=vertex_end_-vertices_.size();
    size_t triangle_copy_begin=triangles_end_-indices_.size();

    glBindBuffer(GL_COPY_WRITE_BUFFER,vbo_);
    glBufferSubData(GL_COPY_WRITE_BUFFER,vertex_copy_begin*sizeof(Animated_vertex),vertices_.size()*sizeof(Animated_vertex),&vertices_[0]);
    check_gl_error();

    glBindBuffer(GL_COPY_WRITE_BUFFER,ibo_);
    glBufferSubData(GL_COPY_WRITE_BUFFER,triangle_copy_begin*sizeof(Triangle),indices_.size()*sizeof(Triangle),&indices_[0]);
    check_gl_error();

    vertices_.clear();
    indices_.clear();
    transforms_.commit();

    commited_=true;
}

void Animated_mesh_storage::set_up(Batch_group batch_group)
{
    v_format_.apply(vbo_,0,0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,ibo_);
}

Animated_mesh_storage::~Animated_mesh_storage()
{
    if(vbo_){
        GLuint buffers[4]={vbo_,vbo_back_,ibo_,ibo_back_};
        glDeleteBuffers(4,buffers);
        vbo_=vbo_back_=ibo_=ibo_back_=0;
    }
}

}
