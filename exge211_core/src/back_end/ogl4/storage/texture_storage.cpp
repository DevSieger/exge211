#include "../../../public_headers.h"
#include PUBLIC_HEADER(logging.h)

#include "texture_storage.h"

namespace exge211 {

namespace{

void set_up_batch_index(Texture_type type,Batch_subgroup value, Batch_group & batch){
    switch (type) {
        case Texture_type::ALBEDO:
            batch.albedo(value);
            break;
        case Texture_type::SPECULAR:
            batch.spec(value);
            break;
        case Texture_type::NORMAL:
            batch.normal(value);
            break;
        default:
            break;
    }
}

}

bool Texture_storage::configure(Texture_config & tx, Texture_type type)
{
    tx=get_data(type).config;
    return true;
}

bool Texture_storage::store(Texture_data_uptr && tx_ptr, Texture_type type, Index & ix_out, Batch_group & bg_out)
{
    Data& data=get_data(type);

    assert(!data.array_tx.empty());

    size_t txt_ix=data.array_tx.size()-1 + (data.pending_upload.size() / max_index() );
    set_up_batch_index(type,txt_ix,bg_out);

    if(data.current==max_index()){
        data.current=0;
    }
    ix_out=data.current++;
    data.pending_upload.emplace_back(std::move(tx_ptr));
    return true;
}

void Texture_storage::init(Size_value width, Size_value height)
{
    for(auto tx_type: {Texture_type::ALBEDO,Texture_type::SPECULAR, Texture_type::NORMAL}){
        Texture_config& config=get_data(tx_type).config;
        config.height  = width;
        config.width   = height;
    }

    get_data(Texture_type::ALBEDO).config.format    = Image_format::RGBA8888;
    get_data(Texture_type::SPECULAR).config.format  = Image_format::RGB888;
    get_data(Texture_type::NORMAL).config.format    = Image_format::RGB888;

    add_texture(Texture_type::ALBEDO);
    add_texture(Texture_type::SPECULAR);
    add_texture(Texture_type::NORMAL);
}

void Texture_storage::set_up(Batch_group batch)
{
    for(Texture_type type: {Texture_type::ALBEDO,Texture_type::SPECULAR, Texture_type::NORMAL} ) {
        Data& data=get_data(type);
        Binding_index batch_value=get_batch_value(type,batch); //can't be invalid_binding(): sizeof(Batch_subgroup) < sizeof(Binding_index)
        if( batch_value!=data.current_binding ){
            assert(batch_value<data.array_tx.size());
            data.array_tx[batch_value].bind_to_texture_unit(data.texture_unit);
            data.current_binding=batch_value;
        }
    }
}

void Texture_storage::bind(GLuint texture_unit, Texture_type type)
{
    Data& data=get_data(type);
    data.texture_unit=texture_unit;
    data.current_binding=invalid_binding();
}

void Texture_storage::commit()
{
    for(Texture_type tx_type: {Texture_type::ALBEDO,Texture_type::SPECULAR, Texture_type::NORMAL}){

        Data& data=get_data(tx_type);

        if(data.pending_upload.empty()) continue;

        assert(data.uploaded<=max_index());

        //to simplify mipmap updating, see below.
        if(data.uploaded==max_index()){
            add_texture(tx_type);
        }

        for(Texture_data_uptr& ptr: data.pending_upload){

            if(data.uploaded==max_index()){ //it is impossible on the first iteration, if max_index()!=0
                data.array_tx.back().update_mipmaps();
                add_texture(tx_type);
            }

            Texture_array& tx=data.array_tx.back();
            tx.load(*ptr,data.uploaded);
            ++data.uploaded;
        }

        data.array_tx.back().update_mipmaps();
        data.pending_upload.clear();
    }
}

Texture_storage::Texture_storage()
{
    for(Data& d:data_){
        d.current_binding=invalid_binding();
    }
}

void Texture_storage::add_texture(Texture_type type)
{
    Data& data=get_data(type);
    Texture_config const& config=data.config;
    data.array_tx.emplace_back();
    Texture_array& tx=data.array_tx.back();
    int format=GL_RGBA8;
    if(type==Texture_type::ALBEDO) format=GL_SRGB8_ALPHA8;
    tx.storage(config.width,config.height,max_index(),8,format);
    tx.anisotropy(4);
    tx.filter(GL_LINEAR_MIPMAP_LINEAR,GL_LINEAR);
    data.uploaded=0;
}

}
