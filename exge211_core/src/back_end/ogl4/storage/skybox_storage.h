#pragma once

#include "../tools/cube_map.h"
#include "../../../front_end/scene/skybox.h"
#include <forward_list>
#include "../../../private_handles.h"

namespace exge211 {

struct Cube_map_data;
struct Texture_config;

/// A class for managing the skybox texture.
class Skybox_storage
{
public:

    void configure(Texture_config& tx_config);

    /// Binds the skybox texture to the given texture unit.
    void bind(GLuint texture_unit);

    // returns nullptr on failure.
    Skybox_handle add(Cube_map_data const &data);
    void remove(Skybox_handle skybox);
    void use(Skybox_handle skybox);

private:

    struct Skybox_impl final: Skybox{
        Cube_map tx_;
    };

    using Container = std::forward_list<Skybox_impl>;
    Container skies_;
    GLuint binding_point_;
};

}
