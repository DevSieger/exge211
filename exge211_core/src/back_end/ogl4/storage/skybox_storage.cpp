#include "../../../public_headers.h"
#include PUBLIC_HEADER(logging.h)
#include PUBLIC_HEADER(res/resource_data.h)

#include "skybox_storage.h"

namespace exge211 {

void Skybox_storage::configure(Texture_config& tx_config)
{
    tx_config.format=Image_format::RGB_32F;
}

void Skybox_storage::bind(GLuint texture_unit)
{
    binding_point_ = texture_unit;
}

Skybox_handle Skybox_storage::add(const Cube_map_data& data)
{
    skies_.emplace_front();
    Skybox_impl& sk = skies_.front();

    Texture_data const& tx=data.data[0];
    if(!tx.width || !tx.height){
        log_msg(Detail_level::NON_CRITICAL,"skybox failed to load");
        return nullptr;
    }

    sk.tx_.storage(tx.width,tx.height,3,GL_R11F_G11F_B10F);
    sk.tx_.load(data);
    sk.tx_.filter(GL_LINEAR_MIPMAP_LINEAR,GL_LINEAR);
    sk.tx_.update_mipmaps();

    return &sk;
}

void Skybox_storage::remove(Skybox_handle skybox)
{
    skies_.remove_if([skybox](Skybox_impl const& element) { return &element == skybox;  });
}

void Skybox_storage::use(Skybox_handle skybox)
{
    // where is no texture recreations.
    static_cast<const Skybox_impl*>(skybox)->tx_.bind_to_texture_unit_once(binding_point_);
}

}
