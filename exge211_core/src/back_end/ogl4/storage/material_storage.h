#pragma once
#include "../../../public_headers.h"
#include PUBLIC_HEADER(common_types.h)

#include "../../ogl_common/ogl_functions.h"
#include "../../ogl_common/buffer/ssbo.h"
#include "../../ogl_common/shader/shader_program.h"
#include "texture_storage.h"
#include "batch_group.h"
#include <vector>
#include <dk/concepts.h>
#include "../binding_points.h"

namespace exge211 {

struct Material_simple{
    u32 albedo      = 0;    //the index of the albedo texture in the texture array.
    u32 specular    = 0;    //the index of the specular texture.
    u32 normal      = 0;    //the index of the normal map texture.
    float spec_pow  = 12;   // the shininess from the Phong reflection model.
};

/// A class for managing a storage of the materials.
class Material_storage: dk::Non_copyable<Material_storage>
{
public:
    Material_storage();
    using Index=GLuint;
    void init();

    /*!
     * \brief Adds the given material to the storage.
     * \param mat           The given material.
     * \param ix_out        A variable to output the index of the new mesh.
     * \param bg_out        The resulting batch group, actually unused.
     * \return              true on success.
     *
     * \todo Remove bg_out.
     * \todo Make index_out the return value.
     */
    bool store(Material_simple const & mat,Index & ix_out,Batch_group & bg_out);

    /// Binds the materail SSBO to the given storage block of the specified shader.
    void bind(ogl::Shader_program & sp,const char * block_name);

    /// Commits all deferred actions, e.g. upload data to the GPU memory.
    void commit();
private:
    //TODO: store content only in GPU memory
    ogl::SSBO<void,Material_simple> ssbo_{ssbo_binding_points::MATERIAL};
    bool invalidated_=false;

};

}
