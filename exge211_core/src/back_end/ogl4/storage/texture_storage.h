#pragma once

#include "../../../public_headers.h"
#include PUBLIC_HEADER(res/resource_data.h)

#include "../tools/texture_array.h"
#include "batch_group.h"
#include <dk/concepts.h>
#include <memory>
#include <vector>

namespace exge211 {

enum class Texture_type: size_t{
    ALBEDO=0,
    SPECULAR=1,
    NORMAL=2,   //The normal map.
    TOTAL=3
};

using Texture_data_uptr=std::unique_ptr<Texture_data>;

class Texture_storage: dk::Non_copyable<Texture_storage>
{
public:

    using Index=unsigned int;

    /*!
     * \brief Sets the given config to match the storage.
     * \param tx            The given config.
     * \param type          The texture type.
     * \return              true on success.
     */
    bool configure(Texture_config & tx, Texture_type type);

    /*!
     * \brief Adds the given texture to the storage.
     * \param tx_ptr        The pointer to the texture.
     * \param type          The texture type.
     * \param ix_out        A variable to output the index of the stored texture.
     * \param bg_out        The resulting batch group. The batch index corresponding to the texture type is updated.
     * \return              true on success (always).
     *
     * The unique pointer is used to avoid a copying for the deferred uploading.
     * The upload is deferred because dependency on the OpenGL context.
     */
    bool store(Texture_data_uptr && tx_ptr,Texture_type type, Index & ix_out, Batch_group & bg_out);

    using Size_value=Texture_config::Size_value;
    /*!
     * \brief Initializes the storage to use the specified texture configuration.
     */
    void init(Size_value width, Size_value height);

    /// Binds the texture objects which match the specified batch.
    void set_up(Batch_group batch);

    /// Sets the texture objects of the specified type to use the given texture unit.
    void bind(GLuint texture_unit,Texture_type type);

    /// Commits all deferred actions, e.g. upload data to the GPU memory.
    void commit();

    Texture_storage();

private:

    using Binding_index=u32;

    struct Data{
        std::vector<Texture_array> array_tx;
        std::vector<Texture_data_uptr> pending_upload;
        Texture_config config;
        Index current=0;

        /// The number of the textures uploaded to the last texture array.
        Index uploaded=0;
        GLuint texture_unit=0;

        /// The index of the binded texture array.
        Binding_index current_binding;
    };

    /// Returns the size of the texture arrays.
    Index max_index() const{
        return 64;
    }
    /// \brief The maximum number of the texture arrays.
    /// \details Depends on the maximum value of the batch index.
    static constexpr size_t max_textures(){
        return 1<<(sizeof(Batch_subgroup)*8);
    }

    static constexpr Binding_index invalid_binding(){
        return 0xffffffff;
    }

    /// Adds a new texture array of the given type.
    void add_texture(Texture_type type);

    Data const& get_data(Texture_type type)const{
        return data_[(size_t)type];
    }
    Data& get_data(Texture_type type){
        return data_[(size_t)type];
    }

    /// Gets the batch index of the given batch group for the specified texture type.
    inline Binding_index get_batch_value(Texture_type type, Batch_group const& batch);

    Data data_[(size_t)Texture_type::TOTAL];

};


Texture_storage::Binding_index Texture_storage::get_batch_value(Texture_type type, const Batch_group& batch){
    switch (type) {
        case Texture_type::ALBEDO:
            return batch.albedo();
        case Texture_type::SPECULAR:
            return batch.spec();
        case Texture_type::NORMAL:
            return batch.normal();
        default:
            return 0;
    }
}

}
