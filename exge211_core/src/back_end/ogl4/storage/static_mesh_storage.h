#pragma once

#include "../../../public_headers.h"
#include PUBLIC_HEADER(res/resource_data.h)

#include "../../ogl_common/ogl_functions.h"
#include "../tools/vertex_format.h"
#include "batch_group.h"
#include <vector>
#include <dk/concepts.h>

namespace exge211 {

///A class to manage a storage of the animated meshes.
class Static_mesh_storage: dk::Non_copyable<Static_mesh_storage>{
public:
    using Index=u32;
    using Buffer_index=unsigned int;

    struct Mesh_info{
        //to use with the `Draw_inderect_command` structure
        GLuint first_index;
        GLuint index_count;
        GLuint base_vertex;
    };

    Static_mesh_storage();
    ~Static_mesh_storage();

    void init();

    /*!
     * \brief Adds a new mesh with the given data to the storage.
     * \param mesh_in           The given mesh data.
     * \param index_out         A variable to output the index of the new mesh.
     * \param batch_out         The resulting batch group, a value of Batch_group::mesh is updated.
     */
    void add(Static_mesh const & mesh_in,Index & index_out,Batch_group & batch_out);

    /// Sets up the bindings of managed objects to match the specified batch.
    void set_up(Batch_group const& batch_group);

    /// Commits all deferred actions, e.g. upload data to the GPU memory.
    void commit();

    /// Returns an information about the specified mesh.
    Mesh_info const & operator[](Index index) const{
        return registry_[index];
    }

    Function<void()> buffer_replaced;

private:
    using Vertex_store=std::vector<Static_vertex>;
    using Index_store=std::vector<Triangle>;
    using Registry=std::vector<Mesh_info>;

    Registry registry_;
    Vertex_format v_format_;

    size_t vbo_size_=0;             //in bytes
    size_t ibo_size_=0;             //in bytes
    size_t vertex_end_=0;           //an index
    size_t triangles_end_=0;        //an index

    //Back buffers are used to resize the VBO via a content copying.
    GLuint vbo_=0;
    GLuint vbo_back_=0;
    GLuint ibo_=0;                  ///< The index buffer
    GLuint ibo_back_=0;

    bool commited_=true;
    Vertex_store vertices_;         ///< The vertex data to upload to the GPU.
    Index_store indices_;           ///< The index data (triangles, topology) to upload to the GPU.
};

}
