#include "../../../public_headers.h"
#include PUBLIC_HEADER(logging.h)

#include "static_instance_storage.h"
#include "../tools/buffer_util.h"
#include "../../ogl_common/error_checking.h"

namespace exge211 {

namespace{

constexpr size_t STEP_SIZE=1<<10;

}

Static_instance_storage::Static_instance_storage()
{
    using AType=Attribute_format::Type;
    format_.set_stride<Static_instance_storage::Instance_data>();
    format_.attributes={
        {AType::INTEGER,10,1,GL_UNSIGNED_INT,offsetof(Instance_data,mat)},   //for IDE searches
        {AType::FLOAT,  11,3,GL_FLOAT,offsetof(Instance_data,pos)},
        {AType::FLOAT,  12,4,GL_FLOAT,offsetof(Instance_data,rot)},
        {AType::FLOAT,  13,3,GL_FLOAT,offsetof(Instance_data,scale)},
    };
}

void Static_instance_storage::install(ogl4::Static_model_instance * model, const Rotation & rotation, const Position & position, glm::vec3 const& scale)
{
    for(ogl4::Static_model_instance::Index l_ix=0;l_ix < model->lod_count();++l_ix){
        auto& slod=model->lod(l_ix);
        auto& tlod=ogl4::Static_model::downcast(*model->type()).lod(l_ix);
        for(ogl4::Static_model_instance::LOD::Size i=0;i<slod.size();++i){
            Instance_data data{tlod[i].material,position,rotation,scale};
            slod[i].instance=add(data);
        }
    }
    commited_=true;
}

void Static_instance_storage::uninstall(ogl4::Static_model_instance* model)
{
    for(ogl4::Static_model_instance::Index l_ix=0;l_ix < model->lod_count();++l_ix){
        for(ogl4::Static_model_instance::LOD::Value& sm:model->lod(l_ix)){
            remove(sm.instance);
        }
    }
}

void Static_instance_storage::init()
{
    if(bo_){
        log_msg(Detail_level::CRITICAL,"an attempt to initialize already initialized Static_instance_storage");
        return;
    }
    GLuint buffers[2];
    glGenBuffers(2,buffers);

    bo_=buffers[0];
    bo_back_=buffers[1];

    allocated_=STEP_SIZE;
    assert(allocated_);

    unused_.resize(allocated_);
    for(Index i=0; i<allocated_;++i){
        unused_[i]=allocated_-1-i;
    }

    glBindBuffer(GL_ARRAY_BUFFER,bo_);
    glBufferData(GL_ARRAY_BUFFER, allocated_*sizeof(Instance_data), NULL, GL_STATIC_DRAW);
    check_gl_error();
}

void Static_instance_storage::commit()
{
    if(!commited_){
        return;
    }
    if(requested_){
        grow(allocated_+requested_);
        requested_=0;
    }
    glBindBuffer(GL_COPY_WRITE_BUFFER,bo_);
    for(Cache_entry& entry: cache_){
        glBufferSubData(GL_COPY_WRITE_BUFFER,entry.index*sizeof(Instance_data),sizeof(Instance_data),&entry.data);
        check_gl_error();
    }
    commited_=false;
}

void Static_instance_storage::set_up()
{
    format_.apply(bo_,1,0);
}

Static_instance_storage::~Static_instance_storage()
{
    if(bo_){
        GLuint buffers[2]={bo_,bo_back_};
        glDeleteBuffers(2,buffers);
        check_gl_error();
        bo_=bo_back_=0;
    }
}

void Static_instance_storage::grow(Index requested_size)
{
    size_t allocation_size=allocated_*sizeof(Instance_data);
    if(swapping_buffer_resize(bo_,bo_back_,allocation_size,requested_size*sizeof(Instance_data),STEP_SIZE*sizeof(Instance_data))){
        buffer_replaced();
        size_t new_allocated=allocation_size/sizeof(Instance_data);
        for(Index i = new_allocated-1 ; i>=allocated_ ;--i){
            unused_.push_back(i);
        }
    }
}

Static_instance_storage::Index Static_instance_storage::add(const Static_instance_storage::Instance_data & data)
{
    Index ret=0;
    if(unused_.empty()){
        ret=allocated_+requested_;
        ++requested_;
    }else{
        ret=unused_.back();
        unused_.pop_back();
    }
    cache_.emplace_back(ret,data);
    return ret;
}

void Static_instance_storage::remove(Static_instance_storage::Index index)
{
    unused_.push_back(index);
}

}
