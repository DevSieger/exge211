#include "full_screen_quad_drawer.h"
#include "../../ogl_common/error_checking.h"
#include <glm/vec2.hpp>
#include "../tools/vertex_format.h"

namespace exge211 {

using VAO_binding=ogl::VAO::Binding;

void Full_screen_quad_drawer::init()
{
    using vec2=glm::vec2;
    VAO_binding vao_binding(vao_);
    check_gl_error();
    vbo_.bind();
    Vertex_format format;
    format.set_stride<vec2>();
    format.add_f(0,2,GL_FLOAT,0);
    format.apply(vbo_.id(),0);

    // an oversized triangle is used to avoid overshading.
    vec2 data[3]={
        {-1,3},
        {-1,-1},
        {3,-1},
    };
    glBufferData(GL_ARRAY_BUFFER,sizeof(data),&data[0][0],GL_STATIC_DRAW);
    check_gl_error();
}

void Full_screen_quad_drawer::draw()
{
    VAO_binding vao_binding(vao_);
    glDrawArrays(GL_TRIANGLES,0,3);
    check_gl_error();
}

}
