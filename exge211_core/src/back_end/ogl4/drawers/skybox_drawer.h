#pragma once
#include "../../ogl_common/ogl_functions.h"
#include "../../ogl_common/vao.h"
#include "../../ogl_common/buffer/buffer_base.h"

namespace exge211 {

// It is possible to draw the skybox using the compute shader, but I doubt that this is an optimal solution.
class Skybox_drawer
{
public:
    // See Light_drawer::init notes.
    void init();
    /// Draws the axis-aligned box with coordinates: [-1,1]x[-1,1]x[-1,1].
    void draw();
private:
    ogl::VAO vao_;
    ogl::Buffer vbo_{ GL_ARRAY_BUFFER, GL_STATIC_DRAW };
    ogl::Buffer ixbo_{ GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW };
};

}
