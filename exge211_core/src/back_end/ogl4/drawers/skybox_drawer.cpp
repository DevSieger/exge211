#include "../../../public_headers.h"
#include PUBLIC_HEADER(res/resource_data.h)

#include "skybox_drawer.h"
#include <glm/vec3.hpp>
#include <vector>
#include "../../ogl_common/error_checking.h"
#include "../tools/vertex_format.h"


namespace exge211 {

using glm::vec3;
using VAO_binding=ogl::VAO::Binding;

namespace{

constexpr size_t triangle_count(){
    return 12;
}

using Vertex_vector=std::vector<vec3>;
using Index_vector=std::vector<Triangle>;

/// Fills the given vectors with the box geometry.
void set_geometry(Vertex_vector & vv,Index_vector & ix){
    vec3 p[8]={
        {-1,1,-1},{-1,-1,-1},{1,-1,-1},{1,1,-1},
        {-1,1,1},{-1,-1,1},{1,-1,1},{1,1,1}
    };
    vv.clear();
    vv.reserve(8);
    for(int i = 0 ; i<8;++i){
        vv.emplace_back(p[i]);
    };
    ix.clear();
    ix.reserve(12);
    Triangle t[triangle_count()]={
        {0,1,2},{0,2,3},    //bottom
        {4,6,5},{4,7,6},    //top
        {0,4,1},{4,5,1},
        {3,2,6},{3,6,7},
        {1,5,2},{5,6,2},
        {3,4,0},{4,3,7}
    };
    for(size_t i=0; i<triangle_count();++i){
        ix.push_back(t[i]);
    };
}

}

void Skybox_drawer::init()
{
    VAO_binding vao_binding(vao_);
    Vertex_format v_format_;
    v_format_.set_stride<vec3>();
    v_format_.add_f(0,3,GL_FLOAT,0);
    v_format_.apply(vbo_.id(),0);

    Vertex_vector v;
    Index_vector ix;
    set_geometry(v,ix);

    vbo_.bind();
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3)*v.size(), &v[0], GL_STATIC_DRAW);
    check_gl_error();
    ixbo_.bind();
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Triangle)*ix.size(), &ix[0], GL_STATIC_DRAW);
    check_gl_error();
}

void Skybox_drawer::draw()
{
    VAO_binding vao_binding(vao_);
    glDrawElements(GL_TRIANGLES,triangle_count()*3,GL_UNSIGNED_INT,0);
    check_gl_error();
}

}
