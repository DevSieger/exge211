#pragma once

#include "../../ogl_common/ogl_functions.h"
#include "../../../front_end/scene/scene_access.h"
#include <dk/concepts.h>
#include <vector>
#include "../../../front_end/lighting/lighting_data.h"
#include "../../ogl_common/vao.h"
#include "../../ogl_common/buffer/buffer_base.h"

namespace exge211 {

/// An object for drawing the point light sources.
class Light_drawer: dk::Non_copyable<Light_drawer>
{
    //TODO rewrite reload, write resource release(?!).
public:
    // The initialization is dedicated because it depends on the OpenGL context.
    // and I don't want to constrain the constructor.
    // Make sure that appropriate context is current in the current thread before this method is called.
    void init();

    /// Loads the light source data to the GPU memory.
    void reload(Scene_access::Point_light_output::const_iterator begin, size_t count);    //TODO: rewrite, using right argument

    /// Draws all sources.
    void draw();

    /// \brief Draws only one point light source.
    /// \param source_index An index of the source in the sequence which was passed in the last Light_drawer::reload call.
    void draw_one(int source_index);

    /// Returns the degree of the sphere approximation.
    static unsigned side_count();
private:
    ogl::VAO vao_;
    ogl::Buffer vbo_{GL_ARRAY_BUFFER, GL_STATIC_DRAW};                  // VBO used for the geometry.
    ogl::Buffer vbi_{GL_ARRAY_BUFFER, GL_STREAM_DRAW};                  // VBO used for the instance data.
    ogl::Buffer ixbo_{GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW};         // VBO used for indices.
    GLsizei count_=0;                                                   // The number of triangles in the geometry.
    GLsizei instances_=0;                                               // The number of loaded light sources.
    std::vector<Point_light_data> data_;
};

}
