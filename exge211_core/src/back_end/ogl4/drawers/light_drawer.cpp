#include "../../../public_headers.h"
#include PUBLIC_HEADER(res/resource_data.h)

#include "light_drawer.h"
#include <glm/vec4.hpp>
#include <glm/trigonometric.hpp>
#include "../tools/vertex_format.h"
#include "../../ogl_common/error_checking.h"
#include "../../../front_end/lighting/lighting_data.h"

namespace exge211 {

using vec3 = glm::vec3;
using vec4 = glm::vec4;

using Vertex_vector=std::vector<vec3>;
using Index_vector=std::vector<Triangle>;
using VAO_binding=ogl::VAO::Binding;

namespace{

/// Fills the given vectors with the box geometry.
void box_geometry(Vertex_vector & vv,Index_vector & ix){
    const int init_size=vv.size();
    vec3 p[8]={
        {-1,1,-1},{-1,-1,-1},{1,-1,-1},{1,1,-1},
        {-1,1,1},{-1,-1,1},{1,-1,1},{1,1,1}
    };
    vv.reserve(8);
    for(int i = 0 ; i<8;++i){
        vv.push_back(p[i]);
    };
    ix.reserve(12);
    Triangle t[12]={
        {0,2,1},{0,3,2},    //bottom
        {4,5,6},{4,6,7},    //top
        {0,1,4},{4,1,5},
        {3,6,2},{3,7,6},
        {1,2,5},{5,2,6},
        {3,0,4},{4,7,3}
    };
    for(int i=0; i<12;++i){
        t[i][0]+=init_size;
        t[i][1]+=init_size;
        t[i][2]+=init_size;
        ix.push_back(t[i]);
    };
}

/// Fills the given vectors with the ball geometry.
void ball_geometry(Vertex_vector & vv,Index_vector & ix){
    // todo: change index types
    using Index=Triangle::Index;
    const u32 count=Light_drawer::side_count();
    const float sec=3.1415f/count;
    vv.emplace_back(0.0f,0.0f,1.0f);
    for(size_t i=1;i<count;++i){
        for(size_t j=0;j<count*2;++j){
            float x=glm::cos(sec*j)*glm::sin(sec*i);
            float y=glm::sin(sec*j)*glm::sin(sec*i);
            float z=glm::cos(sec*i);
            vv.emplace_back(x,y,z);
        }
    }
    vv.emplace_back(0.0f,0.0f,-1.0f);

    for(Index h=1;h<count*2;++h){
        ix.push_back({0,h, h+1});
    };
    ix.push_back({0,count*2,1});

    for(Index v=0;v<count-2;++v){
        for( Index h=1;h< count*2 + 1 ;++h){
            ix.push_back({ v*2*count + h, (v+1)*2*count+h, (v+1)*2*count + h%(2*count)+1 });
            ix.push_back({ v*2*count + h, (v+1)*2*count + h%(2*count)+1, v*2*count + h%(2*count)+1 });
        };
    };
    for( Index h=1,v=count-2; h< 1+(2*count) ;++h){
        ix.push_back({ v*2*count + h%(2*count)+1,v*2*count + h, (v+1)*2*count+1 });
    };
}


}

void Light_drawer::init()
{
    VAO_binding binding(vao_);
    vbo_.bind();
    Vertex_format mesh_format;
    mesh_format.set_stride<vec3>();
    mesh_format.add_f(0,3,GL_FLOAT,0);
    mesh_format.apply(vbo_.id(),0);

    Vertex_vector v;
    Index_vector ix;
    ball_geometry(v,ix);
    glBufferData(vbo_.type(), sizeof(vec3)*v.size(), &v[0], GL_STATIC_DRAW);
    check_gl_error();

    Vertex_format instance_format;
    instance_format.set_stride<Point_light_data>();
    instance_format.add_f(1,3,GL_FLOAT,offsetof(Point_light_data,Point_light_data::position));
    instance_format.add_f(2,3,GL_FLOAT,offsetof(Point_light_data,Point_light_data::intensity));
    instance_format.add_f(3,3,GL_FLOAT,offsetof(Point_light_data,Point_light_data::attenuation));
    instance_format.add_f(4,1,GL_FLOAT,offsetof(Point_light_data,Point_light_data::range));
    instance_format.add_f(5,1,GL_FLOAT,offsetof(Point_light_data,Point_light_data::radius));
    instance_format.add_f(6,1,GL_FLOAT,offsetof(Point_light_data,Point_light_data::cut_off));
    instance_format.apply(vbi_.id(),1);
    vbi_.size_align(sizeof(Point_light_data)*16);
    check_gl_error();

    ixbo_.bind();
    glBufferData(ixbo_.type(), sizeof(Triangle)*ix.size(), &ix[0], GL_STATIC_DRAW);
    check_gl_error();
    count_=ix.size();
}

void Light_drawer::reload(Scene_access::Point_light_output::const_iterator begin, size_t count)
{
    instances_=count;
    if(instances_){
        vbi_.bind();
        data_.clear();
        data_.reserve(instances_);
        auto it=begin;
        for(size_t i=0;i<count;++i){
            data_.push_back(**it++);
        }
        /*
         * TODO: may be better to use map?
         */
        vbi_.reallocate(instances_*sizeof(Point_light_data));
        vbi_.upload(0,data_.data(),instances_);
    }
}

void Light_drawer::draw()
{
    VAO_binding binding(vao_);
    if(instances_){
        glDrawElementsInstanced(GL_TRIANGLES,count_*3,GL_UNSIGNED_INT,0,instances_);
        check_gl_error();
    }
}

void Light_drawer::draw_one(int ix)
{
    VAO_binding binding(vao_);
    if(instances_){
        glDrawElementsInstancedBaseInstance(GL_TRIANGLES,count_*3,GL_UNSIGNED_INT,0,1,ix);
        check_gl_error();
    }
}

unsigned Light_drawer::side_count()
{
    return 8;   // the best way to explaing - just look at the usage in the 'ball_geometry' function.
}

}
