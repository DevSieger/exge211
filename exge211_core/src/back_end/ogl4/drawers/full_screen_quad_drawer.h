#pragma once
#include "../../ogl_common/ogl_functions.h"
#include "../../ogl_common/vao.h"
#include "../../ogl_common/buffer/buffer_base.h"

namespace exge211 {

// The full-screen quad is used because the compute shader can't perform the stencil test.
class Full_screen_quad_drawer
{
public:
    void init();
    void draw();
private:
    ogl::VAO vao_;
    ogl::Buffer vbo_{GL_ARRAY_BUFFER, GL_STATIC_DRAW};
};

}
