#pragma once

#include "../../public_headers.h"
#include PUBLIC_HEADER(res/resource_data.h)
#include PUBLIC_HEADER(handles.h)
#include "../../front_end/actor/model.h"
#include "../../front_end/scenery/model.h"
#include "storage/batch_group.h"

namespace exge211 {

namespace ogl4{

namespace detail {

template<typename Base_class, typename Derived_class>
struct Downcast_interface{
    static constexpr Derived_class& downcast(Base_class& base){
        return static_cast<Derived_class&>(base);
    }
    static constexpr Derived_class const& downcast(Base_class const& base){
        return static_cast<Derived_class const&>(base);
    }
};

}

struct Submodel{
    Batch_group bg;
    u32 mesh;
    u32 material;
    Material_type type;
};

/// Data to handle the 3D model data of the Actor object.
class Model final:
        public Model_type_base<Submodel>,
        public Model_base,
        public detail::Downcast_interface<Model_base, Model>
{};

/// Data to handle the 3D model data of the Scenery object.
class Static_model final:
        public Model_type_base<Submodel>,
        public Static_model_base,
        public detail::Downcast_interface<Static_model_base, Static_model>
{};

class Model_instance final:
        public Model_instance_base,
        public detail::Downcast_interface<Model_instance_base, Model_instance>
{
    using Model_instance_base::Model_instance_base;
};

struct Static_model_instance_part{
    Batch_group bg;
    u32 mesh;
    u32 instance;
    Material_type type;
};

/// Data to handle the instance of the scenery 3D model.
class Static_model_instance final:
        public Model_type_base<Static_model_instance_part>,
        public Static_model_instance_base,
        public detail::Downcast_interface<Static_model_instance_base, Static_model_instance>
{
public:
    Static_model_instance(Static_model_handle a_type);
};


}

}
