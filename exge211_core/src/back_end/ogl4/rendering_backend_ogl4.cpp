#include "../../public_headers.h"
#include PUBLIC_HEADER(logging.h)
#include PUBLIC_HEADER(camera.h)

#include "rendering_backend_ogl4.h"
#include <vector>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec4.hpp>
#include <glm/trigonometric.hpp>
#include <random>
#include <chrono>
#include "../ogl_common/error_checking.h"
#include "../../front_end/lighting/lighting_data.h"
#include "../../front_end/actor/skeleton_instance_base.h"
#include "../../front_end/actor/actor_data.h"
#include "misc.h"
#include "../cpu_animator/skeleton_instance.h"
#include "tools/draw_inderect_command.h"
#include "../../front_end/scenery/scenery_data.h"
#include <algorithm>
#include <iterator>
#include "../ogl_common/shader/shader.h"

//HATE YOU MS!
#undef near

namespace exge211 {

using glm::mat4x4;
using glm::vec3;
using glm::vec4;
using glm::vec2;
using glm::ivec2;
using glm::uvec2;

const mat4x4 cube_sides[6]={
    mat4x4{{0,0,-1,0},{0,-1,0,0},{-1,0,0,0},{0,0,0,1}},
    mat4x4{{0,0,1,0},{0,-1,0,0},{1,0,0,0},{0,0,0,1}},
    mat4x4{{1,0,0,0},{0,0,-1,0},{0,1,0,0},{0,0,0,1}},
    mat4x4{{1,0,0,0},{0,0,1,0},{0,-1,0,0},{0,0,0,1}},
    mat4x4{{1,0,0,0},{0,-1,0,0},{0,0,-1,0},{0,0,0,1}},
    mat4x4{{-1,0,0,0},{0,-1,0,0},{0,0,1,0},{0,0,0,1}}
};

namespace{

template<typename T>
T aligned_value(T const& value, T const&  align){
    T s=value+align-1;
    return s-(s%align);
}

template<typename Data_type>
GLintptr buffer_data_offset(int index,int offset_align)
{
    return index*aligned_value<int>(sizeof(Data_type),offset_align);
}

template<typename Data_type>
size_t buffer_data_size(size_t count,int offset_align)
{
    if(count){
        return (count-1)*aligned_value<size_t>(sizeof(Data_type),offset_align)+sizeof(Data_type);
    }else{
        return 0;
    }
}

// it is used with compute shaders
uvec2 get_num_groups(uvec2 const& sreen_size, uvec2 const& group_size,unsigned border){
    return (sreen_size-uvec2(1))/(group_size-uvec2(border*2))+uvec2(1);
}

// The size of an area reduced to 1 texel after single cs_reduce dispatch.
const glm::uvec2 reduce_group_size{64,16*4};

// the tile size of SAT shader.
const glm::uvec2 lsat_group_size{64,16*4};


}

Rendering_backend_OGL4::Rendering_backend_OGL4(Resource_loader const& res_loader):
    shader_loader_(res_loader)
{
    animation_engine_.loader(&res_loader);

    //TODO: convert Resource_loader* to ref or handle
    r_factory_.init(&materials_,&textures_,&meshes_,&res_loader,&st_meshes_,&st_instances_,&skybox_);
}

std::random_device::result_type get_random_seed(){
    std::random_device rgd;
    //rgd() returns constant on mingw, we also need timestamp

    using Rep=std::random_device::result_type;
    using Clock=std::chrono::high_resolution_clock;
    using Ms=std::chrono::duration<Rep,std::micro>;
    auto t=std::chrono::time_point_cast<Ms>(Clock::now());

    static_assert(std::is_unsigned<Rep>::value,"result type of random device is supposed to be unsigned");
    return rgd()+t.time_since_epoch().count();
}

void Rendering_backend_OGL4::init_fixed_size()
{
    tx_sepl_sm_.storage(PL_SHADOW_MAP_SIZE,PL_SHADOW_MAP_SIZE,1,GL_DEPTH_COMPONENT32F);
    tx_shadow_map_.storage(DL_SHADOW_MAP_SIZE,DL_SHADOW_MAP_SIZE,MAX_SHADOWMAPS_IN_CASCADE,1,GL_DEPTH_COMPONENT32F);
    tx_rotation_.storage(32,32,32,1,GL_RG8);

    tx_lum_g_mean_a_.storage(1,1,1,GL_R32F);
    tx_lum_g_mean_b_.storage(1,1,1,GL_R32F);
    tx_luminance_geom_mean_front_ = &tx_lum_g_mean_a_;
    tx_luminance_geom_mean_back_ = &tx_lum_g_mean_b_;

    tx_rotation_.bind();
    std::vector<vec2> rnd_map;
    rnd_map.reserve(32*32*32);
    std::default_random_engine reng(get_random_seed());
    std::uniform_real_distribution<float> rdist(0,3.1415*2);
    for(int i=0;i<32*32*32;++i){
        float rx=rdist(reng);
        rnd_map.push_back({glm::cos(rx),glm::sin(rx)});
    }
    glTexSubImage3D(GL_TEXTURE_3D, 0, 0,0,0, 32,32,32, GL_RG, GL_FLOAT,&rnd_map[0]);
    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,GL_REPEAT);
    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,GL_REPEAT);
    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,GL_REPEAT);
    check_gl_error();

    /* Cascade Shadows */
    GLenum dls_buffers_data[]={GL_NONE};
    for(int i=0;i<MAX_SHADOWMAPS_IN_CASCADE;++i){
        sedl_sm_fbo_[i].bind(GL_DRAW_FRAMEBUFFER);
        sedl_sm_fbo_[i].attach_layer_to_depth(tx_shadow_map_,i);
        glDrawBuffers(sizeof(dls_buffers_data)/sizeof(GLenum),dls_buffers_data);
        check_gl_error();
        sedl_sm_fbo_[i].check();
    }

    /* Cube Shadows */
    sepl_sm_fbo_.bind(GL_DRAW_FRAMEBUFFER);
    sepl_sm_fbo_.attach_to_depth(tx_sepl_sm_);
    GLenum cs_buffers_data[]={GL_NONE};
    glDrawBuffers(sizeof(cs_buffers_data)/sizeof(GLenum),cs_buffers_data);
    check_gl_error();
    sepl_sm_fbo_.check();

    tx_rotation_.bind_to_texture_unit(txt_binding_points::RANDOM_ROT);
    tx_shadow_map_.bind_to_texture_unit(txt_binding_points::RT_SHADOW_MAP);
    tx_shadow_map_.bind();
    glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_COMPARE_MODE,GL_COMPARE_REF_TO_TEXTURE);
    glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_COMPARE_FUNC,GL_LEQUAL);
    glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);

    tx_sepl_sm_.bind_to_texture_unit(txt_binding_points::RT_CUBE_SHADOW_MAP);
    tx_sepl_sm_.bind();
    glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_COMPARE_MODE,GL_COMPARE_REF_TO_TEXTURE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_COMPARE_FUNC,GL_LEQUAL);
    glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
    check_gl_error();
    smaa_tx.init();
}

namespace {

void set_clamp_and_linear(Texture_2D & tx){
    tx.filter(GL_LINEAR,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
}

}

void Rendering_backend_OGL4::resize( Window_frame_size const& new_size)
{
    //error_log()->message("resize");
    //size dependend code
    frame_size(new_size);
    using Size_value=Window_frame_size::Value;
    Size_value const& width=rendering_size().width;
    Size_value const& height=rendering_size().height;
    depth_buffer_.storage(rendering_size().width,rendering_size().height,1,GL_DEPTH24_STENCIL8);
    tx_rt_albedo_.storage(rendering_size().width,rendering_size().height,1,GL_RGB10_A2);
    tx_rt_specular_.storage(rendering_size().width,rendering_size().height,1,GL_RGBA8);
    tx_rt_normals_.storage(rendering_size().width,rendering_size().height,1,GL_RG8);

    tx_rt_light_.storage(rendering_size().width,rendering_size().height,1,GL_RGBA16F);
    tx_rt_light_l2_.storage(rendering_size().width/2,rendering_size().height/2,1,GL_RGBA16F);
    tx_rt_light_l3_.storage(rendering_size().width/4,rendering_size().height/4,1,GL_RGBA16F);

    tx_rt_color_.storage(rendering_size().width,rendering_size().height,1,GL_RGB10_A2);
    tx_rt_smaa_edges_.storage(rendering_size().width,rendering_size().height,1,GL_RG8);
    tx_rt_smaa_bw_.storage(rendering_size().width,rendering_size().height,1,GL_RGBA8);
    tx_rt_smaa_stencil_.storage(rendering_size().width,rendering_size().height,1,GL_STENCIL_INDEX8);

    tx_bp_medium_blured_aux_.storage(rendering_size().width/2,rendering_size().height/2,1,GL_RGBA16F);
    tx_bp_medium_blured_.storage(rendering_size().height/2,rendering_size().width/2,1,GL_RGBA16F);
    tx_bp_large_blured_aux_.storage(rendering_size().width/4,rendering_size().height/4,1,GL_RGBA16F);
    tx_bp_large_blured_.storage(rendering_size().height/4,rendering_size().width/4,1,GL_RGBA16F);
    tx_bp_small_blured_.storage(rendering_size().width,rendering_size().height,1,GL_RGBA16F);

    glm::uvec2 const& rgs = reduce_group_size;
    tx_total_log_lum_aux_.storage( (width-1)/rgs.x + 1, (height-1)/rgs.y+1, 1, GL_R32F);

    check_gl_error();

    /* Data */
    {
        data_fbo_.bind(GL_DRAW_FRAMEBUFFER);
        data_fbo_.attach_to_color(tx_rt_normals_,0);
        data_fbo_.attach_to_color(tx_rt_albedo_,1);
        data_fbo_.attach_to_color(tx_rt_specular_,2);
        data_fbo_.attach_to_depth_stencil(depth_buffer_);
        GLenum draw_buffers[]={GL_COLOR_ATTACHMENT0,GL_COLOR_ATTACHMENT1,GL_COLOR_ATTACHMENT2};
        glDrawBuffers(sizeof(draw_buffers)/sizeof(GLenum),draw_buffers);
        check_gl_error();

        check_gl_error();
        data_fbo_.check();
    }

    /* Light */
    {
        light_fbo_.bind(GL_DRAW_FRAMEBUFFER);
        light_fbo_.attach_to_color(tx_rt_light_,0);
        light_fbo_.attach_to_depth_stencil(depth_buffer_);
        GLenum draw_buffers[]={GL_COLOR_ATTACHMENT0};
        glDrawBuffers(sizeof(draw_buffers)/sizeof(GLenum),draw_buffers);
        check_gl_error();
        light_fbo_.check();
    }

    /* color, dirty =( */
    {
        color_fbo_.bind(GL_DRAW_FRAMEBUFFER);
        color_fbo_.attach_to_color(tx_rt_color_,0);
        color_fbo_.attach_to_depth_stencil(depth_buffer_);
        GLenum draw_buffers[]={GL_COLOR_ATTACHMENT0};
        glDrawBuffers(sizeof(draw_buffers)/sizeof(GLenum),draw_buffers);
        check_gl_error();
        color_fbo_.check();
    }
    /* SMAA */
    {
        GLenum draw_buffers[]={GL_COLOR_ATTACHMENT0};

        smaa_edges_.bind(GL_DRAW_FRAMEBUFFER);
        smaa_edges_.attach_to_color(tx_rt_smaa_edges_,0);
        glDrawBuffers(sizeof(draw_buffers)/sizeof(GLenum),draw_buffers);
        check_gl_error();

        smaa_edges_.attach_to_stencil(tx_rt_smaa_stencil_,0);
        smaa_edges_.check();

        smaa_blend_.bind(GL_DRAW_FRAMEBUFFER);
        smaa_blend_.attach_to_color(tx_rt_smaa_bw_,0);
        glDrawBuffers(sizeof(draw_buffers)/sizeof(GLenum),draw_buffers);
        check_gl_error();
        smaa_blend_.attach_to_stencil(tx_rt_smaa_stencil_,0);
        smaa_blend_.check();
    }

    //binding
    tx_rt_albedo_.bind_to_texture_unit(txt_binding_points::RT_ALBEDO);
    tx_rt_specular_.bind_to_texture_unit(txt_binding_points::RT_SPECULAR);
    tx_rt_normals_.bind_to_texture_unit(txt_binding_points::RT_NORMALS);
    tx_rt_light_.bind_to_texture_unit(txt_binding_points::RT_LIGHT);
    tx_rt_color_.bind_to_texture_unit(txt_binding_points::RT_COLOR);

    depth_buffer_.bind_to_texture_unit(txt_binding_points::DEPTH);

    tx_bp_small_blured_.bind_to_texture_unit(txt_binding_points::BP_BLUR_SMALL);
    tx_bp_medium_blured_.bind_to_texture_unit(txt_binding_points::BP_BLUR_MEDIUM);
    tx_bp_large_blured_.bind_to_texture_unit(txt_binding_points::BP_BLUR_LARGE);

    tx_rt_smaa_edges_.bind_to_texture_unit(txt_binding_points::RT_SMAA_EDGES);
    tx_rt_smaa_bw_.bind_to_texture_unit(txt_binding_points::RT_SMAA_BW);

    set_clamp_and_linear(tx_bp_medium_blured_);
    set_clamp_and_linear(tx_bp_medium_blured_aux_);
    set_clamp_and_linear(tx_bp_large_blured_);
    set_clamp_and_linear(tx_bp_large_blured_aux_);
    set_clamp_and_linear(tx_rt_light_);
    set_clamp_and_linear(tx_rt_light_l2_);
    set_clamp_and_linear(tx_rt_light_l3_);
    set_clamp_and_linear(tx_rt_smaa_edges_);
    set_clamp_and_linear(tx_rt_smaa_bw_);
    set_clamp_and_linear(tx_bp_small_blured_);

    vec4& metric = sc_cfg_buffer_.prefix_data().rt_metric;
    metric.z=rendering_size().width;
    metric.w=rendering_size().height;
    metric.x=1/metric.z;
    metric.y=1/metric.w;

    check_gl_error();
}

glm::mat4x4 Rendering_backend_OGL4::get_sepl_view_matrix(Point_light_data const& pd, glm::mat4x4 const& cam_view){
    // the camera rotation.
    glm::mat4x4 output(cam_view[0],cam_view[1],cam_view[2],vec4(0,0,0,1));
    // now set an origin of the resulting view space to the light source position.
    output=glm::translate(output,-pd.position);
    return output;
}

void Rendering_backend_OGL4::set_up_sepl_sm_data(Sepl_sm_data& output, Point_light_data const& pd, glm::mat4x4 const& sepl_view)
{
    mat4x4 proj90=glm::perspective(glm::radians(90.0f),1.0f,pd.radius,pd.range);

    for(int i=0;i<6;++i){
        output.transform[i]=proj90*cube_sides[i]*sepl_view;
    }
    glm::mat2x2 zw_m( proj90[2][2],proj90[2][3],proj90[3][2],proj90[3][3]);
    output.proj_z=zw_m[0];
    output.proj_w=zw_m[1];
}

void Rendering_backend_OGL4::set_up_sepl_culling_matrix(glm::mat4x4& output, const Point_light_data& pd,  glm::mat4x4 const& sepl_view)
{
    const mat4x4 proj=glm::ortho(-pd.range,pd.range,-pd.range,pd.range,-pd.range,pd.range);
    output=proj*sepl_view;
}

void Rendering_backend_OGL4::init_storage()
{
    meshes_.init();
    st_meshes_.init();
    materials_.init();
    st_instances_.init();
    textures_.init(512,512);
    textures_.bind(txt_binding_points::ALBEDO,Texture_type::ALBEDO);
    textures_.bind(txt_binding_points::SPECULAR,Texture_type::SPECULAR);
    textures_.bind(txt_binding_points::NORMAL_MAP,Texture_type::NORMAL);
    skybox_.bind(txt_binding_points::SKYBOX);

}

void Rendering_backend_OGL4::init_shaders()
{
    using Shader = ogl::Shader;
    u32 glsl_version = 430;

    Shader vs_full_screen_dispatch(shader_loader_,GL_VERTEX_SHADER, "fs-dispatch", glsl_version);
    vs_full_screen_dispatch.add_file("vs_unit.glsl");
    vs_full_screen_dispatch.compile();

    Shader fs_gbuffer(shader_loader_,GL_FRAGMENT_SHADER, "gbuffer", glsl_version);
    fs_gbuffer.add_file("gbuffer.glsl").compile();

    Shader fs_sedl_light(shader_loader_,GL_FRAGMENT_SHADER, "fs_sedl_light", glsl_version);
    fs_sedl_light.add_file("fs_sedl_light.glsl").compile();

    Shader fs_lighting(shader_loader_,GL_FRAGMENT_SHADER, "fs_lighting", glsl_version);
    fs_lighting.add_file("fs_lighting.glsl").compile();

    Shader fs_sepl_light(shader_loader_,GL_FRAGMENT_SHADER, "fs_sepl_light", glsl_version);
    fs_sepl_light.add_file("fs_sepl_light.glsl").compile();

    Shader fs_sdpl_light(shader_loader_,GL_FRAGMENT_SHADER, "fs_sdpl_light", glsl_version);
    fs_sdpl_light.add_file("fs_sdpl_light.glsl").compile();

    Shader fs_ambient(shader_loader_,GL_FRAGMENT_SHADER, "fs_ambient", glsl_version);
    fs_ambient.add_file("fs_ambient.glsl").compile();

    Shader vs_light_sphere(shader_loader_,GL_VERTEX_SHADER, "vs_light_sphere", glsl_version);
    vs_light_sphere.add_file("vs_light.glsl").compile();

    sedli_sp_.attach(vs_full_screen_dispatch).attach(fs_sedl_light).attach(fs_lighting).attach(fs_gbuffer).link();
    sedli_sp_.set_uniform_i("shadow_map",txt_binding_points::RT_SHADOW_MAP);
    sedli_sp_.set_uniform_i("rotation_tx",txt_binding_points::RANDOM_ROT);

    sepli_sp_.attach(vs_light_sphere).attach(fs_sepl_light).attach(fs_lighting).attach(fs_gbuffer).link();
    sepli_sp_.set_uniform_i("cube_shadow",txt_binding_points::RT_CUBE_SHADOW_MAP);
    sepli_sp_.set_uniform_i("rotation_tx",txt_binding_points::RANDOM_ROT);

    sdpli_sp_.attach(vs_light_sphere).attach(fs_sdpl_light).attach(fs_lighting).attach(fs_gbuffer).link();
    ambient_sp_.attach(vs_full_screen_dispatch).attach(fs_ambient).attach(fs_gbuffer).link();

    Shader fs_skybox(shader_loader_,GL_FRAGMENT_SHADER, "fs_skybox", glsl_version);
    fs_skybox.add_file("fs_skybox.glsl").compile();

    Shader vs_skybox(shader_loader_,GL_VERTEX_SHADER, "vs_skybox", glsl_version);
    vs_skybox.add_file("vs_skybox.glsl").compile();

    sky_sp_.attach(fs_skybox).attach(vs_skybox).link();


    Shader fs_lamps(shader_loader_,GL_FRAGMENT_SHADER, "fs_lamps", glsl_version);
    fs_lamps.add_file("fs_lamps.glsl").compile();

    Shader vs_lamps(shader_loader_,GL_VERTEX_SHADER, "vs_lamps", glsl_version);
    vs_lamps.add_file("vs_lamps.glsl").compile();

    lamp_sp_.attach(fs_lamps).attach(vs_lamps).link();

    Shader fs_debug(shader_loader_,GL_FRAGMENT_SHADER, "fs_debug", glsl_version);
    fs_debug.add_file("fs_debug.glsl").compile();

    debug_sp_.attach(vs_full_screen_dispatch).attach(fs_debug).attach(fs_gbuffer).link();

    Shader vs_smaa(shader_loader_,GL_VERTEX_SHADER, "smaa_vs", glsl_version);
    vs_smaa.define("SMAA_PRESET_HIGH",1);
    vs_smaa.define("SMAA_INCLUDE_PS",0);
    vs_smaa.define("SMAA_INCLUDE_VS",1);
    vs_smaa.add_file("smaa.glsl");
    vs_smaa.compile();

    Shader fs_smaa(shader_loader_,GL_FRAGMENT_SHADER, "smaa_fs", glsl_version);
    fs_smaa.define("SMAA_PRESET_HIGH",1);
    fs_smaa.define("SMAA_INCLUDE_PS",1);
    fs_smaa.define("SMAA_INCLUDE_VS",0);
    fs_smaa.add_file("smaa.glsl");
    fs_smaa.compile();

    Shader vs_smaa_detect(shader_loader_,GL_VERTEX_SHADER, "vs_smaa_detect", glsl_version);
    vs_smaa_detect.add_file("vs_smaa_detect.glsl").compile();

    Shader fs_smaa_detect(shader_loader_,GL_FRAGMENT_SHADER, "fs_smaa_detect", glsl_version);
    fs_smaa_detect.add_file("fs_smaa_detect.glsl").compile();

    smaa_sp_detect.attach(vs_smaa).attach(fs_smaa).attach(vs_smaa_detect).attach(fs_smaa_detect).link();


    Shader vs_smaa_weight(shader_loader_,GL_VERTEX_SHADER, "vs_smaa_weight", glsl_version);
    vs_smaa_weight.add_file("vs_smaa_weight.glsl").compile();

    Shader fs_smaa_weight(shader_loader_,GL_FRAGMENT_SHADER, "fs_smaa_weight", glsl_version);
    fs_smaa_weight.add_file("fs_smaa_weight.glsl").compile();

    smaa_sp_weight.attach(vs_smaa).attach(fs_smaa).attach(vs_smaa_weight).attach(fs_smaa_weight).link();


    Shader vs_smaa_blend(shader_loader_,GL_VERTEX_SHADER, "vs_smaa_blend", glsl_version);
    vs_smaa_blend.add_file("vs_smaa_blend.glsl").compile();

    Shader fs_smaa_blend(shader_loader_,GL_FRAGMENT_SHADER, "fs_smaa_blend", glsl_version);
    fs_smaa_blend.add_file("fs_smaa_blend.glsl").compile();

    smaa_sp_blend.attach(vs_smaa).attach(fs_smaa).attach(vs_smaa_blend).attach(fs_smaa_blend).link();


    Shader cs_main(shader_loader_,GL_COMPUTE_SHADER, "cs_main", glsl_version);
    cs_main.add_file("cs_main.glsl").compile();

    Shader cs_gbuffer(shader_loader_,GL_COMPUTE_SHADER, "cs_gbuffer", glsl_version);
    cs_gbuffer.add_file("gbuffer.glsl").compile();

    main_sp_.attach(cs_main).attach(cs_gbuffer).link();
    main_sp_.set_uniform_i("small_blur_tx",txt_binding_points::BP_BLUR_SMALL);
    main_sp_.set_uniform_i("medium_blur_tx",txt_binding_points::BP_BLUR_MEDIUM);
    main_sp_.set_uniform_i("large_blur_tx",txt_binding_points::BP_BLUR_LARGE);
    main_sp_.set_uniform_i("tx_total_log_lum",txt_binding_points::TOTAL_LOG_LUM);
    main_sp_.set_uniform_i("rt_color",img_binding_points::CS_OUTPUT_A);


    float medium_blur_sigma = 3.5;
    u32 medium_blur_radius  = 12;

    float large_blur_sigma = 5.5;
    u32 large_blur_radius  = 18;

    Shader cs_bright_pass(shader_loader_,GL_COMPUTE_SHADER, "cs_bright_pass", glsl_version);
    cs_bright_pass.add_file("bright_pass.glsl").compile();

    Shader cs_gaussian_filter_medium_h(shader_loader_,GL_COMPUTE_SHADER, "cs_gaussian_filter_medium_h", glsl_version);
    cs_gaussian_filter_medium_h.define("GAUSS_SIGMA",medium_blur_sigma);
    cs_gaussian_filter_medium_h.define("FILTER_RADIUS",medium_blur_radius);
    cs_gaussian_filter_medium_h.add_file("cs_gaussian_filter_large.glsl").compile();
    gauss_blur_medium_h_sp_.attach(cs_gaussian_filter_medium_h).link();
    gauss_blur_medium_h_sp_.set_uniform_i("tx_input_a",txt_binding_points::CS_INPUT_A);
    gauss_blur_medium_h_sp_.set_uniform_i("img_output_a",img_binding_points::CS_OUTPUT_A);

    Shader cs_gaussian_filter_medium_v(shader_loader_,GL_COMPUTE_SHADER, "cs_gaussian_filter_medium_v", glsl_version);
    cs_gaussian_filter_medium_v.define("HORIZONTAL_PASS",0);
    cs_gaussian_filter_medium_v.define("GAUSS_SIGMA",medium_blur_sigma);
    cs_gaussian_filter_medium_v.define("FILTER_RADIUS",medium_blur_radius);
    cs_gaussian_filter_medium_v.add_file("cs_gaussian_filter_large.glsl").compile();
    gauss_blur_medium_v_sp_.attach(cs_gaussian_filter_medium_v).link();
    gauss_blur_medium_v_sp_.set_uniform_i("tx_input_a",txt_binding_points::CS_INPUT_A);
    gauss_blur_medium_v_sp_.set_uniform_i("img_output_a",img_binding_points::CS_OUTPUT_A);


    Shader cs_gaussian_filter_large_h(shader_loader_,GL_COMPUTE_SHADER, "cs_gaussian_filter_large_h", glsl_version);
    cs_gaussian_filter_large_h.define("GAUSS_SIGMA",large_blur_sigma);
    cs_gaussian_filter_large_h.define("FILTER_RADIUS",large_blur_radius);
    cs_gaussian_filter_large_h.add_file("cs_gaussian_filter_large.glsl").compile();
    gauss_blur_large_h_sp_.attach(cs_gaussian_filter_large_h).link();
    gauss_blur_large_h_sp_.set_uniform_i("tx_input_a",txt_binding_points::CS_INPUT_A);
    gauss_blur_large_h_sp_.set_uniform_i("img_output_a",img_binding_points::CS_OUTPUT_A);

    Shader cs_gaussian_filter_large_v(shader_loader_,GL_COMPUTE_SHADER, "cs_gaussian_filter_large_v", glsl_version);
    cs_gaussian_filter_large_v.define("GAUSS_SIGMA",large_blur_sigma);
    cs_gaussian_filter_large_v.define("FILTER_RADIUS",large_blur_radius);
    cs_gaussian_filter_large_v.define("HORIZONTAL_PASS",0);
    cs_gaussian_filter_large_v.add_file("cs_gaussian_filter_large.glsl").compile();
    gauss_blur_large_v_sp_.attach(cs_gaussian_filter_large_v).link();
    gauss_blur_large_v_sp_.set_uniform_i("tx_input_a",txt_binding_points::CS_INPUT_A);
    gauss_blur_large_v_sp_.set_uniform_i("img_output_a",img_binding_points::CS_OUTPUT_A);

    Shader cs_mip_generation(shader_loader_,GL_COMPUTE_SHADER, "cs_mip_generation", glsl_version);
    cs_mip_generation.define("LEVELS_TO_GENERATE",2);
    cs_mip_generation.define("BRIGHT_PASS",1);
    cs_mip_generation.add_file("cs_mip_generation.glsl").compile();
    mip_generation_sp_.attach(cs_mip_generation).attach(cs_bright_pass).link();
    mip_generation_sp_.set_uniform_i("tx_total_log_lum",txt_binding_points::TOTAL_LOG_LUM);
    mip_generation_sp_.set_uniform_i("img_level_0",txt_binding_points::CS_INPUT_A);
    mip_generation_sp_.set_uniform_i("img_level_1",img_binding_points::CS_OUTPUT_A);
    mip_generation_sp_.set_uniform_i("img_level_2",img_binding_points::CS_OUTPUT_B);

    Shader cs_gaussian_filter_small(shader_loader_,GL_COMPUTE_SHADER, "cs_gaussian_filter_small", glsl_version);
    cs_gaussian_filter_small.add_file("cs_gaussian_filter_small.glsl").compile();
    gauss_blur_small_sp_.attach(cs_gaussian_filter_small).attach(cs_bright_pass).link();
    gauss_blur_small_sp_.set_uniform_i("tx_total_log_lum",txt_binding_points::TOTAL_LOG_LUM);
    gauss_blur_small_sp_.set_uniform_i("input_img",txt_binding_points::CS_INPUT_A);
    gauss_blur_small_sp_.set_uniform_i("output_img",img_binding_points::CS_OUTPUT_A);


    Shader cs_reduce_lumi_1(shader_loader_,GL_COMPUTE_SHADER, "cs_reduce_lumi_1", glsl_version);
    cs_reduce_lumi_1.define("ILLUMINATION_INPUT",1);
    cs_reduce_lumi_1.add_file("cs_reduce.glsl").compile();
    reduce_.attach(cs_reduce_lumi_1).attach(cs_gbuffer).link();
    {
        GLuint img_out=glGetUniformLocation(reduce_.id(),"img_out");
        check_gl_error();
        glProgramUniform1i(reduce_.id(),img_out, img_binding_points::CS_OUTPUT_A);
        check_gl_error();
    }

    Shader cs_reduce_lumi_2(shader_loader_,GL_COMPUTE_SHADER, "cs_reduce_lumi_2", glsl_version);
    cs_reduce_lumi_2.define("ILLUMINATION_INPUT",0);
    cs_reduce_lumi_2.define("LUMINANCE_G_MEAN_OUTPUT",1);
    cs_reduce_lumi_2.add_file("cs_reduce.glsl").compile();
    reduce2_.attach(cs_reduce_lumi_2).attach(cs_gbuffer).link();
    {
        GLuint img_in=glGetUniformLocation(reduce2_.id(),"img_in");
        check_gl_error();
        glProgramUniform1i(reduce2_.id(),img_in, txt_binding_points::CS_INPUT_A);
        check_gl_error();
        GLuint img_out=glGetUniformLocation(reduce2_.id(),"img_out");
        check_gl_error();
        glProgramUniform1i(reduce2_.id(),img_out, img_binding_points::CS_OUTPUT_A);
        check_gl_error();

        reduce2_.set_uniform_i("tx_total_log_lum",txt_binding_points::TOTAL_LOG_LUM);
    }

    ufrm_id_.near_l=glGetUniformLocation(sdpli_sp_.id(),"near_plane");
    ufrm_id_.near_t=glGetUniformLocation(lamp_sp_.id(),"near_plane");
    ufrm_id_.near_sl=glGetUniformLocation(sepli_sp_.id(),"near_plane");
    ufrm_id_.debug_mode=glGetUniformLocation(debug_sp_.id(),"debug_mode");


    float radius_mod    = 1/glm::cos(glm::pi<float>()/Light_drawer::side_count());
    sdpli_sp_.set_uniform_f("radius_modifier",radius_mod);
    sepli_sp_.set_uniform_f("radius_modifier",radius_mod);

    transform_buffer_.set_binding(sdpli_sp_,"Transform");
    transform_buffer_.set_binding(sepli_sp_,"Transform");
    transform_buffer_.set_binding(sedli_sp_,"Transform");
    transform_buffer_.set_binding(sky_sp_,"Transform");
    transform_buffer_.set_binding(debug_sp_,"Transform");
    transform_buffer_.set_binding(main_sp_,"Transform");

    sc_cfg_buffer_.set_binding(main_sp_,"Scene_cfg");
    sc_cfg_buffer_.set_binding(sky_sp_,"Scene_cfg");
    sc_cfg_buffer_.set_binding(smaa_sp_detect,"Scene_cfg");
    sc_cfg_buffer_.set_binding(smaa_sp_weight,"Scene_cfg");
    sc_cfg_buffer_.set_binding(smaa_sp_blend,"Scene_cfg");
    sc_cfg_buffer_.set_binding(ambient_sp_,"Scene_cfg");
    sc_cfg_buffer_.set_binding(reduce2_,"Scene_cfg");
    sc_cfg_buffer_.set_binding(mip_generation_sp_,"Scene_cfg");
    sc_cfg_buffer_.set_binding(gauss_blur_small_sp_,"Scene_cfg");

    sepl_sm_buffer_.set_binding(sepli_sp_,"PointLightData");

    cascade_buffer_.set_binding(sedli_sp_,"DlFullData");
    cascade_buffer_.set_binding(debug_sp_,"DlFullData");

    init_mesh_shaders();

    shader_loader_.clear();                 // to make possible a shader update.
}

void Rendering_backend_OGL4::init(Window_frame_size const& new_size)
{
    init_fixed_size();
    resize(new_size);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
    glEnable(GL_MULTISAMPLE);
    check_gl_error();

    init_storage();
    init_shaders();

    light_drawer_.init();
    pls_drawer_.init();
    unit_drawer_.init();
    skybox_drawer_.init();

    //get opengl params
    glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT,&ogl_properties_.ubo_offset_align);
    glGetIntegerv(GL_SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT,&ogl_properties_.ssbo_offset_align);

    using VAO_guard=ogl::VAO::Binding;

    {//set up actor drawing
        VAO_guard vao_binding_guard(a_vao);
        Vertex_format act_instance_format;
        act_instance_format.set_stride<Actor_instance_data>();
        act_instance_format.add_i(10,1,GL_UNSIGNED_INT,offsetof(Actor_instance_data,Actor_instance_data::joints_begin));
        act_instance_format.add_i(11,1,GL_UNSIGNED_INT,offsetof(Actor_instance_data,Actor_instance_data::material));
        act_instance_format.add_i(12,1,GL_UNSIGNED_INT,offsetof(Actor_instance_data,Actor_instance_data::inv_bp_transform_begin));
        act_instance_format.apply(a_instance_buffer_.id(),1,0);

        meshes_.set_up(a_binded_batch);
        meshes_.buffer_replaced=
                [this](){ VAO_guard guard(a_vao); meshes_.set_up(a_binded_batch); };

    }
    {//set up sceneries drawing
        VAO_guard vao_binding_guard(s_vao);

        st_meshes_.set_up(s_binded_batch);
        st_meshes_.buffer_replaced =
                [this](){ VAO_guard guard(s_vao); st_meshes_.set_up(s_binded_batch); };

        st_instances_.set_up();
        st_instances_.buffer_replaced =
                [this](){ VAO_guard guard(s_vao); st_instances_.set_up(); };
    }
}

void Rendering_backend_OGL4::prepare()
{
    glViewport(0, 0, (GLint)rendering_size().width, (GLint)rendering_size().height);

    textures_.commit();
    materials_.commit();
    meshes_.commit();
    st_meshes_.commit();
    st_instances_.commit();

    light_fbo_.bind();
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    color_fbo_.bind();
    glClearColor(0, 1, 0, 1);
    //glClear(GL_COLOR_BUFFER_BIT);

    smaa_blend_.bind();
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT);
    smaa_edges_.bind();
    glClearColor(0, 0, 0, 0);
    glClearStencil(0);
    glClear(GL_COLOR_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
}

namespace {

//auxiliary stuff for process_scene method
namespace process_scene_auxiliary{

struct AD_command_proto{
    Batch_group bg;
    u32 instance;   // assumed size of instance buffer less than 64 gb :)
    u32 mesh_ix;
};

using AD_cmd_prototypes=std::vector<AD_command_proto>;
using Index_output=Scene_access::Index_output;

struct PL_task_info{
    Index_output::iterator begin;
    Index_output::iterator end;
    AD_cmd_prototypes* dest=nullptr;
    ogl4::Model::Index lod;
};



constexpr ogl4::Model::Index MAX_LOD_COUNT=8;
constexpr size_t UNDEFINED_INSTANCE_OFFSET=-1;

ogl4::Model::Index get_object_lod(float dist_to_cam){
    return 0;
}

ogl4::Model::Index get_csm_lod(size_t sm_ix){
    return 0;
}

Position get_aabb_corner(AABB const& aabb,size_t ix){
    return Position(
                ix&1<<0 ? aabb.max.x:aabb.min.x,
                ix&1<<1 ? aabb.max.y:aabb.min.y,
                ix&1<<2 ? aabb.max.z:aabb.min.z
           );
}

/*
 * note: split_visible_set and fill_pl_tasks is used now only once, but they have to be used more in case of streaming instance data of sceneries.
 */

//the function for dividing the visible set of objects into the buckets according to LOD of the object
template<typename Boundary_functor>
void split_visible_set(
        Index_output (&buckets_out)[MAX_LOD_COUNT], //buckets output
        Index_output const& visible_set,
        Direction const& cam_dir,   //camera direction, unit vector
        Position const& cam_pos,    //camera position
        size_t corner_ix,           //index of nearest to camera corner of boundary box
        Boundary_functor get_boundary //a functor for getting a boundary box of object, the functor signature is "AABB const& (u32 index_of_object)"
        )
{
    for(u32 a_ix:visible_set){
        AABB const& boundary=get_boundary(a_ix);
        float view_space_z_val=glm::dot(cam_dir,( get_aabb_corner(boundary,corner_ix)-cam_pos));
        ogl4::Model::Index lod_ix=get_object_lod(view_space_z_val);
        buckets_out[lod_ix].push_back(a_ix);
    }
}

template<typename Boundary_functor>
void fill_pl_tasks(
        std::vector<PL_task_info>& tasks_output,
        Scene_access::Point_light_output& point_lights,
        Index_output* visible_set,  //array of visible object sets, one set for each element in point_lights
        Direction const& cam_dir,   //camera direction, unit vector
        Position const& cam_pos,    //camera position
        size_t corner_ix,           //index of nearest to camera corner of boundary box
        AD_cmd_prototypes* destinations, //command prototype containers array for destinations of generated tasks, one container for each element in point_lights
        Boundary_functor get_boundary //a functor for getting a boundary box of object, the functor signature is "AABB const& (u32 index_of_object)"
        )
{
    for(size_t i=0, i_e=point_lights.size() ; i!=i_e ; ++i){
        Point_light_data const& pl_data=*point_lights[i];
        Index_output& pl_set=visible_set[i];
        const float pl_center_proj=glm::dot(pl_data.position-cam_pos,cam_dir); //z coordinate of point light in view space
        const ogl4::Model::Index near_lod=get_object_lod(pl_center_proj-pl_data.range); //lod index for nearest to camera half of light source sphere
        const ogl4::Model::Index far_lod=get_object_lod(pl_center_proj); //lod index for farest to camera half of light source sphere

        auto pred=[pl_center_proj,corner_ix,get_boundary,&cam_pos,&cam_dir](u32 ix)->bool{
            AABB const& boundary=get_boundary(ix);
            float proj=glm::dot(cam_dir,( get_aabb_corner(boundary,corner_ix)-cam_pos)); //upper bound of z-coordinate of object in view space
            return proj<pl_center_proj;
        };

        Index_output::iterator far_part_begin=std::partition(pl_set.begin(),pl_set.end(),pred);
        if(pl_set.begin()!=far_part_begin){
            PL_task_info task;
            task.begin=pl_set.begin();
            task.end=far_part_begin;
            task.dest=destinations+i;
            task.lod=near_lod;
            tasks_output.emplace_back(std::move(task));
        }
        if(pl_set.end()!=far_part_begin){
            PL_task_info task;
            task.begin=far_part_begin;
            task.end=pl_set.end();
            task.dest=destinations+i;
            task.lod=far_lod;
            tasks_output.emplace_back(std::move(task));
        }
    }
    std::sort(tasks_output.begin(),tasks_output.end(), [](PL_task_info const& lhv,PL_task_info const& rhv)->bool{return lhv.lod<rhv.lod;});
}

//Forward_iterator have to be dereferenceable to AD_command_proto&
template<typename Forward_iterator, typename Functor>
void make_commands(
        std::vector<Draw_inderect_command>& commands_output,
        Frame_data::Batches& batch_output,
        Forward_iterator first, // [first, last) - is the range of commands prototypes to convert
        Forward_iterator last,
        Functor convert //a functor for creating a real command from a prototype, the functor signature is "Draw_inderect_command (AD_command_proto const&)"
        )
{
    batch_output.emplace_back();
    Frame_data::Batch_info* current_batch=&batch_output.back();
    current_batch->begin=commands_output.size();
    if(first!=last){
        current_batch->batch=first->bg;

        for(;first!=last; ++first){
            AD_command_proto & proto=*first;
            if(current_batch->batch!=proto.bg){
                current_batch->end=commands_output.size();

                batch_output.emplace_back();
                current_batch=&batch_output.back();
                current_batch->begin=commands_output.size();
                current_batch->batch=proto.bg;
            }
            commands_output.emplace_back(convert(proto));
        }
    }
    current_batch->end=commands_output.size();
}

}
}


void Rendering_backend_OGL4::process_scene(Scene_access& scene_access)
{
    using namespace process_scene_auxiliary;
    //ensure cleanness of frame_data
    Camera const& cam=scene_access.camera();
    Matrix_transforms& camera_data=transform_buffer_.prefix_data();
    Scene_time elapsed_time = scene_access.collect();
    Scene_time total_time   = scene_access.total_time();

    //camera data preparation
    {
        generate_camera_matrices(cam,rendering_size(),camera_data.view,camera_data.proj,cam.farthest(),cam.nearest());
        transform_buffer_.commit();
        glProgramUniform1f(sdpli_sp_.id(),ufrm_id_.near_l,cam.nearest());
        glProgramUniform1f(lamp_sp_.id(),ufrm_id_.near_t,cam.nearest());
        glProgramUniform1f(sepli_sp_.id(),ufrm_id_.near_sl,cam.nearest());
    }

    {
        Skybox_handle sk = scene_access.skybox();
        if(sk){
            skybox_.use(sk);
        }else{
            //TODO: rendering without skybox. scattering?
        }
    }

    // the scene configuration
    {
        float dt = elapsed_time;
        bool reload_shading_variables = scene_access.shading_var_changes();

        if( scene_is_changed_ ){
            dt = std::numeric_limits<float>::max();
            reload_shading_variables  = true;
            scene_is_changed_       = false;
        }

        Scene_cfg_data& scene_cfg   = sc_cfg_buffer_.prefix_data();
        scene_cfg.ambient_light     = scene_access.ambient_light();
        scene_cfg.sky_light_scale   = scene_access.sky_light_scale();
        scene_cfg.dt                = dt;

        // it is not necessary to read shading variables of the scene at every frame.
        // read it only if they are changed.
        if(reload_shading_variables){
            scene_cfg.burn_out_ratio    = scene_access.get_shading_var_f("tone_mapping::burn_out_ratio",16);
            scene_cfg.bloom_weights.x   = scene_access.get_shading_var_f("bloom::small_blur_weight",0.075);
            scene_cfg.bloom_weights.y   = scene_access.get_shading_var_f("bloom::medium_blur_weight",0.075);
            scene_cfg.bloom_weights.z   = scene_access.get_shading_var_f("bloom::large_blur_weight",0.075);
            scene_cfg.bp_threshold_r    = scene_access.get_shading_var_f("bloom::threshold_ratio",2);
            scene_cfg.tm_key            = scene_access.get_shading_var_f("tone_mapping::key_value",0.18);

            scene_cfg.adaptation_rate   = scene_access.get_shading_var_f("lum_adaptation::rate",0.04);
            scene_cfg.adaptation_min    = scene_access.get_shading_var_f("lum_adaptation::min",std::numeric_limits<float>::min());
            scene_cfg.adaptation_max    = scene_access.get_shading_var_f("lum_adaptation::max",std::numeric_limits<float>::max());

        }

        sc_cfg_buffer_.commit();

    }

    //misc parametrs set up
    {
        auto debug_mode=scene_access.debug_mode();
        if(debug_mode){
            glProgramUniform1ui(debug_sp_.id(),ufrm_id_.debug_mode,debug_mode);
        }
    }

    glm::mat4x4 world_to_screen=camera_data.proj*camera_data.view;
    glm::mat4x4 screen_to_world=glm::inverse(world_to_screen);
    glm::mat4x4 view_to_scene=glm::inverse(camera_data.view);

    //directional light sources' frame data preparation
    {
        std::vector<glm::mat4x4>& sm_proj=frame_data_.dl_shadow_map_proj;
        std::vector<size_t>& sm_count=frame_data_.dl_shadow_map_count;
        AABB const& scene_boundary=scene_access.boundary();

        selection_cache_.directional_lights.clear();
        Scene_access::Directional_light_output& dls=selection_cache_.directional_lights;
        dls.clear();
        scene_access.directional_lights(dls);

        if(!dls.empty()){
            sm_count.reserve(dls.size());
            sm_proj.reserve(DL_CSM_data::MAX_CASCADE_SIZE*dls.size());

            cascade_buffer_.bind();
            cascade_buffer_.reallocate(csm_data_size(dls.size()));

            char* csm_data_ptr=static_cast<char*>(glMapBuffer(cascade_buffer_.type(),GL_WRITE_ONLY));
            check_gl_error();
            if(csm_data_ptr==NULL){
                log_msg(Detail_level::CRITICAL,"failed to map csm buffer data");
            }

            for(size_t i=0,end=dls.size() ; i<end ; ++i){
                DL_CSM_data& csm_data=*static_cast<DL_CSM_data*>(static_cast<void*>(csm_data_ptr+csm_data_offset(i)));
                set_up_csm_data(csm_data,*dls[i],scene_boundary,cam,screen_to_world,DL_SHADOW_MAP_SIZE);
                sm_count.emplace_back(csm_data.map_count);

                for(size_t sm_ix=0;sm_ix<csm_data.map_count; ++sm_ix){
                    glm::mat4x4 & proj_ix=csm_data.cascade[sm_ix].proj;
                    sm_proj.emplace_back(proj_ix);
                    proj_ix=proj_ix*view_to_scene;
                }
            }

            if(glUnmapBuffer(cascade_buffer_.type())!=GL_TRUE){
                log_msg(Detail_level::CRITICAL,"failed to unmap csm buffer data");
            }

            frame_data_.dl_shadow_pass_draws.resize(sm_proj.size());
        }

    }

    std::vector<Scene_access::Transform_matrix> scpl_culling_transforms;
    //shadow casting point lights processing
    {
        Scene_access::Point_light_output & point_lights=selection_cache_.sepls;
        point_lights.clear();
        scene_access.select(world_to_screen,true,point_lights);

        if(!point_lights.empty()){
            scpl_culling_transforms.resize(point_lights.size());
            sepl_sm_buffer_.reallocate(sepl_data_size(point_lights.size()));
            sepl_sm_buffer_.bind();
            char* scpl_data_ptr=static_cast<char*>(glMapBuffer(sepl_sm_buffer_.type(),GL_WRITE_ONLY));
            check_gl_error();
            if(scpl_data_ptr==NULL){
                log_msg(Detail_level::CRITICAL,"failed to map cpl data buffer");
            }
            for(size_t i=0,i_e=point_lights.size();i<i_e;++i){
                Sepl_sm_data& scpl_data=*static_cast<Sepl_sm_data*>(static_cast<void*>(scpl_data_ptr+sepl_data_offset(i)));
                mat4x4 scpl_view=get_sepl_view_matrix(*point_lights[i],camera_data.view);
                set_up_sepl_sm_data(scpl_data,*point_lights[i],scpl_view);
                set_up_sepl_culling_matrix(scpl_culling_transforms[i],*point_lights[i],scpl_view);
            }

            if(glUnmapBuffer(sepl_sm_buffer_.type())!=GL_TRUE){
                log_msg(Detail_level::CRITICAL,"failed to unmap cpl data buffer");
            }
            frame_data_.pl_shadow_pass_draws.resize(point_lights.size());
            pls_drawer_.reload(point_lights.begin(),point_lights.size());
        }

    }

    //point lights processing
    {
        Scene_access::Point_light_output & point_lights=selection_cache_.sdpls;
        point_lights.clear();
        scene_access.select(world_to_screen,false,point_lights);
        if(!point_lights.empty()){
            light_drawer_.reload(point_lights.begin(),point_lights.size());
            frame_data_.simple_pl_count=point_lights.size();
        }
    }

    std::vector<Draw_inderect_command> commands;

    using Index_output=Scene_access::Index_output;
    //actors' index 'set' for gbuffer pass (data pass), 'set' doesn't mean std::set
    Scene_access::Index_output data_pass_set;

    //actors' index 'sets' to draw shadow maps for directional light sources
    std::vector<Scene_access::Index_output> dl_sm_pass_set(frame_data_.dl_shadow_map_proj.size());

    //actors' index 'sets' to draw cube shadow maps for point light sources
    std::vector<Scene_access::Index_output> pl_sm_pass_set(frame_data_.pl_shadow_pass_draws.size());

    //vector of pointers to 'sets' above for scene access interface
    std::vector<Scene_access::Index_output*> indices_ptr;

    //vector of pointers to transform matrices of 'sets' above
    std::vector<const Scene_access::Transform_matrix*> transforms;

    //setting up vectors of pointers listed above ^
    {
        indices_ptr.reserve(1+dl_sm_pass_set.size()+pl_sm_pass_set.size());
        transforms.reserve(indices_ptr.size());

        transforms.push_back(&world_to_screen);
        indices_ptr.push_back(&data_pass_set);

        for( size_t ix=0, i_e=dl_sm_pass_set.size() ; ix<i_e ; ++ix ){
            transforms.push_back(&frame_data_.dl_shadow_map_proj[ix]);
            indices_ptr.push_back(&dl_sm_pass_set[ix]);
        }

        for( size_t ix=0, i_e=pl_sm_pass_set.size() ; ix<i_e ; ++ix ){
            transforms.push_back(&scpl_culling_transforms[ix]);
            indices_ptr.push_back(&pl_sm_pass_set[ix]);
        }
    }

    //variables for lod choosing
    Direction cam_dir=glm::normalize(cam.direction());

    //nearest corner of object aabb to cam
    const size_t corner_ix=
            (cam_dir.x>0?0:1<<0)+
            (cam_dir.y>0?0:1<<1)+
            (cam_dir.z>0?0:1<<2);
    //end of variables for lod choosing

    //gbuffer pass auxiliary variables
    //buckets for grouping indices for gbuffer pass by lod
    Scene_access::Index_output data_pass_lod_backets[MAX_LOD_COUNT];
    //prototypes of draw commands for gbuffer pass
    AD_cmd_prototypes data_pass_cmds;
    //end of gbuffer pass auxiliary variables

    //point light shadow maps generation pass auxiliary variables
    //prototypes of draw commands for point light shadow maps generation pass
    std::vector<AD_cmd_prototypes> pl_sm_pass_cmds(frame_data_.pl_shadow_pass_draws.size());
    std::vector<PL_task_info> pl_tasks;
    pl_tasks.reserve(pl_sm_pass_cmds.size()*2);
    //end of point light shadow maps generation pass auxiliary variables

    //directional light shadow maps generation pass auxiliary variables
    std::vector<size_t> dl_sm_offsets;
    dl_sm_offsets.reserve(frame_data_.dl_shadow_map_count.size()+1);
    dl_sm_offsets.push_back(0);
    std::partial_sum(frame_data_.dl_shadow_map_count.begin(),frame_data_.dl_shadow_map_count.end(),std::back_inserter(dl_sm_offsets));
    //end of directional light shadow maps generation pass auxiliary variables

    //this closure is used for sorting
    auto cmd_comp=[](AD_command_proto const& lhv, AD_command_proto const& rhv){
        return lhv.bg.value()<rhv.bg.value();
    };

    //setting up visible sets
    selection_cache_.actors.clear();
    scene_access.select(transforms.size(),transforms.data(),selection_cache_.actors,indices_ptr.data());
    Scene_access::Actors_output const& total_actors=selection_cache_.actors;

    //Actor processing
    if(!total_actors.empty())
    {
        //now index sets are filled

        std::vector<GLuint> joint_offset;   //value is "byte offset / sizeof(Joint)", i.e. index in array of Joint
        joint_offset.resize(total_actors.size());

        //joints uploading
        {
            /*
             * joint buffer data has following structure:
             * struct{
             *      struct {
             *          vec4 rotation;
             *          vec3 position;
             *          float scale;
             *      } state; //sizeof(state) == sizeof(Joint)
             *      Joint joints[sk_size]; // sk_size is number of joints in skeleton
             * } elemets[total_actors.size()]
             *
             * "(Joint*)start_addr+joint_offset[i]" points to elemets[i].joints[0], not elemets[i].state,
             */

            GLuint total_joints=1;
            for(size_t i=0, i_e=total_actors.size() ; i<i_e ; ++i){
                const size_t sk_size=cpu_animator::Skeleton_instance::downcast(total_actors[i]->state.skeleton()).size();
                joint_offset[i]=total_joints;
                total_joints+=sk_size+1;
            }
            --total_joints;

            if(total_joints){
                joint_buffer_.reallocate(total_joints*sizeof(Joint));
                joint_buffer_.bind();
                Joint* joint_ptr=static_cast<Joint*>(glMapBuffer(joint_buffer_.type(),GL_WRITE_ONLY));
                check_gl_error();
                if(joint_ptr==NULL){
                    log_msg(Detail_level::CRITICAL,"failed to map joint_buffer");
                }

                for(size_t i=0, i_e=total_actors.size() ; i<i_e ; ++i){
                    Animation_state & ast=const_cast<Animation_state&>(static_cast<Animation_state const&>(total_actors[i]->state));
                    if(&ast.engine()==&animation_engine_){
                        animation_engine_.write(joint_ptr+joint_offset[i],ast,total_time);
                        const size_t data_offset=joint_offset[i]-1;
                        joint_ptr[data_offset].real=ast.skeleton().rotatation();
                        joint_ptr[data_offset].dual.x=ast.skeleton().position().x;
                        joint_ptr[data_offset].dual.y=ast.skeleton().position().y;
                        joint_ptr[data_offset].dual.z=ast.skeleton().position().z;
                        joint_ptr[data_offset].dual.w=ast.skeleton().scale();
                    }
                }
                if(glUnmapBuffer(joint_buffer_.type())!=GL_TRUE){
                    log_msg(Detail_level::CRITICAL,"failed to unmap joint_buffer");
                }
            }

        }   //joints uploaded

        //filling draw commands,
        //setting up actor drawing batch data,
        //uploading actors' instance data to gpu
        {

            size_t max_command_count=0;
            for(const Actor_data * a_data: total_actors){
                for(const Model_instance_base* mi:a_data->models){
                    size_t max_sm=0;
                    ogl4::Model const& model=ogl4::Model::downcast(*mi->type());
                    for(ogl4::Model::Index lod=0;lod<model.lod_count();++lod){
                        if(max_sm<model.lod(lod).size()){
                            max_sm=model.lod(lod).size();
                        }
                    }
                    max_command_count+=max_sm;
                }
            }

            //dirty hack, temporary implementation
            std::vector<Actor_instance_data> instance_data;
            instance_data.reserve(max_command_count);
            commands.reserve(max_command_count);

            //self explaining functor
            auto get_boundary=[&total_actors](u32 ix)->AABB const&{return total_actors[ix]->local_boundary;};

            //setting up gbuffer pass auxiliary variables
            split_visible_set(data_pass_lod_backets,data_pass_set,cam_dir,cam.position(),corner_ix,get_boundary);

            //setting up point light shadow maps generation pass auxiliary variables
            fill_pl_tasks(pl_tasks,selection_cache_.sepls,pl_sm_pass_set.data(),cam_dir,cam.position(),corner_ix,pl_sm_pass_cmds.data(),get_boundary);

            //setting up:
            //-instance data
            //-drawing commands
            //-batch info (e.g. frame_data_.a_batches)
            //-actor draw list in frame_data_
            {
                /*
                 * instance_offset is used to get the offset to the corresponding instance data of submodel,
                 * to avoid unnecessary instance data duplication.
                 * in the general case instance data is unique for each level of detail.
                 */
                std::vector<size_t> instance_offset;
                size_t csm_level=0;
                std::vector<AD_command_proto> dl_csm_cmd;   //one for all shadow maps of directional light sources, reuseable.

                //this one is pointing to next task for generation point light shadow map generation related data.
                auto pl_task=pl_tasks.begin();

                //I think closure is better than method, that have eight arguments, and have no significant meaning outside this method
                //this closure builds a list of command prototypes to sort it later, and at the same time fills instance data
                auto process_actors=[&total_actors,&joint_offset,&instance_offset,this](
                        Scene_access::Index_output::const_iterator ix_begin,
                        Scene_access::Index_output::const_iterator ix_end,
                        u32 lod_ix,
                        std::vector<Rendering_backend_OGL4::Actor_instance_data>& instance_data_output,
                        std::vector<AD_command_proto>& command_proto_output
                        )->void
                {
                    for(;ix_begin!=ix_end;++ix_begin){
                        Scene_access::Index_output::value_type a_ix=*ix_begin;

                        bool new_instance = instance_offset[a_ix]==UNDEFINED_INSTANCE_OFFSET;
                        if(new_instance){
                            assert(instance_data_output.size()!=UNDEFINED_INSTANCE_OFFSET);
                            instance_offset[a_ix]=instance_data_output.size();
                        }
                        u32 instance=instance_offset[a_ix];

                        for(const Model_instance_base* mi: total_actors[a_ix]->models){
                            ogl4::Model const& model=ogl4::Model::downcast(*mi->type());
                            for(ogl4::Submodel const& sm: model.lod(lod_ix)){
                                Animated_mesh_storage::Mesh_info const& mesh=meshes_[sm.mesh];
                                //imho branching here is better than additional for loop with random memory access
                                if(new_instance){
                                    instance_data_output.emplace_back(Actor_instance_data{joint_offset[a_ix],sm.material,mesh.transform_offset,0});
                                }
                                command_proto_output.emplace_back(AD_command_proto{sm.bg, instance, sm.mesh});
                                ++instance;
                            }
                        }
                    }
                };

                //closure for use with make_commands
                auto make_cmd=[this](AD_command_proto const& proto)-> Draw_inderect_command{
                    Animated_mesh_storage::Mesh_info const& mesh=meshes_[proto.mesh_ix];
                    return Draw_inderect_command{
                                                     mesh.index_count,
                                                     1,
                                                     mesh.first_index,
                                                     mesh.base_vertex,
                                                     proto.instance
                                                 };
                };

                for(ogl4::Model::Index current_lod=0; current_lod<MAX_LOD_COUNT ; ++current_lod){
                    instance_offset.clear();
                    instance_offset.resize(total_actors.size(),UNDEFINED_INSTANCE_OFFSET);

                    //directional light CSM part
                    for(; get_csm_lod(csm_level)==current_lod && csm_level<DL_CSM_data::MAX_CASCADE_SIZE ; ++csm_level){
                        for(size_t ls_ix=0,ls_ix_end=dl_sm_offsets.size()-1; ls_ix<ls_ix_end ;++ls_ix){
                            const size_t sm_ix=dl_sm_offsets[ls_ix]+csm_level;  //global index of shadow map, i.e. in frame_data_.dl_shadow_map_proj for example.
                            if(sm_ix<dl_sm_offsets[ls_ix+1]){
                                Index_output const& visible_set=dl_sm_pass_set[sm_ix];
                                dl_csm_cmd.clear();
                                process_actors(visible_set.begin(),visible_set.end(),current_lod,instance_data,dl_csm_cmd);

                                for(AD_command_proto& cmd: dl_csm_cmd){
                                    cmd.bg=cmd.bg.shadow_pass();    //converting it to shadow map pass variant
                                }
                                std::sort(dl_csm_cmd.begin(),dl_csm_cmd.end(),cmd_comp);

                                frame_data_.dl_shadow_pass_draws[sm_ix].actors.begin=frame_data_.a_batches.size();
                                make_commands(commands,frame_data_.a_batches,dl_csm_cmd.begin(),dl_csm_cmd.end(),make_cmd);
                                frame_data_.dl_shadow_pass_draws[sm_ix].actors.end=frame_data_.a_batches.size();
                            }
                        }
                    }

                    //point light shadow map pass part
                    {
                        for(; pl_task!=pl_tasks.end() && pl_task->lod==current_lod ; ++pl_task){
                            process_actors(pl_task->begin,pl_task->end,current_lod,instance_data,*pl_task->dest);
                        }
                    }

                    //gbuffer pass part
                    {
                        Index_output const& visible_set=data_pass_lod_backets[current_lod];
                        //all LOD are drawn at once, so real commands are generated after all command prototypes are gathered.
                        process_actors(visible_set.begin(),visible_set.end(),current_lod,instance_data,data_pass_cmds);
                    }
                }

                //gbuffer pass draw commands, batch info and draw list generation
                {
                    std::sort(data_pass_cmds.begin(),data_pass_cmds.end(),cmd_comp);

                    frame_data_.data_pass_draw.actors.begin=frame_data_.a_batches.size();
                    make_commands(commands,frame_data_.a_batches,data_pass_cmds.begin(),data_pass_cmds.end(),make_cmd);
                    frame_data_.data_pass_draw.actors.end=frame_data_.a_batches.size();
                }

                //point light shadow map generation pass draw commands, batch info and draw list generation
                {
                    for(size_t pl_ix=0,pl_ix_end=pl_sm_pass_cmds.size();pl_ix<pl_ix_end;++pl_ix){

                        AD_cmd_prototypes& prototypes=pl_sm_pass_cmds[pl_ix];
                        for(AD_command_proto& cmd: prototypes){
                            cmd.bg=cmd.bg.shadow_pass();    //converting it to shadow map pass variant
                        }
                        std::sort(prototypes.begin(),prototypes.end(),cmd_comp);

                        frame_data_.pl_shadow_pass_draws[pl_ix].actors.begin=frame_data_.a_batches.size();
                        make_commands(commands,frame_data_.a_batches,prototypes.begin(),prototypes.end(),make_cmd);
                        frame_data_.pl_shadow_pass_draws[pl_ix].actors.end=frame_data_.a_batches.size();
                    }
                }
            }

            a_instance_buffer_.reallocate(sizeof(Actor_instance_data)*instance_data.size());
            a_instance_buffer_.upload<Actor_instance_data>(0,instance_data.data(),instance_data.size());
        }
    }

    //clearing reusable containers
    {
        data_pass_set.clear();
        for(auto& v:dl_sm_pass_set){
            v.clear();
        }
        for(auto& v:pl_sm_pass_set){
            v.clear();
        }
    }

    //setting up visible sets
    selection_cache_.sceneries.clear();
    scene_access.select(transforms.size(),transforms.data(),selection_cache_.sceneries,indices_ptr.data());
    Scene_access::Scenery_output const& total_sceneries=selection_cache_.sceneries;

    //sceneries processing
    if(!total_sceneries.empty())
    {
        AD_cmd_prototypes cmd_prototypes;

        //this closure builds a list of command prototypes to sort it later
        auto process_scenery=
                [&total_sceneries,this]
                (std::vector<AD_command_proto>& cmd_proto_output, u32 sc_ix, u32 lod_ix)->void
        {
            ogl4::Static_model_instance::LOD const& lod=ogl4::Static_model_instance::downcast(total_sceneries[sc_ix]->model()).lod(lod_ix);
            for(ogl4::Static_model_instance_part const& sm:lod){
                cmd_proto_output.emplace_back(AD_command_proto{sm.bg, sm.instance, sm.mesh});
            }
        };

        //closure for use with make_commands
        auto make_cmd=[this](AD_command_proto const& proto)-> Draw_inderect_command{
            Static_mesh_storage::Mesh_info const& mesh=st_meshes_[proto.mesh_ix];
            return Draw_inderect_command{
                                             mesh.index_count,
                                             1,
                                             mesh.first_index,
                                             mesh.base_vertex,
                                             proto.instance
                                         };
        };

        //gbuffer pass sceneries processing
        {
            for(u32 s_ix: data_pass_set){
                Scenery_data const& sc=*total_sceneries[s_ix];
                AABB const& boundary=sc.boundary();
                float view_space_z_val=glm::dot(cam_dir,( get_aabb_corner(boundary,corner_ix)-cam.position()));
                ogl4::Model::Index lod_ix=get_object_lod(view_space_z_val);
                process_scenery(cmd_prototypes,s_ix,lod_ix);
            }

            std::sort(cmd_prototypes.begin(),cmd_prototypes.end(),cmd_comp);

            frame_data_.data_pass_draw.sceneries.begin=frame_data_.s_batches.size();
            make_commands(commands,frame_data_.s_batches,cmd_prototypes.begin(),cmd_prototypes.end(),make_cmd);
            frame_data_.data_pass_draw.sceneries.end=frame_data_.s_batches.size();
        }

        //directional light shadow map pass sceneries processing
        {
            cmd_prototypes.clear();
            for(size_t ls_ix=0,ls_ix_end=frame_data_.dl_shadow_map_count.size(); ls_ix<ls_ix_end ;++ls_ix){
                for(size_t csm_lvl=0, csm_lvl_end=frame_data_.dl_shadow_map_count[ls_ix] ; csm_lvl<csm_lvl_end ; ++csm_lvl){
                    ogl4::Model::Index lod_ix=get_csm_lod(csm_lvl);
                    const size_t sm_ix=dl_sm_offsets[ls_ix]+csm_lvl;
                    for(u32 s_ix: dl_sm_pass_set[sm_ix]){
                        process_scenery(cmd_prototypes,s_ix,lod_ix);
                    }

                    for(AD_command_proto& cmd: cmd_prototypes){
                        cmd.bg=cmd.bg.shadow_pass();    //converting it to shadow map pass variant
                    }
                    std::sort(cmd_prototypes.begin(),cmd_prototypes.end(),cmd_comp);

                    frame_data_.dl_shadow_pass_draws[sm_ix].sceneries.begin=frame_data_.s_batches.size();
                    make_commands(commands,frame_data_.s_batches,cmd_prototypes.begin(),cmd_prototypes.end(),make_cmd);
                    frame_data_.dl_shadow_pass_draws[sm_ix].sceneries.end=frame_data_.s_batches.size();
                }
            }
        }

        //point light shadow map pass sceneries processing
        {
            cmd_prototypes.clear();
            Scene_access::Point_light_output const& point_lights=selection_cache_.sepls;
            for(size_t ls_ix=0,ls_ix_end=frame_data_.pl_shadow_pass_draws.size(); ls_ix<ls_ix_end ;++ls_ix){
                Point_light_data const& pl_data=*point_lights[ls_ix];
                Index_output& pl_set=pl_sm_pass_set[ls_ix];
                const float pl_center_proj=glm::dot(pl_data.position-cam.position(),cam_dir); //z coordinate of point light in view space
                const ogl4::Model::Index near_lod=get_object_lod(pl_center_proj-pl_data.range); //lod index for nearest to camera half of light source sphere
                const ogl4::Model::Index far_lod=get_object_lod(pl_center_proj); //lod index for farest to camera half of light source sphere

                for(u32 s_ix: pl_set){
                    AABB const& boundary=total_sceneries[s_ix]->boundary();
                    float proj=glm::dot(cam_dir,( get_aabb_corner(boundary,corner_ix)-cam.position())); //upper bound of z-coordinate of object in view space
                    const ogl4::Model::Index lod_ix=(proj<pl_center_proj)?near_lod:far_lod;
                    process_scenery(cmd_prototypes,s_ix,lod_ix);
                }

                for(AD_command_proto& cmd: cmd_prototypes){
                    cmd.bg=cmd.bg.shadow_pass();    //converting it to shadow map pass variant
                }
                std::sort(cmd_prototypes.begin(),cmd_prototypes.end(),cmd_comp);

                frame_data_.pl_shadow_pass_draws[ls_ix].sceneries.begin=frame_data_.s_batches.size();
                make_commands(commands,frame_data_.s_batches,cmd_prototypes.begin(),cmd_prototypes.end(),make_cmd);
                frame_data_.pl_shadow_pass_draws[ls_ix].sceneries.end=frame_data_.s_batches.size();
            }
        }

    }
    command_buffer_.reallocate(sizeof(Draw_inderect_command)*commands.size());
    command_buffer_.upload<Draw_inderect_command>(0,commands.data(),commands.size());
}

GLintptr Rendering_backend_OGL4::csm_data_offset(int index) const
{
    return buffer_data_offset<DL_CSM_data>(index,ogl_properties_.ubo_offset_align);
}

size_t Rendering_backend_OGL4::csm_data_size(size_t count) const
{
    return buffer_data_size<DL_CSM_data>(count,ogl_properties_.ubo_offset_align);
}

GLintptr Rendering_backend_OGL4::sepl_data_offset(int index) const
{
    return buffer_data_offset<Sepl_sm_data>(index,ogl_properties_.ubo_offset_align);
}

size_t Rendering_backend_OGL4::sepl_data_size(size_t count) const
{
    return buffer_data_size<Sepl_sm_data>(count,ogl_properties_.ubo_offset_align);
}

size_t Rendering_backend_OGL4::get_stage_sps_ix(Rendering_backend_OGL4::Stage stage)
{
    size_t stage_ix;
    switch (stage) {
        case Stage::DATA:
            stage_ix=0;
            break;
        case Stage::DIRECT_SHADOW:
            stage_ix=1;
            break;
        case Stage::POINT_SHADOW:
            stage_ix=2;
            break;
        default:
            stage_ix=0;
            break;
    }
    return stage_ix;
}

size_t Rendering_backend_OGL4::mesh_shader_index(Rendering_backend_OGL4::Stage stage, Rendering_backend_OGL4::Mesh_type type)
{
    return get_stage_sps_ix(stage)*(size_t)Mesh_type::TOTAL_SIZE + (size_t)type;
}

void Rendering_backend_OGL4::use_mesh_shader(Rendering_backend_OGL4::Stage stage, Rendering_backend_OGL4::Mesh_type type)
{
    size_t ix=mesh_shader_index(stage,type);
    mesh_sps_[ix].use();
}

void Rendering_backend_OGL4::init_mesh_shaders()
{
    using Shader = ogl::Shader;
    u32 glsl_version = 430;

    std::vector<Shader*> vs_mesh_type((size_t)Mesh_type::TOTAL_SIZE);

    Shader vs_mesh_animated(shader_loader_,GL_VERTEX_SHADER, "vs_mesh_animated", glsl_version);
    vs_mesh_animated.add_file("vs_mesh_animated.glsl").compile();

    Shader vs_mesh_static(shader_loader_,GL_VERTEX_SHADER, "vs_mesh_static", glsl_version);
    vs_mesh_static.add_file("vs_mesh_static.glsl").compile();

    vs_mesh_type[(size_t)Mesh_type::ANIMATED]=&vs_mesh_animated;
    vs_mesh_type[(size_t)Mesh_type::STATIC]=&vs_mesh_static;

    const size_t total_types=vs_mesh_type.size();
    const size_t total_stages=3;

    for(size_t i=0;i<total_stages*total_types;++i){
        mesh_sps_.emplace_back(dk::fstr("mesh shader s%1% %2%",i/total_types, vs_mesh_type[i%total_types]->name()));
    }

    Shader vs_data(shader_loader_,GL_VERTEX_SHADER, "vs_data", glsl_version);
    vs_data.add_file("vs_data.glsl").compile();

    Shader fs_gbuffer(shader_loader_,GL_FRAGMENT_SHADER, "gbuffer", glsl_version);
    fs_gbuffer.add_file("gbuffer.glsl").compile();

    Shader fs_data(shader_loader_,GL_FRAGMENT_SHADER, "fs_data", glsl_version);
    fs_data.add_file("fs_data.glsl").compile();

    Shader vs_dl(shader_loader_,GL_VERTEX_SHADER, "vs_dl", glsl_version);
    vs_dl.add_file("vs_dl.glsl").compile();

    Shader fs_shadowmap(shader_loader_,GL_FRAGMENT_SHADER, "fs_shadowmap", glsl_version);
    fs_shadowmap.add_file("fs_shadowmap.glsl").compile();

    Shader vs_cube(shader_loader_,GL_VERTEX_SHADER, "vs_cube", glsl_version);
    vs_cube.add_file("vs_cube.glsl").compile();

    Shader gs_cube(shader_loader_,GL_GEOMETRY_SHADER, "gs_cube", glsl_version);
    gs_cube.add_file("gs_cube.glsl").compile();

    for(size_t i=0;i<total_types;++i){
        size_t ix=get_stage_sps_ix(Stage::DATA)*total_types + i;
        mesh_sps_[ix].attach(*vs_mesh_type[i]).attach(vs_data).attach(fs_data).attach(fs_gbuffer).link();
        transform_buffer_.set_binding(mesh_sps_[ix],"Transform");
        materials_.bind(mesh_sps_[ix],"MaterialUBO");

        ix=get_stage_sps_ix(Stage::DIRECT_SHADOW)*total_types + i;
        mesh_sps_[ix].attach(*vs_mesh_type[i]).attach(vs_dl).attach(fs_shadowmap).link();
        ufrm_id_.csm_transform[i]=glGetUniformLocation(mesh_sps_[ix].id(),"transform_matrix");

        ix=get_stage_sps_ix(Stage::POINT_SHADOW)*total_types + i;
        mesh_sps_[ix].attach(*vs_mesh_type[i]).attach(vs_cube).attach(gs_cube).attach(fs_shadowmap).link();
        sepl_sm_buffer_.set_binding(mesh_sps_[ix],"PointLightData");
    }

    //animation shader initialization

    const size_t animatation_shader_index=(size_t)Mesh_type::ANIMATED;
    for(size_t i=0;i<total_stages;++i){
        size_t ix=i*total_types + animatation_shader_index;
        meshes_.bind(mesh_sps_[ix],"Offsets");
        joint_buffer_.set_binding(mesh_sps_[ix],"Joints");
    }
}

void Rendering_backend_OGL4::use_acc_fbuffer(bool use)
{
    if(use){
        light_fbo_.bind();
        glEnable(GL_BLEND);
        glDepthMask(GL_FALSE);
        glBlendEquationSeparate(GL_FUNC_ADD,GL_FUNC_ADD);
        glBlendFuncSeparate(GL_ONE,GL_ONE,GL_ONE,GL_ZERO);
    }else{
        glDepthMask(GL_TRUE);
        glDisable(GL_BLEND);
    }
}

void Rendering_backend_OGL4::draw_geometry(Rendering_backend_OGL4::Stage stage, Frame_data::Draw_list const& draws)
{
    command_buffer_.bind();
    using Vao_guard=ogl::VAO::Binding;

    assert(draws.sceneries.end>=draws.sceneries.begin);
    size_t draws_s_size=draws.sceneries.end-draws.sceneries.begin;
    if(draws_s_size){
        Frame_data::Batch_info const& first_batch=frame_data_.s_batches[draws.sceneries.begin];
        //check if it is possible to do nothing
        if(draws_s_size>1 || first_batch.end>first_batch.begin){
            use_mesh_shader(stage,Mesh_type::STATIC);
            Vao_guard guard(s_vao);
            for(size_t batch_ix=draws.sceneries.begin;batch_ix!=draws.sceneries.end;++batch_ix){
                Frame_data::Batch_info const& batch=frame_data_.s_batches[batch_ix];
                if(batch.end>batch.begin){

                    if(a_binded_batch!=batch.batch){
                        if(a_binded_batch.mesh()!=batch.batch.mesh()){
                            meshes_.set_up(batch.batch);
                            a_binded_batch.mesh(batch.batch);
                        }
                    }
                    textures_.set_up(batch.batch);  //texture bindings are independent from VAOs, and they are tracked in Texture_storage object

                    glMultiDrawElementsIndirect(GL_TRIANGLES,GL_UNSIGNED_INT,(void*)(batch.begin*sizeof(Draw_inderect_command)),batch.end-batch.begin,sizeof(Draw_inderect_command));
                    check_gl_error();
                }
            }
        }
    }

    assert(draws.actors.end>=draws.actors.begin);
    size_t draws_a_size=draws.actors.end-draws.actors.begin;
    if(draws_a_size){
        Frame_data::Batch_info const& first_batch=frame_data_.a_batches[draws.actors.begin];
        //check if it is possible to do nothing
        if(draws_a_size>1 || first_batch.end>first_batch.begin){
            use_mesh_shader(stage,Mesh_type::ANIMATED);
            Vao_guard guard(a_vao);
            for(size_t batch_ix=draws.actors.begin;batch_ix!=draws.actors.end;++batch_ix){
                Frame_data::Batch_info const& batch=frame_data_.a_batches[batch_ix];
                if(batch.end>batch.begin){

                    if(s_binded_batch!=batch.batch){
                        if(s_binded_batch.mesh()!=batch.batch.mesh()){
                            st_meshes_.set_up(batch.batch);
                            s_binded_batch.mesh(batch.batch);
                        }
                    }
                    textures_.set_up(batch.batch);

                    glMultiDrawElementsIndirect(GL_TRIANGLES,GL_UNSIGNED_INT,(void*)(batch.begin*sizeof(Draw_inderect_command)),batch.end-batch.begin,sizeof(Draw_inderect_command));
                    check_gl_error();
                }
            }
        }
    }
}

void Rendering_backend_OGL4::render_light()
{
    light_drawer_.draw();
}

void Rendering_backend_OGL4::geometry_stencil_test(bool b)
{
    if(b){
        glEnable(GL_STENCIL_TEST);
        glStencilFunc(GL_LESS,0,0xff);
        glStencilOp(GL_KEEP,GL_KEEP,GL_KEEP);
        check_gl_error();
    }else{
        glDisable(GL_STENCIL_TEST);
    }
}

void Rendering_backend_OGL4::frame_size(const Window_frame_size& new_size)
{
    frame_size_=new_size;
}

void Rendering_backend_OGL4::data_stage()
{
    data_fbo_.bind();
    glClearColor(0, 0, 0, 1);
    glEnable(GL_STENCIL_TEST);
    glClearStencil(0);
    glClear(GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // this is encoded (0,0,1).
    vec4 default_normal(0.5,0.5,0,0);

    vec4 default_albedo(0,0,0,0);
    vec4 default_spec(0,0,0,0);

    glClearBufferfv(GL_COLOR,0,&default_normal[0]);
    glClearBufferfv(GL_COLOR,1,&default_albedo[0]);
    glClearBufferfv(GL_COLOR,2,&default_spec[0]);

    glStencilOp(GL_KEEP,GL_KEEP,GL_REPLACE);
    glStencilFunc(GL_ALWAYS,1,0xff);
    draw_geometry(Stage::DATA,frame_data_.data_pass_draw);
    glDisable(GL_STENCIL_TEST);
}

void Rendering_backend_OGL4::sedl_stage()
{
    std::vector<size_t> const& sm_count=frame_data_.dl_shadow_map_count;
    size_t shadow_map_processed=0;
    for(size_t dl_ix=0;dl_ix<sm_count.size() ; ++dl_ix){
        cascade_buffer_.bind_range(csm_data_offset(dl_ix),sizeof(DL_CSM_data));
        glViewport(0, 0, DL_SHADOW_MAP_SIZE,DL_SHADOW_MAP_SIZE);
        glCullFace(GL_FRONT);
        check_gl_error();

        //generation of shadow maps
        for(size_t i=0;i<sm_count[dl_ix];++i){
            const size_t sm_global_ix=shadow_map_processed+i;
            Frame_data::Draw_list const& draws=frame_data_.dl_shadow_pass_draws[sm_global_ix];
            if(draws.actors.end>draws.actors.begin || draws.sceneries.end>draws.sceneries.begin){
                for(size_t t=0;t<(size_t)Mesh_type::TOTAL_SIZE;++t){
                    size_t ix=mesh_shader_index(Stage::DIRECT_SHADOW,(Mesh_type)t);
                    glm::mat4x4 const& transform=frame_data_.dl_shadow_map_proj[sm_global_ix];
                    glProgramUniformMatrix4fv(mesh_sps_[ix].id(),ufrm_id_.csm_transform[t],1,false,&transform[0][0]);
                }
                sedl_sm_fbo_[i].bind();
                glClear(GL_COLOR_BUFFER_BIT |GL_DEPTH_BUFFER_BIT);
                draw_geometry(Stage::DIRECT_SHADOW,draws);
                check_gl_error();
            }
        };

        glCullFace(GL_BACK);
        check_gl_error();
        glViewport(0, 0, (GLint)rendering_size().width, (GLint)rendering_size().height);

        {   //drawing of illumination
            use_acc_fbuffer(true);
            geometry_stencil_test(true);
            sedli_sp_.use();
            unit_drawer_.draw();
            geometry_stencil_test(false);
            use_acc_fbuffer(false);
        }
        shadow_map_processed+=sm_count[dl_ix];
    }
}

void Rendering_backend_OGL4::sepl_stage()
{
    for(size_t i=0,i_e=frame_data_.pl_shadow_pass_draws.size(); i!=i_e; ++i){
        {//shadow map generation
            Frame_data::Draw_list const& draws=frame_data_.pl_shadow_pass_draws[i];
            if(draws.actors.end>draws.actors.begin || draws.sceneries.end>draws.sceneries.begin){
                glViewport(0, 0, PL_SHADOW_MAP_SIZE,PL_SHADOW_MAP_SIZE);
                sepl_sm_buffer_.bind_range(sepl_data_offset(i),sizeof(Sepl_sm_data));
                sepl_sm_fbo_.bind();
                glCullFace(GL_FRONT);
                check_gl_error();
                glClear(GL_COLOR_BUFFER_BIT |GL_DEPTH_BUFFER_BIT);
                draw_geometry(Stage::POINT_SHADOW,draws);
                glViewport(0, 0, (GLint)rendering_size().width, (GLint)rendering_size().height);
                glCullFace(GL_BACK);
                //tx_cube_.updateMipMaps();
                check_gl_error();
            }
        }
        {   //drawing of illumination
            use_acc_fbuffer(true);
            /* todo : stencil test for back-faces*/
            geometry_stencil_test(true);
            sepli_sp_.use();
            pls_drawer_.draw_one(i);
            use_acc_fbuffer(false);
            geometry_stencil_test(false);
        }
    }
}

void Rendering_backend_OGL4::sdpl_stage()
{
    if(frame_data_.simple_pl_count){
        use_acc_fbuffer(true);
        geometry_stencil_test(true);
        sdpli_sp_.use();
        render_light();
        geometry_stencil_test(false);
        use_acc_fbuffer(false);
    }
}

void Rendering_backend_OGL4::ambient_stage()
{
    use_acc_fbuffer(true);
    glDisable(GL_DEPTH_TEST);
    geometry_stencil_test(true);
    ambient_sp_.use();
    unit_drawer_.draw();
    geometry_stencil_test(false);
    glEnable(GL_DEPTH_TEST);
    use_acc_fbuffer(false);
}

void Rendering_backend_OGL4::sky_stage()
{
    use_acc_fbuffer(true);
    sky_sp_.use();
    skybox_drawer_.draw();
    use_acc_fbuffer(false);
}

void Rendering_backend_OGL4::luminance_mean()
{
    // just pointers
    std::swap(tx_luminance_geom_mean_front_, tx_luminance_geom_mean_back_);
    tx_luminance_geom_mean_front_->bind_to_texture_unit_once(txt_binding_points::TOTAL_LOG_LUM);

    {
        reduce_.use();
        // an input is load_intensity() from gbuffer.
        tx_total_log_lum_aux_.bind_to_image_unit_once(img_binding_points::CS_OUTPUT_A,GL_WRITE_ONLY);
        uvec2 num_groups=get_num_groups({rendering_size().width,rendering_size().height},reduce_group_size,0);
        glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);
        glDispatchCompute(num_groups.x,num_groups.y,1);

        reduce2_.use();
        tx_total_log_lum_aux_.bind_to_texture_unit_once(txt_binding_points::CS_INPUT_A);
        tx_luminance_geom_mean_back_->bind_to_image_unit_once(img_binding_points::CS_OUTPUT_A,GL_WRITE_ONLY);
        glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);
        glDispatchCompute(1,1,1);

    }
}

void Rendering_backend_OGL4::bloom_blurs()
{
    {
        uvec2 tile_size{32,32};
        uvec2 num_groups=get_num_groups(tx_bp_small_blured_.size(),tile_size,2);

        gauss_blur_small_sp_.use();
        tx_rt_light_.bind_to_texture_unit_once(txt_binding_points::CS_INPUT_A);
        tx_bp_small_blured_.bind_to_image_unit_once(img_binding_points::CS_OUTPUT_A,GL_WRITE_ONLY);
        glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);
        glDispatchCompute(num_groups.x,num_groups.y,1);
    }

    // mips are used in the large radius blur.
    {
        uvec2 tile_size{64,64};
        uvec2 num_groups=get_num_groups({rendering_size().width,rendering_size().height},tile_size,0);

        mip_generation_sp_.use();
        tx_rt_light_.bind_to_texture_unit_once(txt_binding_points::CS_INPUT_A);
        tx_rt_light_l2_.bind_to_image_unit_once(img_binding_points::CS_OUTPUT_A,GL_WRITE_ONLY);
        tx_rt_light_l3_.bind_to_image_unit_once(img_binding_points::CS_OUTPUT_B,GL_WRITE_ONLY);
        glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);
        glDispatchCompute(num_groups.x,num_groups.y,1);
    }

    {
        tx_rt_light_l2_.bind_to_texture_unit_once(txt_binding_points::CS_INPUT_A);
        tx_bp_medium_blured_aux_.bind_to_image_unit_once(img_binding_points::CS_OUTPUT_A,GL_WRITE_ONLY);
        gauss_blur_medium_h_sp_.use();
        glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);

        glDispatchCompute(1,tx_bp_medium_blured_aux_.size().y,1);
        glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);

        tx_bp_medium_blured_aux_.bind_to_texture_unit_once(txt_binding_points::CS_INPUT_A);
        tx_bp_medium_blured_.bind_to_image_unit_once(img_binding_points::CS_OUTPUT_A,GL_WRITE_ONLY);
        gauss_blur_medium_v_sp_.use();
        glDispatchCompute(1,tx_bp_medium_blured_.size().y,1);
        glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);
    }

    {
        tx_rt_light_l3_.bind_to_texture_unit_once(txt_binding_points::CS_INPUT_A);
        tx_bp_large_blured_aux_.bind_to_image_unit_once(img_binding_points::CS_OUTPUT_A,GL_WRITE_ONLY);
        gauss_blur_large_h_sp_.use();
        glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);

        glDispatchCompute(1,tx_bp_large_blured_aux_.size().y,1);
        glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);

        tx_bp_large_blured_aux_.bind_to_texture_unit_once(txt_binding_points::CS_INPUT_A);
        tx_bp_large_blured_.bind_to_image_unit_once(img_binding_points::CS_OUTPUT_A,GL_WRITE_ONLY);
        gauss_blur_large_v_sp_.use();
        glDispatchCompute(1,tx_bp_large_blured_.size().y,1);
        glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);
    }
}

void Rendering_backend_OGL4::main_postproc()
{
    {
        uvec2 group_size{32,32};
        uvec2 num_groups=get_num_groups({rendering_size().width,rendering_size().height},group_size,1);
        tx_rt_color_.bind_to_image_unit_once(img_binding_points::CS_OUTPUT_A,GL_WRITE_ONLY,0);
        main_sp_.use();
        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
        glDispatchCompute(num_groups.x,num_groups.y,1);
        glMemoryBarrier(GL_FRAMEBUFFER_BARRIER_BIT);
    }
}

void Rendering_backend_OGL4::antialiasing()
{
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);

    // the 1st SMAA pass
    smaa_edges_.bind();
    glEnable(GL_STENCIL_TEST);
    glStencilOp(GL_KEEP,GL_KEEP,GL_REPLACE);
    glStencilFunc(GL_ALWAYS,1,0xff);
    smaa_sp_detect.use();
    unit_drawer_.draw();

    // the 2nd SMAA pass
    smaa_blend_.bind();
    glStencilOp(GL_KEEP,GL_KEEP,GL_KEEP);
    glStencilFunc(GL_EQUAL,1,0xff);
    smaa_sp_weight.use();
    unit_drawer_.draw();
    glDisable(GL_STENCIL_TEST);

    // the 3rd SMAA pass
    Frame_buffer::default_fbo().bind();
    smaa_sp_blend.use();

    glEnable(GL_FRAMEBUFFER_SRGB);
    unit_drawer_.draw();
    glDisable(GL_FRAMEBUFFER_SRGB);

    glDepthMask(GL_TRUE);
    glEnable(GL_DEPTH_TEST);
}

void Rendering_backend_OGL4::lamp_stage()
{
    use_acc_fbuffer(true);
    lamp_sp_.use();
    if(frame_data_.simple_pl_count){
        light_drawer_.draw();
    }
    if(!frame_data_.pl_shadow_pass_draws.empty()){
        pls_drawer_.draw();
    }
    use_acc_fbuffer(false);
}

void Rendering_backend_OGL4::debug_stage()
{
    color_fbo_.bind(GL_READ_FRAMEBUFFER);
    Frame_buffer::default_fbo().bind(GL_DRAW_FRAMEBUFFER);
    glBlitFramebuffer(0,0,rendering_size().width,rendering_size().height,0,0,frame_size().width,frame_size().height, GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT,GL_NEAREST);
    check_gl_error();
    Frame_buffer::default_fbo().bind();
    glDepthMask(GL_FALSE);
    debug_sp_.use();
    unit_drawer_.draw();
    glDepthMask(GL_TRUE);
    glDisable(GL_BLEND);
}

void Rendering_backend_OGL4::render(Scene_access& scene_access)
{
    prepare();
    process_scene(scene_access);

    sections_.data.section_begin();
    data_stage();
    sections_.data.section_end();

    sections_.sedl.section_begin();
    sedl_stage();
    sections_.sedl.section_end();

    sections_.sepl.section_begin();
    sepl_stage();
    sections_.sepl.section_end();

    sections_.sdpl.section_begin();
    sdpl_stage();
    sections_.sdpl.section_end();

    sections_.ambient.section_begin();
    ambient_stage();
    sections_.ambient.section_end();

    sections_.skybox.section_begin();
    if(scene_access.skybox()) sky_stage();
    sections_.skybox.section_end();

    sections_.lamps.section_begin();
    lamp_stage();
    sections_.lamps.section_end();

    sections_.luminance_mean.section_begin();
    luminance_mean();
    sections_.luminance_mean.section_end();

    sections_.bloom_build.section_begin();
    bloom_blurs();
    sections_.bloom_build.section_end();

    sections_.postproc.section_begin();
    main_postproc();
    sections_.postproc.section_end();

    sections_.smaa.section_begin();
    antialiasing();
    sections_.smaa.section_end();

    if(scene_access.debug_mode()){
        debug_stage();
    }
    frame_data_.clear();

    if(frame_number_ == 1023){
        frame_number_ = 0;
        print_performance_counters();
    }else{
        ++frame_number_;
    }
}

void Rendering_backend_OGL4::on_scene_change()
{
    scene_is_changed_ = true;
}

void Rendering_backend_OGL4::print_performance_counters()
{
    String performance_msg  = "\n";
    double total_time       = 0;

    auto add_output = [&performance_msg,&total_time](ogl::Performance_counter& pc, String const& name)
            -> void
    {
        auto time           = pc.get();
        performance_msg    += dk::fstr("%1%: %2% ms\n", name, time*1000.0 );
        pc.clear();
        total_time         += time;
    };

    add_output(sections_.data,          "data");
    add_output(sections_.sedl,          "sedl");
    add_output(sections_.sepl,          "sepl");
    add_output(sections_.sdpl,          "sdpl");
    add_output(sections_.ambient,       "ambient");
    add_output(sections_.skybox,        "skybox");
    add_output(sections_.lamps,         "lamps");
    add_output(sections_.luminance_mean,"luminance_mean");
    add_output(sections_.bloom_build,   "bloom_build");
    add_output(sections_.postproc,      "postproc");
    add_output(sections_.smaa,          "smaa");

    performance_msg += dk::fstr("total: %1% ms", total_time * 1000);

    log_msg(Detail_level::DETAILED,performance_msg);
}

Rendering_backend_OGL4::~Rendering_backend_OGL4()
{
}

}
