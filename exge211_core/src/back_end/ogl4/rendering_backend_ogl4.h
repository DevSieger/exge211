#pragma once

#include "../../public_headers.h"
#include PUBLIC_HEADER(renderer.h)
#include PUBLIC_HEADER(context.h)

#include <glm/mat4x4.hpp>
#include "../rendering_backend.h"
#include "../ogl_common/ogl_functions.h"
#include "../ogl_common/shader/shader_loader.h"
#include "../ogl_common/shader/shader_program.h"
#include "drawers/light_drawer.h"
#include "drawers/full_screen_quad_drawer.h"
#include "drawers/skybox_drawer.h"
#include "tools/texture_array.h"
#include "../ogl_common/buffer/ubo.h"
#include "tools/frame_buffer.h"
#include "tools/texture_2d.h"
#include "tools/cube_map.h"
#include <vector>
#include "storage/material_storage.h"
#include "storage/animated_mesh_storage.h"
#include "storage/texture_storage.h"
#include "storage/static_mesh_storage.h"
#include "storage/static_instance_storage.h"
#include "storage/skybox_storage.h"
#include "simple_resource_factory.h"
#include "../cpu_animator/animation_engine_hb.h"
#include "csm.h"
#include "binding_points.h"
#include "frame_data.h"
#include "smaa_textures.h"
#include "../ogl_common/performance_counter.h"
#include <dk/timer.hpp>

namespace exge211 {

/*
 * The backend architecture is designed to be as simple as possible.
 * Any advanced architecture is considered to be harmful,
 * if it can't help to solve current problems.
 * There is no reason to think about the long-term support and the super flexibility of the code,
 * since the graphic APIs evolving too fast.
 * I.e. the new features of the API can break the architecture, or make it relatively ineffective comparing to the new solutions.
 * It is easier to write the new backend from scratch than support the old one.
 */
///An AZDO-like implemenation of the rendering backend which is based on the OpenGL 4.
class Rendering_backend_OGL4 : public Rendering_backend
{
public:
    Rendering_backend_OGL4(Resource_loader const& res_loader );
    void resize(Window_frame_size const& new_size) override;
    void init(Window_frame_size const& new_size) override;
    void on_scene_change() override;
    void render(Scene_access& scene_access) override;
    Animation_engine* animation_engine()override{return &animation_engine_;}

    Resource_factory& resource_factory()override{
        return r_factory_;
    }
    Resource_factory const& resource_factory()const override{
        return r_factory_;
    }

    ~Rendering_backend_OGL4();

private:
    // the maximum number of shadow maps per directional light source.
    static constexpr int MAX_SHADOWMAPS_IN_CASCADE=DL_CSM_data::MAX_CASCADE_SIZE;

    // the incomplete list of the rendering passes.
    // it is used for the shader selection.
    enum class Stage{
        DATA,                                               // the gbuffer generation.
        DIRECT_SHADOW,                                      // the calculation of a directional lighting.
        POINT_SHADOW,                                       // the calculation of an illumination from the shadow-enabled point light sources.
        TOTAL_SIZE
    };

    // it is used for the shader selection.
    enum class Mesh_type: size_t{
        ANIMATED,
        STATIC,
        TOTAL_SIZE
    };

    struct Matrix_transforms{
        glm::mat4x4 view;                                   // the world space -> the camera view space.
        glm::mat4x4 proj;                                   // the camera view space -> the clip space.
    };

    struct Sepl_sm_data{                                    // the shadow map data of the shadow-enabled point light source.
        glm::mat4x4 transform[6];

        // proj_z and proj_w are used to recreate the fragment's z and w coordinates in the shadow map clip space to obtain the fragment depth.
        glm::vec2 proj_z;                                   // the 'z-component' of the shadow map projection matrix, i.e. "proj[2].zw" (proj[2].xy is zeros)
        glm::vec2 proj_w;                                   // the 'w-component' of the shadow map projection matrix, i.e. "proj[3].zw"
    };

    struct Scene_cfg_data{
        glm::vec3 ambient_light;
        float sky_light_scale;
        glm::vec4 rt_metric;                                // the render target "metric": { texel_width, texel_height, width, height }

        glm::vec3 bloom_weights;
        float bp_threshold_r;

        float adaptation_rate;
        float adaptation_min;
        float adaptation_max;
        float dt;

        float burn_out_ratio;
        float tm_key;
    };

    struct Actor_instance_data{
        GLuint joints_begin;
        GLuint material;
        GLuint inv_bp_transform_begin;
        GLuint padding;                                     // to align this VBO data to 128-bit.
    };

    /// Initializes the fixed size textures.
    void init_fixed_size();

    /// Initializes the storage objects.
    void init_storage();

    void init_shaders();

    void prepare();                                         // sets the viewport, commits the deferred uploads, clears the framebuffers..

    /// Uploads the scene data to the GPU, and sets up the #frame_data_ which is required to drawing.
    void process_scene(Scene_access& scene_access);

    ///Returns the data offset of the directional light source with the given index in #cascade_buffer_ .
    GLintptr csm_data_offset(int index) const;

    /// Returns the data size of #cascade_buffer_ for the given number of the light sources.
    size_t csm_data_size(size_t count) const;

    ///Returns the data offset of the point light source with the given index in #sepl_sm_buffer_ .
    GLintptr sepl_data_offset(int index) const;

    /// Returns the data size of #sepl_sm_buffer_ for the given number of the light sources.
    size_t sepl_data_size(size_t count) const;

    /// Returns the index of the given stage.
    size_t get_stage_sps_ix(Stage stage);                   // TODO: rewrite

    size_t mesh_shader_index(Stage stage,Mesh_type type);   // returns the index of the shader in #mesh_sps_
    void use_mesh_shader(Stage stage,Mesh_type type);
    void init_mesh_shaders();

    /// Starts/stops using of the accumulation buffer (i.e. #light_fbo_ ).
    void use_acc_fbuffer(bool use);

    void data_stage();                                      // fills the g-buffer.
    void sedl_stage();                                      // renders illumination of shadow-enabled directional lights.
    void sepl_stage();                                      // renders illumination of shadow-enabled point lights.
    void sdpl_stage();                                      // renders illumination of the shadow-disabled point lights.
    void ambient_stage();                                   // renders ambient lighting.
    void sky_stage();                                       // renders the skybox.
    void luminance_mean();                                  // calculates the geometric mean of the luminance for the tone-mapping & the bloom.
    void bloom_blurs();                                     // generates blured textures of the bright-pass.
    void main_postproc();                                   // the tone-mapping + outlining
    void antialiasing();                                    // SMAA
    void lamp_stage();                                      // TODO: remove this
    void debug_stage();                                     // the debug information drawing

    void draw_geometry(Stage stage, Frame_data::Draw_list const& draws);

    /// Renders shadow-disabled point lights.
    void render_light();

    /// Enables the stencil test to filter empty zones of g-buffer.
    void geometry_stencil_test(bool b);

    // also clears them
    void print_performance_counters();

    void frame_size(Window_frame_size const& new_size);     // Sets size variables, FBO isn't resized.

    /// Returns the size of the window framebuffer.
    Window_frame_size const& frame_size() const             {return frame_size_;}

    /// Returns the size of FBO buffers used for deferred shading (g-buffer, etc..).
    Window_frame_size const& rendering_size() const         {return frame_size_;}

    // Sepl stands for "shadow enabled point light [source]"
    // cam_view is the view matrix of the active camera.
    // sepl_view is a value returned from get_sepl_view_matrix.
    static glm::mat4x4 get_sepl_view_matrix(Point_light_data const& pd, glm::mat4x4 const& cam_view);
    static void set_up_sepl_sm_data(Sepl_sm_data& output, Point_light_data const& pd, glm::mat4x4 const& sepl_view);

    /// Sets up a matrix used for culling objects which are outside the range of the point light source.
    static void set_up_sepl_culling_matrix(glm::mat4x4& output, Point_light_data const& pd, glm::mat4x4 const& sepl_view);

    /*
     * fields
     */

    // IDs of shader unifroms
    struct {
        // "near_plane" from the shader programm of the shadow-disabled illumination (light_sp_).
        GLint near_l;
        // "near_plane" from the shader programm of the "lamp" stage (lamp_sp_)
        GLint near_t;
        // "near_plane" from the shader programm of the shadow-enabled point light illumination (pls_sp_).
        GLint near_sl;
        // "transform_matrix" from the shader programm of the CSM generation for directional lights.
        GLint csm_transform[static_cast<size_t>(Mesh_type::TOTAL_SIZE)];
        // "debug_mode" from #post_sp_
        GLint debug_mode;
    } ufrm_id_;

    // Variables that are used for processing scene to keep visible objects.
    // They are non-local to reduce the number of allocations.
    // TODO: make them local.
    struct {
        Scene_access::Actors_output actors;
        Scene_access::Scenery_output sceneries;
        Scene_access::Point_light_output sdpls;
        Scene_access::Point_light_output sepls;
        Scene_access::Directional_light_output directional_lights;
    } selection_cache_;

    // Properties of the OpenGL implementation.
    struct{
        int ubo_offset_align;
        int ssbo_offset_align;
    } ogl_properties_;

    // The size of the window framebuffer.
    Window_frame_size frame_size_;

    Frame_data frame_data_;

    ogl::Shader_loader shader_loader_;

    ogl::UBO<Matrix_transforms> transform_buffer_{ubo_binding_points::CAMERA_TRANSFORMS};

    // This stores Sepl_sm_data
    ogl::UBO_base sepl_sm_buffer_{ubo_binding_points::SEPL_SM_DATA};

    ogl::UBO<Scene_cfg_data> sc_cfg_buffer_{ubo_binding_points::SCENE_CFG_DATA};

    // This stores DL_CSM_data
    ogl::UBO_base cascade_buffer_{ubo_binding_points::CSM_DATA};

    ogl::Buffer command_buffer_{GL_DRAW_INDIRECT_BUFFER};

    // To store joints + the skeleton position/rotation/scale for all visible actors.
    ogl::SSBO_base joint_buffer_{ssbo_binding_points::JOINTS};
    // To store Actor_instance_data
    ogl::Buffer a_instance_buffer_{GL_ARRAY_BUFFER};

    ogl::VAO a_vao;                                             // for drawing actors
    ogl::VAO s_vao;                                             // for drawing sceneries

    // It stores the key of the active mesh related binding for actors.
    Batch_group a_binded_batch;
    // It stores the key of the active mesh related binding for sceneries.
    Batch_group s_binded_batch;

    Animated_mesh_storage meshes_;
    Material_storage materials_;
    Texture_storage textures_;
    Static_mesh_storage st_meshes_;
    Static_instance_storage st_instances_;
    Skybox_storage skybox_;

    Simple_resource_factory r_factory_;
    Animation_engine_HB animation_engine_;

    // to draw shadow disabled point light sources.
    Light_drawer light_drawer_;

    // to draws shadow enabled point light sources.
    Light_drawer pls_drawer_;

    Full_screen_quad_drawer unit_drawer_;
    Skybox_drawer skybox_drawer_;

    // Composite shaders to draw meshes.
    std::vector<ogl::Shader_program> mesh_sps_;

    /* the g-buffer stage */
    Frame_buffer data_fbo_;
    Texture_2D tx_rt_normals_, tx_rt_albedo_, tx_rt_specular_, tx_rt_color_;

    // points to a texture which is used to output the geometric mean of the luminance for the current frame.
    Texture_2D *tx_luminance_geom_mean_back_  = nullptr;
    // points to a texture which is used as an input of the geometric mean of the luminance that was written in the previous frame.
    Texture_2D *tx_luminance_geom_mean_front_ = nullptr;
    Texture_2D tx_lum_g_mean_a_, tx_lum_g_mean_b_;
    Texture_2D tx_total_log_lum_aux_;                           // an auxiliary texture for an intermediate data of the reduction.

    Texture_2D depth_buffer_;                                   //used as the depthbuffer for both the data and the light stages.

    /* the directional lighting stage*/
    ogl::Shader_program sedli_sp_{"sedl illumination"};
    Frame_buffer sedl_sm_fbo_[MAX_SHADOWMAPS_IN_CASCADE];
    Texture_array tx_shadow_map_;
    Texture_3D tx_rotation_;

    /* the shadow-enabled point light illumination stage */
    ogl::Shader_program sepli_sp_{"sepl illumination"};
    Frame_buffer sepl_sm_fbo_;
    // The shadow map for the point light source.
    Cube_map tx_sepl_sm_;

    /* the shadow-disabled point light illumination stage */
    ogl::Shader_program sdpli_sp_{"sdpl illumination"};

    Frame_buffer light_fbo_;                                    // the accumulation buffer for illumination.
    Texture_2D tx_rt_light_;

    ogl::Shader_program ambient_sp_{"ambient"};

    // shaders which produce the geometric mean of the luminance.
    ogl::Shader_program reduce_{"lumi-log-reduction"};
    ogl::Shader_program reduce2_{"lumi-log-reduction-2"};       // for the second pass of the reduction

    /* smaa */
    Smaa_textures smaa_tx;
    Texture_2D tx_rt_smaa_edges_;
    Texture_2D tx_rt_smaa_bw_;                                  // the SMAA "blendTex"
    Texture_2D tx_rt_smaa_stencil_;
    Frame_buffer smaa_edges_;
    Frame_buffer smaa_blend_;                                   // to hold "blendTex"
    ogl::Shader_program smaa_sp_detect{"smaa detect"};
    ogl::Shader_program smaa_sp_weight{"smaa weight"};
    ogl::Shader_program smaa_sp_blend{"smaa blend"};


    /* other */
    Frame_buffer color_fbo_;
    ogl::Shader_program main_sp_{"main"};                       // the main post-processing shader
    ogl::Shader_program sky_sp_{"sky"};
    ogl::Shader_program lamp_sp_{"lamp"};
    ogl::Shader_program debug_sp_{"post"};

    ogl::Shader_program gauss_blur_medium_h_sp_{"gauss_blur_h"};
    ogl::Shader_program gauss_blur_medium_v_sp_{"gauss_blur_v"};

    ogl::Shader_program gauss_blur_large_h_sp_{"gauss_blur_large_h"};
    ogl::Shader_program gauss_blur_large_v_sp_{"gauss_blur_large_v"};

    ogl::Shader_program gauss_blur_small_sp_{"gauss_blur_small"};

    // it looks like 'textureLod' don't work as it should, so this textures are a workaround.
    Texture_2D tx_rt_light_l2_,tx_rt_light_l3_;

    // textures for bloom.
    Texture_2D tx_bp_medium_blured_aux_, tx_bp_medium_blured_;
    Texture_2D tx_bp_large_blured_aux_, tx_bp_large_blured_;
    Texture_2D tx_bp_small_blured_;

    ogl::Shader_program mip_generation_sp_{"mip generation"};

    struct{
        ogl::Performance_counter data;
        ogl::Performance_counter sedl;
        ogl::Performance_counter sepl;
        ogl::Performance_counter sdpl;
        ogl::Performance_counter ambient;
        ogl::Performance_counter skybox;
        ogl::Performance_counter lamps;
        ogl::Performance_counter luminance_mean;
        ogl::Performance_counter bloom_build;
        ogl::Performance_counter postproc;
        ogl::Performance_counter smaa;
    } sections_;

    // this is used to output performance counters' data
    u32 frame_number_ = 0;

    bool scene_is_changed_  = false;

    /*
     * TODO:
     * Make this configurable.
     */
    static constexpr int PL_SHADOW_MAP_SIZE=256+512;
    static constexpr int DL_SHADOW_MAP_SIZE=1024;

};

}
