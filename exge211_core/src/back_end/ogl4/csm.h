#pragma once
#include "../../public_headers.h"
#include PUBLIC_HEADER(vector_types.h)
#include PUBLIC_HEADER(common_types.h)

namespace exge211 {

struct Directional_light_data;
struct AABB;
class Camera;

// Data of the directional light and its shadow maps to use in shaders.
// The layout of the data is std140.
struct DL_CSM_data{
    static constexpr size_t MAX_CASCADE_SIZE=12;
    struct Shadow_map_data{
        glm::mat4x4 proj;                           // the world space -> the clip space of the shadow map.
        float near_plane;                           // the z-coordinate of the scene's view space to start using the shadow map.
        float overlap;                              // the size of the overlap of the areas that use different shadow maps.

        // The scale of x and y coordinates in the projection matrix of the shadow map.
        // This is used in the PCF to select the sampling range.
        glm::vec2 xy_scale;
    };

    Direction direction;                            // the direction of the light.
    u32 map_count;                                  // the number of the shadow maps.
    RGB intensity;                                  // the intensity of the light.
    float padding;                                  // the std140 layout used in shaders.
    Shadow_map_data cascade[MAX_CASCADE_SIZE];
};

/*!
 * \brief Sets the data for calculating the lighting of the scene from the given light source.
 * \param output                                    The variable to output.
 * \param light                                     The data of the given light.
 * \param scene_boundary                            The bounding box of the scene.
 * \param camera                                    The camera used in the scene.
 * \param inv_camera_pvm                            The transform from the clip space (of the camera) to the world space.
 * \param shadow_map_size                           The resolution of the shadow maps.
 * \return                                          The number of the shadow maps.
 */
size_t set_up_csm_data( DL_CSM_data& output, Directional_light_data const& light, AABB const& scene_boundary,
                        Camera const& camera, glm::mat4x4 const& inv_camera_pvm, size_t shadow_map_size );

static_assert( offsetof(DL_CSM_data,cascade)%16 == 0, "the wrong offset of the member 'cascade', add padding");
static_assert( sizeof(DL_CSM_data::Shadow_map_data)%16 == 0, "the wrong size of DL_shadow_map_data, change padding for it");
static_assert( sizeof(DL_CSM_data) < 16*1024, "DL_CSM_data is too big for the UBO");

}
