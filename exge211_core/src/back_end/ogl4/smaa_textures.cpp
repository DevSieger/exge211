#include "smaa_textures.h"
#include "binding_points.h"
#include "../../../3rd-party/smaa/AreaTex.h"
#include "../../../3rd-party/smaa/SearchTex.h"
#include "../ogl_common/ogl_functions.h"
#include "../ogl_common/error_checking.h"

#include <vector>
#include <cstring>

namespace{

using Byte=unsigned char;
void flipping_copy( std::vector<Byte>& tx_out,const Byte* tx_in, size_t tx_height, size_t tx_pitch, size_t tx_size)
{
    tx_out.clear();
    tx_out.resize(tx_size);
    for(size_t line=0; line<tx_height; ++line){
        const Byte* in= tx_in + tx_pitch*line;
        Byte* out= tx_out.data()+tx_pitch*(tx_height-1-line);
        std::memcpy(out,in,tx_pitch);
    }
}

}

void exge211::Smaa_textures::init()
{
    area_tx_.storage(AREATEX_WIDTH,AREATEX_HEIGHT,1,GL_RG8);
    area_tx_.bind_to_texture_unit(txt_binding_points::SMAA_AREA);
    area_tx_.filter(GL_LINEAR,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
    check_gl_error();

    search_tx_.storage(SEARCHTEX_WIDTH,SEARCHTEX_HEIGHT,1,GL_R8);
    search_tx_.bind_to_texture_unit(txt_binding_points::SMAA_SEARCH);
    search_tx_.filter(GL_LINEAR,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
    check_gl_error();

    // We need to flip textures vertically, because the OpenGL has the different texture-data layout.
    //TODO: edit original arrays instead of conversion.

    std::vector<Byte> tmp;
    // The area texture is greater, process it first to avoid reallocations.
    flipping_copy(tmp,areaTexBytes,AREATEX_HEIGHT,AREATEX_PITCH,AREATEX_SIZE);
    area_tx_.bind();
    glTexSubImage2D(GL_TEXTURE_2D,0,0,0,AREATEX_WIDTH,AREATEX_HEIGHT,GL_RG,GL_UNSIGNED_BYTE,tmp.data());
    check_gl_error();

    flipping_copy(tmp,searchTexBytes,SEARCHTEX_HEIGHT,SEARCHTEX_PITCH,SEARCHTEX_SIZE);
    search_tx_.bind();
    glTexSubImage2D(GL_TEXTURE_2D,0,0,0,SEARCHTEX_WIDTH,SEARCHTEX_HEIGHT,GL_RED,GL_UNSIGNED_BYTE,tmp.data());
    check_gl_error();
}
