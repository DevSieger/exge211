#include "model_data.h"
namespace exge211 {
namespace ogl4 {

Static_model_instance::Static_model_instance(Static_model_handle a_type):Static_model_instance_base(a_type){
    Static_model const& r_type=Static_model::downcast(*a_type);

    init(r_type);

    for(Index i=0;i<lod_count();++i){
        auto & tar=lod(i);
        auto & dest=r_type.lod(i);

        for(LOD::Size j=0;j<tar.size();++j){
            tar[j].bg=dest[j].bg;
            tar[j].mesh=dest[j].mesh;
            tar[j].type=dest[j].type;
            tar[j].instance=0;
        }
    }
}
}

}
