#pragma once
#include <vector>
#include "../../front_end/scene/scene_access.h"
#include "storage/batch_group.h"
#include "csm.h"

namespace exge211{

/*!
 * \brief Data required for draw calls (i.e. draw-batches, uniform transformation matrices, etc..).
 *
 * The draw-batch is a set of objects which are drawn using one call of the draw command (i.e. no state changes).
 */
struct Frame_data{

    struct Index_pair{
        size_t begin=0;
        size_t end=0;
    };

    // .begin and .end are command buffer indices.
    struct Batch_info: Index_pair{
        Batch_group batch;
    };

    struct Draw_list{
        Index_pair actors;                                  // Frame_data::a_batches indices
        Index_pair sceneries;                               // Frame_data::s_batches indices
    };

    using Batches=std::vector<Batch_info>;

    Batches a_batches;                                      // Draw-batches of Actor objects.
    Batches s_batches;                                      // Draw-batches of Scenery objects.

    Draw_list data_pass_draw;                               // Draw-batches for gbuffer pass.

    /*
     * The order of elements in the following containers is determined by Rendering_backend_OGL4::process_scene
     * which is used to upload other necessary data to GPU.
     * However, the alignment of the data offset of the UBO binding is large (~128 bytes),
     * so simple uniforms is used to store a single matrix per the shadow map.
     */
    std::vector<size_t> dl_shadow_map_count;                // The number of shadow maps for each directional light source.
    std::vector<glm::mat4x4> dl_shadow_map_proj;            // each element corresponds to the shadow map of the directional light source.
    std::vector<Draw_list> dl_shadow_pass_draws;            // each element corresponds to the shadow map of the directional light source.
    std::vector<Draw_list> pl_shadow_pass_draws;            // each element corresponds to the shadow-enabled point light source.

    size_t simple_pl_count=0;                               // The number of the shadow-disabled point light sources.

    void clear(){
        a_batches.clear();
        s_batches.clear();
        data_pass_draw.actors.begin=0;
        data_pass_draw.actors.end=0;
        data_pass_draw.sceneries.begin=0;
        data_pass_draw.sceneries.end=0;
        dl_shadow_map_count.clear();
        dl_shadow_map_proj.clear();
        dl_shadow_pass_draws.clear();
        pl_shadow_pass_draws.clear();
        simple_pl_count=0;
    }

};

}
