#include "misc.h"
#include "../../public_headers.h"
#include PUBLIC_HEADER(camera.h)
#include PUBLIC_HEADER(common_types.h)

#include <glm/gtc/matrix_transform.hpp>

namespace exge211 {

void generate_camera_matrices( Camera const& cam,Window_frame_size const& size, glm::mat4x4 & vm_out, glm::mat4x4 & pm_out,float far_plane,float near_plane){
    const glm::vec3 p=cam.position();
    const glm::mat4x4 rot=glm::mat4_cast(glm::conjugate(cam.rotation()));
    vm_out=rot*glm::mat4x4{{1,0,0,0}, {0,1,0,0}, {0,0,1,0}, {-p.x,-p.y,-p.z,1}};
    pm_out=glm::perspectiveFov(cam.fov(),(float)size.width,(float)size.height,near_plane,far_plane);
}

}
