#include "rendering_backend_factory_ogl4.h"
#include "rendering_backend_ogl4.h"

namespace exge211{

Rendering_backend_factory_OGL4::Rendering_backend_factory_OGL4(){
}

Rendering_backend_ptr Rendering_backend_factory_OGL4::new_rendering_backend(Resource_loader const& rf)
{
    return std::make_shared<Rendering_backend_OGL4>(rf);
}

}
