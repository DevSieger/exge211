#pragma once

#include "../rendering_backend_factory.h"

namespace exge211 {

/// The abstract factory for the OpenGL4 AZDO-like implementation of the rendering backend.
class Rendering_backend_factory_OGL4: public Rendering_backend_factory{
public:
    Rendering_backend_factory_OGL4();
    Rendering_backend_ptr new_rendering_backend(Resource_loader const& rf) override;
private:
};

}
