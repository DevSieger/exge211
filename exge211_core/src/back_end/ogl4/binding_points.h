#pragma once

namespace exge211{

// The texture unit indices.
namespace txt_binding_points{
enum: unsigned int{
    //do not use 0! It is reserved for the generic binding.
    ALBEDO                  = 1,        // an albedo texture of the rendered object.
    NORMAL_MAP              = 2,        // a normal map texture of the rendered object.
    SPECULAR                = 3,        // a specular highlight texture of the rendered object.
    SKYBOX                  = 4,
    DEPTH                   = 5,        // the depth buffer
    RANDOM_ROT              = 6,        // The pseudo-random texture used in the PCF
    RT_SHADOW_MAP           = 7,        // The shadow map used in the directional light calculation (RT - render target).
    RT_CUBE_SHADOW_MAP      = 8,        // The shadow map used in the point light calculation.

    SMAA_AREA               = 9,        // the SMAA "areaTex", see http://www.iryoku.com/smaa/
    SMAA_SEARCH             = 10,       // the SMAA "searchTex"
    RT_SMAA_EDGES           = 11,       // the SMAA "edgesTex"
    RT_SMAA_BW              = 12,       // the SMAA "blendTex"

    RT_COLOR                = 14,       // an LDR color, after the tone-mapping.
    CS_INPUT_A              = 15,       // the "standart" binding point for a texture input of compute shaders.
    TOTAL_LOG_LUM           = 16,       // the geometric mean of the image luminance (of the previous frame)
    BP_BLUR_SMALL           = 17,       // the "bright-pass" blured with a small-kernel (for bloom).
    BP_BLUR_MEDIUM          = 18,       // the "bright-pass" blured with a medium-kernel.
    BP_BLUR_LARGE           = 19,       // the "bright-pass" blured with a large-kernel.

    RT_ALBEDO               = 20,       // an albedo (gbuffer)
    RT_SPECULAR             = 21,       // a specular color (gbuffer)
    RT_NORMALS              = 22,       // a normal (gbuffer).
    RT_LIGHT                = 23        // an accumulation buffer of illumination.
};
}

// The image unit indices.
namespace img_binding_points{
enum: unsigned int{
    CS_OUTPUT_A = 0,                    // A & B are "standart" binding points for an image output of compute shaders.
    CS_OUTPUT_B = 1
};
}

// The UBO binding point indices.
namespace ubo_binding_points{
enum: unsigned int{
    CAMERA_TRANSFORMS       = 0,
    SEPL_SM_DATA            = 1,        // The data used for the calculation of a lighting from the shadow-enabled point light source.
    SCENE_CFG_DATA          = 2,        // A large buffer that contains shading variables, most of which are specified in the scene object.
    CSM_DATA                = 3,        // The data used for the calculation of a lighting from the shadow-enabled directional light source.
};
}

// The SSBO binding point indices.
namespace ssbo_binding_points{
enum: unsigned int{
    MATERIAL                = 0,
    JOINTS                  = 1,
    MESH_INV_BP_TRANSFROMS  = 2         // The binding pose data.
};
}

}
