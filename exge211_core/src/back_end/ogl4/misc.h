#pragma once
#include <glm/mat4x4.hpp>

namespace exge211 {

class Camera;
struct Window_frame_size;

/*!
 * \brief Generates vertex transformation matrices for the given camera.
 * \param cam               The given camera.
 * \param size              The framebuffer (viewport) size.
 * \param [out]vm_out       The resulting view matrix.
 * \param [out]pm_out       The resulting projection matrix.
 * \param far_plane         A distance from the camera location to the far plane of the view frustum of the camera.
 * \param near_plane        A distance from the camera location to the near plane of the view frustum of the camera.
 */
void generate_camera_matrices( Camera const& cam,Window_frame_size const& size, glm::mat4x4 & vm_out, glm::mat4x4 & pm_out,float far_plane = 500.0f,float near_plane = 0.5f );

}
