#include "../../public_headers.h"
#include PUBLIC_HEADER(logging.h)
#include PUBLIC_HEADER(context.h)

#include "ogl_functions.h"

namespace exge211 {

void OGL_context::initialize()
{
   int res = ogl_LoadFunctions();
   if(res!=ogl_LOAD_SUCCEEDED){
       log_msg(Detail_level::CRITICAL,"can't load functions, gl version %1%,%2%",ogl_GetMajorVersion(),ogl_GetMinorVersion());
   };
}

}
