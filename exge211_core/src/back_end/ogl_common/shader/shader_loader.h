#pragma once
#include "../../../public_headers.h"
#include PUBLIC_HEADER(common_types.h)
#include PUBLIC_HEADER(resource_loader.h)
#include <dk/concepts.h>
#include <unordered_map>

namespace exge211 {
namespace ogl{

class Shader_loader: dk::Non_copyable<Shader_loader>{
public:
    Shader_loader(Resource_loader const& a_rs_loader): rs_loader_(a_rs_loader){}

    // also replaces "#version" with "//#version"
    String const& get_source(Resource_request const& name);

    void clear(){
        cache_.clear();
    }

private:
    Resource_loader const& rs_loader_;
    using Cache=std::unordered_map<Resource_request, String>;
    Cache cache_;
};

}
}
