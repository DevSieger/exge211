#include "shader.h"
#include "shader_loader.h"
#include PUBLIC_HEADER(logging.h)

namespace exge211 {
namespace ogl{

namespace{

String get_version_str(u32 glsl_version, bool compatibility){
    return dk::fstr("#version %1% ",glsl_version) + ( compatibility ? "compatibility" : "core") + "\n";
}

}

Shader::Shader(Shader_loader& a_loader, GLenum a_type, const String& a_name, u32 glsl_version, bool compatibility):
    name_(a_name), version_str_(get_version_str(glsl_version,compatibility)), loader_(a_loader), type_(a_type)
{

}

Shader& Shader::add_file(const Resource_request& shader_name)
{
    return add_source(loader_.get_source(shader_name));
}

bool Shader::compile()
{
    GLint res=GL_FALSE;
    if(sources_.size()){
        glDeleteShader(id_);
        id_=glCreateShader(type_);
        std::vector<const char*> source_list;
        source_list.reserve(sources_.size()+1);
        std::vector<GLint> source_sizes;
        source_sizes.reserve(sources_.size()+1);

        source_list.push_back(version_str_.data());
        source_sizes.push_back(version_str_.size());

        for(auto& src: sources_  ){
            source_list.push_back(src.data());
            source_sizes.push_back(src.size());
        };

        glShaderSource(id_,source_list.size(),source_list.data(),source_sizes.data());
        glCompileShader(id_);
        int log_len=0;
        glGetShaderiv(id_,GL_COMPILE_STATUS,&res);
        glGetShaderiv(id_,GL_INFO_LOG_LENGTH,&log_len);
        if(res!=GL_TRUE){
            log_msg(Detail_level::CRITICAL,"failed to compile shader %1%",name());
        }
        if(log_len>1){
            std::vector<char> log(log_len);
            glGetShaderInfoLog(id_,log_len,0,&log[0]);
            log_msg(Detail_level::DEBUG,"Shader %1% compilation log: %2%",name(), &log[0]);
        }
    }else{
        log_msg(Detail_level::CRITICAL,"shader %1% is empty",name());
    }
    return res;
}

void Shader::clear()
{
    sources_.clear();
    glDeleteShader(id_);   //zero is ignored.
    id_ = 0 ;
}

String Shader::get_source() const
{
    String source=version_str_;
    for(auto& src: sources_){
        source += src ;
    }
    return source;
}

Shader::Shader(Shader&& other):
    sources_(std::move(other.sources_)),
    name_(std::move(other.name())),
    version_str_(std::move(other.version_str_)),
    loader_(other.loader_),
    id_(other.id_),
    type_(other.type_)
{
    other.sources_.clear();
    other.id_=0;
}

Shader::~Shader()
{
    clear();
}

}
}
