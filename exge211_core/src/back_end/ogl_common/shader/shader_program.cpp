#include "shader_program.h"
#include "../error_checking.h"
#include "shader.h"
#include PUBLIC_HEADER(logging.h)

namespace exge211{
namespace ogl{

Shader_program::Shader_program(const String& name_arg):
    name_(name_arg)
{
    id_=glCreateProgram();
}

Shader_program::Shader_program(Shader_program&& old):
    name_(old.name()), id_(old.id())
{
    old.id_=0;
}

void Shader_program::use()
{
    glUseProgram(id_);
    check_gl_error();
}

void Shader_program::recreate()
{
    attached_.clear();
    glDeleteProgram(id_);
    id_=glCreateProgram();
}

bool Shader_program::link()
{
    glLinkProgram(id());
    check_gl_error();
    GLint res=GL_FALSE;
    int log_len=0;
    glGetProgramiv(id(),GL_LINK_STATUS,&res);
    glGetProgramiv(id(),GL_INFO_LOG_LENGTH,&log_len);
    if(res!=GL_TRUE){
        log_msg(Detail_level::CRITICAL,"failed to link shaders for %1%",name());
    }
    if(log_len>1){
        std::vector<char> log(log_len);
        glGetProgramInfoLog(id(),log_len,0,&log[0]);
        log_msg(Detail_level::DEBUG,&log[0]);
    }
    for(GLuint & sh_id: attached_){
        glDetachShader(id(),sh_id);
    }
    attached_.clear();
    return res;
}

Shader_program& Shader_program::attach( Shader const& shader)
{
    glAttachShader(id(),shader.id());
    attached_.push_back(shader.id());
    check_gl_error();
    return *this;
}

namespace {

template<typename Funct, typename Value>
bool set_uniform(Funct funct, Shader_program& s_prog, const char * unif_name, Value value ){
    GLint location  = glGetUniformLocation(s_prog.id(), unif_name);
    check_gl_error();
    if(location!=-1){
        funct(s_prog.id(),location,value);
        return check_gl_error();
    }else{
        log_msg(Detail_level::NON_CRITICAL,"can't find the uniform %1% in %2%",unif_name,s_prog.name());
    }
    return false;
}

}

bool Shader_program::set_uniform_i(const char * uniform_name, GLint value)
{
    return set_uniform(glProgramUniform1i,*this, uniform_name,value);
}

bool Shader_program::set_uniform_u(const char * uniform_name, GLuint value)
{
    return set_uniform(glProgramUniform1ui,*this, uniform_name,value);
}

bool Shader_program::set_uniform_f(const char * uniform_name, GLfloat value)
{
    return set_uniform(glProgramUniform1f,*this, uniform_name,value);
}

Shader_program::~Shader_program()
{
    glDeleteProgram(id_);
}

}
}
