#include "shader_loader.h"
#include PUBLIC_HEADER(res/resource_data.h)
#include PUBLIC_HEADER(logging.h)
#include <regex>
#include <iterator>
#include <utility>

namespace exge211 {
namespace ogl{

namespace {

std::regex const& version_regex(){
    static std::regex value{"#version"};
    return value;
}

}

String const& Shader_loader::get_source(Resource_request const& name)
{
    auto it=cache_.find(name);
    if(it==cache_.end()){
        Shader_source source;
        rs_loader_.load_shader_source(name,source);

        if(source.empty()){
            log_msg(Detail_level::CRITICAL,"can't load a shader %1% or the shader is empty",name);
        }

        String result;
        std::regex_replace(std::back_inserter(result),source.begin(),source.end(),version_regex(),"//$&");
        auto pair=cache_.emplace(Cache::value_type{name,std::move(result)});
        it=pair.first;
    }
    return it->second;
}

}
}
