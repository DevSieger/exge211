#pragma once
#include "../../../public_headers.h"
#include PUBLIC_HEADER(common_types.h)
#include "../gl_func/gl_core_uq_4_3.h"
#include <dk/concepts.h>
#include <vector>

namespace exge211{
namespace ogl{

class Shader;

class Shader_program
{
public:
    Shader_program(String const & name_arg);
    Shader_program(Shader_program&& old);

    GLuint const & id()const{
        return id_;
    }
    String const & name()const{
        return name_;
    }

    void use();
    void recreate();

    // detaches all shaders
    bool link();

    Shader_program& attach(Shader const& shader);

    // Just a wrappers for glProgramUniform1* + glGetUniformLocation.
    bool set_uniform_u(const char * uniform_name, GLuint value);
    bool set_uniform_f(const char * uniform_name, GLfloat value);
    // only this variant should be used to set samplers.
    bool set_uniform_i(const char * uniform_name, GLint value);

    ~Shader_program();
private:
    std::vector<GLuint> attached_;
    const String name_;
    GLuint id_=0;
};

}
}
