#pragma once
#include "../../../public_headers.h"
#include PUBLIC_HEADER(common_types.h)
#include "../gl_func/gl_core_uq_4_3.h"
#include <vector>
#include <dk/format_string.hpp>

namespace exge211 {
namespace ogl{

class Shader_loader;

class Shader{
public:

    // `compatibility` means the compatibility profile
    // sh_loader is used to cache repeated file loads.
    // a_type is GL_VERTEX_SHADER, GL_FRAGMENT_SHADER, etc.
    Shader(Shader_loader& sh_loader, GLenum a_type,String const& a_name, u32 glsl_version, bool compatibility = false);

    // do not call this method twice with the same name.
    template<typename T>
    Shader& define(String const& name, T&& value){
        return add_source(dk::fstr("#define %1% %2%",name, std::forward<T>(value)));
    }

    Shader& add_file(Resource_request const& shader_name);

    bool compile();
    void clear();

    String get_source() const;

    GLenum type() const         { return type_;}
    String const& name() const  { return name_;}
    GLuint id() const           { return id_;}

    Shader& add_source(String const& str){
        if(!str.empty()) sources_.emplace_back(str+"\n");
        return *this;
    }

    Shader& add_source(String && str){
        if(!str.empty()) sources_.emplace_back(str+"\n");
        return *this;
    }

    Shader(Shader&& other);

    ~Shader();
private:
    std::vector<String> sources_;
    const String name_;
    const String version_str_;

    // I think we don't need to change the loader after an initialization.
    Shader_loader& loader_;
    GLuint id_ =0;
    GLenum type_;
};

}

}
