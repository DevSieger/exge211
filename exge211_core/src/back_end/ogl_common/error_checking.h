#pragma once

namespace exge211 {

bool check_gl_error_f(char const * file,int line);
#define OGL_ERROR_CHECKING
#ifdef OGL_ERROR_CHECKING
#define check_gl_error(x) check_gl_error_f(__FILE__,__LINE__)
#else
#define check_gl_error(x) false
#endif

}
