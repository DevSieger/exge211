#pragma once

#include "../../../public_headers.h"
#include PUBLIC_HEADER(logging.h)

#include "buffer_base.h"
#include "buffer_content.h"
#include "../shader/shader_program.h"


namespace exge211 {
namespace ogl{

//required version of opengl: 4.3
class SSBO_base:public Ibp_buffer{
public:
    SSBO_base(GLuint a_binding_point):Ibp_buffer(GL_SHADER_STORAGE_BUFFER,a_binding_point){
        bind();
        bind_to_ibp();
    }

    void set_binding(Shader_program const & shader,const char * name ){
        GLuint ix=glGetProgramResourceIndex(shader.id(),GL_SHADER_STORAGE_BLOCK,name);
        if(ix==GL_INVALID_INDEX){
            log_msg(Detail_level::CRITICAL,"SSBO %1% not found in %2%",name,shader.name());
            return;
        }
        check_gl_error();
        glShaderStorageBlockBinding(shader.id(),ix,binding_point());
        check_gl_error();
    }

};

template<typename Prefix_data_type,typename Array_data_type>
using SSBO=Buffer_with_content_interface<SSBO_base,Prefix_data_type,Array_data_type>;

}
}
