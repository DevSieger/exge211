#pragma once

#include "../../../public_headers.h"
#include PUBLIC_HEADER(logging.h)

#include "../shader/shader_program.h"
#include "buffer_base.h"
#include "buffer_content.h"

namespace exge211 {
namespace ogl{

//required version of opengl: 3.1
class UBO_base:public Ibp_buffer{
public:
    UBO_base(GLuint a_binding_point):Ibp_buffer(GL_UNIFORM_BUFFER,a_binding_point){
        bind();
        bind_to_ibp();
    }

    void set_binding(Shader_program const & shader,GLuint uniform_block_index){
        glUniformBlockBinding( shader.id(), uniform_block_index, binding_point() );
        check_gl_error();
    }
    void set_binding( Shader_program const & shader, const char * uniform_block_name ){
        GLuint u_block_index = glGetUniformBlockIndex( shader.id(), uniform_block_name );
        if( u_block_index != GL_INVALID_INDEX ){
            set_binding(shader, u_block_index);
        }else{
            log_msg(Detail_level::NON_CRITICAL,"not found uniform block %1% at shader program %2%, perhaps due to shader compiler optimization",uniform_block_name,shader.name());
        }
        check_gl_error();
    }
};

template<typename Prefix_data_type,typename Array_data_type=void>
using UBO=Buffer_with_content_interface<UBO_base,Prefix_data_type,Array_data_type>;

}
}
