#pragma once

#include "../../ogl_common/ogl_functions.h"
#include "../../ogl_common/error_checking.h"
#include <utility>
#include <memory>

namespace exge211 {
namespace ogl{

//required version of opengl: 1.5
class Buffer{
public:
    Buffer(GLenum a_type,GLenum a_usage=GL_STREAM_DRAW):
        type_(a_type),usage_(a_usage)
    {
        glGenBuffers(1,&id_);
        check_gl_error();
    }

    Buffer(Buffer&& source):
        type_(source.type_),
        allocated_(source.allocated_),
        size_align_(source.size_align_),
        usage_(source.usage_),
        id_(source.id_)
    {
        source.id_=0;
        source.allocated_=0;
    }

    // the virtual keyword is not necessary here, since a polymorphic usage is not expected.
    virtual ~Buffer(){
        glDeleteBuffers(1,&id_);
    }

    GLuint id()const{
        return id_;
    }
    GLenum type()const{
        return type_;
    }

    void bind(){
        glBindBuffer(type(),id());
        check_gl_error();
    }

    void reallocate(size_t new_size){
        bind();
        allocated_=get_allocation_size(new_size);
        glBufferData(type(),allocated_, NULL, usage());
        check_gl_error();
    }

    template<typename Value>
    void upload(size_t offset, Value const& value){
        bind();
        glBufferSubData(type(),offset,sizeof(Value),std::addressof(value));
        check_gl_error();
    }
    template<typename Value>
    void upload(size_t offset, Value const* value, size_t count){
        bind();
        glBufferSubData(type(),offset,sizeof(Value)*count,value);
        check_gl_error();
    }

    size_t size_align()const{
        return size_align_;
    }
    void size_align(size_t new_align){
        if(new_align){
            size_align_=new_align;
        }else{
            size_align_=1;
        }
    }

    size_t allocated()const{
        return allocated_;
    }

    void usage(GLenum new_usage){
        usage_=new_usage;
    }
    GLenum usage()const{
        return usage_;
    }

private:
    size_t get_allocation_size(size_t requested_size)const{
        size_t s=requested_size+size_align()-1;
        return s-(s%size_align());
    }

    const GLenum type_;
    size_t allocated_=0;
    size_t size_align_=64;
    GLenum usage_=GL_STREAM_DRAW;
    GLuint id_=0;
};

//required version of opengl: 3.0
class Ibp_buffer: public Buffer{
public:
    Ibp_buffer(GLenum a_type, GLuint a_binding_point):Buffer(a_type),binding_point_(a_binding_point){
    }

    GLuint binding_point()const{
        return binding_point_;
    }
    void binding_point(GLuint a_binding_point){
        glBindBufferBase(type(),binding_point_,0);
        check_gl_error();
        binding_point_=a_binding_point;
        return bind_to_ibp();
    }
    GLintptr binding_offset()const{
        return binding_offset_;
    }
    GLintptr binding_size()const{
        return binding_size_;
    }
    /* let ogl do error checking :P */
    void bind_range(GLintptr offset,GLsizeiptr data_size){
        binding_offset_=offset;
        binding_size_=data_size;
        bind_to_ibp();
    }
    void bind_entire(){
        binding_offset_=0;
        binding_size_=0;
        bind_to_ibp();
    }

protected:
    void bind_to_ibp(){
        if(id()){
            if(binding_size_>0){
                glBindBufferRange(type(),binding_point_,id(),binding_offset_,binding_size_);
                check_gl_error();
            }else{
                glBindBufferBase(type(),binding_point_,id());
                check_gl_error();
            }
        }
    }

private:
    GLuint binding_point_;
    GLintptr binding_offset_=0;
    GLsizeiptr binding_size_=0;
};

}
}
