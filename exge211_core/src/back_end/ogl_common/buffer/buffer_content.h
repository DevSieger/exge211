#pragma once
#include "../../ogl_common/ogl_functions.h"
#include "../../ogl_common/error_checking.h"
#include <vector>
#include <utility>
#include "buffer_base.h"

namespace exge211{
namespace ogl{

template<typename Prefix_data_type,typename Array_element_type>
class Buffer_content;

template<typename Array_element_type>
class Buffer_content<void,Array_element_type>{
public:
    using Array_element=Array_element_type;
    using Array_data=std::vector<Array_element>;

    Array_data& array_data(){
        return array_data_;
    }
    Array_data const& array_data()const{
        return array_data_;
    }

    GLsizeiptr size()const{
        return array_data_.size()*sizeof(Array_element);
    }

protected:
    void upload_content(Buffer & buffer,size_t offset=0){
        if(!array_data().empty()){
            buffer.upload(offset,array_data().data(),array_data().size());
        };
    }

private:
    Array_data array_data_;
};

template<typename Prefix_data_type>
class Buffer_content<Prefix_data_type,void>{
public:
    using Prefix_data=Prefix_data_type;

    Prefix_data const & prefix_data()const{
        return prefix_data_;
    }
    Prefix_data & prefix_data(){
        return prefix_data_;
    }

    GLsizeiptr size()const{
        return sizeof(Prefix_data);
    }

protected:
    void upload_content(Buffer & buffer,size_t offset=0){
        buffer.upload(offset,prefix_data());
    }

private:
    Prefix_data prefix_data_;
};

template<typename Prefix_data_type,typename Array_element_type>
class Buffer_content: Buffer_content<Prefix_data_type,void>,Buffer_content<void, Array_element_type>{
public:
    using Prefix_base=Buffer_content<Prefix_data_type,void>;
    using Array_base=Buffer_content<void, Array_element_type>;

    using Prefix_base::prefix_data;
    using Array_base::array_data;

    GLsizeiptr size() const{
        return Prefix_base::size()+Array_base::size();
    }
protected:
    void upload_content(Buffer & buffer,size_t offset=0){
        Prefix_base::upload_content(buffer,offset);
        Array_base::upload_content(buffer,Prefix_base::size()+offset);
    }
};

template<typename Base,typename Prefix_data_type,typename Array_data_type>
class Buffer_with_content_interface:public Base, public  Buffer_content<Prefix_data_type,Array_data_type>{
public:
    using Content=Buffer_content<Prefix_data_type,Array_data_type>;

    template<typename... Base_ctor_args>
    Buffer_with_content_interface(Base_ctor_args&&... args):Base(std::forward<Base_ctor_args>(args)...){
    }
    void commit(){
        Base::reallocate(Content::size());
        Content::upload_content(*this,0);
    }
private:
    using Base::upload;
    using Base::reallocate;
};

}
}
