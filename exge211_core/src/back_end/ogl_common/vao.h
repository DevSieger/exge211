#pragma once
#include "ogl_functions.h"
#include <dk/concepts.h>

namespace exge211{
namespace ogl{

class VAO{
public:
    class Binding: dk::Non_copyable<Binding>{
    public:
        Binding(VAO const& vao){
            glBindVertexArray(vao.id());
        }

        ~Binding(){
            glBindVertexArray(0);
        }
    };

    VAO(){
        glGenVertexArrays(1,&id_);
    }

    VAO(VAO&& other):id_(other.id_){
        other.id_=0;
    }

    ~VAO(){
        if(id_){
            glDeleteVertexArrays(1,&id_);
        }
    }

    GLuint id()const{
        return id_;
    }

private:
    GLuint id_=0;
};

}
}
