#include "../../public_headers.h"
#include PUBLIC_HEADER(logging.h)

#include "error_checking.h"
#include "ogl_functions.h"
#include <unordered_set>
#include <utility>

namespace exge211 {

bool check_gl_error_f(const char * file, int line){
    GLenum err;
    bool errors=false;
    static std::unordered_set<String> reported;  // to prevent spamming.
    while( (err=glGetError())!=GL_NO_ERROR){
        std::string error_type;
        switch(err){
            case GL_INVALID_ENUM: error_type="INVALID_ENUM";break;
            case GL_INVALID_VALUE: error_type="INVALID_VALUE";break;
            case GL_INVALID_OPERATION: error_type="INVALID_OPERATION";break;
            case GL_INVALID_FRAMEBUFFER_OPERATION: error_type="INVALID_FRAMEBUFFER_OPERATION";break;
            case GL_OUT_OF_MEMORY: error_type="OUT_OF_MEMORY";break;
            case GL_STACK_UNDERFLOW: error_type="STACK_UNDERFLOW";break;
            case GL_STACK_OVERFLOW: error_type="STACK_OVERFLOW";break;
            default: error_type=dk::fstr("\"%1%\"",(unsigned)err);break;
        }
        String error_string=dk::fstr("%1%, %2%:%3%",file,line,err);
        if(!reported.count(error_string)){
            log_msg(Detail_level::CRITICAL,"gl error %1% at %3% line %2%",error_type,line,file);
            reported.emplace(std::move(error_string));
        }
        errors=true;

    }
    return errors;
}

}
