#pragma once
#include "../../public_headers.h"
#include PUBLIC_HEADER(common_types.h)
#include "ogl_functions.h"

namespace exge211 {
namespace ogl{

/// An utility class, wraps ogl 'GL_TIME_ELAPSED' query object.
class ET_query{
public:

    ET_query();

    ~ET_query();

    ET_query(ET_query&& other) noexcept;

    // DO NOT INTERSECT SECTIONS OF DIFFERENT QUERIES !
    void section_begin();
    void section_end();

    /// Tests if the measured section has completed and the result is ready.
    bool ready() const;

    // returns elapsed time, in nanoseconds.
    // blocking call.
    u32 get_value() const;

private:
    GLuint id_  = 0;
};

class Performance_counter{
public:
    using Time = double;

    /// Clears the accumulated results.
    void clear();

    /// Gets the mean time of the measured section execution on the GPU.
    Time get() const{
        return mean_;
    }

    /// Starts the measured section.
    void section_begin();

    /// Ends the measured section.
    void section_end();

    Performance_counter();
private:

    /// Updates the mean time using the given query.
    void gather(ET_query const& query);

    Time mean_          = 0;
    u64 sample_count_   = 0;
    ET_query query_a_;
    ET_query query_b_;
    ET_query * current_ = nullptr;
};

}
}
