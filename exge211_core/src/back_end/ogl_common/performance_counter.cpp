#include "performance_counter.h"
#include "error_checking.h"

namespace exge211 {
namespace ogl {

ET_query::ET_query()
{
    glGenQueries(1,&id_);
}

ET_query::~ET_query()
{
    if(id_)
        glDeleteQueries(1,&id_);
}

ET_query::ET_query(ET_query&& other) noexcept:
    id_(other.id_)
{
    other.id_=0;
}

void ET_query::section_begin()
{
    glBeginQuery(GL_TIME_ELAPSED,id_);
    check_gl_error();
}

void ET_query::section_end()
{
    glEndQuery(GL_TIME_ELAPSED);
    check_gl_error();
}

bool ET_query::ready() const
{
    GLuint is_ready = 0;
    glGetQueryObjectuiv(id_, GL_QUERY_RESULT_AVAILABLE , &is_ready);
    check_gl_error();
    return is_ready;
}

u32 ET_query::get_value() const
{
    u32 value;
    glGetQueryObjectuiv(id_, GL_QUERY_RESULT , &value);
    check_gl_error();
    return value;
}

void Performance_counter::clear()
{
    mean_           = 0;
    sample_count_   = 0;
}

void Performance_counter::section_begin()
{
    current_ = (current_ == &query_b_)? &query_a_ : &query_b_;
    gather(*current_);
    current_->section_begin();
}

void Performance_counter::section_end()
{
    current_->section_end();
}

Performance_counter::Performance_counter()
{
    // to simplify #section_begin method.
    query_a_.section_begin();
    query_a_.section_end();
    query_b_.section_begin();
    query_b_.section_end();
    current_ = &query_a_;
}

void Performance_counter::gather(const ET_query& query)
{
    u32 nanosec = query.get_value();
    Time sec    = nanosec * 0.000000001;
    mean_       = (mean_ * sample_count_  + sec)/(sample_count_+1);
    ++ sample_count_;
}


}
}
