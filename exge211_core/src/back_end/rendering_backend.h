#pragma once

#include "../public_headers.h"
#include PUBLIC_HEADER(handles.h)
#include PUBLIC_HEADER(common_types.h)
#include "../front_end/scene/scene_access.h"
#include <dk/concepts.h>



namespace exge211 {

class Resource_factory;
class Animation_engine;

class Rendering_backend: dk::Non_copyable<Rendering_backend>{
public:
    virtual void init(Window_frame_size const& new_size){}
    virtual void render(Scene_access& scene_access)=0;
    virtual void resize(Window_frame_size const& new_size){}
    virtual void on_scene_change(){}

    virtual Resource_factory& resource_factory()=0;
    virtual Resource_factory const& resource_factory()const=0;

    virtual Animation_engine* animation_engine(){return nullptr;}
    virtual ~Rendering_backend(){}
};

}
