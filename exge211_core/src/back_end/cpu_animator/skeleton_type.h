#pragma once
#include "../../front_end/actor/skeleton_type_base.h"
#include <dk/concepts.h>

namespace exge211 {

struct Skeleton_info;

namespace cpu_animator {

class Skeleton_type final: public Skeleton_type_base, dk::Non_copyable<Skeleton_type>{
public:
    using Joint_index=unsigned char;
    Skeleton_type(const Skeleton_info & info);
    ~Skeleton_type();

    /// Returns the parent joint of the given joint.
    Joint_index const & parent(Joint_index target)const{
        return hierarchy_[target];
    }
    /// Returns the parent joint of the given joint.
    Joint_index & parent(Joint_index target){
        return hierarchy_[target];
    }

    /// Returns the number of joints.
    Joint_index size()const{
        return size_;
    }

private:
    void init(const Skeleton_info & info);
    void clear();
    Joint_index * hierarchy_=nullptr;
    Joint_index size_=0;
};

}
}
