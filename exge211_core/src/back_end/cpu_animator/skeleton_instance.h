#pragma once
#include "../../front_end/actor/skeleton_instance_base.h"
#include "skeleton_type.h"

namespace exge211 {
namespace cpu_animator {

class Skeleton_instance final: public Skeleton_instance_base
{
public:
    using Joint_index=Skeleton_type::Joint_index;

    Skeleton_instance(Skeleton_type const& type):type_(type){}
    ~Skeleton_instance()=default;

    /// Returns the parent joint of the given joint.
    Joint_index const & parent(Joint_index target)const{
        return type_.parent(target);
    }

    /// Returns the number of joints.
    Joint_index size()const{
        return type_.size();
    }

    static constexpr Skeleton_instance& downcast(Skeleton_instance_base& base){
        return static_cast<Skeleton_instance&>(base);
    }
    static constexpr Skeleton_instance const& downcast(Skeleton_instance_base const& base){
        return static_cast<Skeleton_instance const&>(base);
    }

private:
    Skeleton_type const& type_;
};

}
}
