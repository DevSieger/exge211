#include "skeleton_type.h"
#include "../../public_headers.h"
#include PUBLIC_HEADER(res/resource_data.h)
#include PUBLIC_HEADER(logging.h)

namespace exge211 {
namespace cpu_animator{

Skeleton_type::Skeleton_type(const Skeleton_info & info){
    init(info);
}

void Skeleton_type::init(const Skeleton_info & info){
    auto & joints=info.joints;
    if(joints.empty()){
        log_msg(Detail_level::NON_CRITICAL,"Skeleton info without joints");
        return;
    };
    clear();
    size_=joints.size();
    hierarchy_=new Joint_index[size_];
    for(size_t i=0;i<size_;++i){
        hierarchy_[i]=joints[i].parent;
    }
}

Skeleton_type::~Skeleton_type(){
    clear();
}

void Skeleton_type::clear(){
    if(size_){
        delete [] hierarchy_;
        hierarchy_=nullptr;
        size_=0;
    }
}

}
}
