#include "../../public_headers.h"
#include PUBLIC_HEADER(logging.h)
#include PUBLIC_HEADER(res/rs_fwd.h)
#include "animation_engine_hb.h"
#include "skeleton_instance.h"
#include <cassert>

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/dual_quaternion.hpp>

namespace exge211 {
using cpu_animator::Skeleton_instance;
using cpu_animator::Skeleton_type;

namespace{

/// The helper function for getting the frames to interpolate.
template<typename Frame_type,typename Container_type>
void get_frame(
        Frame_type & prev_out,
        Frame_type & next_out,
        Frame_type hint,
        Animation_channel::Time cur_t,
        Animation_rate rate,
        Container_type const & v)
{
    assert(hint<v.size());
    assert(v.size());

    if(rate>=0){
        prev_out=(cur_t>=v[hint].time) ? hint : 0;

        if( prev_out == 0 ){
            // this is a weird case, that may produce small glitches if the animation doesn't have keyframes for the zero time point.
            // ( a negative value 't' will be passed to v = mix( v_prev, v_next, t), but the extrapolation may be undesirable)
            if( cur_t < v[0].time){
                next_out = 0;
                return;
            }
        }

        while(prev_out+1<v.size() && cur_t >= v[prev_out+1].time ){
                ++prev_out;
        }
        next_out=prev_out;
        if(next_out+1 < v.size()){
            ++next_out;
        };
    }else{
        prev_out=(cur_t<=v[hint].time) ? hint : (v.size()-1);
        while(prev_out>0 && cur_t <= v[prev_out-1].time){
                --prev_out;
        }
        next_out=prev_out;
        if(next_out>0){
            --next_out;
        };
    }
}

}

Animation_engine_HB::Animation_engine_HB()
{}

Animation_handle Animation_engine_HB::load(const Resource_request & req)
{
    auto it=animations_.find(req);
    if(it==animations_.end()){
        Animation_data_impl anim;
        loader().load_animation(req,anim);
        auto res=animations_.emplace(req,std::move(anim));
        if(!res.second){
            log_msg(Detail_level::CRITICAL,"animation{%1%} not inserted in cache",req);
        }
        return &res.first->second;
    }else{
        return &it->second;
    }
}

bool Animation_engine_HB::relax(Animation_handle animation)
{
    return false;
}

void Animation_engine_HB::play(Animation_state & obj, Animation_handle animation, Scene_time time_point)
{
    assert(&obj.engine()==this);
    assert(obj.entry());

    Animation_data_impl& anim=Animation_data_impl::downcast(*animation);
    assert(Skeleton_instance::downcast(obj.skeleton()).size()==anim.channels.size());

    Entry & entry=get_entry(obj);

    entry.current=&anim;
    entry.time  = 0;
    entry.last  = time_point;
    for(auto &i:entry.hint){
        i.reset();
    }
}

void Animation_engine_HB::change_rate(Animation_state & obj, Animation_rate rate, Scene_time time_point)
{
    assert(&obj.engine()==this);
    assert(obj.entry());
    update_animation_time(get_entry(obj),time_point);
    get_entry(obj).rate=rate;
}

void Animation_engine_HB::animate(Animation_state & obj)
{
    Entry *new_entry=new Entry;
    obj.entry(new_entry);

    // this actually does nothing.
    new_entry->last = 0;
    new_entry->hint.resize(Skeleton_instance::downcast(obj.skeleton()).size());
}

void Animation_engine_HB::release(Animation_state & obj)
{
    assert(&obj.engine()==this);
    assert(obj.entry());
    delete &get_entry(obj);
    obj.entry(nullptr);
}

void Animation_engine_HB::write(Joint * out, Animation_state & obj, Scene_time time_point)
{
    assert(&obj.engine()==this);
    assert(obj.entry());
    using ATime=Animation_channel::Time;
    using glm::vec3;
    Skeleton_instance const& skeleton=Skeleton_instance::downcast(obj.skeleton());
    Entry & entry=get_entry(obj);
    if(entry.current){
        Animation_data &anim=*entry.current;
        ATime cur_t=update_animation_time(entry, time_point);
        for(size_t ch_ix=0;ch_ix<entry.hint.size();++ch_ix){
            Animation_channel& ch=anim.channels[ch_ix];
            Channel_hint& hint=entry.hint[ch_ix];

            Animation_channel::Rotations & rotations=ch.rotations;
            glm::quat rot;
            Animation_channel::Translations & translations=ch.translations;
            glm::vec3 tr;
            Animation_channel::Scales & scales=ch.scales;
            glm::vec3 sc{1,1,1};
            if(!rotations.empty()){
                Frame_index prev;
                Frame_index next;
                get_frame(prev,next,hint.rot,cur_t,entry.rate,rotations);
                ATime fdt=rotations[next].time-rotations[prev].time;
                ATime t=cur_t-rotations[prev].time;
                hint.rot=prev;

                if(fdt!=0){
                    rot=glm::slerp(rotations[prev],rotations[next],t/fdt);
                }else{
                    rot=rotations[next];
                }
            };
            if(!translations.empty()){
                Frame_index prev;
                Frame_index next;
                get_frame(prev,next,hint.trans,cur_t,entry.rate,translations);
                ATime fdt=translations[next].time-translations[prev].time;
                ATime t=cur_t-translations[prev].time;
                hint.trans=prev;

                if(fdt!=0){
                    tr=glm::mix((vec3&)translations[prev],(vec3&)translations[next],t/fdt);
                }else{
                    tr=translations[next];
                }
            }
            if(!scales.empty()){
                Frame_index prev;
                Frame_index next;
                get_frame(prev,next,hint.scale,cur_t,entry.rate,scales);
                ATime fdt=scales[next].time-scales[prev].time;
                ATime t=cur_t-scales[prev].time;
                hint.scale=prev;

                if(fdt!=0){
                    sc=glm::mix((vec3&)scales[prev],(vec3&)scales[next],t/fdt);
                }else{
                    sc=scales[next];
                }
            }
            Skeleton_type::Joint_index parent=skeleton.parent(ch_ix);
            Joint transform(rot,tr);
            out[ch_ix]=transform;
            if(ch_ix>parent){
                out[ch_ix]=out[parent]*out[ch_ix];
            }
        }
    }else{
        for(size_t i=0;i<skeleton.size();++i){
            out[i]=Joint();
        }
    }
}

Animation_channel::Time Animation_engine_HB::update_animation_time(Animation_engine_HB::Entry & entry, Scene_time current_time)
{
    double dt   = current_time - entry.last;
    entry.last  = current_time;
    entry.time  = fmod(entry.current->duration+entry.time + entry.rate*dt , entry.current->duration);
    return entry.time;
}

}
