#pragma once
#include "../../public_headers.h"
#include PUBLIC_HEADER(resource_loader.h)
#include PUBLIC_HEADER(vector_types.h)
#include PUBLIC_HEADER(res/animation_data.h)
#include <unordered_map>
#include "../animation_engine.h"
#include "../../front_end/actor/animation_state.h"

namespace exge211 {

/// A class used for computation of classical skeletal animation on the CPU side.
class Animation_engine_HB: public Animation_engine{
public:
    Animation_engine_HB();
    Animation_handle load(Resource_request const & req) override;
    bool relax(Animation_handle animation) override;
    void play(Animation_state & obj,Animation_handle animation, Scene_time time_point) override;
    void change_rate(Animation_state & obj,Animation_rate rate, Scene_time time_point) override;

    Animation_engine_HB& loader(Resource_loader const* a_loader){
        loader_=a_loader;
        return * this;
    }
    Resource_loader const& loader()const{
        return *loader_;
    }

    /// Writes joints in the current pose of the object with the given animation state.
    void write(Joint* out_start,Animation_state & obj, Scene_time time_point);

protected:
    void animate(Animation_state & obj) override;
    void release(Animation_state & obj) override;
private:

    using Frame_index=size_t;

    /// A hint for finding the current frame of the animation.
    struct Channel_hint{
        // The indices to start search from.
        Frame_index rot=0;
        Frame_index trans=0;
        Frame_index scale=0;
        void reset(){
            rot=0;
            trans=0;
            scale=0;
        }
    };

    // The animation handle for that engine is just Animation_data.
    struct Animation_data_impl final: Animation_base, Animation_data{
        static constexpr Animation_data_impl& downcast(Animation_base& obj){
            return static_cast<Animation_data_impl&>(obj);
        }
        static constexpr Animation_data_impl const& downcast(Animation_base const& obj){
            return static_cast<Animation_data_impl const&>(obj);
        }
    };

    //
    struct Entry final:Animation_entry{
        using Hint=std::vector<Channel_hint>;   // one hint per joint.
        Hint hint;
        Animation_channel::Time time;   // time of the animation playback, not the total elapsed time.
        dk::Timer::Time_period last=-1; // time point of the last computation.
        Animation_data_impl* current=nullptr;
        Animation_rate rate=1.0;
    };

    Animation_channel::Time update_animation_time(Entry& entry, Scene_time current_time);    // Returns the new value of entry.time

    Entry & get_entry(Animation_state & obj)const{
        return static_cast<Entry&>(*obj.entry());
    }
    Entry const & get_entry(Animation_state const & obj)const{
        return static_cast<Entry const&>(*obj.entry());
    }

    using Cache=std::unordered_map<Resource_request,Animation_data_impl>;
    Cache animations_;  //the loaded animations.
    Resource_loader const* loader_ = nullptr;
};

}

