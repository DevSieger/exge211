#pragma once

#include <memory>
#include <dk/concepts.h>

namespace exge211 {

class Rendering_backend;
using Rendering_backend_ptr=std::shared_ptr<Rendering_backend>;

class Context;
class Resource_loader;
class Rendering_backend_factory;
using Rendering_backend_factory_ptr = std::shared_ptr<Rendering_backend_factory>;

class Rendering_backend_factory: dk::Non_copyable<Rendering_backend_factory>{
public:
    static Rendering_backend_factory_ptr get_factory(Context const& ctx);
    virtual Rendering_backend_ptr new_rendering_backend(Resource_loader const& rf)=0;

    //main reason to implement this class via abstract factory is possible future methods like below:
    //virtual GUI_backend_ptr new_gui_backend()=0;

    virtual ~Rendering_backend_factory()=default;
};


}
