#pragma once
#include <dk/concepts.h>

namespace exge211 {

class Animation_state;
class Animation_base{
protected:
    ~Animation_base() = default;
};

/*!
 * \brief An abstract class to deal with the animation of exge211::Actor.
 *
 * \note Where is two reasons to make that 'overengineering' in the way how it is:
 * - possibility to make different 'animation engines', considering 'physics engine' as one of them
 *         -> an Animation_system base class .
 * - possibility to do computations of an animation on the GPU
 *         -> the animation engines depends on the rendering backend.
 */
class Animation_system: dk::Non_copyable<Animation_system>{
public:
    virtual ~Animation_system()=default;
private:
    /*
     * This methods are called when a new animation engine is set for the Actor object.
     * They are responsible to the (de)initialization of the given object in the animation engine.
     * The only user of this methods has to be Animation_state, since it is responsible for the engine change.
     */
    virtual void animate(Animation_state & obj)=0;
    virtual void release(Animation_state & obj)=0;
    friend class Animation_state;
};

}
