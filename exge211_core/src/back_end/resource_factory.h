#pragma once

#include "../public_headers.h"
#include PUBLIC_HEADER(common_types.h)
#include PUBLIC_HEADER(handles.h)
#include PUBLIC_HEADER(vector_types.h)
#include <dk/concepts.h>
#include "../private_handles.h"

namespace exge211 {

struct Model_instance_base;
struct Static_model_instance_base;

struct Animation_data;
class Skeleton_instance_base;

class Resource_factory: dk::Non_copyable<Resource_factory>
{
public:
    virtual Model_handle request_amodel(Resource_request const & model,Resource_request const & skin)=0;
    virtual bool relax(Model_handle prototype)=0;

    virtual Model_instance_base * construct_model( Model_handle proto)=0;
    virtual void release(Model_instance_base * model)=0;

    virtual Static_model_handle request_smodel(Resource_request const & model,Resource_request const & skin)=0;
    virtual bool relax(Static_model_handle prototype)=0;

    virtual Static_model_instance_base * construct_model( Static_model_handle proto,Rotation const& rotation, Position const& position,Direction const& scale)=0;
    virtual void release(Static_model_instance_base * model)=0;

    /* TODO: решить проблему с дубляжом метода в движке анимации */
    virtual Animation_data * request_animation(Resource_request const & animation)=0;
    virtual bool relax(Animation_data * prototype)=0;

    virtual Skeleton_handle request_skeleton_type(Resource_request const & skeleton_type)=0;
    virtual bool relax(Skeleton_handle Skeleton_handle)=0;

    virtual Skeleton_instance_base * construct_skeleton(Skeleton_handle skeleton_type)=0;
    virtual void release(Skeleton_instance_base * skeleton)=0;

    virtual Skybox_ptr get_skybox(Resource_request const & name)=0;

    virtual ~Resource_factory()=default;
};

}
