#include "../public_headers.h"
#include PUBLIC_HEADER(camera.h)
#include <glm/gtx/quaternion.hpp>
#include <limits>

namespace exge211 {

namespace{

// the following functions are used in build_rotation.
// build_rotation can't use protected static members of Camera.
Direction const& get_screen_space_fow()
{
    static const Direction dir{0,0,-1};
    return dir;
}
Direction const& get_screen_space_up()
{
    static const Direction dir{0,1,0};
    return dir;
}

//IMHO arguments and return value are self explaining
Rotation build_rotation(Rotation const& old_rotation, Direction const& new_view_direction,  Direction const& new_up_direction)
{
    // let's try to use NRVO
    Rotation result{0,0,0,0};
    if(glm::dot( new_view_direction, new_view_direction)){
        const Direction dir=glm::normalize( new_view_direction);
        const Direction old_dir=old_rotation*get_screen_space_fow();

        //an orthogonalization of the new up direction
        Direction new_up=new_up_direction-glm::dot(new_up_direction,dir)*dir;

        //now we change the resulting rotation to set camera view direction to new_view_direction
        // if vectors in opposite directions glm::rotation can't produce correct axis.
        if(glm::dot(old_dir,dir) > -1 + std::numeric_limits<float>::epsilon()){
            result=glm::rotation(old_dir,dir)*old_rotation;
        }else{
            // if vectors in opposite directions, than axis is old up direction, angle is 2*pi.
            result=Rotation(0,old_rotation*get_screen_space_up())*old_rotation;
        }

        // now we adjust the resulting rotation to set the up direction of camera, using view direction as axis.
        if(glm::dot(new_up,new_up)){
            new_up=glm::normalize(new_up);
            const Direction tmp_up=result*get_screen_space_up();

            // if vectors in opposite directions glm::rotation can't produce correct axis.
            if(glm::dot(tmp_up,new_up) >  -1 + std::numeric_limits<float>::epsilon()){
                result=glm::rotation(tmp_up,new_up)*result;
            }else{
                result=Rotation(0,result*get_screen_space_fow())*result;
            }
        }
    }
    return glm::normalize(result);
}

Rotation build_rotation(Rotation const & old_rotation, Position const & cam_position, Position const& target_location, Direction const& new_up)
{
    return build_rotation(old_rotation,target_location-cam_position,new_up);
}

}

Direction const& Camera::default_up()
{
    static const Direction dir{0,0,1};
    return dir;
}

Direction const& Camera::orig_forward()
{
    return get_screen_space_fow();
}

Direction const& Camera::orig_up()
{
    return get_screen_space_up();
}

Camera::Camera()
{
}

Camera &Camera::direction(const Direction &new_view_direction, const Direction & new_up_direction){
    return rotation(build_rotation(rotation(),new_view_direction,new_up_direction));
}

Camera &Camera::rotate( Rotation const&r){
    rotation(rotation()* ( Rotation{0.70710678118654752440084436210485f,-0.70710678118654752440084436210485f,0,0}
                           *r
                           * Rotation{0.70710678118654752440084436210485f,0.70710678118654752440084436210485f,0,0})
             );
    return *this;
}

void Camera::look_at( Position const &target_position, Direction const& v_up){
    direction(target_position - position(),v_up);
}

}
