#include "../public_headers.h"
#include PUBLIC_HEADER(application_framework.h)
#include PUBLIC_HEADER(logging.h)
#include PUBLIC_HEADER(platform.h)
#include PUBLIC_HEADER(renderer.h)
#include PUBLIC_HEADER(context.h)

#include <dk/timer.hpp>

namespace exge211 {

GUI_application::GUI_application(Platform_factory &factory,Resource_loader& loader)
{
    event_loop_=factory.get_GUI_event_loop();
    wnd_=factory.new_window(640,480,"test");
    renderer_=new Renderer(wnd_->context(),loader);


    GUI_event_loop* event_loop=&*event_loop_;
    wnd_->on_close=[event_loop]()->bool{
        event_loop->running(false);
        return true;
    };

}

bool GUI_application::init()
{
    return true;
}

int GUI_application::run()
{
    dk::Timer timer;
    wnd_->show();
    timer.start();
    if(!event_loop_->running())
        return 1;
// the hack to prevent swapping buffers of windows that are not-exposed (the linux version with the Qt).
// todo: rewrite GL_window_Qt::show
    if(event_loop_->running())
        event_loop_->process_events();

    while(event_loop_->running()){
        event_loop_->process_events();
        if(!iteration(timer.restart())){
            break;
        }
        renderer_->render();
    };
    return 0;
}

GUI_application::~GUI_application()
{
    delete renderer_;
}

bool GUI_application::iteration(GUI_application::Time t)
{
    return true;
}

}
