#include "../public_headers.h"
#include PUBLIC_HEADER(fps_counter.h)

namespace exge211 {

FPS_counter::FPS_counter(Time_period decay_a):
    decay_(decay_a)
{
}

FPS_counter::Time_period FPS_counter::get(double dt)
{
    if(aft_>0){
        aft_=aft_*decay_+(1-decay_)*dt;
    }else{
        aft_=dt;
    };
    return 1/aft_;
}

void FPS_counter::reset(){
    aft_=0;
}

}
