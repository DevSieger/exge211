#pragma once

namespace exge211 {

/*!
 * \brief A class responsible for a skeleton properties which are shared between instances of the same type/structure.
 *
 * This class is empty because a method of the data storage is backend defined (e.g. it may be just a handle to the GPU memory).
 */
class Skeleton_type_base{
protected:
    ~Skeleton_type_base() = default;
};

}
