#pragma once

#include "../../public_headers.h"
#include PUBLIC_HEADER(actor.h)
#include PUBLIC_HEADER(animation.h)
#include "actor_data.h"
#include "../../private_handles.h"
#include "../scene/scene_implementation.h"
#include <list>

namespace exge211{

class Actor_impl: public Actor, public Animation{
public:
    Actor_impl(Scene_impl_ptr scene_a, Skeleton_instance_base& skeleton): data_(skeleton), scene_(scene_a){}

    Actor_data& data(){
        return data_;
    }
    Actor_data const& data()const{
        return data_;
    }

    Scene_implementation & scene(){
        return *scene_;
    }

    void on_position_change(Position const& old_position);
    void on_rotation_change(Rotation const& old_rotation);
    void on_scale_change(float old_scale);
    void on_boundary_change();


    void set(Slot_index slot, Model_handle item);
    void empty(Slot_index slot);

protected:

    //this is not the most derived class.
    ~Actor_impl() = default;
private:
    void calculate_boundary();

    Actor_data data_;
    const Scene_impl_ptr scene_;
};

}
