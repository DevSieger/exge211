#pragma once

#include "skeleton_type_base.h"
#include "../../public_headers.h"
#include PUBLIC_HEADER(handles.h)
#include PUBLIC_HEADER(vector_types.h)

namespace exge211 {

/// A class responsible for the instance-specific skeleton properties.
class Skeleton_instance_base
{
public:
    Position const & position()const{
        return position_;
    }
    void position(const Position & a_position){
        position_=a_position;
    }

    Rotation const & rotatation()const{
        return rotatation_;
    }
    void rotatation(const Rotation & a_rotatation){
        rotatation_=a_rotatation;
    }

    float scale()const{
        return scale_;
    }
    void scale(float sc){
        scale_=sc;
    }
protected:
    ~Skeleton_instance_base() = default;
private:
    Rotation rotatation_;
    Position position_;
    float scale_=1;
};

}

