#include "../../public_headers.h"
#include PUBLIC_HEADER(animation.h)

#include "../../back_end/animation_engine.h"
#include "skeleton_instance_base.h"
#include "actor_impl.h"

namespace exge211 {

namespace{

constexpr Actor_impl& get_impl(Animation& anim){
    return static_cast<Actor_impl&>(anim);
}
constexpr Actor_impl const& get_impl(Animation const& anim){
    return static_cast<Actor_impl const&>(anim);
}

Animation_state& get_state(Animation& anim){
    return get_impl(anim).data().state;
}
Animation_state const& get_state(Animation const& anim){
    return get_impl(anim).data().state;
}

}

void Animation::play(Animation_handle animation)
{   
    Animation_state& animation_state    = get_state(*this);
    Scene_time total_time               = get_impl(*this).scene().total_time();
    static_cast<Animation_engine&>(animation_state.engine()).play(animation_state, animation, total_time);
}

void Animation::change_rate(Animation_rate rate)
{
    Animation_state& animation_state    = get_state(*this);
    Scene_time total_time               = get_impl(*this).scene().total_time();
    static_cast<Animation_engine&>(animation_state.engine()).change_rate(animation_state,rate, total_time );
}

Position const& Animation::position() const
{
    Animation_state const& animation_state=get_state(*this);
    return animation_state.skeleton().position();
}

Animation& Animation::position(const Position & a_position)
{
    Animation_state& animation_state=get_state(*this);
    Position old_value=animation_state.skeleton().position();
    animation_state.skeleton().position(a_position);
    get_impl(*this).on_position_change(old_value);
    return *this;
}

Rotation const& Animation::rotatation() const
{
    Animation_state const& animation_state=get_state(*this);
    return animation_state.skeleton().rotatation();
}

Animation& Animation::rotatation(Rotation const& a_rotatation)
{
    Animation_state& animation_state=get_state(*this);
    Rotation old_value=animation_state.skeleton().rotatation();
    animation_state.skeleton().rotatation(a_rotatation);
    get_impl(*this).on_rotation_change(old_value);
    return *this;

}

float Animation::scale() const
{
    Animation_state const& animation_state=get_state(*this);
    return animation_state.skeleton().scale();
}

Animation &Animation::scale(float sc)
{
    Animation_state& animation_state=get_state(*this);
    animation_state.skeleton().scale(sc);
    float old_value=animation_state.skeleton().scale();
    get_impl(*this).on_scale_change(old_value);
    return *this;
}

}

