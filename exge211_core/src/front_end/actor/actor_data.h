#pragma once

#include "../../public_headers.h"
#include PUBLIC_HEADER(aabb.h)

#include <vector>
#include "animation_state.h"

namespace exge211 {

struct Model_instance_base;

struct Actor_data{
    std::vector<Model_instance_base*> models;
    Animation_state state;
    AABB local_boundary;
    AABB global_boundary;
    Actor_data(Skeleton_instance_base& skeleton):state(skeleton){}
};

}
