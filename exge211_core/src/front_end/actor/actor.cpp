#include "../../public_headers.h"
#include PUBLIC_HEADER(actor.h)
#include PUBLIC_HEADER(logging.h)

#include "actor_impl.h"

namespace exge211 {

namespace{

constexpr Actor_impl& get_impl(Actor& actor){
    return static_cast<Actor_impl&>(actor);
}
constexpr Actor_impl const& get_impl(Actor const& actor){
    return static_cast<Actor_impl const&>(actor);
}

}


Actor &Actor::set(Actor::Slot_index slot, Model_handle item)
{
    get_impl(*this).set(slot,item);
    return *this;
}

Actor&Actor::empty(Actor::Slot_index slot)
{
    get_impl(*this).empty(slot);
    return *this;
}

Animation & Actor::animation()
{
    return get_impl(*this);
}

Animation const& Actor::animation() const
{
    return get_impl(*this);
}


}
