#pragma once

#include "../../public_headers.h"
#include PUBLIC_HEADER(res/resource_data.h)
#include PUBLIC_HEADER(handles.h)

namespace exge211 {

class Model_base: public Bounded{
protected:
    ~Model_base() = default;
};

class Model_instance_base{
public:
    Model_instance_base(Model_handle a_type):type_(a_type){}

    Model_handle type()const{
        return type_;
    }

protected:
    ~Model_instance_base() = default;
private:
    Model_handle const type_;
    //u32 some_item_state_;
};

}
