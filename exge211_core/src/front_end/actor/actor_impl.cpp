#include "actor_impl.h"
#include "model.h"
#include "../scene/scene_implementation.h"
#include "skeleton_instance_base.h"

namespace exge211{

void Actor_impl::on_position_change(const Position& old_position){
    AABB& boundary=data().global_boundary;
    Direction delta=data().state.skeleton().position()-old_position;
    boundary.min+=delta;
    boundary.max+=delta;
    scene().on_boundary_change(*this);
}

void Actor_impl::on_rotation_change(const Rotation&){
    on_boundary_change();
}

void Actor_impl::on_scale_change(float old_scale){
    assert(old_scale);

    AABB& boundary=data().global_boundary;
    auto const& skel=data().state.skeleton();
    Position const& pos=skel.position();

    float ratio=skel.scale()/old_scale;
    boundary.min=(boundary.min-pos)*ratio+pos;
    boundary.max=(boundary.max-pos)*ratio+pos;
    scene().on_boundary_change(*this);
}

void Actor_impl::on_boundary_change(){
    AABB& boundary=data().global_boundary;
    auto const& skel=data().state.skeleton();
    Rotation const& rotation=skel.rotatation();
    Position const& position=skel.position();

    boundary.reset();
    AABB scaled=data().local_boundary;
    scaled.min*=skel.scale();
    scaled.max*=skel.scale();

    for(size_t corner=0 ; corner<8 ; ++corner){
        boundary.add(rotation*scaled.corner(corner));
    }
    boundary.min+=position;
    boundary.max+=position;
    scene().on_boundary_change(*this);
}

void Actor_impl::calculate_boundary(){
    data().local_boundary.reset();
    auto & models=data().models;
    for(Actor::Slot_index ix=0;ix<models.size();++ix){
        if(models[ix]){
            data().local_boundary.add(models[ix]->type()->boundary());
        }
    }
    on_boundary_change();
}

void Actor_impl::set(Slot_index slot, Model_handle item)
{
    scene().set_actor_item(*this,slot,item);
    calculate_boundary();
}

void Actor_impl::empty(Slot_index slot)
{
    scene().empty_actor_item(*this,slot);
    calculate_boundary();
}


}
