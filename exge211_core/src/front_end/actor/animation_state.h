#pragma once

#include <cassert>
#include "../../private_fwd.h"
#include <dk/concepts.h>

namespace exge211 {

class Animation_entry{
protected:
    ~Animation_entry() = default;
};

class Animation_system;
class Skeleton_instance_base;

class Animation_state: dk::Non_copyable<Animation_state>{
public:
    Animation_state(Skeleton_instance_base& skeleton_inst):skeleton_(skeleton_inst){}

    Skeleton_instance_base& skeleton(){
        return skeleton_;
    }
    Skeleton_instance_base const& skeleton()const{
        return skeleton_;
    }
    Animation_system & engine(){
        assert(engine_);
        return *engine_;
    }
    Animation_system const& engine()const{
        assert(engine_);
        return *engine_;
    }
    Animation_entry* entry(){
        return entry_;
    }
    const Animation_entry * entry()const{
        return entry_;
    }
    void entry(Animation_entry* new_entry){
        assert(!entry_ || !new_entry);
        entry_=new_entry;
    }

    void engine(Animation_system* new_engine);
    ~Animation_state();

private:
    Skeleton_instance_base & skeleton_;
    Animation_system * engine_=nullptr;
    Animation_entry * entry_=nullptr;
};

}
