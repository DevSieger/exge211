#include "../../public_headers.h"
#include PUBLIC_HEADER(logging.h)

#include "animation_state.h"
#include "../../back_end/animation_system.h"

namespace exge211 {

Animation_state::~Animation_state()
{
    if(engine_){
        engine(nullptr);
    }
}

void Animation_state::engine(Animation_system* new_engine)
{
    if(engine_){
        engine_->release(*this);
        engine_=nullptr;
    }
    if(new_engine){
        engine_=new_engine;
        new_engine->animate(*this);
    }
    if(engine_!=new_engine){
        log_msg(Detail_level::NON_CRITICAL,"can't animate object");
    }
}

}
