#include "../../public_headers.h"
#include PUBLIC_HEADER(directional_light.h)
#include "directional_light_impl.h"

namespace exge211 {

namespace{

constexpr Directional_light_impl& get_impl(Directional_light& dl){
    return static_cast<Directional_light_impl&>(dl);
}
constexpr Directional_light_impl const& get_impl(Directional_light const& dl){
    return static_cast<Directional_light_impl const&>(dl);
}

inline Directional_light_data& get_data(Directional_light& dl){
    return get_impl(dl).data();
}
inline Directional_light_data const& get_data(Directional_light const& dl){
    return get_impl(dl).data();
}

}

Direction const& Directional_light::direction() const
{
    return get_data(*this).direction;
}

Directional_light& Directional_light::direction(Direction const & new_direction)
{
    get_data(*this).direction=new_direction;
    return *this;
}

RGB const& Directional_light::intensity() const
{
    return get_data(*this).intensity;
}

Directional_light& Directional_light::intensity(RGB const& new_intensity)
{
    get_data(*this).intensity=new_intensity;
    return *this;
}

}
