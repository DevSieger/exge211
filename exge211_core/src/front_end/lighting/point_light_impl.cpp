#include "point_light_impl.h"
#include "../scene/scene_implementation.h"

void exge211::Point_light_impl::cast_shadows(bool a_cast_shadows){
    if(cast_shadows_!=a_cast_shadows){
        scene_->set_shadow(*this,a_cast_shadows);
        cast_shadows_=a_cast_shadows;
    }
}
