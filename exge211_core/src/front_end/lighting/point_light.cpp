#include "../../public_headers.h"
#include PUBLIC_HEADER(point_light.h)

#include <glm/common.hpp>
#include <glm/common.hpp>
#include <glm/exponential.hpp>
#include "lighting_data.h"
#include "point_light_impl.h"
#include "../scene/scene_implementation.h"

namespace exge211 {

namespace{

constexpr Point_light_impl& get_impl(Point_light& pl){
    return static_cast<Point_light_impl&>(pl);
}
constexpr Point_light_impl const& get_impl(Point_light const& pl){
    return static_cast<Point_light_impl const&>(pl);
}

inline Point_light_data& get_data(Point_light & pl){
    return get_impl(pl).data();
}
inline Point_light_data const& get_data(Point_light const& pl){
    return get_impl(pl).data();
}

}

float Point_light::max_intensity() const{
    Point_light_data const& data=get_data(*this);
    return glm::max(glm::max(data.intensity.r,data.intensity.g),data.intensity.b);
}

Point_light& Point_light::intensity(RGB const&intens){
    Point_light_data& data=get_data(*this);
    data.intensity=intens;
    data.range=range();
    get_impl(*this).on_boundary_change();
    return *this;
}

Point_light& Point_light::intensity(const exge211::RGB_norm & color, float dist, float intensity_on_dist){
    Point_light_data& data=get_data(*this);
    data.intensity=intensity_on_dist/attenuation(dist)*color;
    data.range=range();
    get_impl(*this).on_boundary_change();
    return *this;
}

Point_light& Point_light::radius(float r, bool reset){
    Point_light_data& data=get_data(*this);
    data.radius=r;
    if(reset){
        reset_attenuation();
    }else{
        data.range=range();
        get_impl(*this).on_boundary_change();
    };
    return *this;
}

Point_light& Point_light::reset_attenuation(){
    Point_light_data& data=get_data(*this);
    data.attenuation={1,2/radius(),1/(radius()*radius())};
    data.range=range();
    get_impl(*this).on_boundary_change();
    return *this;
}

Point_light& Point_light::attenuation(Attenuation_mult const& a){
    Point_light_data& data=get_data(*this);
    data.attenuation=a;
    data.range=range();
    get_impl(*this).on_boundary_change();
    return *this;
}

RGB const& Point_light::intensity() const{

    return get_data(*this).intensity;
}

float Point_light::radius() const{
    return get_data(*this).radius;
}

const Point_light::Attenuation_mult& Point_light::attenuation() const{
    return get_data(*this).attenuation;
}

Point_light &Point_light::range(float r){
    Point_light_data& data=get_data(*this);
    data.range=r;
    get_impl(*this).on_boundary_change();
    data.cut_off=cut_off();
    return *this;
}

float Point_light::range() const{
    Point_light_data const& data=get_data(*this);
    float a12=0.5f*data.attenuation[1]/data.attenuation[2];
    float t=a12*a12-(data.attenuation[0]-max_intensity()/data.cut_off)/data.attenuation[2];
    if(t<=0){
        t=0;
    }
    return -a12+glm::sqrt(t)+data.radius;
}

Point_light &Point_light::cut_off(float c){
    Point_light_data& data=get_data(*this);
    data.cut_off=c;
    data.range=range();
    get_impl(*this).on_boundary_change();
    return *this;
}

float Point_light::cut_off() const{
    return max_intensity()*attenuation(get_data(*this).range);
}

Point_light &Point_light::position(const Position & pos){
    get_data(*this).position=pos;
    get_impl(*this).on_boundary_change();
    return *this;
}

Point_light &Point_light::position(float x, float y, float z)
{
    Point_light_data& data=get_data(*this);
    data.position.x=x;
    data.position.y=y;
    data.position.z=z;
    get_impl(*this).on_boundary_change();
    return *this;
}

const Position & Point_light::position() const{
    return get_data(*this).position;
}

bool Point_light::shadow() const
{
    return get_impl(*this).cast_shadows();
}

Point_light &Point_light::shadow(bool s)
{
    get_impl(*this).cast_shadows(s);
    return *this;
}


float Point_light::attenuation(float dist) const{
    Point_light_data const& data=get_data(*this);
    float d=dist-data.radius;
    return 1/(data.attenuation[0]+data.attenuation[1]*d+data.attenuation[2]*d*d);
}

}
