#pragma once

#include "../../public_headers.h"
#include PUBLIC_HEADER(directional_light.h)
#include "lighting_data.h"
#include "../../private_handles.h"

namespace exge211{

class Directional_light_impl final: public Directional_light{
public:
    Directional_light_data const & data()const{
        return data_;
    }
    Directional_light_data & data(){
        return data_;
    }
    Directional_light_impl(Scene_impl_ptr scene_a):
        scene_(scene_a)
    {
    }

    Scene_implementation& scene()const{
        return *scene_;
    }

private:
    Directional_light_data data_;
    const Scene_impl_ptr scene_;
};

}
