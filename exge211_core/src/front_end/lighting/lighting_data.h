#pragma once

#include "../../public_headers.h"
#include PUBLIC_HEADER(common_types.h)
#include PUBLIC_HEADER(vector_types.h)

#include <utility>
#include <cassert>
#include <memory>

namespace exge211 {

struct Point_light_data{
    using vec3=glm::vec3;

    vec3 position={0,0,0};
    float range=0;

    vec3 intensity={0,0,0};
    float radius=1;

    vec3 attenuation={1,1,1};
    float cut_off=default_epsilon();

    static constexpr float default_epsilon(){
        return 4.0*10/256;
    }
    Point_light_data()=default;
};

struct Directional_light_data{
    RGB direction; //ensure that value was normalized!!!
    RGB intensity;
};

}
