#pragma once

#include "../../public_headers.h"
#include PUBLIC_HEADER(point_light.h)

#include "../../private_handles.h"
#include "lighting_data.h"
#include "../scene/scene_implementation.h"

namespace exge211 {

class Point_light_impl: public Point_light{
public:

    Point_light_data& data(){
        return data_;
    }
    Point_light_data const& data() const{
        return data_;
    }
    Scene_implementation & scene(){
        return *scene_;
    }
    bool cast_shadows()const{
        return cast_shadows_;
    }
    void cast_shadows(bool a_cast_shadows);

    void on_boundary_change(){
        scene().on_boundary_change(*this);
    }

    Point_light_impl(Scene_impl_ptr scene_a,bool cast_shadows_a):
        scene_(scene_a), cast_shadows_(cast_shadows_a)
    {}

protected:
    ~Point_light_impl() = default;
private:
    Point_light_data data_;
    const Scene_impl_ptr scene_;
    bool cast_shadows_;
};

}
