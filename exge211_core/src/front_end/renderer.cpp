#include "../public_headers.h"
#include PUBLIC_HEADER(renderer.h)
#include PUBLIC_HEADER(context.h)
#include PUBLIC_HEADER(logging.h)

#include "../back_end/rendering_backend.h"
#include "../back_end/rendering_backend_factory.h"
#include "scene/scene_implementation.h"

namespace exge211 {

struct Renderer::Data{
    Context & ctx;
    Rendering_backend_ptr backend;
    Scene_ptr scene;
    Window_frame_size size;

    Data(Context& ctx_a): ctx(ctx_a){}
};


Renderer::Renderer(Context & ctx, Resource_loader const& res_loader)
{
    ctx.make_current();
    ctx.initialize();
    data_=new Data(ctx);
    data_->size=ctx.size();
    Rendering_backend_factory_ptr factory=Rendering_backend_factory::get_factory(ctx);
    data_->backend=factory->new_rendering_backend(res_loader);
    data_->backend->init(ctx.size());
}

Renderer::~Renderer()
{
    delete data_;
}

Renderer & Renderer::scene(Scene_ptr const& scene_arg)
{
    if(scene_arg){
        data_->scene=scene_arg;
        data_->backend->on_scene_change();
    }
    return *this;
}

Scene_ptr const& Renderer::scene() const
{
    return data_->scene;
}

Scene_ptr Renderer::new_scene()
{
    Scene_implementation* impl_ptr=new Scene_implementation(data_->backend->resource_factory(),*data_->backend->animation_engine());
    Scene_impl_ptr sc(impl_ptr);
    Scene_ptr ret(sc);
    if(!scene()){
        scene(ret);
    }
    return ret;
}

void Renderer::render()
{
    Window_frame_size wfs=data_->ctx.size();
    if(!wfs){
        // the window is invisible?
        return;     //the call is ignored to avoid many errors.
    }

    if(data_->scene){
        data_->ctx.make_current();

        if(data_->size!=wfs){
            data_->size = wfs;
            data_->backend->resize(data_->size);
        }

        data_->backend->render(Scene_implementation::get_from(*data_->scene));
        data_->ctx.swap_buffers();
    }else{
        static bool warned=false;
        if(!warned){
            log_msg(Detail_level::CRITICAL,"Renderer::render is called when Renderer has no scene");
            warned=true;
        }
    }
}

}
