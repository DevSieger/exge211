 #include "../public_headers.h"
#include PUBLIC_HEADER(logging.h)
#include <iostream>
#include <fstream>
#include <ctime>
#include <memory>

namespace exge211 {

namespace logging {

class Logger{
public:

    void msg(Detail_level lvl, String const& str){
        if(accept(lvl)){
            output_(lvl,format_(lvl,str));
        }
    }
    void output(Output_functor new_output){
        output_=new_output;
    }

    void format(Formatting_functor new_format){
        format_=new_format;
    }

    Output_functor const& output()const{
        return output_;
    }
    Formatting_functor const& format()const{
        return format_;
    }

    void detail_level(Detail_level level){
        detail_=level;
    }
    Detail_level detail_level()const{
        return detail_;
    }
    static Logger& get();

private:
    Logger()=default;
    bool accept(Detail_level lvl){
        return (detail_level() != Detail_level::DISABLED) && lvl <= detail_level() && output_ && format_;
    }
    Output_functor output_;
    Formatting_functor format_;
    Detail_level detail_=Detail_level::CRITICAL;
};

Logger& Logger::get()
{
    static Logger logger;
    return logger;
}

class Date_time_formatter{
public:
    String operator()(Detail_level,String const & str){
        time_t t=time(nullptr);
        tm ttm=*localtime(&t);
        char time_string[32];
        strftime(time_string,32,format_.c_str(),&ttm);
        return String(time_string)+" "+str+"\n";
    }
    Date_time_formatter(String const & frmt):format_(frmt){
    }
protected:
    String format_;
};

bool init(String const& file_path, String const& date_format_prefix){
    Output_functor output;
    auto log_file=std::make_shared<std::ofstream>(file_path,std::ios_base::trunc|std::ios_base::out);
    bool success=false;
    if(log_file->is_open()){
        output=[log_file](Detail_level,String const & str){
            (*log_file)<<str;
            log_file->flush();
        };
        success=true;
    }else{
        std::cerr<<"can't open file "<<file_path<<std::endl;
    }
    init(output,date_format_prefix);
    return success;
}

void init(Output_functor output, const String & date_format_prefix)
{
    Formatting_functor formater;
    auto format_ptr=std::make_shared<Date_time_formatter>(date_format_prefix);
    formater=[format_ptr](Detail_level lvl,String const & str){
        return format_ptr->operator()(lvl,str);
    };
    init(output,formater);
}

void init(Output_functor output, Formatting_functor format)
{
    Logger & logger=Logger::get();
    logger.output(output);
    logger.format(format);
}

void EXGE211_CORE_EXPORT format(Formatting_functor new_format){
    Logger::get().format(new_format);
}
Formatting_functor const& EXGE211_CORE_EXPORT format(){
    return Logger::get().format();
}

void EXGE211_CORE_EXPORT output(Output_functor new_output){
    Logger::get().output(new_output);
}
Output_functor  const& EXGE211_CORE_EXPORT output(){
    return Logger::get().output();
}


void EXGE211_CORE_EXPORT detail_level(Detail_level level){
    Logger::get().detail_level(level);
}

Detail_level EXGE211_CORE_EXPORT detail_level(){
    return Logger::get().detail_level();
}

}

void log_msg(Detail_level lvl, String const& message)
{
    logging::Logger::get().msg(lvl,message);
}

}
