#pragma once

#include <vector>
#include <cstdint>
#include <glm/mat4x4.hpp>
#include <dk/concepts.h>
#include "../../public_headers.h"
#include PUBLIC_HEADER(vector_types.h)
#include PUBLIC_HEADER(common_types.h)
#include "../../private_handles.h"

namespace exge211{

class AABB;
class Actor_data;
class Camera;
class Point_light_data;
class Scenery_data;
struct Directional_light_data;

class Scene_access: dk::Non_copyable<Scene_access>{
public:
    using Actors_output=std::vector<const Actor_data*>;
    using Scenery_output=std::vector<const Scenery_data*>;
    using Point_light_output=std::vector<const Point_light_data*>;
    using Directional_light_output=std::vector<const Directional_light_data*>;
    using Index_output=std::vector<std::uint32_t>;
    using Transform_matrix=glm::mat4x4;

    /*
     * the transform is a transform from the world (scene) space to the clip space, that denotes the culling frustum.
     */
    void select(size_t count, Transform_matrix const** transforms,Actors_output& objects,Index_output** indices)const;
    void select(size_t count, Transform_matrix const** transforms,Scenery_output& objects,Index_output** indices)const;
    void select(Transform_matrix const& transform, bool cast_shadows, Point_light_output& output)const;
    void directional_lights(Directional_light_output& output)const;

    RGB const& ambient_light()const;
    AABB const& boundary()const;
    Camera const& camera()const;
    unsigned int debug_mode()const;
    float sky_light_scale()const;

    float get_shading_var_f(String const& name, float default_value) const;
    int get_shading_var_i(String const& name, int default_value) const;
    bool shading_var_changes();

    Skybox_handle skybox() const;

    /* Skybox? */

    Scene_time collect();
    Scene_time total_time() const;

private:
    friend class Scene_implementation;
    Scene_access()=default;
    ~Scene_access()=default;
};

}
