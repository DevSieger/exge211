#pragma once

#include "../../public_headers.h"
#include PUBLIC_HEADER(aabb.h)
#include PUBLIC_HEADER(actor.h)
#include PUBLIC_HEADER(camera.h)
#include PUBLIC_HEADER(common_types.h)
#include PUBLIC_HEADER(handles.h)
#include PUBLIC_HEADER(point_light.h)
#include PUBLIC_HEADER(vector_types.h)
#include PUBLIC_HEADER(scene.h)
#include PUBLIC_HEADER(scenery.h)

#include "../../private_handles.h"
#include "scene_access.h"
#include "actor_manager.h"
#include "scenery_manager.h"
#include "point_light_manager.h"
#include "../lighting/directional_light_impl.h"
#include "shading_variables.h"
#include "time_manager.h"
#include <list>


namespace exge211 {

class Resource_factory;
class Animation_engine;
class Directional_light_impl;

class Scene_implementation final: public std::enable_shared_from_this<Scene_implementation>, public Scene_access, public Scene {
public:
    Point_light_ptr add_point_light(bool shadow=false);
    void remove_point_light(Point_light_impl* pl);
    // assumed moving from oposite set, no checking
    void set_shadow(Point_light_impl& pl, bool shadow);

    Scene_implementation & camera(Camera_ptr cam);
    Camera_ptr camera()const;


    Skeleton_handle load_skeleton_type(Resource_request const & skeleton_type);
    bool relax(Skeleton_handle skeleton);

    Model_handle load_actor_item_proto(Resource_request const & model,Resource_request const & skin);
    bool relax(Model_handle model);

    Static_model_handle load_scenery_proto(Resource_request const & model,Resource_request const & skin);
    bool relax(Static_model_handle model);

    Actor_ptr new_actor(Skeleton_handle skeleton_type);
    void release(Actor_impl * actor);

    void set_actor_item(Actor_impl & actor, Actor::Slot_index slot, Model_handle item);
    void empty_actor_item( Actor_impl & actor,Actor::Slot_index slot);
    void set_standart_animation_engine(Actor_impl& actor);

    Scenery_ptr new_scenery(Static_model_handle proto, Rotation const& rotation, Position const& position,  const Direction& scale);
    void release(Scenery_impl* scenery);


    Animation_handle load_animation(Resource_request const& animation);
    bool relax(Animation_handle animation);

    void set_skybox(Resource_request const& name);

    Directional_light_ptr add_directional_light(RGB const& intensity, Direction const& direction);
    void remove_directional_light(Directional_light_impl* light);

    AABB const& boundary()const{
        return boundary_;
    }

    void boundary(AABB const& new_boundary){
        boundary_=new_boundary;
        actor_manager_->on_scene_boundary_change(boundary_);
        scenery_manager_->on_scene_boundary_change(boundary_);
        pl_manager_->on_scene_boundary_change(boundary_);
    }
    void ambient_light(RGB const& new_intencity){
        ambient_light_=new_intencity;
    }
    void sky_light_scale(float value){
        sky_light_scale_=value;
    }
    void debug_mode(unsigned int new_mode){
        debug_mode_=new_mode;
    }

    /* culling system 'callbacks' */

    void on_boundary_change(Actor_impl& actor){
        actor_manager_->on_boundary_change(actor);
    }
    void on_boundary_change(Point_light_impl& light_source){
        pl_manager_->on_boundary_change(light_source);
    }

    /* Scene_access part */
    void select(size_t count, Transform_matrix const** transforms, Actors_output& objects,Index_output** indices)const{
        actor_manager_->select(count,transforms,objects,indices);
    }
    void select(size_t count, Transform_matrix const** transforms, Scenery_output& objects,Index_output** indices)const{
        scenery_manager_->select(count,transforms,objects,indices);
    }
    void select(Transform_matrix const& transform, bool cast_shadows, Point_light_output& output)const{
        pl_manager_->select(transform,cast_shadows,output);
    }

    void directional_lights(Directional_light_output& output)const;

    RGB const& ambient_light()const{
        return ambient_light_;
    }

    Camera const& camera_ref()const{
        return *cam_;
    }
    unsigned int debug_mode()const{
        return debug_mode_;
    }
    float sky_light_scale()const{
        return sky_light_scale_;
    }

    /*end of Scene_access part */

    static constexpr Scene_implementation& get_from(Scene& scene){
        return static_cast<Scene_implementation&>(scene);
    }
    static constexpr Scene_implementation const& get_from(Scene const& scene){
        return static_cast<Scene_implementation const&>(scene);
    }

    void set_shading_var_f(String const& name, float value){
        shading_var_.set_var_f(name,value);
    }

    void set_shading_var_i(String const& name, int value){
        shading_var_.set_var_i(name,value);
    }

    float get_shading_var_f(String const& name, float defaul_value) const{
        return shading_var_.get_var_f(name,defaul_value);
    }

    int get_shading_var_i(String const& name, int defaul_value) const{
        return shading_var_.get_var_i(name,defaul_value);
    }

    bool shading_var_changes(){
        return shading_var_.changes();
    }

    Skybox_handle skybox() const{
        return skybox_.get();
    }

    void advance(Scene_time dt){
        time_manager_.advance(dt);
    }

    Scene_time total_time() const{
        return time_manager_.total();
    }

    Scene_time collect(){
        return time_manager_.collect();
    }
    void auto_advance(bool enabled){
        time_manager_.auto_advance(enabled);
    }

    bool auto_advance() const{
        return time_manager_.auto_advance();
    }


private:
    using Directional_lights=std::list<Directional_light_impl>;

    friend class Renderer;

    Shading_variables shading_var_;
    scene::Time_manager time_manager_;
    AABB boundary_;
    RGB ambient_light_=glm::vec3(0.04,0.4,0.04);
    float sky_light_scale_=1.0f;
    Scene::Visual_debug_type debug_mode_=0;
    Camera_ptr cam_=nullptr;
    Resource_factory & resource_factory_;
    std::shared_ptr<Actor_manager> actor_manager_;
    std::shared_ptr<Scenery_manager> scenery_manager_;
    std::shared_ptr<Point_light_manager> pl_manager_;
    Skybox_ptr skybox_;
    Directional_lights dir_lights_;
    Animation_engine& animation_engine_;

    Scene_implementation(Resource_factory& factory,Animation_engine& animation_engine);
};

}
