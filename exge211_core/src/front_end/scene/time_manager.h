#pragma once
#include "../../public_headers.h"
#include PUBLIC_HEADER(common_types.h)
#include <dk/timer.hpp>

namespace exge211 {

namespace scene {

class Time_manager{
public:

    Time_manager();

    /*!
     * \brief Adds the given time period to the accumulated time value.
     * \param dt    the given time period.
     * \warning     Use of this method turns off the auto-advancing.
     */
    void advance(Scene_time dt){
        if(auto_advance())
            auto_advance(false);

        accumulated_    += dt;
        total_          += dt;
    }

    /// Returns total eplapsed time.
    Scene_time total() const{
        Scene_time ret  = total_;
        if( auto_advance() )
            ret        += timer_.elapsed();
        return ret;
    }

    /// Returns the accumulated time value and reset it to zero.
    Scene_time collect(){
        if(auto_advance()){
            Scene_time ret  = timer_.restart();
            total_         += ret;
            return ret;
        }else{
            auto tmp        = accumulated_;
            accumulated_    = 0;
            return tmp;
        }
    }

    /*!
     * \brief   Turns on the auto-advance mode if 'enabled' argument is true, turns off otherwise.
     * \note    The auto-advancing is turned on by default.
     *
     * The auto-advancing mode means that the internal timer is used instead of values that are accumulated using #advance method.
     * On entering the auto-advance mode the internal timer is set to the accumulated time value.
     * On leaving the auto-advance mode the accumulated time value is set to the value stored in the internal timer.
     */
    void auto_advance(bool enabled);

    bool auto_advance() const{
        return auto_advance_;
    }

    // IMHO it is not worth it to make separate objects for different modes.

private:
    dk::Timer timer_;
    Scene_time accumulated_     = 0;
    Scene_time total_           = 0;
    bool auto_advance_          = true;
};

}

}
