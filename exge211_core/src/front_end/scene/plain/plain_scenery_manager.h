#pragma once
#include "../scenery_manager.h"
#include <list>
#include "../../scenery/scenery_impl.h"

namespace exge211{

namespace plain_manager{

class Managed_scenery;

class Scenery_manager: public exge211::Scenery_manager{
public:
    using Container=std::list<Managed_scenery>;
    Scenery_impl* new_scenery(Scene_impl_ptr scene_ptr, Static_model_instance_base & model, Rotation const& rotation, Position const& position, Direction const& scale) override;
    void destroy(Scenery_impl* ptr) override;
    void select(size_t count, Scene_access::Transform_matrix const** transforms, Scene_access::Scenery_output& objects, Scene_access::Index_output** indices)const override;
private:
    Container objects_;
};

class Managed_scenery final: public Scenery_impl{
public:
    using Scenery_impl::Scenery_impl;
    using Iterator=Scenery_manager::Container::iterator;

    Iterator where()const{
        return where_;
    }
    void where(Iterator new_dest){
        where_=new_dest;
    }

private:
    Iterator where_;
};

}

}
