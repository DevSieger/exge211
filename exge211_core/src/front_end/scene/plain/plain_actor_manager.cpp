#include "plain_actor_manager.h"
#include <utility>
#include <cassert>
#include "../../actor/skeleton_instance_base.h"
#include "../utility/frustum_culling.h"

namespace exge211{
namespace plain_manager{

namespace{

constexpr Managed_actor& get_actor(Actor_impl& actor){
    return static_cast<Managed_actor&>(actor);
}
constexpr Managed_actor const& get_actor(Actor_impl const& actor){
    return static_cast<Managed_actor const&>(actor);
}

}

Actor_impl* Actor_manager::new_actor(Scene_impl_ptr scene_ptr, Skeleton_instance_base& skeleton)
{
    Container::iterator it=actors_.emplace(actors_.end(),std::move(scene_ptr),skeleton);
    it->where(it);
    return &*it;
}

void Actor_manager::destroy(Actor_impl* actor_ptr)
{
    Managed_actor* actor=static_cast<Managed_actor*>(actor_ptr);
    actors_.erase(actor->where());
}

void Actor_manager::select(size_t count, const Scene_access::Transform_matrix** transforms, Scene_access::Actors_output& objects, Scene_access::Index_output** indices) const
{
    if(!count){
        return;
    }

    using Index=Scene_access::Index_output::value_type;
    const Index UNUSED_IX=-1;
    std::vector<Index> obj_indices(actors_.size(),UNUSED_IX);

    for(size_t ix=0;ix<count;++ix){

        assert(indices[ix]);
        assert(transforms[ix]);

        Frustum_tester tester(*transforms[ix]);
        Scene_access::Index_output& output=*indices[ix];
        auto obj_ix=obj_indices.begin();

        for(Managed_actor const& obj: actors_){
            Test_hint hint=tester.initial_hint();
            Test_result res=tester.test(obj.data().global_boundary,hint);
            if(res!=Test_result::OUTSIDE){
                if(*obj_ix==UNUSED_IX){
                    assert(objects.size() < UNUSED_IX);
                    *obj_ix=objects.size();
                    objects.push_back(&obj.data());
                }
                output.push_back(*obj_ix);
            }
            ++obj_ix;
        }
    }
}

}
}
