#include "plain_point_light_manager.h"
#include <utility>
#include "../utility/frustum_culling.h"

namespace exge211{
namespace plain_manager{

Point_light_impl* Point_light_manager::new_point_light(Scene_impl_ptr scene_ptr,bool shadow)
{
    Container& lights=container(shadow);
    Container::iterator it=lights.emplace(lights.end(),std::move(scene_ptr),shadow);
    it->where(it);
    return &*it;
}

void Point_light_manager::set_shadow_casting(Point_light_impl& point_light, bool shadow)
{
    Managed_point_light& pl=static_cast<Managed_point_light&>(point_light);
    Container& target=container(!shadow);
    Container& dest=container(shadow);
    dest.splice(dest.end(),target,pl.where());
}

void Point_light_manager::destroy(Point_light_impl* ptr)
{
    Container& lights=container(ptr->cast_shadows());
    Managed_point_light* pl=static_cast<Managed_point_light*>(ptr);
    lights.erase(pl->where());
}

void Point_light_manager::select(Scene_access::Transform_matrix const& transform, bool shadow, Scene_access::Point_light_output& output) const
{
    Container const& lights=container(shadow);
    Frustum_tester tester(transform);
    for(Managed_point_light const& pl: lights){
        Test_hint hint=tester.initial_hint();
        if(tester.test(pl.data(),hint)!=Test_result::OUTSIDE){
            output.emplace_back(&pl.data());
        }
    }
}

}
}
