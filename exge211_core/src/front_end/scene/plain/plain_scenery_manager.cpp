#include "plain_scenery_manager.h"
#include <cassert>
#include "../utility/frustum_culling.h"

namespace exge211 {
namespace plain_manager {

Scenery_impl*Scenery_manager::new_scenery(Scene_impl_ptr scene_ptr, Static_model_instance_base& model, Rotation const& rotation, Position const& position, Direction const& scale)
{
    Container::iterator it=objects_.emplace(objects_.end(),std::move(scene_ptr),model,rotation,position,scale);
    it->where(it);
    return &*it;
}

void Scenery_manager::destroy(Scenery_impl* ptr)
{
    Managed_scenery* obj=static_cast<Managed_scenery*>(ptr);
    objects_.erase(obj->where());
}

void Scenery_manager::select(size_t count, const Scene_access::Transform_matrix** transforms, Scene_access::Scenery_output& objects, Scene_access::Index_output** indices) const
{
    if(!count){
        return;
    }
    assert(indices);
    assert(*indices);

    std::vector<Managed_scenery const*> objects_tmp;
    using Index=Scene_access::Index_output::value_type;
    const Index UNUSED_IX=-1;
    std::vector<Index> obj_indices(objects_.size(),UNUSED_IX);

    for(size_t ix=0;ix<count;++ix){

        assert(indices[ix]);
        assert(transforms[ix]);

        Frustum_tester tester(*transforms[ix]);
        Scene_access::Index_output& output=*indices[ix];
        auto obj_ix=obj_indices.begin();

        for(Managed_scenery const& sc: objects_){
            Test_hint hint=tester.initial_hint();
            Test_result res=tester.test(sc.data().boundary(),hint);
            if(res!=Test_result::OUTSIDE){
                if(*obj_ix==UNUSED_IX){
                    assert(objects.size() < UNUSED_IX);
                    *obj_ix=objects.size();
                    objects.push_back(&sc.data());
                }
                output.push_back(*obj_ix);
            }
            ++obj_ix;
        }
    }
}

}
}
