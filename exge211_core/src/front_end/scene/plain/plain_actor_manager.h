#pragma once
#include "../../../public_headers.h"
#include PUBLIC_HEADER(aabb.h)

#include "../actor_manager.h"
#include <list>
#include "../../actor/actor_impl.h"

namespace exge211{


namespace plain_manager{

class Managed_actor;

class Actor_manager: public exge211::Actor_manager{
public:
    using Container=std::list<Managed_actor>;
    Actor_impl* new_actor(Scene_impl_ptr scene_ptr, Skeleton_instance_base& skeleton) override;
    void destroy(Actor_impl* actor_ptr) override;
    void select(size_t count, Scene_access::Transform_matrix const** transforms, Scene_access::Actors_output& objects,Scene_access::Index_output** indices)const override;

private:
    Container actors_;
};

class Managed_actor final: public Actor_impl{
public:
    using Actor_impl::Actor_impl;
    using Iterator=Actor_manager::Container::iterator;

    Iterator where()const{
        return where_;
    }
    void where(Iterator new_dest){
        where_=new_dest;
    }
private:
    Iterator where_;
};

}

}
