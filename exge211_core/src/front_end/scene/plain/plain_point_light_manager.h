#pragma once
#include "../point_light_manager.h"
#include <list>
#include "../../lighting/point_light_impl.h"

namespace exge211{
namespace plain_manager{

class Managed_point_light;

class Point_light_manager: public exge211::Point_light_manager{
public:
    using Container=std::list<Managed_point_light>;

    Point_light_impl* new_point_light(Scene_impl_ptr scene_ptr,bool shadow) override;
    void set_shadow_casting(Point_light_impl & point_light,bool shadow) override;
    void destroy(Point_light_impl* ptr) override;
    void select(Scene_access::Transform_matrix const& transform, bool shadow, Scene_access::Point_light_output& output) const override;

private:
    Container& container(bool shadow){
        return (shadow)?shadow_casting_pl_:simple_pl_;
    }
    Container const& container(bool shadow)const{
        return (shadow)?shadow_casting_pl_:simple_pl_;
    }

    Container simple_pl_;
    Container shadow_casting_pl_;
};

class Managed_point_light final: public Point_light_impl{
public:
    using Point_light_impl::Point_light_impl;
    using Iterator=Point_light_manager::Container::iterator;

    Iterator where()const{
        return where_;
    }
    void where(Iterator new_dest){
        where_=new_dest;
    }

private:
    Iterator where_;
};

}

}
