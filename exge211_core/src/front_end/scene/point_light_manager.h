#pragma once

#include <dk/concepts.h>
#include "scene_access.h"
#include "../../private_handles.h"

namespace exge211{

class Point_light_impl;

class Point_light_manager: dk::Non_copyable<Point_light_manager>{
public:
    virtual Point_light_impl* new_point_light(Scene_impl_ptr scene_ptr,bool cast_shadows) = 0;
    virtual void set_shadow_casting(Point_light_impl & point_light,bool shadow)=0;
    virtual void destroy(Point_light_impl* ptr)=0;
    virtual void select(Scene_access::Transform_matrix const& transform, bool cast_shadows, Scene_access::Point_light_output& output)const = 0;
    virtual void on_scene_boundary_change(AABB const& new_boundary){}
    virtual void on_boundary_change(Point_light_impl& source){}
    virtual ~Point_light_manager()=default;
};

}
