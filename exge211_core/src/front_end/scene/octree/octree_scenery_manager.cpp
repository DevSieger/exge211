#include "octree_scenery_manager.h"
#include <cassert>
#include "../utility/frustum_culling.h"

namespace exge211 {
namespace octree_manager {

Scenery_impl*Scenery_manager::new_scenery(Scene_impl_ptr scene_ptr, Static_model_instance_base& model, Rotation const& rotation, Position const& position, Direction const& scale)
{
    Managed_scenery* raw_ptr=new Managed_scenery(std::move(scene_ptr),model,rotation,position,scale);
    octree().insert(*raw_ptr);
    return raw_ptr;
}

void Scenery_manager::destroy(Scenery_impl* ptr)
{
    Managed_scenery* obj=static_cast<Managed_scenery*>(ptr);
    octree().erase(*obj);
    delete obj;
}

//i don't want to use same template function for sceneries and actors
void Scenery_manager::select(size_t count, const Scene_access::Transform_matrix** transforms, Scene_access::Scenery_output& objects, Scene_access::Index_output** indices) const
{
    if(!count){
        return;
    }
    assert(indices);
    assert(*indices);

    using Output_iterator_type=Smart_insert_iterator<Managed_scenery>;

    std::vector<Managed_scenery*> output_sceneries;
    const size_t index_offset=objects.size();

    for(size_t ix=0;ix<count;++ix){
        assert(indices[ix]);
        assert(transforms[ix]);

        Frustum_tester tester(*transforms[ix]);
        Output_iterator_type output_it(output_sceneries,*indices[ix],index_offset);
        octree().select<Frustum_tester&,Output_iterator_type&>(output_it,tester);
    }

    for(Managed_scenery* sc_ptr: output_sceneries){
        objects.push_back(&sc_ptr->data());
        sc_ptr->aux_ix(Managed_scenery::invalid_index());
    }
}

void Scenery_manager::on_scene_boundary_change(const AABB& new_boundary)
{
    const Position center=(new_boundary.max+new_boundary.min)*0.5f;
    octree().rebuild(center,new_boundary.size());
}

}
}
