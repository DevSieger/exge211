#pragma once
#include "../../../public_headers.h"
#include PUBLIC_HEADER(aabb.h)
#include PUBLIC_HEADER(vector_types.h)

#include <cmath>
#include <cassert>
#include <dk/indexed_bag.hpp>
#include "../utility/testing_common.h"

namespace exge211{
namespace octree_manager {

namespace octree_detail{

template<typename Object_type>
struct Default_object_traits{
    using Object=Object_type;
    using Index=typename Object::Index;

    static void* get_node(Object& obj){
        return obj.node();
    }
    static Index get_index(Object& obj){
        return obj.index();
    }
    static void set_node(Object& obj,void* node){
        obj.node(node);
    }
    static void set_index(Object& obj, Index ix){
        obj.index(ix);
    }
    static auto get_boundary(Object const& obj)->decltype(obj.boundary()){
        return obj.boundary();
    }
};

/*
 * returns index of an octant that contains an object with the given boundary, or 8 if there is more than one such an octant.
 * the octant indexing is same as the indexing of AABB corners.
 */
template<typename Boundary_type>
inline unsigned int get_octant(Boundary_type const& object_boundary, Position const& origin);

/*
 * returns true if an object with the given boundary is fully inside AABB(center-half_size,center+half_size), false otherwise.
 */
template<typename Boundary_type>
inline bool is_inside(Boundary_type const& object_boundary, Position const& center, Direction const& half_size);


/*
 * specialization for AABB, since that is the most common case.
 */
inline int less_mask(Position const& lhv, Position const& rhv){
        return (lhv.x<rhv.x) | ((lhv.y<rhv.y)<<1) | ((lhv.z<rhv.z)<<2);
}

inline int less_equal_and(Position const& lhv, Position const& rhv){
        return (lhv.x<=rhv.x) && (lhv.y<=rhv.y) && (lhv.z<=rhv.z);
}

template<>
inline unsigned int get_octant(AABB const& object_boundary, Position const& origin){
    int inf_ix=less_mask(origin,object_boundary.min);
    int sup_ix=less_mask(origin,object_boundary.max);
    return (inf_ix==sup_ix)?inf_ix:8;
}


template<>
inline bool is_inside(AABB const& object_boundary, Position const& center, Direction const& half_size){
    return less_equal_and(center-half_size,object_boundary.min) &&
            less_equal_and(object_boundary.max,center + half_size);
}

}

template<typename Object_type,typename Object_traits=octree_detail::Default_object_traits<Object_type>>
class Octree
{
public:
    using Item=Object_type;
    using Object_access=Object_traits;
    using Index=typename Object_access::Index;

    void insert(Item& item){
        set_node(item,nullptr);
        relocate_item(&item);
        ++total_items_;
    }
    void update(Item& item){
        relocate_item(&item);
    }
    void erase(Item& item){
        remove(&item);
        --total_items_;
    }

    /*
     * the functor signature is: (AABB const& border, Test_hint& hint) -> Test_result, hint may be changed
     */
    template<typename AABB_tester, typename Output_iterator, bool skip_item_check=false>
    void select(Output_iterator out_it,AABB_tester box_test)const{
        Test_hint hint=Test_hint();
        Test_result res=box_test(AABB(root_.center - root_.half_size,root_.center + root_.half_size),hint);
        if(res==Test_result::PARTIALLY_INSIDE){
            select_recursive<skip_item_check,AABB_tester,Output_iterator>(out_it,box_test,&root_,hint);
        }else if(res==Test_result::INSIDE){
            select_all<Output_iterator>(out_it,&root_);
        }
    }

    template<typename Functor>
    void for_each(Functor f)const{
        auto loop=[f](Node const* node){ for(Item* item:node->values){ f(item);}};
        for_each_node_recursive(loop,&root_);
    }

    Octree(Position const& center, Direction const& initial_size){
        root_.center=center;
        root_.half_size=0.5f*initial_size;
    }
    Octree(Octree&& other):
        root_(std::move(other.root_)),
        node_count_(other.node_count_),
        leaf_count_(other.leaf_count_),
        total_items_(other.total_items_),
        depth_threshold_(other.depth_threshold_)
    {
        other.node_count_=1;
        other.leaf_count_=1;
        other.total_items_=0;
    }

    void rebuild(Position const& center, Direction const& initial_size){
        root_.center=center;
        root_.half_size=0.5f*initial_size;

        std::vector<Item*> total_objects;
        auto gatherer=[&total_objects](Node* node)
        {
            for(Item* item: node->values){
                total_objects.push_back(item);
            }
            node->values.clear();
        };
        for_each_node_recursive(gatherer,&root_);
        if(root_.child_nodes){
            destroy_childs(&root_);
        }
        for(Item* item: total_objects){
            insert(*item);
        }
    }

    size_t depth_threshold_hint()const{
        return depth_threshold_;
    }

    Octree & depth_threshold_hint(size_t a_depth_threshold){
        depth_threshold_=a_depth_threshold;
        return *this;
    }


    void shrink(){
        shrink(&root_);
    }

    AABB boundary()const{
        return AABB(root_.center+root_.half_size,root_.center-root_.half_size);
    }

    ~Octree(){
        if(root_.child_nodes){
            destroy_childs(&root_);
        }
    }

private:

    struct Item_binder{
        void operator()(Item* item,Index ix){
            Object_access::set_index(*item,ix);
        }
    };

    struct Node{
        using Values=dk::Indexed_bag<Item*,Item_binder,Index>;

        // however, it looks like it needs a serious effort to turn this class into the quad-tree.
        static constexpr size_t dimensions_count(){return 3;}
        static constexpr size_t child_count(){return 1<<dimensions_count();}

        const Node* cbegin()const{
            return child_nodes;
        }
        const Node* cend()const{
            return (child_nodes)?child_nodes+child_count() : nullptr;
        }
        const Node* begin()const{
            return cbegin();
        }
        const Node* end()const{
            return cend();
        }
        Node* begin(){
            return const_cast<Node*>(cbegin());
        }
        Node* end(){
            return const_cast<Node*>(cend());
        }

        Node* child_nodes=nullptr;
        Node* parent=nullptr;
        Position center;
        Direction half_size;
        Values values; //3*sizeof(void*);

        Node()=default;

        Node(Node&& other){
            child_nodes=other.child_nodes;
            parent=other.parent;
            center=other.center;
            half_size=other.half_size;
            values=std::move(other.values);

            other.child_nodes=nullptr;
            other.values.clear();
            for(Node& child: *this){
                child.parent=this;
            }
            for(Item* item:values){
                Object_access::set_node(*item,this);
            }
        }
    };

    using Child_index=unsigned int;

    static Node* get_node(Item& item){
        return static_cast<Node*>(Object_access::get_node(item));
    }
    static Index get_index(Item& item){
        return Object_access::get_index(item);
    }
    static void set_node(Item& item,Node* node){
        Object_access::set_node(item,node);
    }
    static void set_index(Item& item, Index ix){
        Object_access::set_index(item,ix);
    }

    static constexpr typename Node::Values::Size split_factor(){ return 128;} //если колличество элементов в узле больше этого числа и нет дочерних узлов, то узел начнет деление
    static constexpr typename Node::Values::Size join_factor(){ return 64;}  //если колличество элементов в потомках узла и самом узле меньше этого числа, и потомки бездетны, то они будут объеденены обратно в родитель

    Node* find_position_to_leafs(Item* item,Node* node){
        assert(node);
        assert(item);
        while(node->child_nodes){
            //sphere boundary
            using octree_detail::get_octant;
            unsigned int octant_ix=get_octant(Object_access::get_boundary(*item),node->center);
            if(octant_ix<8){
                node=node->child_nodes+octant_ix;
            }else{
                break;
            }
        }
        return node;
    }

    bool is_inside_node(Item* item,Node* node){
        assert(node);
        assert(item);
        using octree_detail::is_inside;
        return is_inside(Object_access::get_boundary(*item),node->center,node->half_size);
    }

    size_t node_depth(const Node* node){
        return ilogb(root_.half_size.x)-ilogb(node->half_size.x);
    }

    /*
     * не использовать в for по Node::values принадлежащему узлу!
     * вызывает перестановки и удаление элементов в узле, которому принадлежал item.
     */
    void relocate_item(Item* item){
        assert(item);
        Node* node=get_node(*item);
        if(!node){
            node=&root_;
        }
        while(!is_inside_node(item,node)){
            if(node->parent){
                node=node->parent;
            }else{
                grow_twice();
            }
        }
        node=find_position_to_leafs(item,node);
        if(get_node(*item)!=node){
            remove(item);
            smart_insert(item,node);
        }else{
            optimize_node(node);
        }
    }

    bool optimize_node(Node* node){
        bool optimize=!node->child_nodes && node->values.size()>=split_factor() && node_depth(node) < depth_threshold_;
        if(optimize){
            split(node);
        }
        return optimize;
    }

    /*
     * не использовать в for по Node::values принадлежащему узлу!
     * вызывает перестановки и удаление элементов в узле, которому принадлежал item.
     * не проверяет принадленость к узлу
     */
    void smart_insert(Item* item, Node* node){
        assert(node);
        assert(item);
        hard_insert(node,item);
        optimize_node(node);
    }

    void hard_insert(Node* node, Item* item){
        assert(node);
        assert(item);
        node->values.insert(item); //само обновит item->data.hint.ix
        set_node(*item,node);
    }

    void remove(Item* item){
        assert(item);
        Node* cur_node=get_node(*item);
        if(cur_node){
            assert(cur_node->values[get_index(*item)]==item);
            cur_node->values.erase(get_index(*item));
            set_node(*item,nullptr);
        }
    }

    void create_childs(Node* node){
        assert(node);
        assert(!node->child_nodes);
        node->child_nodes=new Node[Node::child_count()];
        node_count_+=Node::child_count();
        leaf_count_+=Node::child_count()-1;  //родителя вычетаем
        configure_childs(node);
    }

    /* устанавливает границы у детей */
    void configure_childs(Node* parent_node){
        assert(parent_node);
        assert(parent_node->child_nodes);
        Direction half_size=parent_node->half_size*0.5f;
        Node* childs=parent_node->child_nodes;
        for(size_t i=0;i<Node::child_count();++i){
            childs[i].parent=parent_node;
            childs[i].center=parent_node->center;
            childs[i].half_size=half_size;
            for(size_t j=0;j<Node::dimensions_count();++j){
                if(i & 1<<j){
                    childs[i].center[j]+=half_size[j];
                }else{
                    childs[i].center[j]-=half_size[j];
                }
            }
        }
    }

    /*
     * не перемещает элементы из детей в родитель, чтобы ускорить использование в деструкторе
     */
    void destroy_childs(Node* parent_node){
        assert(parent_node);
        assert(parent_node->child_nodes);
        //на всякий случай
        for(Node& child:*parent_node){
            /*for(Item* item:child.values){
                item->data.hint.node=nullptr; //лучше не обнулять, черевато сегфолтами в деструкторе если контейнер с item'ами уже уничтожен.
            }*/
            if(child.child_nodes){
                destroy_childs(&child); //dfs, глубина рекурсии не больше высоты дерева, проблем возникнуть не должно
            }
        }
        delete [] parent_node->child_nodes;
        node_count_-=Node::child_count();
        leaf_count_-=Node::child_count()-1;
        parent_node->child_nodes=nullptr;

    }

    void split(Node* node){
        assert(node);
        assert(!node->child_nodes);
        create_childs(node);
        move_values_down(node,node);
    }

    void join_childs(Node* parent_node){
        assert(parent_node);
        assert(parent_node->child_nodes);
        for(Node& child:*parent_node){
            typename Node::Values tmp;
            tmp.swap(child.values);
            for(Item* item: tmp){
                set_node(*item,nullptr);
                hard_insert(item,parent_node);
            }
        }
        destroy_childs(parent_node);
    }

    /* возвращает true, если у узла нет потомков */
    bool shrink(Node* node){
        assert(node);
        if(node->child_nodes){
            auto total_size=node->values.size();
            bool joinable=true;
            for(Node& child: *node){
                joinable = shrink(&child) && joinable; //dfs, глубина рекурсии не больше высоты дерева, проблем возникнуть не должно
                total_size+=child.values.size();
            }
            if(joinable && total_size<join_factor()){
                join_childs(node);
            }
        }
        return !node->child_nodes;
    }


    template<bool no_item_check=false, typename AABB_tester, typename Output_iterator>
    void select_recursive(Output_iterator out_it,AABB_tester box_tester,const Node* node,Test_hint hint)const{
        //используем dfs
        for(Node const& child:*node){
            Test_hint new_hint=hint;
            Test_result res=box_tester(AABB(child.center - child.half_size,child.center + child.half_size),new_hint);
            if(res!=Test_result::OUTSIDE){  //TODO mb use switch?
                if(res==Test_result::PARTIALLY_INSIDE){
                    select_recursive<no_item_check,AABB_tester,Output_iterator>(out_it,box_tester,&child,new_hint);
                }else{
                    select_all<Output_iterator>(out_it,&child);
                }
            }
        }
        for(Item* item:node->values){
            if(no_item_check){
                *out_it++ = item;
            }else{
                Test_hint new_hint=hint;
                Test_result res=box_tester(Object_access::get_boundary(*item),new_hint);
                if(res!=Test_result::OUTSIDE){
                    *out_it++ = item;
                }
            }
        }
    }

    template<typename Output_iterator>
    void select_all(Output_iterator out_it,const Node* node)const{
        for(Node const& child:*node){
            select_all<Output_iterator>(out_it,&child);
        }
        for(Item* item:node->values){
            *out_it++ = item;
        }
    }

    //Functor: void (Node*)
    template<typename Functor>
    void for_each_node_recursive(Functor f,Node* node){
        for(Node& child:*node){
            for_each_node_recursive(f,&child);
        }
        f(node);
    }
    template<typename Functor>
    void for_each_node_recursive(Functor f,Node const* node)const{
        for(Node const& child:*node){
            for_each_node_recursive(f,&child);
        }
        f(node);
    }

    void move_values_down(Node* whose, Node* starting_node)
    {
        typename Node::Values tmp;
        tmp.swap(whose->values);
        for(Item* item: tmp){
            set_node(*item,nullptr);
            Node* dest=find_position_to_leafs(item,starting_node);
            smart_insert(item,dest);
        }
    }
    void grow_twice(){
        root_.half_size*=2;
        if(root_.child_nodes){
            configure_childs(&root_);
            for(size_t ix=0;ix<Node::child_count();++ix){
                Node* child=root_.child_nodes+ix;
                Node* old_childs=child->child_nodes;
                if(old_childs){
                    child->child_nodes=nullptr;
                    create_childs(child);
                    const Child_index dest_ix=(Node::child_count()-1) & ~ix; //ближний к центру октет, просто инвертируем
                    Node& dest_node=child->child_nodes[dest_ix];
                    dest_node.child_nodes=old_childs;
                    for(Node& oc:dest_node){
                        oc.parent=&dest_node;
                    }
                    assert(!dest_node.values.size());

                    dest_node.values.swap(child->values);
                    for(Item*item:dest_node.values){
                        set_node(*item,&dest_node);
                    }
                }
            }
            move_values_down(&root_,&root_);
        }
    }

    //fields

    Node root_;
    size_t node_count_=1;
    size_t leaf_count_=1;
    size_t total_items_=0;
    size_t depth_threshold_=8;  //может быть нарушен при росте дерева вверх, а не вниз

};

}
}
