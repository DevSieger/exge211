#pragma once
#include "../../../public_headers.h"
#include "../scene_access.h"
#include <iterator>
#include <vector>
#include <type_traits>

namespace exge211{
namespace octree_manager {

class Managed_object_base{
public:
    using Index=size_t;
    using Output_index=Scene_access::Index_output::value_type;

    Index index() const{
        return ix_;
    }
    void* node() const{
        return node_;
    }

    void index(Index ix){
        ix_=ix;
    }
    void node(void* new_node){
        node_=new_node;
    }

    static constexpr Output_index invalid_index(){ return Output_index(-1);}

    //actually, i do not like this solution, but imho this is the shortest and the best way to do it.
    Output_index aux_ix() const{
        return aux_ix_;
    }
    void aux_ix(Output_index value){
        aux_ix_=value;
    }
protected:
    ~Managed_object_base() = default;
private:
    void* node_;
    Index ix_;
    Output_index aux_ix_=invalid_index();
};

template<typename Object_type>
class Smart_insert_iterator:
        std::iterator<std::output_iterator_tag,void,void,void,void>
{
public:
    using Object=Object_type;
    using Objects=std::vector<Object*>;
    using Indices=Scene_access::Index_output;
    using Index=Indices::value_type;

    static_assert(std::is_base_of<Managed_object_base,Object>::value,"Managed_object_base is not the base of Object_type");

    Smart_insert_iterator(Objects& object_output,Indices& index_output,size_t offset):
        total_(object_output), current_selection_(index_output),offset_(offset)
    {}

    Smart_insert_iterator& operator*()          {return *this;}
    Smart_insert_iterator& operator++()         {return *this;}
    Smart_insert_iterator& operator++( int )    {return *this;}

    Smart_insert_iterator& operator=(Object* ptr ){
        Index ix=ptr->aux_ix();
        if(ix==Object::invalid_index()){
            ix=total_.size()+offset_;
            ptr->aux_ix(ix);
            total_.push_back(ptr);
        }
        current_selection_.push_back(ix);
        return *this;
    }
private:
    Objects& total_;
    Indices& current_selection_;
    size_t offset_;
};

}
}
