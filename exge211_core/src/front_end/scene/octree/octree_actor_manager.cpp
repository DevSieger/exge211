#include "octree_actor_manager.h"
#include <utility>
#include <cassert>
#include "../../actor/skeleton_instance_base.h"
#include "../utility/frustum_culling.h"

namespace exge211{
namespace octree_manager{

Actor_impl* Actor_manager::new_actor(Scene_impl_ptr scene_ptr, Skeleton_instance_base& skeleton)
{
    Managed_actor* raw_ptr=new Managed_actor(std::move(scene_ptr),skeleton);
    octree().insert(*raw_ptr);
    return raw_ptr;
}

void Actor_manager::destroy(Actor_impl* actor_ptr)
{
    Managed_actor* obj=static_cast<Managed_actor*>(actor_ptr);
    octree().erase(*obj);
    delete obj;
}

void Actor_manager::select(size_t count, const Scene_access::Transform_matrix** transforms, Scene_access::Actors_output& objects, Scene_access::Index_output** indices) const
{
    if(!count){
        return;
    }
    assert(indices);
    assert(*indices);
    using Output_iterator_type=Smart_insert_iterator<Managed_actor>;

    std::vector<Managed_actor*> selected_objects;
    const size_t index_offset=objects.size();

    for(size_t ix=0;ix<count;++ix){

        assert(indices[ix]);
        assert(transforms[ix]);

        Frustum_tester tester(*transforms[ix]);
        Output_iterator_type output_it(selected_objects,*indices[ix],index_offset);
        octree().select<Frustum_tester&,Output_iterator_type&>(output_it,tester);
    }

    for(Managed_actor* obj: selected_objects){
        objects.push_back(&obj->data());
        obj->aux_ix(Managed_actor::invalid_index());
    }
}

void Actor_manager::on_boundary_change(Actor_impl& actor)
{
    Managed_actor& obj=static_cast<Managed_actor&>(actor);
    octree().update(obj);
}

void Actor_manager::on_scene_boundary_change(const AABB& new_boundary)
{
    const Position center=(new_boundary.max+new_boundary.min)*0.5f;
    octree().rebuild(center,new_boundary.size());
}

}
}
