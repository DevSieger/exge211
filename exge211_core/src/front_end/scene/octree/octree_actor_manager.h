#pragma once
#include "../actor_manager.h"
#include "../../actor/actor_impl.h"
#include "octree.h"
#include "manager_common.h"

namespace exge211{
namespace octree_manager{

class Managed_actor final: public Actor_impl, public Managed_object_base{
public:
    using Actor_impl::Actor_impl;
    using Boundary=AABB;

    Boundary const& boundary()const{
        return data().global_boundary;
    }
};

class Actor_manager: public exge211::Actor_manager{
public:
    Actor_impl* new_actor(Scene_impl_ptr scene_ptr, Skeleton_instance_base& skeleton) override;
    void destroy(Actor_impl* actor_ptr) override;
    void select(size_t count, Scene_access::Transform_matrix const** transforms, Scene_access::Actors_output& objects,Scene_access::Index_output** indices)const override;
    void on_boundary_change(Actor_impl& actor) override;
    void on_scene_boundary_change(AABB const& new_boundary) override;

private:
    using Octree=exge211::octree_manager::Octree<Managed_actor>;
    Octree& octree(){
        return octree_;
    }
    Octree const& octree()const{
        return octree_;
    }
    Octree octree_{Position(0,0,0),Direction(8,8,8)};
};

}
}
