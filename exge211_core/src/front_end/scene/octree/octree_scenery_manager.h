#pragma once
#include "../scenery_manager.h"
#include "../../scenery/scenery_impl.h"
#include "octree.h"
#include "manager_common.h"

namespace exge211{
namespace octree_manager{

class Managed_scenery final: public Scenery_impl,public Managed_object_base{
public:
    using Scenery_impl::Scenery_impl;
    using Boundary=AABB;

    Boundary const& boundary()const{
        return data().boundary();
    }
};

class Scenery_manager: public exge211::Scenery_manager{
public:
    Scenery_impl* new_scenery(Scene_impl_ptr scene_ptr, Static_model_instance_base & model, Rotation const& rotation, Position const& position, Direction const& scale) override;
    void destroy(Scenery_impl* ptr) override;
    void select(size_t count, Scene_access::Transform_matrix const** transforms, Scene_access::Scenery_output& objects, Scene_access::Index_output** indices)const override;
    void on_scene_boundary_change(AABB const& new_boundary) override;
private:
    using Octree=exge211::octree_manager::Octree<Managed_scenery>;
    Octree& octree(){
        return octree_;
    }
    Octree const& octree()const{
        return octree_;
    }
    Octree octree_{Position(0,0,0),Direction(8,8,8)};
};

}
}
