#pragma once
#include "../point_light_manager.h"
#include "manager_common.h"
#include "octree.h"
#include "../../lighting/point_light_impl.h"

namespace exge211{
namespace octree_manager{

namespace octree_detail {

template<>
inline unsigned int get_octant(Point_light_data const& object_boundary, Position const& origin){
    int inf_ix=octree_detail::less_mask(origin,object_boundary.position-Position(object_boundary.range));
    int sup_ix=octree_detail::less_mask(origin,object_boundary.position+Position(object_boundary.range));
    return (inf_ix==sup_ix)?inf_ix:8;
}

/*
 * returns true if object with given boundary is fully inside AABB(center-half_size,center+half_size), false otherwise
 */
template<>
inline bool is_inside(Point_light_data const& object_boundary, Position const& center, Direction const& half_size){
    return octree_detail::less_equal_and(center-half_size,object_boundary.position-Position(object_boundary.range)) &&
            octree_detail::less_equal_and(object_boundary.position+Position(object_boundary.range),center + half_size);
}

}

class Managed_point_light final: public Point_light_impl, public Managed_object_base{
public:
    using Point_light_impl::Point_light_impl;
    using Boundary=Point_light_data;

    Boundary const& boundary()const{
        return data();
    }
};

class Point_light_manager: public exge211::Point_light_manager{
public:
    Point_light_impl* new_point_light(Scene_impl_ptr scene_ptr,bool shadow) override;
    void set_shadow_casting(Point_light_impl & point_light,bool shadow) override;
    void destroy(Point_light_impl* ptr) override;
    void select(Scene_access::Transform_matrix const& transform, bool shadow, Scene_access::Point_light_output& output) const override;
    void on_boundary_change(Point_light_impl& object) override;
    void on_scene_boundary_change(AABB const& new_boundary) override;

private:
    using Octree=exge211::octree_manager::Octree<Managed_point_light>;
    Octree& octree(bool shadow){
        return (shadow)?shadow_casting_pl_:simple_pl_;
    }
    Octree const& octree(bool shadow)const{
        return (shadow)?shadow_casting_pl_:simple_pl_;
    }

    Octree simple_pl_{Position(0,0,0),Direction(8,8,8)};
    Octree shadow_casting_pl_{Position(0,0,0),Direction(8,8,8)};
};

}
}
