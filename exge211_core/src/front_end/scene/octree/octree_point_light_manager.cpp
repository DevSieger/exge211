#include "octree_point_light_manager.h"
#include <utility>
#include "../utility/frustum_culling.h"

namespace exge211{
namespace octree_manager{

Point_light_impl* Point_light_manager::new_point_light(Scene_impl_ptr scene_ptr,bool shadow)
{
    Managed_point_light* raw_ptr=new Managed_point_light(std::move(scene_ptr),shadow);
    octree(shadow).insert(*raw_ptr);
    return raw_ptr;
}

void Point_light_manager::set_shadow_casting(Point_light_impl& point_light, bool shadow)
{
    Managed_point_light& pl=static_cast<Managed_point_light&>(point_light);
    Octree& target=octree(!shadow);
    Octree& dest=octree(shadow);
    target.erase(pl);
    dest.insert(pl);
}

void Point_light_manager::destroy(Point_light_impl* ptr)
{
    Managed_point_light* pl=static_cast<Managed_point_light*>(ptr);
    octree(pl->cast_shadows()).erase(*pl);
    delete pl;
}

namespace{

class PL_data_insert_iterator:
        std::iterator<std::output_iterator_tag,void,void,void,void>
{
public:
    using Objects=Scene_access::Point_light_output;

    PL_data_insert_iterator(Objects& object_output):
        output_(object_output)
    {}

    PL_data_insert_iterator& operator*()          {return *this;}
    PL_data_insert_iterator& operator++()         {return *this;}
    PL_data_insert_iterator& operator++( int )    {return *this;}

    PL_data_insert_iterator& operator=(Managed_point_light* ptr ){
        output_.emplace_back(&ptr->data());
        return *this;
    }
private:
    Objects& output_;
};

}

void Point_light_manager::select(Scene_access::Transform_matrix const& transform, bool shadow, Scene_access::Point_light_output& output) const
{
    Frustum_tester tester(transform);
    PL_data_insert_iterator output_it(output);
    octree(shadow).select<Frustum_tester&,PL_data_insert_iterator&>(output_it,tester);
}


void Point_light_manager::on_boundary_change(Point_light_impl& object)
{
    Managed_point_light& obj=static_cast<Managed_point_light&>(object);
    octree(obj.cast_shadows()).update(obj);
}

void Point_light_manager::on_scene_boundary_change(AABB const& new_boundary)
{
    const Position center=(new_boundary.max+new_boundary.min)*0.5f;
    const Direction size=new_boundary.size();
    octree(true).rebuild(center,size);
    octree(false).rebuild(center,size);
}

}
}
