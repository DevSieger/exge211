#pragma once
#include "../../public_headers.h"
#include PUBLIC_HEADER(vector_types.h)

#include <dk/concepts.h>
#include "scene_access.h"
#include "../../private_handles.h"

namespace exge211{

class Scenery_impl;
class Static_model_instance_base;

class Scenery_manager: dk::Non_copyable<Scenery_manager>{
public:
    virtual Scenery_impl* new_scenery(Scene_impl_ptr scene_ptr,Static_model_instance_base & model, Rotation const& rotation, Position const& position, Direction const& scale) = 0;
    virtual void destroy(Scenery_impl* ptr)=0;
    virtual void select(size_t count, Scene_access::Transform_matrix const** transforms, Scene_access::Scenery_output& objects, Scene_access::Index_output** indices)const=0;
    virtual void on_scene_boundary_change(AABB const& new_boundary){}
    virtual ~Scenery_manager()=default;
};

}
