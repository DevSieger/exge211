#pragma once
#include "../../public_headers.h"
#include PUBLIC_HEADER(common_types.h)
#include <unordered_map>

namespace exge211 {

/*!
 * \brief The class that is used for storing / transfer the values for the uniform variables of shaders.
 *
 * There is two different sets: variables with integral values and variables with floating point values.
 */
class Shading_variables{
public:

    // there is an unfortunate coincidence of arguments that makes impossible to use an usual overloading of the methods :(.
    // Method names are different to make argument conversions possible.
    void set_var_f(String const& name, float value);
    void set_var_i(String const& name, int value);

    /// Checks if there are changes of variables since a previous call of this method.
    inline bool changes();

    float get_var_f(String const& name, float default_value) const;
    int get_var_i(String const& name, int default_value) const;

private:
    template< typename Value>
    using Container = std::unordered_map<String,Value>;

    Container<int> var_i_;
    Container<float> var_f_;
    bool changes_   = false;
};

bool Shading_variables::changes(){
    bool ret = changes_;
    changes_ = false;
    return ret;
}

}
