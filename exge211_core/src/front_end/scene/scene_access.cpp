#include "scene_access.h"
#include "scene_implementation.h"
namespace exge211{

namespace{

constexpr Scene_implementation const& get_impl(Scene_access const& iface){
    return static_cast<Scene_implementation const&>(iface);
}

constexpr Scene_implementation& get_impl(Scene_access& iface){
    return static_cast<Scene_implementation&>(iface);
}

}

void Scene_access::select(size_t count, const Scene_access::Transform_matrix** transforms, Scene_access::Actors_output& objects, Scene_access::Index_output** indices) const
{
    get_impl(*this).select(count,transforms,objects,indices);
}
void Scene_access::select(size_t count, const Scene_access::Transform_matrix** transforms, Scene_access::Scenery_output& objects, Scene_access::Index_output** indices) const
{
    get_impl(*this).select(count,transforms,objects,indices);
}
void Scene_access::select(Transform_matrix const& transform, bool cast_shadows, Point_light_output& output)const{
    get_impl(*this).select(transform,cast_shadows,output);
}
void Scene_access::directional_lights(Directional_light_output& output)const{
    get_impl(*this).directional_lights(output);
}

RGB const& Scene_access::ambient_light()const{
    return get_impl(*this).ambient_light();
}
AABB const& Scene_access::boundary()const{
    return get_impl(*this).boundary();
}
Camera const& Scene_access::camera()const{
    return get_impl(*this).camera_ref();
}
unsigned int Scene_access::debug_mode()const{
    return get_impl(*this).debug_mode();
}
float Scene_access::sky_light_scale()const{
    return get_impl(*this).sky_light_scale();
}

float Scene_access::get_shading_var_f( String const& name, float default_value) const
{
    return get_impl(*this).get_shading_var_f(name,default_value);
}

int Scene_access::get_shading_var_i(const String& name, int default_value) const
{
    return get_impl(*this).get_shading_var_i(name,default_value);
}

bool Scene_access::shading_var_changes()
{
    return get_impl(*this).shading_var_changes();
}

Skybox_handle Scene_access::skybox() const
{
    return get_impl(*this).skybox();
}

Scene_time Scene_access::collect()
{
    return get_impl(*this).collect();
}

Scene_time Scene_access::total_time() const
{
    return get_impl(*this).total_time();
}

}
