#include "shading_variables.h"

namespace exge211 {

namespace {

// Container should be a std::map-like type.
template<typename Value, template< typename Key, typename Value_ttp, typename ... Params> class Container, typename ... Params >
bool set_variable(String const& name, Value value, Container<String, Value, Params...>& container){
    auto it = container.find(name);

    if( it != container.end()){
        if( it->second != value ){
            it->second = value;
            return true;
        }
    }else{
        container.insert({name,value});
        return true;
    }
    return false;
}

template<typename Value, template< typename Key, typename Value_ttp, typename ... Params> class Container, typename ... Params>
Value get_variable(String const& name, Value default_value, Container<String, Value, Params...> const& container){
    auto it = container.find(name);
    if( it != container.end()){
        return it->second;
    }
    return default_value;
}

}

void Shading_variables::set_var_f(String const& name, float value)
{
    if( set_variable(name,value,var_f_) ){
        changes_ = true;
    }
}

void Shading_variables::set_var_i( String const& name, int value)
{
    if( set_variable(name,value,var_i_) ){
        changes_ = true;
    }
}

float Shading_variables::get_var_f( String const& name, float default_value) const
{
    return get_variable(name,default_value,var_f_);
}

int Shading_variables::get_var_i( String const& name, int default_value) const
{
    return get_variable(name,default_value,var_i_);
}

}
