#pragma once
#include <memory>

namespace exge211 {

// an opaque class.
class Skybox{
protected:
    ~Skybox() = default;
};

}
