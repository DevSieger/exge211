#include "time_manager.h"

namespace exge211 {
namespace scene {

Time_manager::Time_manager(){
    if(auto_advance())
        timer_.start();
}

void Time_manager::auto_advance(bool enabled){
    if( enabled == auto_advance_)
        return;

    auto_advance_ = enabled;

    if(enabled){
        total_  -= accumulated_;
        timer_.start(accumulated_);
    }else{
        Scene_time elapsed  = timer_.restart();
        accumulated_        = elapsed;
        total_             += elapsed;
    }
}



}
}
