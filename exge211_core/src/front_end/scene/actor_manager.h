#pragma once

#include "../../public_headers.h"
#include PUBLIC_HEADER(vector_types.h)

#include <dk/concepts.h>
#include "scene_access.h"
#include "../../private_handles.h"

namespace exge211{

class Actor_impl;
class Skeleton_instance_base;

class Actor_manager: dk::Non_copyable<Actor_manager>{
public:
    virtual Actor_impl* new_actor(Scene_impl_ptr scene_ptr, Skeleton_instance_base& skeleton) = 0;
    virtual void destroy(Actor_impl* actor_ptr)=0;
    virtual void select(size_t count, Scene_access::Transform_matrix const** transforms,Scene_access::Actors_output& objects,Scene_access::Index_output** indices)const=0;

    virtual void on_boundary_change(Actor_impl& actor){}
    virtual void on_scene_boundary_change(AABB const& new_boundary){}

    virtual ~Actor_manager()=default;
};

}
