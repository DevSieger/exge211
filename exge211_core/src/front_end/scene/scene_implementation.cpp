#include "../../public_headers.h"
#include PUBLIC_HEADER(logging.h)

#include "scene_implementation.h"
#include "../actor/actor_data.h"
#include "../../back_end/resource_factory.h"
#include "../../back_end/animation_system.h"
#include "../../back_end/animation_engine.h"
#include "plain/plain_actor_manager.h"
#include "plain/plain_scenery_manager.h"
#include "octree/octree_actor_manager.h"
#include "octree/octree_scenery_manager.h"
#include "octree/octree_point_light_manager.h"
#include "plain/plain_point_light_manager.h"

namespace exge211 {

Scene_implementation::Scene_implementation(Resource_factory & factory, Animation_engine& animation_engine):
    resource_factory_(factory),animation_engine_(animation_engine)
{
    actor_manager_=std::make_shared<octree_manager::Actor_manager>();
    scenery_manager_=std::make_shared<octree_manager::Scenery_manager>();
    pl_manager_=std::make_shared<octree_manager::Point_light_manager>();
}

Point_light_ptr Scene_implementation::add_point_light(bool shadow){
    Point_light_impl* raw_ptr=pl_manager_->new_point_light(shared_from_this(),shadow);
    return Point_light_ptr(raw_ptr,[](Point_light_impl* pl){ pl->scene().remove_point_light(pl);});
}

void Scene_implementation::remove_point_light(Point_light_impl* pl){
    pl_manager_->destroy(pl);
}

void Scene_implementation::set_shadow(Point_light_impl& pl, bool shadow){
    pl_manager_->set_shadow_casting(pl,shadow);
}

Scene_implementation &Scene_implementation::camera(Camera_ptr cam){
    cam_=cam;
    return *this;
}

Camera_ptr Scene_implementation::camera() const{
    return cam_;
}

Skeleton_handle Scene_implementation::load_skeleton_type(const Resource_request & skeleton_type)
{
    return resource_factory_.request_skeleton_type(skeleton_type);
}

bool Scene_implementation::relax(Skeleton_handle skeleton)
{
    return resource_factory_.relax(skeleton);
}

Model_handle Scene_implementation::load_actor_item_proto(const Resource_request & model, const Resource_request & skin)
{
    return resource_factory_.request_amodel(model,skin);
}

bool Scene_implementation::relax(Model_handle model)
{
    return resource_factory_.relax(model);
}

Static_model_handle Scene_implementation::load_scenery_proto(const Resource_request & model, const Resource_request & skin)
{
    return resource_factory_.request_smodel(model,skin);
}

bool Scene_implementation::relax(Static_model_handle model)
{
    return resource_factory_.relax(model);
}

Actor_ptr Scene_implementation::new_actor(Skeleton_handle skeleton_type)
{
    Actor_impl* raw_ptr=actor_manager_->new_actor(shared_from_this(),*resource_factory_.construct_skeleton(skeleton_type));
    //set up default animation engine
    set_standart_animation_engine(*raw_ptr);
    return Actor_ptr(raw_ptr,[](Actor_impl* actor_ptr){actor_ptr->scene().release(actor_ptr);});
}

void Scene_implementation::release(Actor_impl* actor)
{
    auto & models=actor->data().models;
    for(Actor::Slot_index ix=0;ix<models.size();++ix){
        if(models[ix]){
            resource_factory_.release(models[ix]);
        }
    }
    auto  sk=&actor->data().state.skeleton();
    actor_manager_->destroy(actor);
    /* skeleton clearing */
    resource_factory_.release(sk);
}

void Scene_implementation::set_actor_item(Actor_impl & actor, Actor::Slot_index slot, Model_handle item)
{
    if( actor.data().models.size() <= slot){
        actor.data().models.resize(slot+1,nullptr);
    }
    if(actor.data().models[slot]){
        resource_factory_.release(actor.data().models[slot]);
    }
    actor.data().models[slot]=resource_factory_.construct_model(item);
}

void Scene_implementation::empty_actor_item(Actor_impl & actor, Actor::Slot_index slot)
{
    if( actor.data().models.size()<slot ){
        if( actor.data().models[slot] ){
            resource_factory_.release(actor.data().models[slot]);
            actor.data().models[slot]=nullptr;
        }else{
            log_msg(Detail_level::NON_CRITICAL,"slot %1% empty in actor",slot); //TODO: remove this logging
        }
    }else{
        log_msg(Detail_level::NON_CRITICAL,"slot %1% not used in actor",slot);
    }
}

void Scene_implementation::set_standart_animation_engine(Actor_impl& actor)
{
    actor.data().state.engine(&animation_engine_);
}

Scenery_ptr Scene_implementation::new_scenery(Static_model_handle proto,  Rotation const& rotation,  Position const& position,Direction const& scale)
{
    Static_model_instance_base* model=resource_factory_.construct_model(proto,rotation,position,scale);
    Scenery_impl* raw_ptr=scenery_manager_->new_scenery(shared_from_this(),*model,rotation,position,scale);
    Scenery_ptr ret(raw_ptr,[](Scenery_impl* s){s->scene().release(s);});
    return ret;
}

void Scene_implementation::release(Scenery_impl* scenery)
{
    auto mptr = &scenery->data().model();
    scenery_manager_->destroy(scenery);
    resource_factory_.release(mptr);
}

Directional_light_ptr Scene_implementation::add_directional_light(RGB const& intensity, Direction const& direction)
{
    dir_lights_.emplace_back(shared_from_this());
    Directional_light_impl& ret=dir_lights_.back();
    ret.data().direction=direction;
    ret.data().intensity=intensity;
    return Directional_light_ptr(&ret,[](Directional_light_impl* dl){ dl->scene().remove_directional_light(dl);});
}

void Scene_implementation::remove_directional_light(Directional_light_impl* light)
{
    //It is assumed small number of directional light sources. Large number will produce serios truble with performance anyway.
    for(auto it=dir_lights_.begin();it!=dir_lights_.end();++it){
        if(&*it==light){
            dir_lights_.erase(it);
            return;
        }
    }
}

Animation_handle Scene_implementation::load_animation(const Resource_request & animation)
{
    return animation_engine_.load(animation);
}

bool Scene_implementation::relax(Animation_handle animation)
{
    return animation_engine_.relax(animation);
}

void Scene_implementation::set_skybox( Resource_request const& name)
{
    skybox_ = resource_factory_.get_skybox(name);
}

void Scene_implementation::directional_lights(Scene_access::Directional_light_output& output) const{
    for(Directional_light_impl const& dli: dir_lights_ ){
        output.emplace_back(&dli.data());
    }
}

}
