#include "../../public_headers.h"
#include PUBLIC_HEADER(scene.h)
#include "scene_implementation.h"

namespace exge211 {

namespace{

constexpr Scene_implementation& get_impl(Scene& sc){
    return Scene_implementation::get_from(sc);
}
constexpr Scene_implementation const& get_impl(Scene const& sc){
    return Scene_implementation::get_from(sc);
}

}

Point_light_ptr Scene::add_point_light(bool shadow)
{
    return get_impl(*this).add_point_light(shadow);
}

Scene &Scene::camera(Camera_ptr cam)
{
    get_impl(*this).camera(cam);
    return *this;
}

Camera_ptr Scene::camera() const
{
    return get_impl(*this).camera();
}

Skeleton_handle Scene::load_skeleton_type(const Resource_request & skeleton_type)
{
    return get_impl(*this).load_skeleton_type(skeleton_type);
}

bool Scene::relax(Skeleton_handle skeleton)
{
    return get_impl(*this).relax(skeleton);
}

Model_handle Scene::load_actor_item_proto(const Resource_request & model, const Resource_request & skin)
{
    return get_impl(*this).load_actor_item_proto(model,skin);
}

bool Scene::relax(Model_handle model)
{
    return get_impl(*this).relax(model);
}

Actor_ptr Scene::new_actor(Skeleton_handle skeleton_type)
{
    return get_impl(*this).new_actor(skeleton_type);
}

Static_model_handle Scene::load_scenery_proto(const Resource_request & model, const Resource_request & skin)
{
    return get_impl(*this).load_scenery_proto(model,skin);
}

bool Scene::relax(Static_model_handle model)
{
    return get_impl(*this).relax(model);
}

Scenery_ptr Scene::new_scenery(Static_model_handle proto, const Rotation & rotation, const Position & position, const Direction& scale)
{
    return get_impl(*this).new_scenery(proto,rotation,position,scale);
}


Animation_handle Scene::load_animation(const Resource_request & animation)
{
    return get_impl(*this).load_animation(animation);
}

bool Scene::relax(Animation_handle animation)
{
    return get_impl(*this).relax(animation);
}

Scene& Scene::set_skybox(const Resource_request & skybox)
{
    get_impl(*this).set_skybox(skybox);
    return *this;
}

float Scene::sky_light_scale() const{
    return get_impl(*this).sky_light_scale();
}

Scene &Scene::sky_light_scale(float new_scale){
    get_impl(*this).sky_light_scale(new_scale);
    return *this;
}

RGB const& Scene::ambient_light() const{
    return get_impl(*this).ambient_light();
}

Scene &Scene::ambient_light(RGB const& a_ambient_light){
    get_impl(*this).ambient_light(a_ambient_light);
    return *this;
}

AABB const& Scene::boundary() const{
    return get_impl(*this).boundary();
}

Scene&Scene::boundary(AABB const& new_boundary)
{
    get_impl(*this).boundary(new_boundary);
    return *this;
}

Directional_light_ptr Scene::add_directional_light(RGB const& intensity, const Direction& direction)
{
    return get_impl(*this).add_directional_light(intensity,direction);
}

Scene& Scene::shading_variable_f( String const& name, float value)
{
    get_impl(*this).set_shading_var_f(name,value);
    return *this;
}

Scene& Scene::shading_variable_i( String const& name, int value)
{
    get_impl(*this).set_shading_var_i(name,value);
    return *this;
}

void Scene::advance(Scene_time dt)
{
    get_impl(*this).advance(dt);
}

Scene& Scene::auto_advance(bool enabled)
{
    get_impl(*this).auto_advance(enabled);
    return *this;
}

bool Scene::auto_advance() const
{
    return get_impl(*this).auto_advance();
}

void Scene::debug(Scene::Visual_debug_type b)
{
    get_impl(*this).debug_mode(b);
}

Scene::Visual_debug_type Scene::debug() const
{
    return get_impl(*this).debug_mode();
}

}
