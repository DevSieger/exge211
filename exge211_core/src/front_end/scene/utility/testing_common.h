#pragma once

namespace exge211{

enum class Test_result{
    OUTSIDE,
    PARTIALLY_INSIDE,
    INSIDE
};
using Test_hint=unsigned int;

}
