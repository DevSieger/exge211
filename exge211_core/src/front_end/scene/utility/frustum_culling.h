#pragma once

#include "../../../public_headers.h"
#include PUBLIC_HEADER(vector_types.h)
#include PUBLIC_HEADER(aabb.h)

#include "testing_common.h"
#include "../../lighting/lighting_data.h"

namespace exge211 {

class Frustum{
public:
    //the clip space is left handed, the world space is right handed
    enum Face_index: size_t{
        LEFT=0,
        RIGHT,
        BOTTOM,
        TOP,
        NEAR,
        FAR,
        FACE_COUNT=6
    };

    /*
     * distance from point to plane face_index.
     * negative = inside
    */
    float distance(Position const& point, size_t face_index) const{
        return glm::dot(faces_[face_index].normal,point)+faces_[face_index].offset;
    }

    bool inside(Position const& point,size_t face_index)const{
        return distance(point,face_index)<=0;
    }
    bool inside(Position const& point)const{
        for(size_t ix=0;ix<Face_index::FACE_COUNT;++ix){
            if(!inside(point,(Face_index)ix))return false;
        }
        return true;
    }
    Frustum(glm::mat4x4 const& scene_to_view);

    Direction const& face_normal(size_t face_index)const{
        return faces_[face_index].normal;
    }
private:
    struct Face{
        Direction normal;
        float offset;
    };
    Face faces_[Face_index::FACE_COUNT];
};

class Frustum_tester{
public:

    Frustum_tester(glm::mat4x4 const& transform): frustum_(transform){
        initialize();
    }
    Frustum_tester & frustum(const Frustum & a_frustum){
        frustum_=a_frustum;
        initialize();
        return *this;
    }

    constexpr static Test_hint initial_hint(){return 0;}

    /* hint_mask used for output */
    Test_result test(AABB const& box, Test_hint& hint_mask)const;
    Test_result test(Point_light_data const& data, Test_hint& hint_mask)const;

    Test_result operator()(AABB const& box, Test_hint& hint_mask)const{
        return test(box,hint_mask);
    }
    Test_result operator()(Point_light_data const& data, Test_hint& hint_mask)const{
        return test(data,hint_mask);
    }

private:
    void initialize();
    Frustum frustum_;
    unsigned char nearest_corner_[Frustum::FACE_COUNT];
};

}
