#include "frustum_culling.h"
#include <glm/gtc/matrix_transform.hpp>

namespace exge211 {

Frustum::Frustum(const glm::mat4x4 & scene_to_view){
    //нужно только 6 из 8 вершин
    Position vrtx[6]; //scene space
    glm::mat4x4 view_to_scene=glm::inverse(scene_to_view);
    for(size_t mask=1;mask<7;++mask){
        glm::vec4 v( (mask&1)*2-1.0f, (mask&2)-1.0f, ((mask&4)>>1)-1.0f,1);
        v=view_to_scene*v;
        v/=v.w;
        vrtx[mask-1]=Position(v);
    }
    //индексы вершин принадлежащих граням
    //система координат в пространстве экрана левая, но в пространстве сцены правая.
    using Byte=unsigned char;
    Byte indices[FACE_COUNT][3]={
        {1,5,3},//left
        {0,4,2},//right
        {0,3,4},//bottom
        {1,2,5},//top
        {0,2,1},//near
        {3,5,4}//far
    };
    for(size_t face_ix=0;face_ix<FACE_COUNT;++face_ix){
        Byte (&ix)[3]=indices[face_ix];
        faces_[face_ix].normal=glm::normalize(glm::cross(vrtx[ix[1]]-vrtx[ix[0]],vrtx[ix[2]]-vrtx[ix[0]]));
        faces_[face_ix].offset=-glm::dot(faces_[face_ix].normal,vrtx[ix[0]]);
    }
}

Test_result Frustum_tester::test(const AABB & box, Test_hint & hint_mask) const{
    bool partialy_inside=false;
    for(size_t face=0;face<Frustum::FACE_COUNT;++face){
        if(!(hint_mask & (1<<face))){
            size_t mask=nearest_corner_[face];
            Position v( ((mask&1)?box.max.x:box.min.x),
                    ((mask&2)?box.max.y:box.min.y),
                    ((mask&4)?box.max.z:box.min.z));
            if(!frustum_.inside(v,face)){
                return Test_result::OUTSIDE;
            }
            mask=~mask;
            v.x=(mask&1)?box.max.x:box.min.x;
            v.y=(mask&2)?box.max.y:box.min.y;
            v.z=(mask&4)?box.max.z:box.min.z;
            if(!frustum_.inside(v,face)){
                partialy_inside=true;
            }else{
                hint_mask|=1<<face;
            }
        }
    }
    return partialy_inside?Test_result::PARTIALLY_INSIDE : Test_result::INSIDE;
}


Test_result Frustum_tester::test( Point_light_data const& data, Test_hint& hint_mask) const
{
    bool partialy_inside=false;
    for(size_t face=0;face<Frustum::FACE_COUNT;++face){
        if(!(hint_mask & (1<<face))){
            size_t mask=nearest_corner_[face];
            Position v=data.position;
            v.x+=(mask&1)?data.range:-data.range;
            v.y+=(mask&2)?data.range:-data.range;
            v.z+=(mask&4)?data.range:-data.range;

            if(!frustum_.inside(v,face)){
                return Test_result::OUTSIDE;
            }
            mask=~mask;
            v.x=data.position.x+((mask&1)?data.range:-data.range);
            v.y=data.position.y+((mask&2)?data.range:-data.range);
            v.z=data.position.z+((mask&4)?data.range:-data.range);
            if(!frustum_.inside(v,face)){
                partialy_inside=true;
            }else{
                hint_mask|=1<<face;
            }
        }
    }
    return partialy_inside?Test_result::PARTIALLY_INSIDE : Test_result::INSIDE;
}

void Frustum_tester::initialize(){
    for(size_t ix=0;ix<Frustum::FACE_COUNT;++ix){
        Direction const& n=frustum_.face_normal(ix);
        nearest_corner_[ix]=(n.x<0)|(n.y<0)<<1|(n.z<0)<<2;
    }
}

}
