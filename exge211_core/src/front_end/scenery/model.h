#pragma once

#include "../../public_headers.h"
#include PUBLIC_HEADER(res/resource_data.h)
#include PUBLIC_HEADER(handles.h)

namespace exge211 {

class Static_model_base: public Bounded{
protected:
    ~Static_model_base() = default;
};

class Static_model_instance_base{
public:
    Static_model_instance_base(Static_model_handle a_type):type_(a_type){}
    Static_model_handle type()const{
        return type_;
    }
protected:
    ~Static_model_instance_base() = default;
private:
    Static_model_handle const type_;
    //u32 filter_state_;
};

}
