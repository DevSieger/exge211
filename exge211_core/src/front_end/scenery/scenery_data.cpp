#include "scenery_data.h"
#include "model.h"
namespace exge211{

Scenery_data::Scenery_data(Static_model_instance_base & a_model, Rotation const& rotation,  Position const& position, Direction const& scale):
    model_(a_model)
{
    AABB scaled_boundary=a_model.type()->boundary();
    scaled_boundary.min*=scale;
    scaled_boundary.max*=scale;
    for(size_t i=0;i<8;++i){
        boundary_.add(rotation*scaled_boundary.corner(i));
    }
    boundary_.min+=position;
    boundary_.max+=position;
}

}
