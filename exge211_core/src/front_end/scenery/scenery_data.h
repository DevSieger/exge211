#pragma once

#include "../../public_headers.h"
#include PUBLIC_HEADER(aabb.h)

namespace exge211 {

class Static_model_instance_base;

class Scenery_data{
public:
    Scenery_data(Static_model_instance_base & a_model, Rotation const& rotation,  Position const& position, Direction const& scale);
    AABB const& boundary() const { return boundary_; }
    Static_model_instance_base const& model() const {return model_;}
    Static_model_instance_base& model() {return model_;}
private:
    Static_model_instance_base & model_;
    AABB boundary_;
};

}
