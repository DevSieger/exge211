#pragma once

#include "../../public_headers.h"
#include PUBLIC_HEADER(scenery.h)
#include "../../private_handles.h"
#include "scenery_data.h"
#include <list>

namespace exge211 {

class Scenery_impl: public Scenery{
public:

    Scenery_impl(Scene_impl_ptr scene_a,Static_model_instance_base & model, Rotation const& rotation, Position const& position, Direction const& scale):
        data_(model,rotation,position,scale), scene_(scene_a)
    {}

    Scenery_data& data(){
        return data_;
    }
    Scenery_data const& data()const{
        return data_;
    }

    Scene_implementation & scene(){
        return *scene_;
    }

protected:
    ~Scenery_impl() = default;
private:
    Scenery_data data_;
    const Scene_impl_ptr scene_;
};

}
