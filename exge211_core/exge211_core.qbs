import qbs

Library{
    name: "exge211_core"
    Depends {
        name: "cpp"
    }
    type: "dynamiclibrary"

    cpp.cxxLanguageVersion: "c++14"
    cpp.defines: [
            "GLM_FORCE_RADIANS",
            "EXGE211_CORE_EXPORTING",
            "PUBLIC_HEADERS_PATH_EXGE211_CORE="+path+"/"+publicHeaders.prefix,
            "GLM_ENABLE_EXPERIMENTAL"
    ]
    cpp.includePaths: config.core.includePaths
    cpp.dynamicLibraries: config.core.libraries
    cpp.libraryPaths: config.core.libraryPaths



    Group {
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: qbs.targetOS.contains("windows")?"bin":"lib"
    }
    Export {
        Depends {
            name: "cpp"
        }
        cpp.defines: [
            "GLM_FORCE_RADIANS",
            "GLM_ENABLE_EXPERIMENTAL"
        ]
        cpp.includePaths: config.core.includePaths.concat(path+"/"+publicHeaders.baseDirectory)
        cpp.cxxLanguageVersion:  "c++14";
    }


    Group{
        id: publicHeaders
        name: "c++ public headers"
        property string baseDirectory: "public_headers"
        property string includePath: project.includeDirectoryName+"/"
        prefix: baseDirectory+"/"+publicHeaders.includePath
        files:[
            "aabb.h",
            "actor.h",
            "animation.h",
            "application_framework.h",
            "camera.h",
            "common_types.h",
            "context.h",
            "defines.h",
            "directional_light.h",
            "fps_counter.h",
            "fwd.h",
            "handles.h",
            "logging.h",
            "platform.h",
            "point_light.h",
            "renderer.h",
            "resource_loader.h",
            "scene.h",
            "scenery.h",
            "user_input.h",
            "vector_types.h",
        ]
        qbs.install: project.installHeaders
        qbs.installDir: "include/"+publicHeaders.includePath
    }
    Group{
        id: publicResourceLoadingHeaders
        name: "c++ resource loading public headers"
        property string includePath: publicHeaders.includePath+"res/"
        prefix: publicHeaders.baseDirectory+"/"+publicResourceLoadingHeaders.includePath
        files:[
            "animation_data.h",
            "model_type.h",
            "resource_data.h",
            "rs_fwd.h",
        ]
        qbs.install: project.installHeaders
        qbs.installDir: "include/"+publicResourceLoadingHeaders.includePath
    }

    Group{
        name: "Shaders"
        prefix: "shaders/"
        files:[
            "../3rd-party/smaa/smaa.glsl",
            "bright_pass.glsl",
            "cs_gaussian_filter_large.glsl",
            "cs_gaussian_filter_small.glsl",
            "cs_main.glsl",
            "cs_reduce.glsl",
            "fs_ambient.glsl",
            "cs_mip_generation.glsl",
            "fs_data.glsl",
            "fs_debug.glsl",
            "fs_lamps.glsl",
            "fs_lighting.glsl",
            "fs_sdpl_light.glsl",
            "fs_sedl_light.glsl",
            "fs_sepl_light.glsl",
            "fs_shadowmap.glsl",
            "fs_skybox.glsl",
            "gbuffer.glsl",
            "gs_cube.glsl",
            "sat/cs_sat3_map.glsl",
            "sat/cs_sat_1st.glsl",
            "smaa/fs_smaa_blend.glsl",
            "smaa/fs_smaa_detect.glsl",
            "smaa/fs_smaa_weight.glsl",
            "smaa/vs_smaa_blend.glsl",
            "smaa/vs_smaa_detect.glsl",
            "smaa/vs_smaa_weight.glsl",
            "vs_cube.glsl",
            "vs_data.glsl",
            "vs_dl.glsl",
            "vs_lamps.glsl",
            "vs_light.glsl",
            "vs_mesh_animated.glsl",
            "vs_mesh_geom.glsl",
            "vs_mesh_static.glsl",
            "vs_skybox.glsl",
            "vs_unit.glsl",
        ]
        qbs.install: true
        qbs.installDir: "share/"+project.projectName+"/shaders/"
    }

    Group{
        name: "3rd-party-src"
        prefix: "3rd-party/"
        files:[
            "smaa/AreaTex.h",
            "smaa/SearchTex.h",
        ]
    }

    Group{
        name:"c++ headers"
        prefix: "src/"
        files:[
            "back_end/animation_engine.h",
            "back_end/animation_system.h",
            "back_end/cpu_animator/animation_engine_hb.h",
            "back_end/cpu_animator/skeleton_instance.h",
            "back_end/cpu_animator/skeleton_type.h",
            "back_end/ogl4/binding_points.h",
            "back_end/ogl4/csm.h",
            "back_end/ogl4/drawers/full_screen_quad_drawer.h",
            "back_end/ogl4/drawers/light_drawer.h",
            "back_end/ogl4/drawers/skybox_drawer.h",
            "back_end/ogl4/frame_data.h",
            "back_end/ogl4/misc.h",
            "back_end/ogl4/model_data.h",
            "back_end/ogl4/rendering_backend_factory_ogl4.h",
            "back_end/ogl4/rendering_backend_ogl4.h",
            "back_end/ogl4/simple_resource_factory.h",
            "back_end/ogl4/smaa_textures.h",
            "back_end/ogl4/storage/animated_mesh_storage.h",
            "back_end/ogl4/storage/batch_group.h",
            "back_end/ogl4/storage/material_storage.h",
            "back_end/ogl4/storage/skybox_storage.h",
            "back_end/ogl4/storage/static_instance_storage.h",
            "back_end/ogl4/storage/static_mesh_storage.h",
            "back_end/ogl4/storage/texture_storage.h",
            "back_end/ogl4/tools/buffer_util.h",
            "back_end/ogl4/tools/cube_map.h",
            "back_end/ogl4/tools/draw_inderect_command.h",
            "back_end/ogl4/tools/frame_buffer.h",
            "back_end/ogl4/tools/image_format_traits.h",
            "back_end/ogl4/tools/texture.h",
            "back_end/ogl4/tools/texture_2d.h",
            "back_end/ogl4/tools/texture_3d.h",
            "back_end/ogl4/tools/texture_array.h",
            "back_end/ogl4/tools/vertex_format.h",
            "back_end/ogl_common/buffer/buffer_base.h",
            "back_end/ogl_common/buffer/buffer_content.h",
            "back_end/ogl_common/buffer/ssbo.h",
            "back_end/ogl_common/buffer/ubo.h",
            "back_end/ogl_common/error_checking.h",
            "back_end/ogl_common/gl_func/gl_core_uq_4_3.h",
            "back_end/ogl_common/ogl_functions.h",
            "back_end/ogl_common/performance_counter.h",
            "back_end/ogl_common/shader/shader.h",
            "back_end/ogl_common/shader/shader_loader.h",
            "back_end/ogl_common/shader/shader_program.h",
            "back_end/ogl_common/vao.h",
            "back_end/rendering_backend.h",
            "back_end/rendering_backend_factory.h",
            "back_end/resource_factory.h",
            "front_end/actor/actor_data.h",
            "front_end/actor/actor_impl.h",
            "front_end/actor/animation_state.h",
            "front_end/actor/model.h",
            "front_end/actor/skeleton_instance_base.h",
            "front_end/actor/skeleton_type_base.h",
            "front_end/lighting/directional_light_impl.h",
            "front_end/lighting/lighting_data.h",
            "front_end/lighting/point_light_impl.h",
            "front_end/scene/actor_manager.h",
            "front_end/scene/octree/manager_common.h",
            "front_end/scene/octree/octree.h",
            "front_end/scene/octree/octree_actor_manager.h",
            "front_end/scene/octree/octree_point_light_manager.h",
            "front_end/scene/octree/octree_scenery_manager.h",
            "front_end/scene/plain/plain_actor_manager.h",
            "front_end/scene/plain/plain_point_light_manager.h",
            "front_end/scene/plain/plain_scenery_manager.h",
            "front_end/scene/point_light_manager.h",
            "front_end/scene/scene_access.h",
            "front_end/scene/scene_implementation.h",
            "front_end/scene/scenery_manager.h",
            "front_end/scene/shading_variables.h",
            "front_end/scene/skybox.h",
            "front_end/scene/time_manager.h",
            "front_end/scene/utility/frustum_culling.h",
            "front_end/scene/utility/testing_common.h",
            "front_end/scenery/model.h",
            "front_end/scenery/scenery_data.h",
            "front_end/scenery/scenery_impl.h",
            "private_fwd.h",
            "private_handles.h",
            "public_headers.h",
        ]
    }

    Group{
        name: "c++ sources"
        prefix: "src/"
        files: [
            "back_end/cpu_animator/animation_engine_hb.cpp",
            "back_end/cpu_animator/skeleton_type.cpp",
            "back_end/ogl4/csm.cpp",
            "back_end/ogl4/drawers/full_screen_quad_drawer.cpp",
            "back_end/ogl4/drawers/light_drawer.cpp",
            "back_end/ogl4/drawers/skybox_drawer.cpp",
            "back_end/ogl4/frame_data.cpp",
            "back_end/ogl4/misc.cpp",
            "back_end/ogl4/model_data.cpp",
            "back_end/ogl4/rendering_backend_factory_ogl4.cpp",
            "back_end/ogl4/rendering_backend_ogl4.cpp",
            "back_end/ogl4/simple_resource_factory.cpp",
            "back_end/ogl4/smaa_textures.cpp",
            "back_end/ogl4/storage/animated_mesh_storage.cpp",
            "back_end/ogl4/storage/material_storage.cpp",
            "back_end/ogl4/storage/skybox_storage.cpp",
            "back_end/ogl4/storage/static_instance_storage.cpp",
            "back_end/ogl4/storage/static_mesh_storage.cpp",
            "back_end/ogl4/storage/texture_storage.cpp",
            "back_end/ogl4/tools/buffer_util.cpp",
            "back_end/ogl4/tools/cube_map.cpp",
            "back_end/ogl4/tools/frame_buffer.cpp",
            "back_end/ogl4/tools/image_format_traits.cpp",
            "back_end/ogl4/tools/texture.cpp",
            "back_end/ogl4/tools/texture_2d.cpp",
            "back_end/ogl4/tools/texture_3d.cpp",
            "back_end/ogl4/tools/texture_array.cpp",
            "back_end/ogl4/tools/vertex_format.cpp",
            "back_end/ogl_common/error_checking.cpp",
            "back_end/ogl_common/gl_func/gl_core_uq_4_3.cpp",
            "back_end/ogl_common/ogl_context.cpp",
            "back_end/ogl_common/performance_counter.cpp",
            "back_end/ogl_common/shader/shader.cpp",
            "back_end/ogl_common/shader/shader_loader.cpp",
            "back_end/ogl_common/shader/shader_program.cpp",
            "back_end/rendering_backend_factory.cpp",
            "front_end/actor/actor.cpp",
            "front_end/actor/actor_impl.cpp",
            "front_end/actor/animation.cpp",
            "front_end/actor/animation_state.cpp",
            "front_end/application_framework.cpp",
            "front_end/camera.cpp",
            "front_end/fps_counter.cpp",
            "front_end/lighting/directional_light.cpp",
            "front_end/lighting/point_light.cpp",
            "front_end/lighting/point_light_impl.cpp",
            "front_end/logging.cpp",
            "front_end/renderer.cpp",
            "front_end/scene/octree/octree_actor_manager.cpp",
            "front_end/scene/octree/octree_point_light_manager.cpp",
            "front_end/scene/octree/octree_scenery_manager.cpp",
            "front_end/scene/plain/plain_actor_manager.cpp",
            "front_end/scene/plain/plain_point_light_manager.cpp",
            "front_end/scene/plain/plain_scenery_manager.cpp",
            "front_end/scene/scene.cpp",
            "front_end/scene/scene_access.cpp",
            "front_end/scene/scene_implementation.cpp",
            "front_end/scene/shading_variables.cpp",
            "front_end/scene/time_manager.cpp",
            "front_end/scene/utility/frustum_culling.cpp",
            "front_end/scenery/scenery_data.cpp",
        ]
    }

}
