var TextFile=loadExtension("qbs.TextFile");
var File=loadExtension("qbs.File");

var dependencies={
    "core": ["dk","opengl","glm"],
    "qt_module": [],
    "glfw_module": ["glfw"],
    "examples": [],
    "converter": ["assimp"]
}

// "target_name": enabled_by_default
var targets={
    "examples":false,
    "core":true,
    "glfw_module": false,
    "converter": false
}

// property names of targets in config
// "name_in_target":"name_in_json_dependencies"
var propertyNames={
    "includePaths": "includePath",
    "libraries": "libraryName",
    "libraryPaths": "libraryPath"
}

function loadConfig(configPath){

    //loading json config
    if(!File.exists(configPath)){
        throw("configuration file "+configPath+" not found");
    }
    var content=new TextFile(configPath).readAll();
    var jsonConfig=JSON.parse(content);

    //generating include paths, etc..
    var config={
        "config": jsonConfig
    }


    for( tName in targets ){
        if(targets[tName] || ( jsonConfig[tName] && jsonConfig[tName].enabled )){
            //gathering paths from dependencies in config to target
            config[tName]={};
            for(targetProp in propertyNames){
                var paths=[];
                for( var ix=0; ix<dependencies[tName].length; ix++ ){
                    var dName=dependencies[tName][ix];
                    var path=jsonConfig["dependences"][dName][propertyNames[targetProp]];
                    if(path){
                        if(path instanceof Array){
                            for(p_ix in path){
                                paths.push(path[p_ix]);
                            }
                        }else{
                            paths.push(path);
                        }
                    }
                }
                config[tName][targetProp]=[].uniqueConcat(paths);
            }
        }
    }

    /*
        //now config looks like:
        var config={
            "config": jsonConfig,
            "core": {
                "includePaths": [],
                "libraries": [],
                "libraryPaths": []
            },
            ...
        }
    */
    return config;
}
