import qbs

Library{

    name: "exge211_glfw"
    Depends {
        name: "cpp"
    }
	
    Depends{
        name: "exge211_core"
    }

    type: "dynamiclibrary"

    cpp.cxxLanguageVersion: "c++14"
    cpp.defines: [
            "EXGE211_GLFW_EXPORTING",
            "PUBLIC_HEADERS_PATH_EXGE211_GLFW="+path+"/"+publicHeaders.prefix
    ]

    cpp.includePaths: config.glfw_module.includePaths
    cpp.dynamicLibraries: config.glfw_module.libraries
    cpp.libraryPaths: config.glfw_module.libraryPaths

    Group {
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: qbs.targetOS.contains("windows")?"bin":"lib"
    }
	
    Export {
        Depends {
            name: "cpp"
        }
        cpp.includePaths: [path+"/"+publicHeaders.baseDir]
        cpp.cxxLanguageVersion:  "c++14";
    }
    Group{
        id: publicHeaders
        name: "c++ public headers"
        property string baseDir: "public_headers"
        property string includePath: project.includeDirectoryName+"/glfw/"
        prefix: baseDir+"/"+ publicHeaders.includePath
        files:[
            "defines.h",
            "platform_factory_glfw.h",
        ]
        qbs.install:  project.installHeaders
        qbs.installDir: "include/"+ publicHeaders.includePath
    }
    Group{
        name: "c++ sources"
        prefix: "src/"
        files:[
            "context_glfw.cpp",
            "context_glfw.h",
            "glfw.h",
            "gui_event_loop_glfw.cpp",
            "gui_event_loop_glfw.h",
            "platform_factory_glfw.cpp",
            "public_headers.h",
            "window_glfw.cpp",
            "window_glfw.h",
        ]
    }
}
