#pragma once

#if defined(_WIN32) && !defined(EXGE211_QT_STATIC)

#ifdef EXGE211_GLFW_EXPORTING
#define EXGE211_GLFW_EXPORT __declspec(dllexport)
#else
#define EXGE211_GLFW_EXPORT __declspec(dllimport)
#endif //#ifdef EXGE211_GLFW_EXPORTING

#else
#define EXGE211_GLFW_EXPORT
#endif
