#pragma once
#include "defines.h"
#include <exge211/platform.h>

namespace exge211 {

class EXGE211_GLFW_EXPORT Platform_factory_GLFW : public Platform_factory
{
public:
    Platform_factory_GLFW();
    ~Platform_factory_GLFW();
    Window_ptr new_window(int width,int height,String const & title)override;
    GUI_event_loop_ptr get_GUI_event_loop()override;
private:
    GUI_event_loop_ptr event_loop_=nullptr;
};

}
