#pragma once
#include <exge211/platform.h>

namespace exge211 {

class GUI_event_loop_GLFW : public GUI_event_loop
{
public:
    GUI_event_loop_GLFW();
    void process_events()override;
    bool running()const override;
    GUI_event_loop & running(bool running_a) override;
private:
    bool running_=true;
};

}
