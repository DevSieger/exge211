#pragma once
#include <exge211/context.h>

struct GLFWwindow;

namespace exge211 {

class Context_GLFW : public OGL_context
{
public:
    Context_GLFW(GLFWwindow * wnd);
    Size size() const override;
    void make_current() override;
    void swap_buffers() override;
private:
    friend class GL_window_GLFW;
    GLFWwindow * wnd_;
};

}
