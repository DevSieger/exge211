#pragma once
#include <exge211/platform.h>
#include <exge211/user_input.h>
#include "context_glfw.h"

struct GLFWwindow;

namespace exge211{

class GL_window_GLFW : public Window, public Keyboard_callbacks, public Mouse_callbacks
{
public:
    GL_window_GLFW(int width, int height, const char *title_);
    ~GL_window_GLFW();
    Context& context()const override;
    bool visible()const override;
    Window & visible(bool visible_) override;
    void show() override;
    void resize(int width_, int height_) override;
    bool full_screen()const override;
    Window & full_screen(bool fullScreen_) override;
    Window & lock_mouse(bool) override;
    Window & lock_keyboard(bool) override;
    bool lock_mouse()const override;
    bool lock_keyboard()const override;

    ctrl::Cursor_position cursor_position() const;

private:
    static void on_cursor_position_event(GLFWwindow* wnd, double cursor_x, double cursor_y);


    Context_GLFW ctx_;
    // to use with 'lock_mouse'
    void save_cursor_position();
    double last_cursor_x;
    double last_cursor_y;

    // to restore the window from the full screen mode.
    int last_window_x=0;
    int last_window_y=0;
    int last_window_w;
    int last_window_h;
};

}
