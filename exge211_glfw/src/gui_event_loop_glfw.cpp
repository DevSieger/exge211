#include "gui_event_loop_glfw.h"
#include "glfw.h"

namespace exge211 {

GUI_event_loop_GLFW::GUI_event_loop_GLFW()
{
}

void GUI_event_loop_GLFW::process_events()
{
    glfwPollEvents();
}

bool GUI_event_loop_GLFW::running() const
{
    return running_;
}

GUI_event_loop &GUI_event_loop_GLFW::running(bool running_a)
{
    running_=running_a;
    return *this;
}

}
