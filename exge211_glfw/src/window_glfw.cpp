#include <exge211/logging.h>
#include "window_glfw.h"
#include "glfw.h"
#include <cassert>

namespace exge211 {

// callbacks
namespace {

GL_window_GLFW& get_window(GLFWwindow * wnd){
    assert(glfwGetWindowUserPointer(wnd));
    return *(GL_window_GLFW *)glfwGetWindowUserPointer(wnd);
}

void on_close_fun(GLFWwindow * wnd){
    GL_window_GLFW& window=get_window(wnd);
    if( window.on_close && window.on_close() ){
        // we can't call glfwDestroyWindow from a callback.
        // TODO: the way to actually close the glfw window.
    }else{
        glfwSetWindowShouldClose(wnd,false);
    }
}

ctrl::Keyboard_modifiers get_keyboard_modifiers(int bitfield){
    ctrl::Keyboard_modifiers md;
    if(bitfield & GLFW_MOD_SHIFT){
        md|=ctrl::Keyboard_modifier::SHIFT;
    };
    if(bitfield & GLFW_MOD_CONTROL){
        md|=ctrl::Keyboard_modifier::CTRL;
    };
    if(bitfield & GLFW_MOD_ALT){
        md|=ctrl::Keyboard_modifier::ALT;
    };
    return md;
}

// For a case if we don't have the event-given value.
ctrl::Keyboard_modifiers get_keyboard_modifiers(GLFWwindow* wnd){
    // The only way to obtain modifiers is the direct testing =(
    ctrl::Keyboard_modifiers mdf;
    if( glfwGetKey(wnd,GLFW_KEY_LEFT_SHIFT)==GLFW_PRESS || glfwGetKey(wnd,GLFW_KEY_RIGHT_SHIFT)==GLFW_PRESS )
    {
        mdf |= ctrl::Keyboard_modifier::SHIFT;
    }
    if( glfwGetKey(wnd,GLFW_KEY_LEFT_CONTROL)==GLFW_PRESS || glfwGetKey(wnd,GLFW_KEY_RIGHT_CONTROL)==GLFW_PRESS )
    {
        mdf |= ctrl::Keyboard_modifier::CTRL;
    }

    if( glfwGetKey(wnd,GLFW_KEY_LEFT_ALT)==GLFW_PRESS  || glfwGetKey(wnd,GLFW_KEY_RIGHT_ALT)==GLFW_PRESS )
    {
        mdf |= ctrl::Keyboard_modifier::ALT;
    }
    return mdf;
}

ctrl::Mouse_button get_mouse_button(int  button){
    switch(button){
        case GLFW_MOUSE_BUTTON_LEFT: return ctrl::Mouse_button::LEFT;
        case GLFW_MOUSE_BUTTON_RIGHT: return ctrl::Mouse_button::RIGHT;
        case GLFW_MOUSE_BUTTON_MIDDLE: return ctrl::Mouse_button::MIDDLE;
        default: return ctrl::Mouse_button::NONE;
    };
}

/// Returns all mouse buttons which are pressed down.
ctrl::Mouse_buttons get_mouse_buttons(GLFWwindow* wnd){
    ctrl::Mouse_buttons mb;
    if(glfwGetMouseButton(wnd,GLFW_MOUSE_BUTTON_LEFT)){
        mb|=ctrl::Mouse_button::LEFT;
    };
    if(glfwGetMouseButton(wnd,GLFW_MOUSE_BUTTON_RIGHT)){
        mb|=ctrl::Mouse_button::RIGHT;
    };
    if(glfwGetMouseButton(wnd,GLFW_MOUSE_BUTTON_MIDDLE)){
        mb|=ctrl::Mouse_button::MIDDLE;
    };
    return mb;
}

void on_key_event_fun(GLFWwindow* wnd, int key, int, int action, int mods){
    GL_window_GLFW& window=get_window(wnd);
    if(action == GLFW_RELEASE){
        if(window.on_key_release){
            window.on_key_release(key,get_keyboard_modifiers(mods));
        }
    }else if(action == GLFW_PRESS){
        if(window.on_key_press){
            window.on_key_press(key,get_keyboard_modifiers(mods));
        }
    }
}

void on_mouse_button_event(GLFWwindow* wnd, int button, int action, int mods){
    GL_window_GLFW& window=get_window(wnd);
    ctrl::Mouse_button mb=get_mouse_button(button);
    ctrl::Mouse_buttons mbs=get_mouse_buttons(wnd);

    if(action == GLFW_PRESS){
        if(window.on_mouse_press){
            mbs |= mb;
            window.on_mouse_press(mb,window.cursor_position(),mbs,get_keyboard_modifiers(mods));
        }
    }else if(action == GLFW_RELEASE){
        if(window.on_mouse_release){
            mbs &= ~ctrl::Mouse_buttons(mb);
            window.on_mouse_release(mb,window.cursor_position(),mbs,get_keyboard_modifiers(mods));
        }
    };
}

void on_scroll_event(GLFWwindow* wnd, double x, double y){
    GL_window_GLFW& window=get_window(wnd);
    if(window.on_wheel){
        // GLFW doesn't give any good explanation about x and y.
        // It looks like the 'y' parameter is 1 or -1 for the mouse input.
        // The qt-module gives 15 degrees on the similiar rotation.
        float correction=15;
        window.on_wheel(y*correction,window.cursor_position(),get_mouse_buttons(wnd),get_keyboard_modifiers(wnd));
    }
}

}

void GL_window_GLFW::on_cursor_position_event(GLFWwindow* wnd, double cursor_x, double cursor_y){
    GL_window_GLFW& window=get_window(wnd);
    if(window.on_mouse_move){
        window.on_mouse_move(window.cursor_position(),get_mouse_buttons(wnd),get_keyboard_modifiers(wnd));
        window.save_cursor_position();
    }
}

ctrl::Cursor_position GL_window_GLFW::cursor_position() const
{
    GLFWwindow* const& wnd=ctx_.wnd_;

    double x,y;
    glfwGetCursorPos(wnd,&x,&y);

    /*
     * We need both the window size and the frame buffer size because
     * the GLFW cursor position is set in 'screen coordinates',
     * but the resulting position is set in pixels.
     */

    int fb_w,fb_h;
    glfwGetFramebufferSize(wnd,&fb_w,&fb_h);

    int w_w,w_h;
    glfwGetWindowSize(wnd,&w_w,&w_h);

    float width=fb_w, height=fb_h;

    if(lock_mouse()){
        return ctrl::Cursor_position( (x-last_cursor_x)*width/w_w, -(y-last_cursor_y)*height/w_h);
    }else{
        return ctrl::Cursor_position( (x/w_w - 0.5)*width, (0.5-y/w_h)*height);
    }
}


GL_window_GLFW::GL_window_GLFW(int width, int height, const char *title_):
    ctx_( nullptr)
{
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,3);
    glfwWindowHint(GLFW_OPENGL_PROFILE,GLFW_OPENGL_CORE_PROFILE);
    ctx_.wnd_=glfwCreateWindow(width,height,title_,0,0);
    if(!ctx_.wnd_){
        log_msg(Detail_level::CRITICAL,"GLFW error: failed to create the window.");
    }
    glfwSetWindowUserPointer(ctx_.wnd_,this);
    glfwSetWindowCloseCallback(ctx_.wnd_,on_close_fun);
    glfwSetKeyCallback(ctx_.wnd_,on_key_event_fun);
    glfwSetMouseButtonCallback(ctx_.wnd_,on_mouse_button_event);
    glfwSetCursorPosCallback(ctx_.wnd_,on_cursor_position_event);
    glfwSetScrollCallback(ctx_.wnd_,on_scroll_event);
    last_window_w=width;
    last_window_h=height;
}

GL_window_GLFW::~GL_window_GLFW()
{
    glfwDestroyWindow(ctx_.wnd_);
}

Context& GL_window_GLFW::context() const
{
    return const_cast<Context_GLFW&>(ctx_);
}

bool GL_window_GLFW::visible() const
{
    return glfwGetWindowAttrib(ctx_.wnd_,GLFW_VISIBLE);
}

Window &GL_window_GLFW::visible(bool visible_)
{
    if(visible_){
        glfwShowWindow(ctx_.wnd_);
    }else{
        glfwHideWindow(ctx_.wnd_);
    }
    return *this;
}

void GL_window_GLFW::show()
{
    glfwShowWindow(ctx_.wnd_);
}

void GL_window_GLFW::resize(int width_a, int height_a)
{
    glfwSetWindowSize(ctx_.wnd_,width_a,height_a);
}

bool GL_window_GLFW::full_screen() const
{
    return glfwGetWindowMonitor(ctx_.wnd_)!=NULL;
}

Window &GL_window_GLFW::full_screen(bool fs)
{
    if(fs != full_screen()){
        GLFWwindow*& wnd = ctx_.wnd_;
        if(fs){
            GLFWmonitor* monitor = glfwGetPrimaryMonitor();
            const GLFWvidmode* mode = glfwGetVideoMode(monitor);

            glfwGetWindowPos(wnd,&last_window_x,&last_window_y);
            glfwGetWindowSize(wnd,&last_window_w,&last_window_h);

            glfwSetWindowMonitor(wnd,monitor,0,0,mode->width,mode->height,mode->refreshRate);
        }else{
            glfwSetWindowMonitor(wnd,NULL,last_window_x,last_window_y,last_window_w,last_window_h,0);
        }
    }
    return *this;
}

Window &GL_window_GLFW::lock_mouse(bool b)
{
    if(b){
        glfwSetInputMode(ctx_.wnd_,GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        save_cursor_position();
    }else{
        glfwSetInputMode(ctx_.wnd_,GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }
    return *this;
}

Window &GL_window_GLFW::lock_keyboard(bool)
{
    log_msg(Detail_level::NON_CRITICAL,"A grab of a keyboard input is not implemented in the GLFW module.");
    return *this;
}

bool GL_window_GLFW::lock_mouse() const
{
    return glfwGetInputMode(ctx_.wnd_,GLFW_CURSOR)==GLFW_CURSOR_DISABLED;
}

bool GL_window_GLFW::lock_keyboard() const
{
    return false;
}

void GL_window_GLFW::save_cursor_position()
{
    glfwGetCursorPos(ctx_.wnd_,&last_cursor_x,&last_cursor_y);
}

}
