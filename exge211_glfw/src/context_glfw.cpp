#include "context_glfw.h"
#include "glfw.h"

namespace exge211 {

Context_GLFW::Context_GLFW(GLFWwindow *wnd):wnd_(wnd)
{
}

Context::Size Context_GLFW::size() const
{
    int w,h;
    glfwGetFramebufferSize(wnd_,&w,&h);
    using Value=Size::Value;
    return Size{ (Value)w,(Value)h };
}


void Context_GLFW::make_current()
{
    glfwMakeContextCurrent(wnd_);
}

void Context_GLFW::swap_buffers()
{
    glfwSwapBuffers(wnd_);
}

}
