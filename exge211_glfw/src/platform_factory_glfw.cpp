#include "glfw.h"
#include "public_headers.h"
#include PUBLIC_HEADER(platform_factory_glfw.h)
#include "window_glfw.h"
#include "gui_event_loop_glfw.h"
#include "context_glfw.h"
#include <exge211/logging.h>

namespace exge211{

static void on_error(int error, const char * desc ){
    log_msg(Detail_level::CRITICAL,"glfw error %1%:\n%2%",error,desc);
}

Platform_factory_GLFW::Platform_factory_GLFW()
{
    glfwSetErrorCallback(on_error);
    glfwInit();
    event_loop_=std::make_shared<GUI_event_loop_GLFW>();
}

Platform_factory_GLFW::~Platform_factory_GLFW()
{
    event_loop_.reset();
    glfwTerminate();
}

Window_ptr Platform_factory_GLFW::new_window(int width, int height, const String &title)
{
    return std::make_shared<GL_window_GLFW>(width,height,title.c_str());
}

GUI_event_loop_ptr Platform_factory_GLFW::get_GUI_event_loop()
{
    return event_loop_;
}

}
