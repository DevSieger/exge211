import qbs

Library{

    name: "exge211_smplres"
    Depends {
        name: "cpp"
    }
    Depends{
        name: "exge211_core";
    }

    type: "dynamiclibrary"

    cpp.cxxLanguageVersion: "c++14"
    cpp.defines: [
            "EXGE211_SMPLRES_EXPORTING",
            "PUBLIC_HEADERS_PATH_EXGE211_SMPLRES="+path+"/"+publicHeaders.prefix
    ]

    Export {
        Depends {
            name: "cpp"
        }
        cpp.includePaths: [path+"/"+publicHeaders.baseDir]
        cpp.cxxLanguageVersion:  "c++14";
    }

    Group {
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: qbs.targetOS.contains("windows")?"bin":"lib"
    }

    Group{
        id: publicHeaders
        name: "c++ public headers"
        property string baseDir: "public_headers"
        property string includePath: project.includeDirectoryName+"/smplres/"
        prefix: baseDir+"/"+publicHeaders.includePath
        files:[
            "default_format.h",
            "defines.h",
            "file_loader.h",
            "skybox_format_stb_image.h",
            "smpl_resource_loader.h",
            "texture_format_stb_image.h",
        ]
        qbs.install: project.installHeaders
        qbs.installDir: "include/"+publicHeaders.includePath
    }
    Group{
        name: "c++ sources"
        prefix: "src/"
        files:[
            "default_format.cpp",
            "file_loader.cpp",
            "public_headers.h",
            "skybox_format_stb_image.cpp",
            "texture_format_stb_image.cpp",
        ]
    }
    Group{
        name: "3rd_party"
        prefix: "3rd_party/"
        files:[
            "stb_image.h",
        ]
    }
}
