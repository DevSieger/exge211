#include "public_headers.h"
#include PUBLIC_HEADER(default_format.h)
#include <exge211/logging.h>
#include <exge211/res/resource_data.h>
#include <exge211/res/animation_data.h>
#include <cassert>
#include <dk/serialize_pod.h>
/*
 *
 *
 *
 *
 *
 *
 *
 */

namespace exge211 {

namespace default_format {

namespace{

// a locale-independent std::isspace
/// Tests if the given character is whitespace character.
bool check_space(char c){
    return c == ' ' || c == '\t' ||
            ( c >= 0x0a && c <= 0x0d);  // \n \v \f \r
}

/*
 * A very-very basic realization of the _locale-independent_ variant of strtof for the decimal notation.
 * The reason to use this one instead of strtof :
 *  Qt (e.g.) may change the locale and break the parsing ( i.e. strtof will expect a comma instead of a dot).
 *  A call of "setlocale" is a bad choice since this changes the locale to ALL threads.
 */
float read_float(const char* str, const char** str_end){
    auto it = str;
    const float default_value = 0;
    bool negative = false;

    assert(it);

    while( *it && check_space(*it))
        ++it;

    if(!*it)
        return default_value;

    if(*it == '-'){
        negative = true;
        ++it;
    }else if(*it == '+'){
        ++it;
    }

    std::uint64_t int_rep = 0;
    auto start = it;

    // the destination type is float, 32 bits is more than enough.
    const int max_characters = 10 + 1;
    int gather_offset   = 0;
    int gathered        = 0;
    int point_position  = -1;

    while (*it) {
        char c = *it;
        if( c >= '0' && c < '0' + 10){
            if(gathered < max_characters){
                int digit   = c - '0';
                if(gathered || digit ){
                    int_rep     = int_rep * 10 + digit;
                     ++gathered;
                }else{
                    ++gather_offset;
                }
            }
        }else if ( c == '.' && point_position == -1){
            point_position = it - start;
        }else{
            break;
        }
        ++it;
    }

    // nothing is converted
    if(it == start){
        return default_value;
    }

    if( point_position == -1 )
        point_position = it - start;

    double exponent = point_position - gathered - gather_offset;
    double tmp = int_rep * pow(10,exponent);
    if( negative ) tmp = -tmp;
    *str_end = it;
    return tmp;
}

void skip_spaces(const char * str, size_t str_len,size_t & i){
    for(;i<str_len;++i){
        if(!check_space(str[i])){
            return;
        }
    }
}
String get_string(const char * str, size_t str_len,size_t & i){
    String ret;
    assert(i<str_len);
    if(str[i]!='"'){
        log_msg(Detail_level::CRITICAL,"ill formed string in %1% : %2%; opening quotation mark was expected, but %3% was recieved",__FILE__,__LINE__,str[i]);
        while(++i<str_len){
            if(str[i]=='"'){
                break;
            }
        }
    }
    for(++i ;i<str_len; ++i){
        if(str[i]!='"'){
            ret.push_back(str[i]);
        }else{
            break;
        }
    }
    if(i>=str_len){
        log_msg(Detail_level::CRITICAL,"ill formed string in %1% : %2%; closing quotation mark was expected, but wasn't recieved",__FILE__,__LINE__,str[i]);
    }else{
        ++i;
    }
    return ret;
}

unsigned long read_natural(const char * str, size_t & i){
    char* tmp=(char*)str;
    unsigned long ret=std::strtoul(str+i,&tmp,10);
    if(!ret){
        log_msg(Detail_level::NON_CRITICAL,"natural number was expected, but null was recieved");
    }
    i=tmp-str;
    return ret;
}

unsigned long read_uint(const char * str, size_t & i){
    char* tmp=(char*)str;
    unsigned long ret=std::strtoul(str+i,&tmp,10);
    i=tmp-str;
    if(!i || check_space(str[i-1])){
        log_msg(Detail_level::NON_CRITICAL,"unsigned integer expected, but wasn't received");
    }
    return ret;
}

}

/*
 * "material key" "material name"
 * ...
 */

bool parse_resource(const char * str, size_t str_len, Skin_info & out)
{
    size_t i=0;
    String key;
    String val;
    while(i<str_len){
        skip_spaces(str,str_len,i);
        if(i<str_len){
            key=get_string(str,str_len,i);
            skip_spaces(str,str_len,i);
            if(i<str_len){
                val=get_string(str,str_len,i);
                if(key.empty()){
                    log_msg(Detail_level::NON_CRITICAL,"empty key in skin, %1%:%2%",__FILE__,__LINE__);
                    return false;
                }
                if(val.empty()){
                    log_msg(Detail_level::NON_CRITICAL,"empty value in skin, %1%:%2%",__FILE__,__LINE__);
                    return false;
                }
                out.emplace(std::move(key),std::move(val));
            }
        }
    }
    return true;
}

bool parse_resource(const char * str_in, size_t str_len, Material_info & out)
{
    size_t i=0;
    const char * nxt=str_in;
    out.shininess=read_float(str_in,&nxt);
    i=size_t(nxt-str_in);
    skip_spaces(str_in,str_len,i);
    if(i<str_len){
        out.albedo=get_string(str_in,str_len,i);
        skip_spaces(str_in,str_len,i);
        if(i<str_len){
            out.specular=get_string(str_in,str_len,i);
            skip_spaces(str_in,str_len,i);
            if(i<str_len){
                out.normal=get_string(str_in,str_len,i);
                return true;
            }
        }
    }
    log_msg(Detail_level::NON_CRITICAL,"material info string is incomplete, %1%:%2%",__FILE__,__LINE__);
    return false;
}

/*
 * "s_name" "f_r.x f_r.y f_r.z f_r.w f_d.x ... f_d.w" u_index u_parent *
 */

bool parse_resource(const char * str_in, size_t str_len, Skeleton_info & out)
{

    size_t i=0;
    bool ret=true;
    skip_spaces(str_in,str_len,i);
    for(size_t index=0;i<str_len;++index){
        Skeleton_info::JointInfo joint_info;
        joint_info.name=get_string(str_in,str_len,i);
        skip_spaces(str_in,str_len,i);
        if(i>=str_len) return false;
        {
            String binding_pose;
            binding_pose=get_string(str_in,str_len,i);
            Joint & joint=joint_info.binding_pose;
            const char * rptr=binding_pose.c_str();
            const char * end=rptr+binding_pose.size();
            for(size_t p=0;p<2;++p){
                for(size_t j=0;j<4 && rptr<end;++j){
                    const char * tmp=rptr;
                    joint[p][j]=read_float(rptr,&tmp);
                    rptr=tmp;
                }
            }
        }
        if(i>=str_len) return false;
        char * tmp=(char*)str_in+i;
        joint_info.index=std::strtoul(str_in+i,&tmp,10);
        i=tmp-str_in;
        if(i>=str_len) return false;
        joint_info.parent=std::strtoul(str_in+i,&tmp,10);
        i=tmp-str_in;
        if(index!=joint_info.index){
            log_msg(Detail_level::NON_CRITICAL,"Joints out of order when loading skeleton info");
            ret=false;
        }
        out.joints.emplace_back(std::move(joint_info));
        skip_spaces(str_in,str_len,i);
    }
    return ret;
}

/*
 * "f_boundary_inf_x ... f_boundary_inf_z f_boundary_sup_x ... f_boundary_sup_z"
 * u_N u_lod_size1 ... u_lod_sizeN
 * "s_material_key_1_1"...
 * u_model_1_1_vertices_begin,u_model_1_1_vertices_end, u_model_1_1_indices_begin,u_model_1_1_indices_end  ...   //byte offset from begining of input
 * binary_data
 */
/* NOT TESTED YET! */

namespace{

String const& binary_magic_word(){
    static const String binary("binary:");
    return binary;
}

}

bool parse_resource(const char * str_in, size_t str_len, Animated_model_data & out)
{
    size_t i=0;
    //reading boudary box
    {
        String boundary_str;
        skip_spaces(str_in,str_len,i);
        if(i>=str_len) return false;
        boundary_str=get_string(str_in,str_len,i);
        if(i>=str_len) return false;
        glm::vec3 v[2];
        const char * rptr=boundary_str.c_str();
        const char * end=rptr+boundary_str.size();
        for(size_t p=0;p<2;++p){
            for(size_t j=0;j<3 && rptr<end;++j){
                const char * tmp=rptr;
                v[p][j]=read_float(rptr,&tmp);
                rptr=tmp;
            }
        }
        out.boundary(AABB{v[0],v[1]});
    }

    //reading number of LODs
    size_t lod_count=read_natural(str_in,i);
    if(i>=str_len) return false;

    size_t submodels_total=0;
    std::vector<size_t> lod_sizes(lod_count);

    for(size_t lod_ix=0 ; lod_ix<lod_count ; ++lod_ix){
        //reading size of each LOD
        size_t part_count=read_natural(str_in,i);
        if(i>=str_len) return false;
        lod_sizes[lod_ix]=part_count;
        submodels_total+=part_count;
    }
    out.init(lod_count,lod_sizes.data());

    //reading skeleton size
    const size_t number_of_poses=read_natural(str_in,i);
    if(i>=str_len) return false;

    //reading number of joints in compatible skeletons
    const size_t skeleton_size=read_natural(str_in,i);
    if(i>=str_len) return false;

    //reading inverse bind pose index for each submodel
    for(size_t ix=0;ix<out.lod_count();++ix){
        for(Animated_model_data::LOD::Value& sm: out.lod(ix)){
            sm.mesh.transform_ix=read_uint(str_in,i);
        }
    }

    //reading material key for each submodel
    for(size_t ix=0;ix<out.lod_count();++ix){
        for(Animated_model_data::LOD::Value& sm: out.lod(ix)){
            skip_spaces(str_in,str_len,i);
            if(i>=str_len) return false;
            sm.material=get_string(str_in,str_len,i);
            if(i>=str_len) return false;
        }
    }

    //containers to store binary size of vertices data and indices data for each submodel
    std::vector<size_t> vertices_size(submodels_total);
    std::vector<size_t> indices_size(submodels_total);

    //reading binary size of vertices for each submodel
    for(size_t sm=0;sm<submodels_total;++sm){
        vertices_size[sm]=read_natural(str_in,i);
        if(i>=str_len) return false;
    }

    //reading binary size of indices for each submodel
    for(size_t sm=0;sm<submodels_total;++sm){
        indices_size[sm]=read_natural(str_in,i);
        if(i>=str_len) return false;
    }

    skip_spaces(str_in,str_len,i);

    //checking "binary:" magic word
    {
        const size_t magic_word_size=binary_magic_word().size();
        if(i+magic_word_size>=str_len) return false;

        String testing_word(str_in+i,magic_word_size);
        if(testing_word!=binary_magic_word()){
            log_msg(Detail_level::NON_CRITICAL, "unexpected \"%1%\", expected: \"%2%\"",testing_word,binary_magic_word());
            return false;
        }
        i+=magic_word_size;
    }

    /*
     * place to add material type
     */

    using Iterator=const char*;
    Iterator it=str_in+i;
    const Iterator it_end=str_in+str_len;

    //reading inverse bind pose transforms
    for(size_t pose_ix=0;pose_ix<number_of_poses;++pose_ix){
        const Iterator pose_end=it+skeleton_size*sizeof(Joint);
        if(pose_end>=it_end) return false;
        out.transforms().emplace_back();
        it=dk::deserialize_cont(out.transforms().back(),it,pose_end);
    }

    //dedicated variable is used for readability
    const size_t last_sm_ix=submodels_total-1;

    for(size_t lod=0,sm_ix=0;lod<lod_count;++lod){
        for(Animated_submodel_data & sm: out.lod(lod)){

            //reading vertices
            const Iterator vertices_end=it+vertices_size[sm_ix];
            if(vertices_end>=it_end) return false;
            it=dk::deserialize_cont(sm.mesh.vertices,it,vertices_end);

            //reading indices
            const Iterator indices_end=it+indices_size[sm_ix];

            if(indices_end>it_end) return false;
            if((sm_ix!=last_sm_ix && indices_end==it_end))return false;

            it=dk::deserialize_cont(sm.mesh.indices,it,indices_end);

            sm.type=Material_type::NORMAL;
            ++sm_ix;
        }
    }

    return true;
}


String serialize(const Animated_model_data & in)
{
    String ret;
    std::ostringstream stream;
    //writing boundary box: six real numbers enclosed in double quotes
    {
        AABB const& bndr=in.boundary();
        stream<<'"'<<bndr.min.x<<' '<<bndr.min.y<<' '<<bndr.min.z<<' '
             <<bndr.max.x<<' '<<bndr.max.y<<' '<<bndr.max.z<<"\"\n";
    }
    //writing number of LODs: integer
    stream<<in.lod_count()<<"\n";

    size_t submodels_total=0;
    for(size_t ix=0;ix<in.lod_count();++ix){
        //writing size of each LOD: integer
        const size_t lod_size=in.lod(ix).size();
        submodels_total+=lod_size;
        stream<<lod_size<<' ';

    }
    stream<<'\n';


    if(in.transforms().empty()){
        log_msg(Detail_level::NON_CRITICAL,"mesh serialization error: inverse bind pose transforms' container is empty");
        return ret;
    }
    const size_t skeleton_size=in.transforms().front().size();
    const size_t number_of_poses=in.transforms().size();

    for(auto& ibp: in.transforms()){
        if(ibp.size()!=skeleton_size){
            log_msg(Detail_level::NON_CRITICAL,"mesh serialization error: inverse bind poses have different skeleton size");
            return ret;
        }
    }

    stream<<'\n';
    //writing number of inversed bind pose transforms' containers: integer
    stream<<number_of_poses<<'\n';
    //writing skeleton size: integer
    stream<<skeleton_size<<'\n';
    stream<<'\n';

    //writing inverse bind pose index for each submodel
    for(size_t ix=0;ix<in.lod_count();++ix){
        for(Animated_model_data::LOD::Value const& sm: in.lod(ix)){
            stream<<sm.mesh.transform_ix<<" ";
        }
    }
    stream<<"\n";

    //writing material key for each submodel
    for(size_t ix=0;ix<in.lod_count();++ix){
        for(Animated_model_data::LOD::Value const& sm: in.lod(ix)){
            stream<<'"'<<sm.material<<"\" ";
        }
    }
    stream<<"\n";

    //containers to store binary size of vertices data and indices data for each submodel
    std::vector<u64> vertices_size(submodels_total);
    std::vector<u64> indices_size(submodels_total);

    //total size of binary data
    u64 bin_data_size=number_of_poses*skeleton_size*sizeof(Joint);

    for(size_t lod=0,sm_ix=0;lod<in.lod_count();++lod){
        for(Animated_submodel_data const& sm: in.lod(lod)){
            vertices_size[sm_ix]=sm.mesh.vertices.size()*sizeof(Animated_mesh::Vertices::value_type);
            indices_size[sm_ix]=sm.mesh.indices.size()*sizeof(Animated_mesh::Indices::value_type);
            bin_data_size += indices_size[sm_ix]+vertices_size[sm_ix];
            ++sm_ix;
        }
    }

    if(stream.fail()){
        log_msg(Detail_level::NON_CRITICAL,"mesh serialization error, stream fail");
        return ret;
    }

    //writing binary size of vertices for each submodel
    for(u64 binary_size: vertices_size){
        stream<<binary_size<<' ';
    }
    stream<<'\n';

    //writing binary size of indices for each submodel
    for(u64 binary_size: indices_size){
        stream<<binary_size<<' ';
    }
    stream<<'\n';
    stream<<'\n';
    stream<<"binary:";

    if(stream.fail()){
        log_msg(Detail_level::NON_CRITICAL,"mesh serialization error, stream fail");
        return ret;
    }
    ret=stream.str();
    const size_t binary_start=ret.size();
    const size_t overall_size=bin_data_size+binary_start;

    ret.resize(overall_size);

    auto it=std::next(ret.begin(),binary_start);
    auto end=ret.end();

    for(size_t pose_ix=0;pose_ix<number_of_poses; ++pose_ix){
        assert(it!=end);
        it=dk::serialize_cont(in.transforms()[pose_ix],it,end);
    }
    for(size_t lod=0,sm_ix=0;lod<in.lod_count();++lod){
        for(Animated_submodel_data const& sm: in.lod(lod)){
            assert(it!=end);
            it=dk::serialize_cont(sm.mesh.vertices,it,end);
            assert(it!=end);
            it=dk::serialize_cont(sm.mesh.indices,it,end);
            ++sm_ix;
        }
    }
    assert(it==end);
    return ret;

}


bool parse_resource(const char * str_in, size_t str_len, Animation_data & out)
{
    u32 next_size=0;
    const char * it=str_in;
    const char * end=str_in+str_len;
    const char * block_end=it;
    assert(it+sizeof(Animation_channel::Time)+sizeof(u32)*3<=end);

    it=dk::deserialize_pod(out.duration,it,end);

    for(;it<end;){
        Animation_channel ch;
        {
            it=dk::deserialize_pod(next_size,it,it+sizeof(next_size));
            if(it>=end)return false;
            block_end=it+sizeof(Animation_channel::Rotation)*next_size;
            if(block_end>end){
                log_msg(Detail_level::NON_CRITICAL,"unexpected end of data when parsing animation data");
                return false;
            }
            ch.rotations.reserve(next_size);
            it=dk::deserialize_cont(ch.rotations,it,block_end);
            if(it>=end)return false;
        }
        {
            it=dk::deserialize_pod(next_size,it,it+sizeof(next_size));
            if(it>=end)return false;
            block_end=it+sizeof(Animation_channel::Translation)*next_size;
            if(block_end>end){
                log_msg(Detail_level::NON_CRITICAL,"unexpected end of data when parsing animation data");
                return false;
            }
            ch.translations.reserve(next_size);
            it=dk::deserialize_cont(ch.translations,it,block_end);
            if(it>=end)return false;
        }
        {
            it=dk::deserialize_pod(next_size,it,it+sizeof(next_size));
            if(it>=end)return false;
            block_end=it+sizeof(Animation_channel::Scale)*next_size;
            if(block_end>end){
                log_msg(Detail_level::NON_CRITICAL,"unexpected end of data when parsing animation data");
                return false;
            }
            ch.scales.reserve(next_size);
            it=dk::deserialize_cont(ch.scales,it,block_end);
        }
        out.channels.emplace_back(std::move(ch));
    }
    out.channels.shrink_to_fit();
    if(it!=end){
        log_msg(Detail_level::NON_CRITICAL,"it!=end when finish parsing animation data (this is madness!)");
    }
    return true;
}


String serialize(const Animation_data & in)
{
    size_t total_size=sizeof(Animation_channel::Time);
    for(Animation_channel const& ch : in.channels){

        total_size+=ch.rotations.size()*sizeof(Animation_channel::Rotation);
        total_size+=ch.translations.size()*sizeof(Animation_channel::Translation);
        total_size+=ch.scales.size()*sizeof(Animation_channel::Scale);

        total_size+=3*sizeof(u32);
    }
    String ret;
    ret.resize(total_size);
    auto it=ret.begin();
    auto end=ret.end();

    it=dk::serialize_pod(in.duration,it,end);

    for(Animation_channel const& ch : in.channels){

        u32 next_size=ch.rotations.size();
        it=dk::serialize_pod(next_size,it,end);
        it=dk::serialize_cont(ch.rotations,it,end);

        next_size=ch.translations.size();
        it=dk::serialize_pod(next_size,it,end);
        it=dk::serialize_cont(ch.translations,it,end);

        next_size=ch.scales.size();
        it=dk::serialize_pod(next_size,it,end);
        it=dk::serialize_cont(ch.scales,it,end);
    }
    return ret;
}


/*
 * "s_name" "f_r.x f_r.y f_r.z f_r.w f_d.x ... f_d.w" u_index u_parent *
 */
String serialize(const Skeleton_info & in)
{
    String ret;
    auto& joints=in.joints;
    bool correct=true;
    std::vector<size_t> counts;
    counts.resize(joints.size());
    for(size_t i=0;i<joints.size();++i){
        Skeleton_info::JointInfo const& j=joints[i];
        if(j.index!=i){
            correct=false;
        }
        if(j.index>=joints.size()){
            log_msg(Detail_level::NON_CRITICAL,"joint %1% index %2% out range (%3%) when serializing skeleton",j.name,j.index,joints.size());
            return ret;
        }
        if(counts[j.index]){
            log_msg(Detail_level::NON_CRITICAL,"joint %1% index %2% dublicated when serializing skeleton",j.name,j.index);
            return ret;
        }
        if(j.index<j.parent){
            log_msg(Detail_level::NON_CRITICAL,"joint %1% index %2% less than index of parent %3%",j.name,j.index,j.parent);
            return ret;
        }
        ++counts[j.index];
    }
    std::vector<Skeleton_info::JointInfo> new_joints;
    std::vector<Skeleton_info::JointInfo>const* joints_ptr=&joints;
    if(!correct){
        new_joints.resize(joints.size());
        for(size_t i=0;i<joints.size();++i){
            Skeleton_info::JointInfo const& j=joints[i];
            new_joints[j.index]=j;
        }
        joints_ptr=&new_joints;
    }

    for(size_t i=0;i<joints_ptr->size();++i){
        Skeleton_info::JointInfo const& j=(*joints_ptr)[i];
        auto& q=j.binding_pose.real;
        auto& d=j.binding_pose.dual;
        ret+=dk::fstr("\"%1%\" \"%2% %3% %4% %5% %6% %7% %8% %9%\" %10% %11% \n",j.name,q.x,q.y,q.z,q.w,d.x,d.y,d.z,d.w,j.index,j.parent);
    }
    return ret;

}


bool parse_resource(const char * str_in, size_t str_len, Static_model_data & out)
{
    size_t i=0;
    {
        String boundary_str;
        skip_spaces(str_in,str_len,i);
        if(i>=str_len) return false;
        boundary_str=get_string(str_in,str_len,i);
        if(i>=str_len) return false;
        glm::vec3 v[2];
        const char * rptr=boundary_str.c_str();
        const char * end=rptr+boundary_str.size();
        for(size_t p=0;p<2;++p){
            for(size_t j=0;j<3 && rptr<end;++j){
                const char * tmp=rptr;
                v[p][j]=read_float(rptr,&tmp);
                rptr=tmp;
            }
        }
        out.boundary(AABB{v[0],v[1]});
    }
    size_t lod_count=read_natural(str_in,i);
    if(i>=str_len) return false;
    size_t submodels_total=0;
    std::vector<size_t> lod_sizes(lod_count);

    for(size_t lod_ix=0 ; lod_ix<lod_count ; ++lod_ix){
        size_t part_count=read_natural(str_in,i);
        if(i>=str_len) return false;
        lod_sizes[lod_ix]=part_count;
        submodels_total+=part_count;
    }

    out.init(lod_count,lod_sizes.data());

    std::vector<Material_key> keys(submodels_total);
    for(size_t sm=0;sm<submodels_total;++sm){
        skip_spaces(str_in,str_len,i);
        if(i>=str_len) return false;
        keys[sm]=get_string(str_in,str_len,i);
        if(i>=str_len) return false;
    }

    std::vector<size_t> vertices_begin(submodels_total);
    std::vector<size_t> vertices_end(submodels_total);

    std::vector<size_t> indices_begin(submodels_total);
    std::vector<size_t> indices_end(submodels_total);

    for(size_t sm=0;sm<submodels_total;++sm){
        vertices_begin[sm]=read_natural(str_in,i);
        if(i>=str_len) return false;
        vertices_end[sm]=read_natural(str_in,i);
        if(i>=str_len) return false;
    }
    for(size_t sm=0;sm<submodels_total;++sm){
        indices_begin[sm]=read_natural(str_in,i);
        if(i>=str_len) return false;
        indices_end[sm]=read_natural(str_in,i);
        if(i>=str_len) return false;
    }

    /*
     * place to add material type
     */

    for(size_t lod=0,sm_ix=0;lod<lod_count;++lod){
        for(Static_submodel_data & sm: out.lod(lod)){
            const char * vx_begin=str_in+vertices_begin[sm_ix];
            const char * vx_end=str_in+vertices_end[sm_ix];
            const char * ix_begin=str_in+indices_begin[sm_ix];
            const char * ix_end=str_in+indices_end[sm_ix];
            dk::deserialize_cont(sm.mesh.vertices,vx_begin,vx_end);
            dk::deserialize_cont(sm.mesh.indices,ix_begin,ix_end);
            sm.material=std::move(keys[sm_ix]);
            sm.type=Material_type::NORMAL;
            ++sm_ix;
        }
    }

    return true;
}


String serialize(const Static_model_data & in)
{
    String ret;
    std::ostringstream stream;
    {
        AABB const& bndr=in.boundary();
        stream<<'"'<<bndr.min.x<<' '<<bndr.min.y<<' '<<bndr.min.z<<' '
             <<bndr.max.x<<' '<<bndr.max.y<<' '<<bndr.max.z<<"\"\n";
    }
    stream<<in.lod_count()<<"\n";
    for(size_t ix=0;ix<in.lod_count();++ix){
        stream<<in.lod(ix).size()<<' ';
    }
    stream<<'\n';
    size_t submodels_total=0;
    for(size_t ix=0;ix<in.lod_count();++ix){
        for(Static_model_data::LOD::Value const& sm: in.lod(ix)){
            stream<<'"'<<sm.material<<"\" ";
            ++submodels_total;
        }
    }
    stream<<"\n";
    std::vector<u64> vertices_begin(submodels_total);
    std::vector<u64> vertices_end(submodels_total);

    std::vector<u64> indices_begin(submodels_total);
    std::vector<u64> indices_end(submodels_total);

    u64 bin_data_size=0;

    for(size_t lod=0,sm_ix=0;lod<in.lod_count();++lod){
        for(Static_submodel_data const& sm: in.lod(lod)){

            vertices_begin[sm_ix]=bin_data_size;
            bin_data_size+=sm.mesh.vertices.size()*sizeof(Animated_mesh::Vertices::value_type);
            vertices_end[sm_ix]=bin_data_size;

            indices_begin[sm_ix]=bin_data_size;
            bin_data_size+=sm.mesh.indices.size()*sizeof(Animated_mesh::Indices::value_type);
            indices_end[sm_ix]=bin_data_size;

            ++sm_ix;
        }
    }
    if(stream.fail()){
        log_msg(Detail_level::NON_CRITICAL,"mesh serialization error, stream fail");
        return ret;
    }
    auto pos=stream.tellp();
    if(pos<0){
        pos=0;
    }
    u64 max_num_length=1;

    const u64 max_offset=(u64)pos + (20+1)*submodels_total*4+2+ bin_data_size;
    if(max_offset<10000000000000000000ull){
        u64 t=10;
        while(max_offset>=t){
            t*=10;
            ++max_num_length;
        }
    }else{
        max_num_length=20;
    }

    const u64 base_position=(u64)pos + (max_num_length+1)*submodels_total*4+2;
    const u64 overall=base_position+bin_data_size;

    for(size_t sm_ix=0;sm_ix<submodels_total;++sm_ix){
        vertices_begin[sm_ix]+=base_position;
        vertices_end[sm_ix]+=base_position;
        indices_begin[sm_ix]+=base_position;
        indices_end[sm_ix]+=base_position;
    }

    for(size_t sm_ix=0;sm_ix<submodels_total;++sm_ix){
        stream<<vertices_begin[sm_ix]<<' '<<vertices_end[sm_ix]<<' ';
    }
    stream<<'\n';

    for(size_t sm_ix=0;sm_ix<submodels_total;++sm_ix){
        stream<<indices_begin[sm_ix]<<' '<<indices_end[sm_ix]<<' ';
    }
    stream<<'\n';

    if(stream.fail()){
        log_msg(Detail_level::NON_CRITICAL,"mesh serialization error, stream fail");
        return ret;
    }
    ret=stream.str();
    ret.resize(overall);

    for(size_t lod=0,sm_ix=0;lod<in.lod_count();++lod){
        for(Static_submodel_data const& sm: in.lod(lod)){
            dk::serialize_cont(sm.mesh.vertices,&ret[vertices_begin[sm_ix]],&ret[vertices_end[sm_ix]]);
            dk::serialize_cont(sm.mesh.indices,&ret[indices_begin[sm_ix]],&ret[indices_end[sm_ix]]);
            ++sm_ix;
        }
    }
    return ret;

}

bool parse_resource(const char * str_in, size_t str_len ,Shader_source & out){
    assert(out.empty());
    out.insert(out.begin(),str_in,str_in+str_len);
    return true;
}

}
}
