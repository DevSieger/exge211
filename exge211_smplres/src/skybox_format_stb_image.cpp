#include "public_headers.h"
#include PUBLIC_HEADER(skybox_format_stb_image.h)

#include <exge211/res/resource_data.h>
#include <exge211/logging.h>
#include <cstring>
#include <memory>

#define STBI_SIMD
#define STBI_FAILURE_USERMSG
#include "../3rd_party/stb_image.h"

namespace exge211 {

bool Skybox_format_stb_image::parse(const char* str_in, size_t str_len, Skybox_format_stb_image::Resource& out)
{
    int width, height, n;
    bool success=false;

    const Cube_map_data::Face faces[6]={
        Cube_map_data::LEFT,
        Cube_map_data::FRONT,
        Cube_map_data::RIGHT,
        Cube_map_data::BACK,
        Cube_map_data::TOP,
        Cube_map_data::BOTTOM
    };

    float* loaded_data = stbi_loadf_from_memory((const stbi_uc *)str_in,str_len, &width, &height, &n, out.data[0].chanels());

    std::unique_ptr<void,decltype(&stbi_image_free)> cleaner(loaded_data,&stbi_image_free);

    if(!loaded_data){
        log_msg(Detail_level::DEBUG,"can't load image, failure reason msg is '%1%'",stbi_failure_reason());
        for(auto& face: out.data){
            if( face.width && face.height){
                //show must go on
                face.resize();
            }
        }
    }else{
        const int side_width = width / 6;

        for(auto& face: out.data){
            face.height = height;
            face.width  = side_width;
            face.resize();
        }

        //data is pixel vector
        const size_t face_pitch     = out.data[0].pixel_size()*side_width;
        const size_t pitch          = face_pitch * 6;
        for(size_t line = 0 ; line < (size_t)height ; ++line){
            const size_t output_start   = face_pitch * line;
            size_t input_start    = pitch * ( height - line - 1);
            for( int i = 0; i<6 ; ++i){
                auto & texture = out[faces[i]];
                memcpy((char*)&texture.data[output_start], (char*)loaded_data + input_start , face_pitch);
                input_start += face_pitch;
            }
        };
        success=true;
    }
    return success;
}

}
