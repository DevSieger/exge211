#include "public_headers.h"
#include PUBLIC_HEADER(file_loader.h)
#include <exge211/logging.h>
#include <fstream>

namespace exge211 {

String File_loader::get(const Resource_request & name)
{
    std::string path=prefix()+name+suffix();
    std::ifstream file(path, std::ios_base::binary);
    String result;
    if(file.is_open()){
        file.seekg(0,std::ios_base::end);
        auto file_size=file.tellg();
        if(file_size>0){
            file.seekg(0,std::ios_base::beg);
            result.resize(file_size);
            file.read(&result[0],file_size);
        }
    }else{
        log_msg(Detail_level::CRITICAL,"can't open file %1%",path);
    }
    return result;
}

}
