#include "public_headers.h"
#include PUBLIC_HEADER(texture_format_stb_image.h)

#include <exge211/res/resource_data.h>
#include <exge211/logging.h>
#include <memory>

#define STBI_SIMD
#define STB_IMAGE_IMPLEMENTATION
#define STBI_FAILURE_USERMSG
#include "../3rd_party/stb_image.h"
#include <string.h>

namespace exge211 {

bool Texture_format_stb_image::parse(const char * str_in, size_t str_len, Resource & out)
{
    int w,h,n;
    bool success=false;

    if(out.format!= Image_format::RGB888 && out.format!= Image_format::RGBA8888)
    {
        log_msg(Detail_level::NON_CRITICAL,"unsupported image format in stb texture loading");
        return false;
    }

    stbi_uc* data=stbi_load_from_memory((const stbi_uc *)str_in,str_len, &w, &h, &n, out.chanels());

    auto deleter=[](stbi_uc* ptr)-> void { if(ptr) stbi_image_free(ptr); };
    std::unique_ptr<stbi_uc,decltype(deleter)> cleaner(data,deleter);

    if(!data){
        log_msg(Detail_level::DEBUG,"can't load image, failure reason msg is '%1%'",stbi_failure_reason());
        if( out.width && out.height){
            //show must go on
            out.resize();
        }
    }else{
        if(!out.width || !out.height){
            out.height=h;
            out.width=w;
        }
        out.resize();
        if(static_cast<Texture_config::Size_value>(w)==out.width && static_cast<Texture_config::Size_value>(h)==out.height){
            //data is pixel vector
            const size_t line_size=out.pixel_size()*out.width;
            for(size_t line=0;line<out.height;++line){
                const size_t output_start=line_size*line;
                const size_t input_start=line_size*(out.height-line-1);
                memcpy(&out.data[output_start],data+input_start,line_size);
            };
            success=true;
        }else{
            log_msg(Detail_level::DEBUG,"wrong image size: %1% x %2%, required: %3% x %4%",w,h,out.width,out.height);
        }
    }
    return success;
}

}
