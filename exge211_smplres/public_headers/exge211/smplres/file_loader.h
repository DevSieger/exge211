#pragma once

#include "defines.h"
#include <exge211/common_types.h>

namespace exge211 {

/// A loader to use with exge211::Smpl_resource_loader
class EXGE211_SMPLRES_EXPORT File_loader
{
public:
    /// Sets a prefix for a requested name.
    void prefix(String const& prefix){
        prefix_=prefix;
    }
    /// Sets a suffix for a requested names.
    void suffix(String const& a_suffix){
        suffix_=a_suffix;
    }
    /// Returns the prefix for requested names.
    String const & prefix()const{
        return prefix_;
    }
    /// Returns the suffix for requested names.
    String const & suffix()const{
        return suffix_;
    }
protected:
    /// \brief Returns a content of the file with the given name.
    /// \param name The given name.
    /// \return The file content.
    /// \details The path to the file with the given name is "prefix() + name + suffix()"
    String get(Resource_request const & name);

private:
    String prefix_="./";
    String suffix_;
};

}
