#pragma once

#if defined(_WIN32) && !defined(EXGE211_SMPLRES_STATIC)

#ifdef EXGE211_SMPLRES_EXPORTING
#define EXGE211_SMPLRES_EXPORT __declspec(dllexport)
#else
#define EXGE211_SMPLRES_EXPORT __declspec(dllimport)
#endif //#ifdef EXGE211_SMPLRES_EXPORTING

#else
#define EXGE211_SMPLRES_EXPORT
#endif
