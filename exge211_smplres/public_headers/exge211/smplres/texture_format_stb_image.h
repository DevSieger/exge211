#pragma once

#include "defines.h"
#include <exge211/res/rs_fwd.h>
#include <exge211/common_types.h>

namespace exge211 {

/// \brief A parser to use with exge211::Smpl_resource_loader
/// \details The implementation uses stb_image function from https://github.com/nothings/stb
class EXGE211_SMPLRES_EXPORT Texture_format_stb_image
{
public:
    using Resource=Texture_data;
    bool parse(const char* str_in,size_t str_len, Resource & out);
};

}
