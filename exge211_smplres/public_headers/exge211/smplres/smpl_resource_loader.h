#pragma once

#include "defines.h"
#include <exge211/common_types.h>
#include <exge211/logging.h>
#include "default_format.h"
#include "texture_format_stb_image.h"
#include "skybox_format_stb_image.h"

namespace exge211 {

/// Type traits for getting a parsing function.
template<typename Resource_type>
struct Default_format{
    using Resource=Resource_type;
    using Parsing_function=bool(*)(const char* str_in,size_t str_len,Resource & out);
    //may be not the best way
    constexpr static  Parsing_function parse =&default_format::parse_resource;
};

/// Texture loading is the special case.
template<>
struct Default_format<Texture_data>: public Texture_format_stb_image{
    using Texture_format_stb_image::Texture_format_stb_image;
};
template<>
struct Default_format<Cube_map_data>: public Skybox_format_stb_image{
    using Skybox_format_stb_image::Skybox_format_stb_image;
};

/*!
 * \brief A composite loader to use with the member functors of exge211::Resource_loader.
 *
 * \tparam Resource_type The type of a resource to load.
 *
 * \tparam Loader_base
 * The type that is responsible for a loading of the content.
 * This type has to have a 'get' method with the following signature: "exge211::String (exge211::Resource_request const &)", which returns the content.
 * See exge211::File_loader for an example of implementation.
 *
 * \tparam Format_base
 * The type that is responsible for a parsing of the loaded content.
 * This type has to have a 'parse' method with the following signature: "bool (const char* str_in, size_t str_len, Resource_type & out)", which returns false if the parsing fails.
 */
template< typename Resource_type,typename Loader_base, typename Format_base=Default_format<Resource_type> >
class Smpl_resource_loader:public Format_base, public Loader_base
{
public:
    using Format=Format_base;
    using Format::parse;
    using Loader_base::get;

    void operator ()(Resource_request const & name, Resource_type & out){
        String content=get(name);
        if(!parse(content.c_str(),content.size(),out)){
            log_msg(Detail_level::NON_CRITICAL,"can't parse resource '%1%'",name);
        }
    }
};

}
