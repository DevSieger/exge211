#pragma once

#include <exge211/res/rs_fwd.h>
#include <exge211/common_types.h>
#include "defines.h"

/*! \file
 * This file contains self explaining parsing/serialization functions.
 * The exge211::Default_format template uses parsing functions.
 * Parsing functions return false if parsing fails.
 */

namespace exge211 {

namespace default_format {

bool EXGE211_SMPLRES_EXPORT parse_resource(const char * str_in, size_t str_len ,Skin_info & out);
bool EXGE211_SMPLRES_EXPORT parse_resource(const char * str_in, size_t str_len ,Material_info & out);
bool EXGE211_SMPLRES_EXPORT parse_resource(const char * str_in, size_t str_len ,Skeleton_info & out);
bool EXGE211_SMPLRES_EXPORT parse_resource(const char * str_in, size_t str_len ,Animated_model_data & out);
bool EXGE211_SMPLRES_EXPORT parse_resource(const char * str_in, size_t str_len ,Animation_data & out);
bool EXGE211_SMPLRES_EXPORT parse_resource(const char * str_in, size_t str_len ,Static_model_data & out);
bool EXGE211_SMPLRES_EXPORT parse_resource(const char * str_in, size_t str_len ,Shader_source & out);
String EXGE211_SMPLRES_EXPORT serialize(Animated_model_data const & in);
String EXGE211_SMPLRES_EXPORT serialize(Static_model_data const & in);
String EXGE211_SMPLRES_EXPORT serialize(Animation_data const & in);
String EXGE211_SMPLRES_EXPORT serialize(Skeleton_info const & in);

}

}
