#pragma once

#include "defines.h"
#include <exge211/res/rs_fwd.h>
#include <exge211/common_types.h>

namespace exge211 {

class EXGE211_SMPLRES_EXPORT Skybox_format_stb_image{
public:
    using Resource=Cube_map_data;
    bool parse(const char* str_in,size_t str_len, Resource & out);
};

}
