/*
 * This module is not a part of the original project,
 * it was written with a lack of time for a sole purpose of engine development.
 * The code of this module is considered TERRIBLE, but it still works well.
 */

#include "exporter.h"
#include <assimp/postprocess.h>
#include <fstream>
#include <exge211/smplres/default_format.h>
#include <glm/gtx/matrix_decompose.hpp>
#include <cctype>

using glm::vec3;
using glm::vec4;
using glm::mat4x4;
using glm::quat;
using glm::dualquat;
using exge211::String;
using exge211::Animated_model_data;
using exge211::Static_model_data;
using exge211::default_format::serialize;
using exge211::default_format::parse_resource;
using exge211::Skeleton_info;
using exge211::Animation_data;
using exge211::Animated_submodel_data;
using exge211::Static_submodel_data;


//various utility functions
namespace{

void rotate(float * v_out, const aiMatrix4x4 & m){
    glm::vec3 t;
    for(unsigned i=0;i<3;++i){
        for(unsigned j=0;j<3;++j){
            t[i]+=m[i][j]*v_out[j];
        }
    }
    for(unsigned i=0;i<3;++i){
        v_out[i]=t[i];
    }
}

void translate(float * v_out, const aiMatrix4x4 & m){
    for(unsigned i=0;i<3;++i){
        v_out[i]+=m[i][3];
    }
}

void get(float *out,aiVector2D const& v){
    out[0]=v[0];
    out[1]=v[1];
}
void get(float *out,aiVector3D const& v){
    out[0]=v[0];
    out[1]=v[1];
    out[2]=v[2];
}
void get(glm::quat & out,aiQuaternion const& v){
    out.x=v.x;
    out.y=v.y;
    out.z=v.z;
    out.w=v.w;
}

void skip_spaces(const char * str, size_t str_len,size_t & i){
    for(;i<str_len;++i){
        if(!std::isspace(str[i])){
            return;
        }
    }
}

String get_string(const char * str, size_t str_len,size_t & i){
    String ret;
    assert(i<str_len);
    if(str[i]!='"'){
        log_msg(exge211::Detail_level::CRITICAL,"ill formed string in %1% : %2%; opening quotation mark was expected, but %3% was recieved",__FILE__,__LINE__,str[i]);
        while(++i<str_len){
            if(str[i]=='"'){
                break;
            }
        }
    }
    for(++i ;i<str_len; ++i){
        if(str[i]!='"'){
            ret.push_back(str[i]);
        }else{
            break;
        }
    }
    if(i>=str_len){
        log_msg(exge211::Detail_level::CRITICAL,"ill formed string in %1% : %2%; closing quotation mark was expected, but wasn't recieved",__FILE__,__LINE__,str[i]);
    }else{
        ++i;
    }
    return ret;
}

glm::dualquat matrix_to_dual(const aiMatrix4x4 & in)
{
    aiVector3D va;
    vec3 tr;
    quat rot;
    aiQuaternion q;
    in.DecomposeNoScaling(q,va);
    get(&tr[0],va);
    get(rot,q);
    return glm::dualquat(rot,tr);
}

template<typename Functor>
bool scene_bfs(aiNode* start,Functor const& func){
    std::vector<aiNode*> w_cur;
    std::vector<aiNode*> w_next={start};

    while(!w_next.empty()){
        w_cur.swap(w_next);
        while(!w_cur.empty()){
            aiNode* cur=w_cur.back();
            w_cur.pop_back();
            if(func(cur)){
                return true;
            }
            for(size_t i=0 ; i<cur->mNumChildren ; ++i){
                w_next.emplace_back(cur->mChildren[i]);
            }
        }
    }
    return false;
}

glm::mat4x4 convert(const aiMatrix4x4 & in){
    glm::mat4x4 ret;
    for(int i=0;i<4;++i){
        for(int j=0;j<4;++j){
            ret[i][j]=in[j][i];
        }
    }
    return ret;
}
std::ostream& operator<<(std::ostream & str,glm::vec4 const& v){
    return str<<"{"<<v.x<<","<<v.y<<","<<v.z<<","<<v.w<<"}";
}

std::ostream& operator<<(std::ostream & str,vec3 const& v){
    return str<<"{"<<v.x<<","<<v.y<<","<<v.z<<"}";
}

std::ostream& operator<<(std::ostream & str,aiVector3D const& v){
    return str<<"{"<<v.x<<","<<v.y<<","<<v.z<<"}";
}

vec3 operator* (glm::mat4x4 const&m, vec3 v){
    vec4 tr=m*vec4(v,1);
    return {tr.x/tr.w,tr.y/tr.w,tr.z/tr.w};
}

String get_file(const String & path)
{
    String res;
    std::ifstream file(path,std::ios_base::binary);
    if(file.is_open()){
        file.seekg(0,std::ios_base::end);
        size_t size=file.tellg();
        file.seekg(0,std::ios_base::beg);
        res.resize(size);
        file.read(&res[0],size);
    }else{
        log_msg(exge211::Detail_level::NON_CRITICAL,"can't open file %1%",path);
    }
    return res;
}

void write_file(const String & path, String content)
{
    std::ofstream file(path,std::ios_base::trunc|std::ios_base::binary);
    if(file.is_open()){
        file<<content;
        file.close();
    }else{
        log_msg(exge211::Detail_level::CRITICAL,"can't open file %1% for writing",path);
    }
}

exge211::AABB get_boundary(Static_model_data const& model){
    exge211::AABB boundary;
    for(Static_model_data::Index lod_ix=0,lod_ix_end=model.lod_count(); lod_ix<lod_ix_end; ++lod_ix){
        for(Static_submodel_data const& sm: model.lod(lod_ix)){
            for(exge211::Static_vertex const& v: sm.mesh.vertices){
                boundary.add(v.pos);
            }
        }
    }
    return boundary;
}

}

Exporter::Exporter(){
    importer_.SetPropertyInteger(AI_CONFIG_PP_RVC_FLAGS,aiComponent_COLORS|aiComponent_LIGHTS|aiComponent_TEXTURES|aiComponent_CAMERAS);
    importer_.SetPropertyInteger(AI_CONFIG_PP_SBP_REMOVE,aiPrimitiveType_LINE|aiPrimitiveType_POINT);
}

void Exporter::load_file(const String & path){
    scene_=importer_.ReadFile(path,
                              aiProcess_CalcTangentSpace | aiProcess_GenSmoothNormals | aiProcess_JoinIdenticalVertices | aiProcess_Triangulate |
                              aiProcess_RemoveComponent | aiProcess_LimitBoneWeights | aiProcess_SortByPType | aiProcess_OptimizeMeshes | aiProcess_FixInfacingNormals);
}

void Exporter::load_static_model_file(const String & path)
{
    scene_=importer_.ReadFile(path,
                              aiProcess_CalcTangentSpace | aiProcess_GenSmoothNormals | aiProcess_JoinIdenticalVertices | aiProcess_Triangulate |
                              aiProcess_RemoveComponent | aiProcess_SortByPType | aiProcess_OptimizeMeshes | aiProcess_FixInfacingNormals |
                              aiProcess_Debone | aiProcess_PreTransformVertices);
}

void Exporter::export_animated_model( String const& path, exge211::AABB const& boundary){

    if(!scene_check())return;
    if(skeleton_.joints.empty()){
        log_msg(exge211::Detail_level::CRITICAL,"trying to export model when skeleton not loaded");
        return;
    }
    Animated_model_data model=get_animated_model();
    model.boundary(boundary);
    String content=serialize(model);
    write_file(path,content);
}

void Exporter::export_static_model(const String & path)
{
    if(!scene_check())return;
    Static_model_data model=get_static_model();
    String content=serialize(model);
    write_file(path,content);
}

void Exporter::export_skeleton(const String & path, const String & root_hint)
{
    if(!scene_check())return;
    aiString hint(root_hint);
    aiNode * root=scene_->mRootNode->FindNode(hint);
    if(!root){
        root=scene_->mRootNode;
    }
    Skeleton_info skel=get_skeleton(root);
    String cont=serialize(skel);
    write_file(path,cont);
}

void Exporter::export_animation(const String & path, size_t index)
{
    if(!scene_check())return;
    if(skeleton_.joints.empty()){
        log_msg(exge211::Detail_level::CRITICAL,"trying to export animation when skeleton not loaded");
        return;
    }
    if(index>scene_->mNumAnimations){
        log_msg(exge211::Detail_level::CRITICAL,"animation index %1% out of range, max index %2%",index,scene_->mNumAnimations-1);
        return;
    }
    Animation_data adata=get_animation(scene_->mAnimations[index]);
    String content=serialize(adata);
    write_file(path,content);
}

size_t Exporter::animation_count() const
{
    assert(scene_);
    return scene_->mNumAnimations;
}

void Exporter::load_skeleton(const String & path)
{
    String content=get_file(path);
    skeleton_.joints.clear();
    parse_resource(content.c_str(),content.size(),skeleton_);
    joint_map_.clear();
    Mapping inv_joint_mpng;
    for(auto&p: joint_mapping_){
        auto it=inv_joint_mpng.emplace(p.second,p.first);
        if(!it.second){
            log_msg(exge211::Detail_level::NON_CRITICAL,"value dublicated in joint mapping");
        }
    }
    for(Skeleton_info::JointInfo& ji: skeleton_.joints){
        joint_map_.emplace(map(ji.name,inv_joint_mpng),ji.index);
    }
}

Animated_model_data Exporter::get_animated_model() const{
    assert(scene_);
    assert(scene_->HasMeshes());
    Animated_model_data data;

    std::vector<Animated_submodel_data> submodels;
    struct Entry{
        aiMatrix4x4 parent_transform;
        aiNode* node;
    };

    std::vector<Entry> stack={
        {
            aiMatrix4x4(),
            scene_->mRootNode
        }
    };
    Usage_masks joint_usages;
    while(!stack.empty()){
        Entry current=stack.back();
        stack.pop_back();

        // The documentation of assimp is not clear enough for me.
        // I can't understand which coordinate system is used to define vertex positions.
        // Looks like it should be a local one, but that just doesn't work.
        aiMatrix4x4 transform;//=current.parent_transform*current.node->mTransformation;
        for(unsigned i=0;i<current.node->mNumMeshes;++i){
            Animated_submodel_data sm=get_animated_submodel(current.node->mMeshes[i],data.transforms(),joint_usages);
            for(exge211::Animated_vertex& v:sm.mesh.vertices){
                rotate(v.pos,transform);
                translate(v.pos,transform);
                rotate(v.t,transform);
                rotate(v.n,transform);
                rotate(v.b,transform);
            }
            submodels.emplace_back(std::move(sm));
        }
        for(unsigned i=0;i<current.node->mNumChildren;++i){
            stack.emplace_back(Entry{transform,current.node->mChildren[i]});
        }
    }

    size_t lod_size=submodels.size();
    data.init(1,&lod_size);
    Animated_model_data::LOD & lod=data.lod(1);

    for(size_t i=0;i<submodels.size();++i){
        lod[i]=std::move(submodels[i]);
    }
    return data;
}

Static_model_data Exporter::get_static_model() const
{
    assert(scene_);
    assert(scene_->HasMeshes());
    Static_model_data data;

    std::vector<Static_submodel_data> submodels;
    struct Entry{
        aiMatrix4x4 parent_transform;
        aiNode* node;
    };

    std::vector<Entry> stack={Entry{aiMatrix4x4(),scene_->mRootNode}};
    while(!stack.empty()){
        Entry current=stack.back();
        stack.pop_back();

        // The documentation of assimp is not clear enough for me.
        // I can't understand which coordinate system is used to define vertex positions.
        // Looks like it should be a local one, but that just doesn't work.
        aiMatrix4x4 transform=current.parent_transform*current.node->mTransformation;
        for(unsigned i=0;i<current.node->mNumMeshes;++i){
            Static_submodel_data sm=get_static_submodel(current.node->mMeshes[i]);
            /*for(StaticVertex& v:sm.mesh.vertices){
                            rotate(&v.pos[0],transform);
                            translate(&v.pos[0],transform);
                            rotate(&v.t[0],transform);
                            rotate(&v.n[0],transform);
                            rotate(&v.b[0],transform);
                        }*/
            submodels.emplace_back(std::move(sm));
        }
        for(unsigned i=0;i<current.node->mNumChildren;++i){
            stack.emplace_back(Entry{transform,current.node->mChildren[i]});
        }
    }

    size_t lod_size=submodels.size();
    data.init(1,&lod_size);
    Static_model_data::LOD & lod=data.lod(1);

    for(size_t i=0;i<submodels.size();++i){
        lod[i]=std::move(submodels[i]);
    }

    data.boundary(get_boundary(data));

    return data;
}

exge211::Skeleton_info Exporter::get_skeleton(aiNode * root) const
{
    Skeleton_info ret;
    auto& joints=ret.joints;
    Joint current_ix=0;
    struct Entry{
        aiNode* node;
        Joint parent;
    };

    std::vector<Entry> w_cur;
    std::vector<Entry> w_next={{root,0}};

    //the BFS using two stacks.
    while(!w_next.empty()){
        w_cur.swap(w_next);
        while(!w_cur.empty()){
            Entry cur=w_cur.back();
            w_cur.pop_back();
            Skeleton_info::JointInfo joint;

            joint.name=map_joint(cur.node->mName.C_Str());
            joint.index=current_ix;
            joint.parent=cur.parent;
            {
                joint.binding_pose=matrix_to_dual(cur.node->mTransformation);
            }
            joints.push_back(joint);

            for(size_t i=0 ; i<cur.node->mNumChildren ; ++i){
                w_next.emplace_back(Entry{cur.node->mChildren[i],current_ix});
            }
            ++current_ix;
        }
    }
    return ret;
}

bool Exporter::scene_check() const
{
    if(!scene_){
        log_msg(exge211::Detail_level::CRITICAL,"attempt to export when scene not loaded");
        return false;
    }
    return true;
}

Animated_submodel_data Exporter::get_animated_submodel(unsigned mesh_ix, Animated_model_data::Transforms_cont& inv_bind_poses, Usage_masks& masks) const{
    assert(mesh_ix<scene_->mNumMeshes);
    aiMesh *mesh=scene_->mMeshes[mesh_ix];
    assert(mesh->HasNormals());
    assert(mesh->HasTangentsAndBitangents());
    assert(inv_bind_poses.size()==masks.size());
    Animated_submodel_data r;
    if(skeleton_.joints.empty()){
        log_msg(exge211::Detail_level::CRITICAL,"trying to get submodel when skeleton not loaded");
        return r;
    }
    exge211::Animated_mesh::Vertices & vert=r.mesh.vertices;
    const size_t skeleton_size=skeleton_.joints.size();
    Animated_model_data::Transforms transforms((skeleton_size));

    //if a mesh doesn't use a bone, we can set the bone to any value to reduce the number of the bind poses.
    //this variable is used to do that.
    Usage_mask joint_usage(skeleton_size,false);
    std::vector<Byte> last_joint;
    r.material=get_material_key(mesh->mMaterialIndex);
    r.type=exge211::Material_type::NORMAL;

    vert.resize(mesh->mNumVertices);
    last_joint.resize(mesh->mNumVertices,0);

    for(unsigned vi=0;vi<mesh->mNumVertices;++vi){
        exge211::Animated_vertex & v=vert[vi];
        get(v.pos,mesh->mVertices[vi]);
        get(v.n,mesh->mNormals[vi].Normalize());
        get(v.t,mesh->mTangents[vi].Normalize());
        get(v.b,mesh->mBitangents[vi].Normalize());
        v.uv[0]=mesh->mTextureCoords[0][vi][0];
        v.uv[1]=mesh->mTextureCoords[0][vi][1];
        for(int i=0;i<4;++i){
            v.weight[i]=0;
            v.joint[i]=0;
        }
    }
    for(unsigned j=0;j<mesh->mNumBones;++j){
        aiBone* b=mesh->mBones[j];
        Joint j_ix=joint(b->mName);
        if(j_ix<0)continue;
        joint_usage[j_ix]=true;
        transforms[j_ix]=matrix_to_dual(b->mOffsetMatrix);
        for(unsigned i=0;i<b->mNumWeights;++i){
            unsigned v=b->mWeights[i].mVertexId;
            Byte p=last_joint[v]++;
            assert(p<4);
            vert[v].joint[p]=j_ix;
            vert[v].weight[p]=b->mWeights[i].mWeight;
        }
    }
    for(auto & v: vert){
        float w=v.weight[0]+v.weight[1]+v.weight[2]+v.weight[3];
        if(w>1.0002 || w<0.98){
            log_msg(
                        exge211::Detail_level::NON_CRITICAL,"incorrect weights: %1%[%5%],%2%[%6%],%3%[%7%],%4%[%8%]",
                        v.weight[0],v.weight[1],v.weight[2],v.weight[3],
                    v.joint[0],v.joint[1],v.joint[2],v.joint[3]
                    );
            for(int i=0;i<3;++i){
                if(v.weight[i]>0){
                    v.weight[i]+=1-w;
                }
            }
        }
    }

    exge211::Animated_mesh::Indices& ind=r.mesh.indices;
    ind.resize(mesh->mNumFaces);
    for(unsigned i=0;i<mesh->mNumFaces;++i){
        aiFace& f=mesh->mFaces[i];
        assert(f.mNumIndices==3);
        ind[i][0]=f.mIndices[0];
        ind[i][1]=f.mIndices[1];
        ind[i][2]=f.mIndices[2];
    }

    r.mesh.transform_ix=inv_bind_poses.size();
    for(size_t pose_ix=0; pose_ix<r.mesh.transform_ix ; ++pose_ix){

        Animated_model_data::Transforms& ibp=inv_bind_poses[pose_ix];
        Usage_mask& mask=masks[pose_ix];

        assert(ibp.size()==transforms.size());  // transforms.size()==skeleton_size
        assert(skeleton_size);  //skeleton has at least one joint

        //imho use of closure here looks much more better than use of an ugly condition.
        //returns false only if joints are in use and aren't equal.
        auto joint_eq=[&](size_t j_ix)->bool
        {
            return !mask[j_ix] || !joint_usage[j_ix] || transforms[j_ix]==ibp[j_ix];
        };

        size_t joint_ix=0;
        for(; joint_ix<skeleton_size && joint_eq(joint_ix);++joint_ix);

        if(joint_ix==skeleton_size){
            r.mesh.transform_ix=pose_ix; // <-- this breaks the loop

            //usage mask updating
            for(size_t j_ix=0;j_ix<skeleton_size;++j_ix){
                if(joint_usage[j_ix]){
                    mask[j_ix]=true;
                    ibp[j_ix]=transforms[j_ix];
                }
            }
        }
    }

    if(r.mesh.transform_ix==inv_bind_poses.size()){
        inv_bind_poses.emplace_back(std::move(transforms));
        masks.emplace_back(std::move(joint_usage));
    }

    return r;
}

Static_submodel_data Exporter::get_static_submodel(unsigned mesh_ix) const
{
    assert(mesh_ix<scene_->mNumMeshes);
    aiMesh *mesh=scene_->mMeshes[mesh_ix];
    assert(mesh->HasNormals());
    assert(mesh->HasTangentsAndBitangents());
    Static_submodel_data r;

    exge211::Static_mesh::Vertices & vert=r.mesh.vertices;
    r.material=get_material_key(mesh->mMaterialIndex);
    r.type=exge211::Material_type::NORMAL;
    vert.resize(mesh->mNumVertices);

    for(unsigned vi=0;vi<mesh->mNumVertices;++vi){
        exge211::Static_vertex & v=vert[vi];
        get(&v.pos[0],mesh->mVertices[vi]);
        get(&v.n[0],mesh->mNormals[vi].Normalize());
        get(&v.t[0],mesh->mTangents[vi].Normalize());
        get(&v.b[0],mesh->mBitangents[vi].Normalize());
        v.uv[0]=mesh->mTextureCoords[0][vi][0];
        v.uv[1]=mesh->mTextureCoords[0][vi][1];
    }

    exge211::Animated_mesh::Indices& ind=r.mesh.indices;
    ind.resize(mesh->mNumFaces);
    for(unsigned i=0;i<mesh->mNumFaces;++i){
        aiFace& f=mesh->mFaces[i];
        assert(f.mNumIndices==3);
        ind[i][0]=f.mIndices[0];
        ind[i][1]=f.mIndices[1];
        ind[i][2]=f.mIndices[2];
    }

    return r;
}

void Exporter::load_mapping(const String & path, bool add, Exporter::Mapping & mapping)
{
    String str=get_file(path);
    if(!str.empty()){
        if(!add){
            mapping.clear();
        }
        deserialize(str.c_str(),str.size(),mapping);
    }
}

Animation_data Exporter::get_animation(aiAnimation * anim) const
{
    using exge211::Animation_channel;
    Animation_data adata;
    if(joint_map_.empty()){
        log_msg(exge211::Detail_level::CRITICAL,"trying to get animation when skeleton not loaded");
        return adata;
    }
    double ticks_ps=(anim->mTicksPerSecond)? 1/anim->mTicksPerSecond : 1/ticks_per_second();

    adata.duration=anim->mDuration*ticks_ps;

    adata.channels.resize(skeleton_.joints.size());
    for(size_t i=0;i<anim->mNumChannels;++i){
        aiNodeAnim* node_anim=anim->mChannels[i];
        Joint j=joint(node_anim->mNodeName);
        if(j<0)continue;
        assert(j<(Joint)adata.channels.size());
        Animation_channel channel;
        {
            auto& rotations=channel.rotations;
            rotations.reserve(node_anim->mNumRotationKeys);
            for(size_t i=0;i<node_anim->mNumRotationKeys;++i){
                quat q;get(q,node_anim->mRotationKeys[i].mValue);
                float t=node_anim->mRotationKeys[i].mTime*ticks_ps;
                rotations.emplace_back(t,q);
            }
        }
        {
            auto& translations=channel.translations;
            translations.reserve(node_anim->mNumPositionKeys);
            for(size_t i=0;i<node_anim->mNumPositionKeys;++i){
                vec3 p;get(&p[0],node_anim->mPositionKeys[i].mValue);
                float t=node_anim->mPositionKeys[i].mTime*ticks_ps;
                translations.emplace_back(t,p);
            }
        }
        {
            auto& scales=channel.scales;
            scales.reserve(node_anim->mNumScalingKeys);
            for(size_t i=0;i<node_anim->mNumScalingKeys;++i){
                vec3 s;get(&s[0],node_anim->mScalingKeys[i].mValue);
                float t=node_anim->mScalingKeys[i].mTime*ticks_ps;
                scales.emplace_back(t,s);
            }
        }
        adata.channels[j]=channel;
    }
    for(size_t i=0;i<adata.channels.size();++i ){
        Animation_channel& ch=adata.channels[i];
        if(ch.rotations.empty()){
            ch.rotations.emplace_back(0,skeleton_.joints[i].binding_pose.real);
        }
        if(ch.translations.empty()){
            ch.translations.emplace_back(0,skeleton_.joints[i].binding_pose*vec3(0,0,0));
        }
        if(ch.scales.empty()){
            ch.scales.emplace_back(0,1,1,1);
        }
    }
    return adata;
}

exge211::Material_key Exporter::get_material_key(unsigned ix) const{
    assert(ix<scene_->mNumMaterials);
    aiMaterial* m=scene_->mMaterials[ix];
    aiString name;
    m->Get(AI_MATKEY_NAME,name);
    return map_material_key(String(name.C_Str()));
}

String const& Exporter::map_material_key(const String & imported) const{
    return map(imported,mat_key_mapping_);
}

String const& Exporter::map_joint(const String & imported) const{
    return map(imported,joint_mapping_);
}

String const& Exporter::map(const String & key, const Exporter::Mapping & mapping)const{
    Mapping::const_iterator it=mapping.find(key);
    if(it==mapping.end()){
        return key;
    }else{
        return it->second;
    }
}

Exporter::Joint Exporter::joint(const aiString & in_model_name) const{
    Joint_map::const_iterator it=joint_map_.find(String(in_model_name.C_Str()));
    if(it==joint_map_.end()){
        log_msg(exge211::Detail_level::CRITICAL,"not found joint %1%",in_model_name.C_Str());
        return -1;
    }
    return it->second;
}

bool Exporter::deserialize(const char * src, size_t len, Exporter::Mapping & mapping_out){
    size_t i=0;
    skip_spaces(src,len,i);
    for(;i<len;){
        String key=get_string(src,len,i);
        skip_spaces(src,len,i);
        if(i<len){
            String value=get_string(src,len,i);
            mapping_out.emplace(std::move(key),std::move(value));
            skip_spaces(src,len,i);
        }else{
            log_msg(exge211::Detail_level::NON_CRITICAL,"unexpected end of data when parsing mapping");
            return false;
        }
    }
    return true;
}
