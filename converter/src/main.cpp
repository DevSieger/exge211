#include <iostream>
#include <fstream>
#include <assimp/DefaultLogger.hpp>
#include <assimp/Logger.hpp>
#include "exporter.h"
#include "commander.h"

using exge211::String;
using std::cout;
using std::endl;

int main(int argc,char** argv)
{
    exge211::logging::init();
    exge211::logging::detail_level(exge211::Detail_level::DETAILED);
    Assimp::DefaultLogger::create("./aimp.log");
    if(argc>1){
        std::ifstream command_file(argv[1]);
        if(command_file.is_open()){
            Commander c(command_file);
            c.run();
        }else{
            log_msg(exge211::Detail_level::CRITICAL,"can't open command file %1%",argv[1]);
        }
    }else{
        Commander c(std::cin);
        c.run();
    }
    Assimp::DefaultLogger::kill();
    return 0;
}

