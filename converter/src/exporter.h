#pragma once

/*
 * This module is not a part of the original project,
 * it was written with a lack of time for a sole purpose of engine development.
 * The code of this module is considered TERRIBLE, but it still works well.
 *
 * The module is used to convert file formats, which are recognized by 'assimp', to the format used as simple example.
 *
 * If you want to use exge211 for some strange reason, it is a good idea to use your own resource file format and an appropriate loader.
 */

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <unordered_map>
#include <set>
#include <exge211/logging.h>
#include <exge211/res/animation_data.h>
#include <exge211/res/resource_data.h>
#include <exge211/common_types.h>

/*!
 * \brief A class for exporting data from the loaded file.
 */

class Exporter{
public:
    Exporter();
    Exporter(Exporter&&)=delete;//no copying/moving

    using String=exge211::String;

    /// Loads a file containing an animated model.
    void load_file(String const& path);

    ///  Loads a file containing a static model.
    void load_static_model_file(String const& path);

    /*!
     * \brief Exports an animated model converted to the 'default_format'.
     * \param path A path of the output file.
     * \param boundary a bounding box of the model in any pose which is used in application.
     */
    void export_animated_model(String const& path,exge211::AABB const& boundary);

    /// \brief Exports a static model converted to the 'default_format'.
    /// \param path A path of the output file.
    void export_static_model(String const& path);

    /*!
     * \brief Exports the skeleton from the loaded animated model.
     * \param path A path of the output file.
     * \param root_hint The name of the node to use as the root.
     */
    void export_skeleton(String const& path,String const& root_hint);

    /*!
     * \brief Loads the mapping from the material names of the loaded model to the material names of the output model.
     * \param path A path of the file to load.
     * \param add false is used to replace the previous mapping, true is used to extend it.
     * \details When the mapping for some key is undefined an identity map is used.
     */
    void load_material_mapping(String const& path,bool add=false){
        load_mapping(path,add,mat_key_mapping_);
    }

    /*!
     * \brief Loads the mapping from the joint names of the loaded model to the joint names of the output skeleton.
     * \param path A path of the file to load.
     * \param add false is used to replace the previous mapping, true is used to extend it.
     * \details When the mapping for some key is undefined an identity map is used.
     */
    void load_joint_mapping(String const& path,bool add=false){
        load_mapping(path,add,joint_mapping_);
    }

    /*!
     * \brief Exports an animation with the given index from the loaded animated model.
     * \param path A path of the file to load.
     * \param index The given index.
     */
    void export_animation(String const& path, size_t index);

    size_t animation_count()const;

    void load_skeleton(String const& path);

    /// Sets the number of animation ticks per second.
    Exporter& ticks_per_second(double t){
        ticks_=t;
        return *this;
    }

    double ticks_per_second()const{
        return ticks_;
    }

private:
    using Mapping=std::unordered_map<String,String>;
    using Joint=int;
    using Byte=unsigned char;
    using Scene_ptr=const aiScene*;
    using Joint_map=std::unordered_map<String,unsigned>;

    using Joint_set=std::set<String>;

    exge211::Animated_model_data get_animated_model()const;
    exge211::Static_model_data get_static_model()const;
    exge211::Skeleton_info get_skeleton(aiNode* root)const;
    bool scene_check()const;

    //this type is used to track joint usage
    using Usage_mask=std::vector<bool>;
    using Usage_masks=std::vector<Usage_mask>;
    exge211::Animated_submodel_data get_animated_submodel(unsigned mesh_ix, exge211::Animated_model_data::Transforms_cont& inv_bind_poses,Usage_masks& masks)const;
    exge211::Static_submodel_data get_static_submodel(unsigned mesh_ix)const;

    void load_mapping(String const& path,bool add,Mapping & mapping);

    exge211::Animation_data get_animation(aiAnimation * anim)const;

    exge211::Material_key get_material_key(unsigned ix)const;
    String const& map_material_key(String const& imported)const;
    String const& map_joint(String const& imported)const;
    String const& map(String const& key,Mapping const& mapping)const;

    Joint joint(aiString const& in_model_name)const;
    static bool deserialize(const char * src, size_t len, Exporter::Mapping & mapping_out);

    Assimp::Importer importer_;
    Joint_map joint_map_;
    Scene_ptr scene_=nullptr;
    Mapping mat_key_mapping_;
    Mapping joint_mapping_;
    exge211::Skeleton_info skeleton_;
    double ticks_=20.0;
};

