#pragma once

/*
 * This module is not a part of the original project,
 * it was written with a lack of time for a sole purpose of engine development.
 * The code of this module is considered TERRIBLE, but it still works well.
 */

#include "exporter.h"
#include <regex>
#include <istream>

/*!
 * \brief A class for executing user commands.
 * \details User commands is read from the given stream.
 */
class Commander{
public:

    using String=exge211::String;

    Commander(std::istream& stream):
        istr_(stream)
    {}

    void run();

private:

    /// Processes a file of the animated model.
    bool process_animated(String const& file);
    /// Processes a file of the static model.
    bool process_static(String const& file);

    struct{
        std::regex open_animated{"^open\\s+\"(.+)\"\\s*$"};
        std::regex open_static{"^open-st\\s+\"(.+)\"\\s*$"};

        std::regex export_skeleton{"^exp-sk\\s+\"(.+)\"\\s+\"(.+)\"\\s*$"};
        std::regex load_skeleton{"^load-sk\\s+\"(.+)\"\\s*$"};

        std::regex export_animated_model{"^exp-model\\s+\"(.+)\""
                                         "\\s+\\{\\s*(-?\\d+\\.?\\d*)\\s*,\\s*(-?\\d+\\.?\\d*)\\s*,\\s*(-?\\d+\\.?\\d*)\\s*\\}"
                                         "\\s+\\{\\s*(-?\\d+\\.?\\d*)\\s*,\\s*(-?\\d+\\.?\\d*)\\s*,\\s*(-?\\d+\\.?\\d*)\\s*\\}"
                                         "\\s*$"};
        std::regex export_static_model{"^exp-model\\s+\"(.+)\"\\s*$"};

        std::regex export_animation{"^exp-anim\\s+\"(.+)\"\\s+(\\d+)\\s*$"};

        std::regex load_material_mapping{"^load-material-map\\s+\"(.+)\"\\s*$"};
        std::regex load_joint_mapping{"^load-joint-map\\s+\"(.+)\"\\s*$"};

        std::regex ticks_per_second{"^ticks-per-second\\s+(\\d+\\.\\d+)\\s*$"};

        std::regex animations_count{"^anims$"};

        std::regex help{"^\\?$"};
        std::regex close{"^exit$"};
        std::regex stop{"^stop$"};
    } const commands_rgx;

    Exporter exp_;
    std::smatch match_;
    std::istream & istr_;
};
