#include "commander.h"
#include <iostream>

using std::cout;
using std::endl;

void Commander::run(){
    String input;
    cout<<"enter ? to help"<<endl;
    while(true){
        getline(istr_,input);
        if(input.empty() && !istr_.eof()){
            continue;
        }
        if(std::regex_search(input,match_,commands_rgx.open_animated)){
            if(match_.size()>1){
                cout<<"loading file '"<<match_.str(1)<<"' as animated"<<endl;
                if(!process_animated(match_.str(1))){
                    return;
                }
            }
        }else if(std::regex_search(input,match_,commands_rgx.open_static)){
            if(match_.size()>1){
                cout<<"loading file '"<<match_.str(1)<<"' as static"<<endl;
                if(!process_static(match_.str(1))){
                    return;
                }
            }
        }else if(std::regex_search(input,match_,commands_rgx.close)){
            cout<<"bye"<<endl;
            break;
        }else if(std::regex_search(input,match_,commands_rgx.help)){
            cout<<"use:\n"
                  "open \"path to file\"\n"
                  "\tto open file with animated model\n"
                  "open-st \"path to file\"\n"
                  "\tto open file with static model\n"
                  "exit\n"
                  "\tto quit"<<endl;
        }else{
            cout<<"invalid command \""<<input<<"\""<<endl;
        }
        if(istr_.eof())break;
    }
}

bool Commander::process_animated(const Commander::String& file){
    exp_.load_file(file);
    String input;
    while(true){
        getline(istr_,input);
        if(input.empty() && !istr_.eof()){
            continue;
        }
        if(std::regex_search(input,match_,commands_rgx.open_animated)){
            if(match_.size()>1){
                cout<<"loading file '"<<match_.str(1)<<"' as animated"<<endl;
                exp_.load_file(match_.str(1));
            }
        }else if(std::regex_search(input,match_,commands_rgx.export_animated_model)){
            if(match_.size()>1){
                cout<<"exporting model to '"<<match_.str(1)<<"'"<<endl;
                exge211::AABB boundary;
                boundary.min.x=std::stof(match_.str(2));
                boundary.min.y=std::stof(match_.str(3));
                boundary.min.z=std::stof(match_.str(4));
                boundary.max.x=std::stof(match_.str(5));
                boundary.max.y=std::stof(match_.str(6));
                boundary.max.z=std::stof(match_.str(7));
                exp_.export_animated_model(match_.str(1),boundary);
            }
        }else if(std::regex_search(input,match_,commands_rgx.export_skeleton)){
            if(match_.size()>2){
                cout<<"exporting skeleton to '"<<match_.str(1)<<"' with root "<<match_.str(2)<<endl;
                exp_.export_skeleton(match_.str(1),match_.str(2));
            }
        }else if(std::regex_search(input,match_,commands_rgx.export_animation)){
            if(match_.size()>2){
                cout<<"exporting animation "<<match_.str(2)<<" to '"<<match_.str(1)<<"'"<<endl;
                exp_.export_animation(match_.str(1),stoi(match_.str(2)));
            }
        }else if(regex_search(input,match_,commands_rgx.load_skeleton)){
            if(match_.size()>1){
                cout<<"loading skeleton from '"<<match_.str(1)<<"'"<<endl;
                exp_.load_skeleton(match_.str(1));
            }
        }else if(regex_search(input,match_,commands_rgx.load_material_mapping)){
            if(match_.size()>1){
                cout<<"loading material mapping from '"<<match_.str(1)<<"'"<<endl;
                exp_.load_material_mapping(match_.str(1));
            }
        }else if(regex_search(input,match_,commands_rgx.load_joint_mapping)){
            if(match_.size()>1){
                cout<<"loading joint mapping from '"<<match_.str(1)<<"'"<<endl;
                exp_.load_joint_mapping(match_.str(1));
            }
        }else if(regex_search(input,match_,commands_rgx.ticks_per_second)){
            if(match_.size()>1){
                double t=stod(match_.str(1));
                cout<<"default ticks per second been set to "<<t<<endl;
                exp_.ticks_per_second(t);
            }
        }else if(regex_search(input,match_,commands_rgx.animations_count)){
            cout<<"model contains "<<exp_.animation_count()<<" animations"<<endl;;
        }else if(regex_search(input,match_,commands_rgx.stop)){
            cout<<"going to file loading";
            return true;
        }else if(regex_search(input,match_,commands_rgx.close)){
            cout<<"bye"<<endl;
            return false;
        }else if(regex_search(input,match_,commands_rgx.help)){
            cout<<"use:\n"
                  "open \"path to file\"\n"
                  "\tto open file with animated model\n"
                  "exp-model \"path to file\" {min_x_coordinate,min_y,min_z} {max_x,max_y,max_z}\n"
                  "\tto export model to file\n"
                  "exp-sk \"path to file\" \"root_node\"\n"
                  "\tto export skeleton with root at \"root_node\" to file\n"
                  "exp-anim \"path to file\" N\n"
                  "\tto export animation to file, N is animation index\n"

                  "load-sk \"path to file\"\n"
                  "\tto load skeleton from file\n"
                  "load-material-map \"path to file\"\n"
                  "\tto load model_key -> material_key map from file\n"
                  "load-joint-map \"path to file\"\n"
                  "\tto load model joint -> skeleton joint map from file\n"
                  "ticks-per-second F\n"
                  "\tto set default animation ticks per second to F,\n"
                  "\twhere F is real number written in fixed notation (e.g. 1.23)\n"
                  "anims\n"
                  "\tto print count of animations\n"
                  "stop\n"
                  "\tfor going to file loading\n"
                  "exit\n"
                  "\tto quit"<<endl;
        }else{
            cout<<"invalid command \""<<input<<"\""<<endl;
        }
        if(istr_.eof())return false;
    }
}

bool Commander::process_static(const Commander::String& file){
    exp_.load_static_model_file(file);
    String input;
    while(true){
        getline(istr_,input);
        if(input.empty() && !istr_.eof()){
            continue;
        }
        if(regex_search(input,match_,commands_rgx.export_static_model)){
            if(match_.size()>1){
                cout<<"exporting to '"<<match_.str(1)<<"'"<<endl;
                exp_.export_static_model(match_.str(1));
            }
            return true;
        }else if(regex_search(input,match_,commands_rgx.close)){
            cout<<"bye"<<endl;
            return false;
        }else if(regex_search(input,match_,commands_rgx.stop)){
            cout<<"going to file loading";
            return true;
        }else if(regex_search(input,match_,commands_rgx.help)){
            cout<<"use:\n"
                  "exp-model \"path to file\"\n"
                  "\tto export model to file\n"
                  "stop\n"
                  "\tfor going to file loading\n"
                  "exit\n"
                  "\tto quit"<<endl;
        }else{
            cout<<"invalid command \""<<input<<"\""<<endl;
        }
        if(istr_.eof())return false;
    }
}
