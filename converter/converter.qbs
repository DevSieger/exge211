import qbs

CppApplication{
    name: "converter"
    consoleApplication: true

    Group {     // Properties for the produced executable
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: "bin"
    }

    Depends{
        name: "exge211_core";
    }
    Depends{
        name: "exge211_smplres"
    }
    Depends{
        name: "cpp"
    }

    cpp.cxxLanguageVersion: "c++14"

    Group{
        name: "c++ sources"
        prefix: "src/"
        files:[
            "commander.cpp",
            "commander.h",
            "exporter.cpp",
            "exporter.h",
            "main.cpp",
        ]
    }

    cpp.includePaths: config.converter.includePaths
    cpp.dynamicLibraries: config.converter.libraries
    cpp.libraryPaths: config.converter.libraryPaths
}
